<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SiteController extends AbstractController
{
    /**
     * @Route("/", name="home") 
     */
    public function home()
    {
        return $this->render('site/home.html.twig');
    }

    /**
     * @Route("/voyager-a-velo", name="voyages") 
     */
    public function voyages()
    {
        return $this->render('site/voyages.html.twig');
    }

    /**
     * @Route("/voyages-sur-mesure", name="mytriptailor") 
     */
    public function mytriptailor()
    {
        return $this->render('site/my_trip_tailor.html.twig');
    }

     /**
     * @Route("/bases-vtt", name="base_vtt") 
     */
    public function base_vtt()
    {
        return $this->render('site/base_vtt.html.twig');
    }

}
