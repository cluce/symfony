<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Manifestation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{
    /**
     * @Route("/dashboard", name="admin_dashboard")
     */
    public function index()
    {
        $repoUsers = $this->getDoctrine()->getRepository(User::class);
        $users = $repoUsers->findByRoles('ROLE_USER');
        return $this->render('admin/dashboard.html.twig', [
            'users' => $users,
        ]);
    }
}
