<?php

namespace App\Controller;

use App\Form\CotationFormType;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CotationController extends AbstractController
{

    /**
     * @Route("/cotation", name="cotation")
     */
    public function cotation()
    {
        return $this->render('cotation/cotation.html.twig');
    }

    /**
     * @Route("/cotation/route", name="cotation_route")
     */
    public function cotation_route(Request $request)
    {
        $form = $this->createForm(CotationFormType::class);
        $form->add('typeVoieRouteChoice', ChoiceType ::class, [
            'choices' => [
                "Exclusivement sur: route, avec trafic < 500 véhicules/j OU route avec trafic > 500 véhicules/j mais < 1000 véhicules/j, aménagée avec BMF ou BC ou ZCA OU voie en site propre (VV ou PC)" => "1",
                "Existence d’une section sur route : non aménagée avec trafic < 1000 véhicules/j) OU aménagée avec BMF, BC ZCA avec trafic < 3000 véhicules/j" => "2",
                "Existence d’une section sur route : non aménagée avec trafic < 3000 véhicules/j OU aménagée avec BMF, BC ou ZCA avec trafic < 7000 véhicules/j" => "3",
                "Existence d’une section sur route passagère avec trafic > 3000 véhicules/j sans aménagement" => "4",
            ],
            'label' => "Type de voie empruntée [3]",
            'multiple' => false,
            'expanded' => true,
            'required' => true,
        ]);
        $form->add('distChoice', ChoiceType ::class, [
            'choices' => [
                "0 à 30 km" => "1",
                "31 à 50 km" => "2",
                "50 à 71 km" => "3",
                "Plus de 71 km" => "4",
            ],
            'label' => "Distance [1]",
            'multiple' => false,
            'expanded' => true,
            'required' => true,
            'attr' => array('class' => 'form-check-inline')
        ]);
        $form->add('denivChoice', ChoiceType ::class, [
            'choices' => [
                "0 à 100 m" => "1",
                "101 à 300 m" => "2",
                "301 à 1000 m" => "3",
                "Plus de 1001 m" => "4",
            ],
            'label' => "Dénivelé cumulé positif [1]",
            'multiple' => false,
            'expanded' => true,
            'required' => true,
            'attr' => array('class' => 'form-check-inline')
        ]);
        $form->add('pente', ChoiceType ::class, [
            'choices' => [
                "3 à 5%" => "1",
                "6 à 7%" => "2",
                "8 à 9%" => "3",
                "10% et plus" => "4",
            ],
            'label' => "Pente [2]",
            'multiple' => false,
            'expanded' => true,
            'required' => true,
            'attr' => array('class' => 'form-check-inline')
        ]);
        $type = "Route";
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->create_pdf($type, $form);
        }
        return $this->render('cotation/cotation_route.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    public function create_pdf($type, $form)
    {
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $dompdf = new Dompdf($pdfOptions);
        $html = $this->renderView('cotation/cotation_pdf.html.twig', [
            'title' => "Cotation des circuits " . $type,
            'type' => $type,
            'header' => 'Cotation d\'un parcours de type ' . $type,
            'departement' => $form["departement"]->getData(),
            'nom' => $form["nom_circuit"]->getData(),
            'commune_depart' => $form["commune_depart"]->getData(),
            'distance' => $form["distance"]->getData(),
            'denivele' => $form["denivele"]->getData(),
            'temps' => $form["temps"]->getData(),
            'difficulte' => $form["difficulte"]->getData(),
            'description' => $form["textDescr"]->getData()
        ]);
        // Load HTML to Dompdf
        $dompdf->loadHtml($html);
        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');
        // Render the HTML as PDF
        $dompdf->render();
        // Output the generated PDF to Browser (inline view)
        $dompdf->stream($form["nom_circuit"]->getData() . ".pdf", [
            "Attachment" => false
        ]);
    }

    /**
     * @Route("/cotation/veloroute", name="cotation_veloroute")
     */
    public function cotation_veloroute(Request $request)
    {
        $form = $this->createForm(CotationFormType::class);

        $form->add('typeVoieRouteChoice', ChoiceType ::class, [
            'choices' => [
                "Exclusivement sur: route, avec trafic < 500 véhicules/j OU route avec trafic > 500 véhicules/j mais < 1000 véhicules/j, aménagée avec BMF ou BC ou ZCA OU voie en site propre (VV ou PC)" => "1",
                "Existence d’une section sur route : non aménagée avec trafic < 1000 véhicules/j) OU aménagée avec BMF, BC ZCA avec trafic < 3000 véhicules/j" => "2",
                "Existence d’une section sur route : non aménagée avec trafic < 3000 véhicules/j OU aménagée avec BMF, BC ou ZCA avec trafic < 7000 véhicules/j" => "3",
                "Existence d’une section sur route passagère avec trafic > 3000 véhicules/j sans aménagement" => "4",
            ],
            'label' => "Type de voie empruntée [3]",
            'multiple' => false,
            'expanded' => true,
            'required' => true,
        ]);
        $form->add('ratioChoice', ChoiceType ::class, [
            'choices' => [
                "0 à 5" => "1",
                "6 à 10" => "2",
                "11 à 15" => "3",
                "16 à 20" => "4",
            ],
            'label' => "Ratio de dénivelé cumulé positif par kilomètre [1]",
            'multiple' => false,
            'expanded' => true,
            'required' => true,
            'attr' => array('class' => 'form-check-inline')
        ]);
        $form->add('revetementChoice', ChoiceType ::class, [
            'choices' => [
                "Revêtement dur et lisse, très bien roulant (béton, bitume ...)" => "1",
                "Existence d'une section hors catégorie 1 étoile (*), au revêtement légèrement rugueux ou stabilisé-compacté, praticable par tout type de vélo" => "2",
                "Existence d'une section, hors catégorie 2 étoiles (**), au revêtement simplement stabilisé (occasionnellement meuble: les pneus peuvent s'enfoncer de quelques millimètres)" => "3",
                "Existence d'une section, hors catégorie 3 étoiles (***): présence d'une section non revêtue (chemin de terre non stabilisé, occasionnellement boueux, ou empierré ou sable non stabilisé, gravillons, ...)" => "4",
            ],
            'label' => "Revêtement",
            'multiple' => false,
            'expanded' => true,
            'required' => true,
        ]);
        $form->add('penteChoice', ChoiceType ::class, [
            'choices' => [
                "moins de 2%" => "1",
                "2 à 4%" => "2",
                "4 à 7%" => "3",
                "7% et plus" => "4",
            ],
            'label' => "Pente [2]",
            'multiple' => false,
            'expanded' => true,
            'required' => true,
            'attr' => array('class' => 'form-check-inline')
        ]);

        $type = "Portion de véloroute";
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->create_pdf($type, $form);
        }
        return $this->render('cotation/cotation_veloroute.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/cotation/vtt", name="cotation_vtt")
     */
    public function cotation_vtt(Request $request)
    {
        $form = $this->createForm(CotationFormType::class);

        $form->add('typeVoieVttChoice', ChoiceType ::class, [
            'choices' => [
                "Voie/piste en stabilisé pouvant être goudronnée" => "1",
                "Piste en terre ou en herbe, voie assez large permettant le passage d’un véhicule" => "2",
                "Sentier ou voie étroite (monotrace)" => "3",
            ],
            'label' => "Type de voie empruntée [3]",
            'multiple' => false,
            'expanded' => true,
            'required' => true,
        ]);
        $form->add('distChoice', ChoiceType ::class, [
            'choices' => [
                "0 à 10 km" => "1",
                "11 à 20 km" => "2",
                "21 à 40 km" => "3",
                "Plus de 41 km" => "4",
            ],
            'label' => "Distance [1]",
            'multiple' => false,
            'expanded' => true,
            'required' => true,
            'attr' => array('class' => 'form-check-inline')
        ]);
        $form->add('denivChoice', ChoiceType ::class, [
            'choices' => [
                "0 à 100 m" => "1",
                "101 à 250 m" => "2",
                "251 à 600 m" => "3",
                "Plus de 601 m" => "4",
            ],
            'label' => "Dénivelé cumulé positif [1]",
            'multiple' => false,
            'expanded' => true,
            'required' => true,
            'attr' => array('class' => 'form-check-inline')
        ]);
        $form->add('techniqueChoice', ChoiceType ::class, [
            'choices' => [
                "Progression ne nécessitant aucune technique particulière." => "1",
                "Progression nécessitant un premier niveau de pilotage VTT. Présence de petits obstacles, peu nombreux, sans réelles difficultés (ornières, pierres, zones humides) sur le parcours." => "2",
                "Progression nécessitant des qualités techniques avérées de pilotage VTT. Nombreux obstacles à franchir sur le parcours." => "3",
                "Progression nécessitant une parfaite maîtrise de toutes les techniques de pilotage VTT. Parcours très exigeant avec des zones trialisantes sur de nombreux secteurs du parcours, descentes ou montées raides." => "4",
            ],
            'label' => "Technique",
            'multiple' => false,
            'expanded' => true,
            'required' => true,
        ]);

        $type = "VTT";
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->create_pdf($type, $form);
        }
        return $this->render('cotation/cotation_vtt.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/cotation/vtt-assistance-electrique", name="cotation_vttae")
     */
    public function cotation_vttae(Request $request)
    {
        $form = $this->createForm(CotationFormType::class);

        $form->add('typeVoieVttChoice', ChoiceType ::class, [
            'choices' => [
                "Voie/piste en stabilisé pouvant être goudronnée" => "1",
                "Piste en terre ou en herbe, voie assez large permettant le passage d’un véhicule" => "2",
                "Sentier ou voie étroite (monotrace)" => "3",
            ],
            'label' => "Type de voie empruntée [3]",
            'multiple' => false,
            'expanded' => true,
            'required' => true,
        ]);
        $form->add('distChoice', ChoiceType ::class, [
            'choices' => [
                "0 à 30 km" => "1",
                "31 à 50 km" => "2",
                "50 à 71 km" => "3",
                "Plus de 71 km" => "4",
            ],
            'label' => "Distance [1]",
            'multiple' => false,
            'expanded' => true,
            'required' => true,
            'attr' => array('class' => 'form-check-inline')
        ]);
        $form->add('denivChoice', ChoiceType ::class, [
            'choices' => [
                "0 à 100 m" => "1",
                "101 à 300 m" => "2",
                "301 à 1000 m" => "3",
                "Plus de 1001 m" => "4",
            ],
            'label' => "Dénivelé cumulé positif [1]",
            'multiple' => false,
            'expanded' => true,
            'required' => true,
            'attr' => array('class' => 'form-check-inline')
        ]);
        $form->add('techniqueChoice', ChoiceType ::class, [
            'choices' => [
                "Progression ne nécessitant aucune technique particulière." => "1",
                "Progression nécessitant un premier niveau de pilotage VTT. Présence de petits obstacles, peu nombreux, sans réelles difficultés (ornières, pierres, zones humides) sur le parcours." => "2",
                "Progression nécessitant des qualités techniques avérées de pilotage VTT. Nombreux obstacles à franchir sur le parcours." => "3",
                "Progression nécessitant une parfaite maîtrise de toutes les techniques de pilotage VTT. Parcours très exigeant avec des zones trialisantes sur de nombreux secteurs du parcours, descentes ou montées raides." => "4",
            ],
            'label' => "Technique",
            'multiple' => false,
            'expanded' => true,
            'required' => true,
        ]);

        $type = "VTT à assistance électrique";
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->create_pdf($type, $form);
        }
        return $this->render('cotation/cotation_vttae.html.twig', [
            'form' => $form->createView(),
        ]);

    }


}
