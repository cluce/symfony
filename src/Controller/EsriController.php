<?php

namespace App\Controller;

use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EsriController extends AbstractController
{
    private $client;
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

     //token arcgis online esri
     private function get_esri_token(){
        //$client = HttpClient::create();
        $response = $this->client->request(
            'GET',
            'https://www.arcgis.com/sharing/rest/oauth2/token?client_id=&client_secret=&grant_type=client_credentials'
        );
        $token = json_decode($response->getContent())->access_token;
        return $token;
    }
    
    /**
     * @Route("/circuits", name="circuits")
     */
    public function circuits() {
        return $this->render('esri/circuits.html.twig', 
            ['token'=> $this->get_esri_token()]
        );
    }

    /**
     * @Route("/circuits/pdf", name="circuits_pdf")
     */
    public function create_pdf()
    {
        $client = HttpClient::create(['headers'=>['Content-Type'=>'application/x-www-form-urlencoded']]);
        $arrCircuit=array();
        $getCircuit = "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/VELOENFRANCE/FeatureServer/1/query?where=SOUS_TYPE_CIRCUIT=201&f=json&outFields=IDENTIFIANT_CIRCUIT,IDENTIFIANT_RP&returnGeometry=false";
        $response = $client->request('GET', $getCircuit);
        $content = $response->getContent() ? json_decode($response->getContent())->features : false;
        if($content){
            foreach ($content as $value) {
                array_push($arrCircuit, $value->attributes);
            }
        }

        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $pdfOptions->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($pdfOptions);
        $html = $this->renderView('esri/calendrier_pdf.html.twig', [
            'title' => "Cotation des circuits " . "route",
            'type' => 'type',
            'header' => 'Cotation d\'un parcours de type ' . 'route',
            'departement' => 'allier',
            'nom' => "nom circuit",
            'commune_depart' => "chazeuil",
            'distance' => "13km",
            'denivele' => "300m",
            'temps' => "1h",
            'difficulte' => "facile",
            'description' => "description"
        ]);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream("circuit.pdf", [
            "Attachment" => false
        ]);
    }

    /**
     * @Route("/circuits/{id}", name="info_feature_circuit")
     */
    public function info_feature_circuit() {
        return $this->render('esri/circuits.html.twig',  
            ['token'=> $this->get_esri_token()]
        );
    }

    /**
     * @Route("/labels-velo", name="labels")
     */
    public function labels() {
        return $this->render('esri/labels_velo.html.twig', 
            ['token'=> $this->get_esri_token()]
        );
    }

    /**
     * @Route("/labels-velo/{id}", name="info_feature_label")
     */
    public function info_feature_label() {
        return $this->render('esri/labels_velo.html.twig',  
            ['token'=> $this->get_esri_token()]
        );
    }

    /**
     * @Route("/bonnes-adresses", name="bonnes_adresses")
     */
    public function bonnes_adresses() {
        return $this->render('esri/bonnes_adresses.html.twig', 
            ['token'=> $this->get_esri_token()]
        );
    }

    /**
     * @Route("/bonnes-adresses/{id}", name="info_feature_bonne_adresse")
     */
    public function info_feature_bonne_adresse() {
        return $this->render('esri/bonnes_adresses.html.twig', 
            ['token'=> $this->get_esri_token()]
        );
    }

    /**
     * @Route("/bcn-bpf", name="bcnbpf")
     */
    public function bcnbpf() {
        return $this->render('esri/bcn_bpf.html.twig');
    }

      /**
     * @Route("/calendrier-randonnees", name="app_manifs")
     */
    public function app_manifs()
    {
        return $this->render('esri/calendrier.html.twig', 
            ['token'=> $this->get_esri_token()]
        );
    }

    /**
     * @Route("/calendrier-randonnees/{id}", name="info_feature_app_manifs")
     */
    public function info_feature_app_manifs()
    {
        return $this->render('esri/calendrier.html.twig', 
            ['token'=> $this->get_esri_token()]
        );
    }

     /**
     * @Route("/clubs", name="app_clubs")
     */
    public function app_clubs()
    {
        return $this->render('esri/clubs.html.twig',
            ['token'=> $this->get_esri_token()]
        );
    }

     /**
     * @Route("/clubs/{id}", name="info_feature_app_clubs")
     */
    public function info_feature_app_clubs()
    {
        return $this->render('esri/clubs.html.twig', 
            ['token'=> $this->get_esri_token()]
        );
    }


     /**
     * @Route("/randonnees-permanentes", name="rp")
     */
    public function rp(Request $request) {
        $hostname = $request->getSchemeAndHttpHost();
        $rps = "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/OIN_TEST2/FeatureServer/2/query?token=".$this->get_esri_token()."&where=1=1&f=json&outFields=Nom,Accueil_Commune,Accueil_CP,OinManifReserveLicencie,OinManifLicTarifAdulte,OinManifLicTarifJeune,OinManifNonLicTarifAdulte,OinManifNonLicTarifJeune,ManifId";
        $getRps = file_get_contents($rps);
        $arrayRp = json_decode($getRps, true);
        $arrayAttributesRp=array();
        $arrayAttributesRpIdsOnly=array();
        foreach ($arrayRp["features"] as $value) {
            array_push($arrayAttributesRp, $value["attributes"]);
            array_push($arrayAttributesRpIdsOnly, $value["attributes"]["ManifId"]);
        }
        $circuits = "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/VELOENFRANCE/FeatureServer/1/query?where=SOUS_TYPE_CIRCUIT=201&f=json&outFields=IDENTIFIANT_CIRCUIT,IDENTIFIANT_RP,NOM&returnGeometry=false";
        $getCircuit = file_get_contents($circuits);
        $arrayCircuit = json_decode($getCircuit, true);
        $arrayAttributesCircuit=array();
        $arrayAttributesCircuitIdsOnly=array();
        foreach ($arrayCircuit["features"] as $value) {
            array_push($arrayAttributesCircuit, $value["attributes"]);
            array_push($arrayAttributesCircuitIdsOnly, $value["attributes"]["IDENTIFIANT_RP"]);
        }

        $extraRps=array_diff($arrayAttributesRpIdsOnly, $arrayAttributesCircuitIdsOnly);//rp chez exalto mais pas chez vef
        $randoPermanentes=array();
        foreach($arrayAttributesRp as $manif){
            $idManif = intval($manif["ManifId"]);
            foreach($extraRps as $extraRpId){
                if ($manif["ManifId"]==$extraRpId){
                    $url="Non disponible";
                    $nom=$manif["Nom"];
                    $commune=$manif["Accueil_Commune"];
                    $cp=$manif["Accueil_CP"];
                    $nonLic=$manif["OinManifReserveLicencie"] == 1 ? "<i class='fas fa-lock'></i>" : "<i class='fas fa-lock-open' style='color:green;'></i>";
                    $talf=$manif["OinManifLicTarifAdulte"];
                    $tjlf=$manif["OinManifLicTarifJeune"];
                    $tanlf=$manif["OinManifReserveLicencie"] == 0 ? $manif["OinManifNonLicTarifAdulte"] : "";
                    $tjnlf=$manif["OinManifReserveLicencie"] == 0 ? $manif["OinManifNonLicTarifJeune"] : "";
                    array_push($randoPermanentes, [$idManif, $nom, $commune, $cp, $nonLic, $talf, $tjlf, $tanlf, $tjnlf, $url]);
                }
            }
            foreach($arrayAttributesCircuit as $circuit){
                $idRp=$circuit["IDENTIFIANT_RP"];
                if ($idManif==$idRp){
                    $idCircuit=strval($circuit["IDENTIFIANT_CIRCUIT"]);
                    $url = $this->generateUrl('circuits')."/".$idCircuit;
                    $url = "<a href=". $url ." target='_blank' role='button'>".$hostname.$url."</a>";
                    $nom=$circuit["NOM"];
                    $commune=$manif["Accueil_Commune"];
                    $cp=$manif["Accueil_CP"];
                    $nonLic=$manif["OinManifReserveLicencie"] == 1 ? "<i class='fas fa-lock'></i>" : "<i class='fas fa-lock-open' style='color:green;'></i>";
                    $talf=$manif["OinManifLicTarifAdulte"];
                    $tjlf=$manif["OinManifLicTarifJeune"];
                    $tanlf=$manif["OinManifReserveLicencie"] == 0 ? $manif["OinManifNonLicTarifAdulte"] : "";
                    $tjnlf=$manif["OinManifReserveLicencie"] == 0 ? $manif["OinManifNonLicTarifJeune"] : "";
                    array_push($randoPermanentes, [$idManif, $nom, $commune, $cp, $nonLic, $talf, $tjlf, $tanlf, $tjnlf, $url]);
                } 
            }
        } 
        return $this->render('esri/randonnees_permanentes.html.twig', ['rps' => $randoPermanentes]);
    }

}