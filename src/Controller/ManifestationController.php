<?php

namespace App\Controller;

use DateTime;
use Cocur\Slugify\Slugify;
//use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ManifestationController extends AbstractController
{
    private $client;
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }
    
    //token arcgis online esri
    private function get_esri_token(){
        //$client = HttpClient::create();
        $response = $this->client->request(
            'GET',
            'https://www.arcgis.com/sharing/rest/oauth2/token?client_id=&client_secret=&grant_type=client_credentials'
        );
        $token = json_decode($response->getContent())->access_token;
        return $token;
    }

    //ws ffcyclo.org - exalto
    private function get_exalto_token() {
        date_default_timezone_set('Europe/Paris');
        //$client = HttpClient::create();
        $response = $this->client->request(
            'GET',
        'https://ffcyclo.org/ws/rest/Oin/GetToken?sessionIdentite=&password='.date('YmdHi')
        );
        $token = json_decode($response->getContent())->Response->token;
        return $token;
    }

    private function lister_manifestations($token){
        //$client = HttpClient::create();
        $response = $this->client->request(
            'GET',
            'https://ffcyclo.org/ws/rest/Oin/ListerManifestations?token='.$token, 
        );
        $manifs = json_decode($response->getContent(), true);
        return $manifs["Response"]["manifestationsListe"];
    }

    private function voir_manifestation($token, $id){
        //$client = HttpClient::create();
        $response = $this->client->request(
            'GET',
            'https://ffcyclo.org/ws/rest/Oin/VoirManifestation?token='.$token.'&ManifestationId='.$id,
        );
        $manif = json_decode($response->getContent(), true);
        return $manif["Response"]["manifestationModel"];
    }

    /**
     * @Route("/maj-manifs", name="sync_manifs")
    */
    public function sync_manifs(){
        $slugify = new Slugify();
        //manifs esri
        //$client = HttpClient::create(['headers'=>['Content-Type'=>'application/x-www-form-urlencoded']]);
        $arrEsriIdsOnly=array();
        $arrEsri=array(); //attention: si republication du service, il faut maj le service definition pour augmenter le maxRecordCount (https://support.esri.com/en/technical-article/000012383)
        $getEsrIds = "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/OIN_TEST2/FeatureServer/0/query?token=".$this->get_esri_token()."&where=1=1&f=json&outFields=OBJECTID,ManifId,date_modif&returnGeometry=false";
        $response = $this->client->request('GET',$getEsrIds,['headers'=>['Content-Type'=>'application/x-www-form-urlencoded']]);
        $content = $response->getContent() ? json_decode($response->getContent())->features : false;
        if($content){
            foreach ($content as $value) {
                array_push($arrEsri, [$value->attributes->ManifId, $value->attributes->OBJECTID, $value->attributes->date_modif]);
                array_push($arrEsriIdsOnly, $value->attributes->ManifId);
            }
        }
    
        //manifs exalto
        $arrExaltoIds = array();
        $arrExaltoIdsOnly=array();
        $getExalto = $this->lister_manifestations($this->get_exalto_token());
        foreach ($getExalto as $value) {
            array_push($arrExaltoIds, [$value["ManifId"],$value["date_modif"]]);
            array_push($arrExaltoIdsOnly, $value["ManifId"]);
        }

        //manifs a supprimer esri
        $deleteFeatures=array();
        $manifsToDelete = array_diff($arrEsriIdsOnly,$arrExaltoIdsOnly); //manifs esri qui ne sont pas chez exalto
        foreach ($manifsToDelete as $id) {
            foreach($arrEsri as $value){
                if($id==$value[0]){
                    array_push($deleteFeatures,$value[1]);
                }
            }
            
        }
        $deleteFeatures=implode(",",$deleteFeatures);
        
        //manifs a mettre a jour esri
        $manifsToUpdate=array();
        foreach ($arrExaltoIds as $dateModifExalto) {
            foreach ($arrEsri as $dateModifEsri) {
                $idExalto=$dateModifExalto[0];
                $dateExalto=new DateTime($dateModifExalto[1]);
                $idEsri=$dateModifEsri[0];
                $objectIdEsri=$dateModifEsri[1];
                $dateEsri=new DateTime($dateModifEsri[2]);
                if(($idExalto == $idEsri)&&($dateExalto > $dateEsri)){
                    array_push($manifsToUpdate,[$idEsri,$objectIdEsri]);
                }
            }
        }
        $arrayUpdate=array();
        $updateFeatures=array();
        foreach ($manifsToUpdate as $value) {
            array_push($updateFeatures,$this->setManifestation($value,$arrayUpdate,$slugify));
        }

        //manifs a ajouter esri
        $arrayAdd=array();
        $addFeatures=array();
        $manifsToAdd = array_diff($arrExaltoIdsOnly,$arrEsriIdsOnly); //manifs exalto qui ne sont pas chez esri
        foreach ($manifsToAdd as $value) {
            array_push($addFeatures,$this->setManifestation($value,$arrayAdd,$slugify));
        }
        //dd($addFeatures);

        //chargement dans gdb arcgis online
        $url = "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/OIN_TEST2/FeatureServer/0/applyEdits";
        $parameters = [
            'f'=> 'json',
            'token' => $this->get_esri_token(),
            'deletes'=> $deleteFeatures,
            'adds'=> json_encode($addFeatures),
            'updates'=> json_encode($updateFeatures),
        ];
        $post_response = $this->client->request('POST', $url, ['body' => $parameters]);
        dd($post_response->getContent());
    }

    private function setManifestation($value,$array,$slugify){
        $id = is_array($value)==true ? /*id update*/$value[0] : /*id add*/$value;
        $objectId=  is_array($value)==true ? $value[1] : null;
        $getExaltoVoirManifs = $this->voir_manifestation($this->get_exalto_token(),$id);
        $OinManifObservation=$getExaltoVoirManifs["OinManifObservation"];
        $OinManifObservation = str_replace(array("\n", "\t", "\r"), '', $OinManifObservation);
        $Nom=$getExaltoVoirManifs["Nom"];
        $Nom = str_replace(array("<", "/", ">"), ' ', $Nom);
        array_push($array, [
            "geometry"=> [
                "x" => floatval($getExaltoVoirManifs["Accueil_Long"]), //longitude
                "y" =>  floatval($getExaltoVoirManifs["Accueil_Lat"]), //latitude
                "spatialReference" => ["wkid"=> 4326]
            ],
            "attributes" => [

                "OBJECTID" => $objectId,
                "date_creation" => $getExaltoVoirManifs["date_creation"],
                "date_modif" => $getExaltoVoirManifs["date_modif"],
                "ManifId" => $getExaltoVoirManifs["ManifId"],
                "Nom" => $Nom,
                "Accueil_DateDbt" => $getExaltoVoirManifs["Accueil_DateDbt"],
                "Arrivee_DateFin" => $getExaltoVoirManifs["Arrivee_DateFin"],
                "Accueil_Adresse" => $getExaltoVoirManifs["Accueil_Adresse"],
                "Accueil_Commune" => $getExaltoVoirManifs["Accueil_Commune"],
                "Accueil_CP" => $getExaltoVoirManifs["Accueil_CP"],
                "heures" => $getExaltoVoirManifs["heures"],
                "Accueil_Lieu" => $getExaltoVoirManifs["Accueil_Lieu"],
                "heureDeCloture" => $getExaltoVoirManifs["heureDeCloture"],
                "InfoLabel" => $getExaltoVoirManifs["InfoLabel"],
                "NumeroLabel" => $getExaltoVoirManifs["NumeroLabel"],
                "CategorieLabel" => $getExaltoVoirManifs["CategorieLabel"],
                "SiteWebRip" => $getExaltoVoirManifs["SiteWebRip"],
                "OinManifLicTarifAdulte" => $getExaltoVoirManifs["OinManifLicTarifAdulte"],
                "OinManifLicTarifJeune" => $getExaltoVoirManifs["OinManifLicTarifJeune"],
                "OinManifNonLicTarifAdulte" => $getExaltoVoirManifs["OinManifNonLicTarifAdulte"],
                "OinManifNonLicTarifJeune" => $getExaltoVoirManifs["OinManifNonLicTarifJeune"],
                "Structure" => $getExaltoVoirManifs["Structure"],
                "StructureDep" => $getExaltoVoirManifs["StructureDep"],
                "StructureReg" => $getExaltoVoirManifs["StructureReg"],
                "Type" => $getExaltoVoirManifs["Type"],
                "ModeleId" => $getExaltoVoirManifs["ModeleId"],
                "OinManifObservation" => $OinManifObservation,
                "OinManifReserveLicencie" => $getExaltoVoirManifs["OinManifReserveLicencie"] == "Oui" ? 1 : 0,
                "Contact_Nom" => $getExaltoVoirManifs["Contact_Nom"],
                "Contact_Prenom" => $getExaltoVoirManifs["Contact_Prenom"],
                "AdresseMail" => $getExaltoVoirManifs["AdresseMail"],
                "AdresseWeb" => $getExaltoVoirManifs["AdresseWeb"],
                "Arrivee_Lieu" => $getExaltoVoirManifs["Arrivee_Lieu"],
                "Arrivee_CP" => $getExaltoVoirManifs["Arrivee_CP"],
                "Arrivee_Commune" => $getExaltoVoirManifs["Arrivee_Commune"],
                "Pratique_VTT" => $getExaltoVoirManifs["Pratique_VTT"] == "Oui" ? 1 : 0,
                "Pratique_Route" => $getExaltoVoirManifs["Pratique_Route"] == "Oui" ? 1 : 0,
                "Pratique_Marche" => $getExaltoVoirManifs["Pratique_Marche"] == "Oui" ? 1 : 0,
                "Pratique_Gravel" => $getExaltoVoirManifs["Pratique_Gravel"] == "Oui" ? 1 : 0,
                "Accueil_Lat" => $getExaltoVoirManifs["Accueil_Lat"],
                "Accueil_Long" => $getExaltoVoirManifs["Accueil_Long"],
                "Arrivee_Lat" => $getExaltoVoirManifs["Arrivee_Lat"],
                "Arrivee_Long" => $getExaltoVoirManifs["Arrivee_Long"],
                "FichierGpx" => $getExaltoVoirManifs["FichierGpx"],
                "FichierFlyer" => $getExaltoVoirManifs["FichierFlyer"],
                "AccesPSH" => $getExaltoVoirManifs["AccesPSH"] == "Oui" ? 1 : 0,
                "DevDurable" => $getExaltoVoirManifs["DevDurable"] == "Oui" ? 1 : 0,
                "SpeFem" => $getExaltoVoirManifs["SpeFem"] == "Oui" ? 1 : 0,
                "InscriptionLigne" => $getExaltoVoirManifs["InscriptionLigne"] == "1" ? 1 : 0,
                //marche
                "OinMarcheDist_1" => intval($getExaltoVoirManifs["OinMarcheDist_1"]),
                "OinMarcheDist_2" => intval($getExaltoVoirManifs["OinMarcheDist_2"]),
                "OinMarcheDist_3" => intval($getExaltoVoirManifs["OinMarcheDist_3"]),
                "OinMarcheDist_4" => intval($getExaltoVoirManifs["OinMarcheDist_4"]),
                "OinMarcheDist_5" => intval($getExaltoVoirManifs["OinMarcheDist_5"]),
                "OinMarcheDist_6" => intval($getExaltoVoirManifs["OinMarcheDist_6"]),
                //route
                "OinRouteDeniv_1" => intval($getExaltoVoirManifs["OinRouteDeniv_1"]),
                "OinRouteDeniv_2" => intval($getExaltoVoirManifs["OinRouteDeniv_2"]),
                "OinRouteDeniv_3" => intval($getExaltoVoirManifs["OinRouteDeniv_3"]),
                "OinRouteDeniv_4" => intval($getExaltoVoirManifs["OinRouteDeniv_4"]),
                "OinRouteDeniv_5" => intval($getExaltoVoirManifs["OinRouteDeniv_5"]),
                "OinRouteDeniv_6" => intval($getExaltoVoirManifs["OinRouteDeniv_6"]),
                "OinRouteDiff_1" => intval($getExaltoVoirManifs["OinRouteDiff_1"]),
                "OinRouteDiff_2" => intval($getExaltoVoirManifs["OinRouteDiff_2"]),
                "OinRouteDiff_3" => intval($getExaltoVoirManifs["OinRouteDiff_3"]),
                "OinRouteDiff_4" => intval($getExaltoVoirManifs["OinRouteDiff_4"]),
                "OinRouteDiff_5" => intval($getExaltoVoirManifs["OinRouteDiff_5"]),
                "OinRouteDiff_6" => intval($getExaltoVoirManifs["OinRouteDiff_6"]),
                "OinRouteDist_1" => intval($getExaltoVoirManifs["OinRouteDist_1"]),
                "OinRouteDist_2" => intval($getExaltoVoirManifs["OinRouteDist_2"]),
                "OinRouteDist_3" => intval($getExaltoVoirManifs["OinRouteDist_3"]),
                "OinRouteDist_4" => intval($getExaltoVoirManifs["OinRouteDist_4"]),
                "OinRouteDist_5" => intval($getExaltoVoirManifs["OinRouteDist_5"]),
                "OinRouteDist_6" => intval($getExaltoVoirManifs["OinRouteDist_6"]),
                //vtt
                "OinVTTDeniv_1" => intval($getExaltoVoirManifs["OinVTTDeniv_1"]),
                "OinVTTDeniv_2" => intval($getExaltoVoirManifs["OinVTTDeniv_2"]),
                "OinVTTDeniv_3" => intval($getExaltoVoirManifs["OinVTTDeniv_3"]),
                "OinVTTDeniv_4" => intval($getExaltoVoirManifs["OinVTTDeniv_4"]),
                "OinVTTDeniv_5" => intval($getExaltoVoirManifs["OinVTTDeniv_5"]),
                "OinVTTDeniv_6" => intval($getExaltoVoirManifs["OinVTTDeniv_6"]),
                "OinVTTDiff_1" => intval($getExaltoVoirManifs["OinVTTDiff_1"]),
                "OinVTTDiff_2" => intval($getExaltoVoirManifs["OinVTTDiff_2"]),
                "OinVTTDiff_3" => intval($getExaltoVoirManifs["OinVTTDiff_3"]),
                "OinVTTDiff_4" => intval($getExaltoVoirManifs["OinVTTDiff_4"]),
                "OinVTTDiff_5" => intval($getExaltoVoirManifs["OinVTTDiff_5"]),
                "OinVTTDiff_6" => intval($getExaltoVoirManifs["OinVTTDiff_6"]),
                "OinVTTDist_1" => intval($getExaltoVoirManifs["OinVTTDist_1"]),
                "OinVTTDist_2" => intval($getExaltoVoirManifs["OinVTTDist_2"]),
                "OinVTTDist_3" => intval($getExaltoVoirManifs["OinVTTDist_3"]),
                "OinVTTDist_4" => intval($getExaltoVoirManifs["OinVTTDist_4"]),
                "OinVTTDist_5" => intval($getExaltoVoirManifs["OinVTTDist_5"]),
                "OinVTTDist_6" => intval($getExaltoVoirManifs["OinVTTDist_6"]),
                //gravel
                "OinGravelDeniv_1" => intval($getExaltoVoirManifs["OinGravelDeniv_1"]),
                "OinGravelDeniv_2" => intval($getExaltoVoirManifs["OinGravelDeniv_2"]),
                "OinGravelDeniv_3" => intval($getExaltoVoirManifs["OinGravelDeniv_3"]),
                "OinGravelDeniv_4" => intval($getExaltoVoirManifs["OinGravelDeniv_4"]),
                "OinGravelDeniv_5" => intval($getExaltoVoirManifs["OinGravelDeniv_5"]),
                "OinGravelDeniv_6" => intval($getExaltoVoirManifs["OinGravelDeniv_6"]),
                "OinGravelDiff_1" => intval($getExaltoVoirManifs["OinGravelDiff_1"]),
                "OinGravelDiff_2" => intval($getExaltoVoirManifs["OinGravelDiff_2"]),
                "OinGravelDiff_3" => intval($getExaltoVoirManifs["OinGravelDiff_3"]),
                "OinGravelDiff_4" => intval($getExaltoVoirManifs["OinGravelDiff_4"]),
                "OinGravelDiff_5" => intval($getExaltoVoirManifs["OinGravelDiff_5"]),
                "OinGravelDiff_6" => intval($getExaltoVoirManifs["OinGravelDiff_6"]),
                "OinGravelDist_1" => intval($getExaltoVoirManifs["OinGravelDist_1"]),
                "OinGravelDist_2" => intval($getExaltoVoirManifs["OinGravelDist_2"]),
                "OinGravelDist_3" => intval($getExaltoVoirManifs["OinGravelDist_3"]),
                "OinGravelDist_4" => intval($getExaltoVoirManifs["OinGravelDist_4"]),
                "OinGravelDist_5" => intval($getExaltoVoirManifs["OinGravelDist_5"]),
                "OinGravelDist_6" => intval($getExaltoVoirManifs["OinGravelDist_6"]),
                "slug"=> $slugify->slugify($getExaltoVoirManifs["ManifId"]."-".$Nom) 
            ]
        ]);
        if($objectId==null){
            unset($array[0]["attributes"]["OBJECTID"]);
        }
        //dd($array[0]);
        return $array[0];
    }

}
