<?php

namespace App\Controller;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;



class SSOController extends AbstractController
{

    private function sso_token(){
        $client = HttpClient::create();
        $response = $client->request('POST', 'https://sso.ffcyclo.org/connect/token', [
            'headers' => [
                'Authorization' => 'Basic '.base64_encode(''),
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'body' => "grant_type=client_credentials"
        ]);
        $token = json_decode($response->getContent())->access_token;
        return $token;
    }

    /**
    * @Route("/sso/login", name="sso_login")
    */
    public function sso_login(Request $request): RedirectResponse {
        $ssoToken = $this->sso_token();
        return new RedirectResponse('https://sso.ffcyclo.org/?access_token='.$ssoToken."&redirect_uri=https://vefr.test/connexion");
        /*$client = HttpClient::create();
        $response = $client->request('GET', "https://sso.ffcyclo.org/resources/me", [
            'headers' => [
                'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0eXBlIjoibGljZW5jaWVMaWJyZSIsImNvZGVBZGhlcmVudCI6IjgwNTA5MyIsInBlcnNvbm5lSWQiOjQyNDM1NSwidXNlcm5hbWUiOiI4MDUwOTMiLCJmaXJzdF9uYW1lIjoiUElFUlJFIEFOVE9JTkUiLCJsYXN0X25hbWUiOiJNT1JJTiIsImVtYWlsIjoiIiwiZW1haWxfdmVyaWZpZWQiOiIiLCJ1c2VyX3Njb3BlIjoiZXh0LWxpYyIsInJlZGlyZWN0X3VyaSI6Imh0dHBzOlwvXC92ZWZyLnRlc3RcL2Nvbm5leGlvbiIsImlkIjoiMzIwOGE0MDU2Njg0NDk5OTcxNGE5OGZjYWJmNzkzYjQwZjVjY2UwZSIsImp0aSI6IjMyMDhhNDA1NjY4NDQ5OTk3MTRhOThmY2FiZjc5M2I0MGY1Y2NlMGUiLCJpc3MiOiIiLCJhdWQiOiJkZXZGRkN5Y2xvIiwic3ViIjoiODA1MDkzIiwiZXhwIjoxNTk5NTcyNTI3LCJpYXQiOjE1OTk1Njg5MjcsInRva2VuX3R5cGUiOiJiZWFyZXIiLCJzY29wZSI6bnVsbH0.v7NJZVMon6aSwtb4zpuGlkE3evdJJFf_HVSeouAeBFk',
                'Content-Type' => 'application/json',
            ],
        ]);*/
    }


     /**
    * @Route("/connexion/licencie", name="login_licencie")
    */
    public function licencie_login(){
        return $this->render('security/login_licencie.html.twig', [
            'token'=> $this->sso_token()
        ]);
    }
  
}
