<?php

namespace App\Controller;

use DateTime;
use Cocur\Slugify\Slugify;
//use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ClubController extends AbstractController
{
    private $client;
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }
    
    //token arcgis online esri
    private function get_esri_token(){
        //$client = HttpClient::create();
        $response = $this->client->request(
            'GET',
            'https://www.arcgis.com/sharing/rest/oauth2/token?client_id=&client_secret=&grant_type=client_credentials'
        );
        $token = json_decode($response->getContent())->access_token;
        //dd($token);
        return $token;
    }

    //token api ffcyclo.org - exalto
    private function get_exalto_token() {
        date_default_timezone_set('Europe/Paris');
        //$client = HttpClient::create();
        $response = $this->client->request(
            'GET',
        'https://ffcyclo.org/ws/rest/Oin/GetToken?sessionIdentite=&password='.date('YmdHi')
        );
        $token = json_decode($response->getContent())->Response->token;
        return $token;
    }

    private function lister_structures($token, $structureCode=null){
        //$client = HttpClient::create();
        if (!$structureCode){
            $response = $this->client->request(
                'GET',
                'https://ffcyclo.org/ws/rest/Oin/ListerStructuresPourManifestation?token='.$token
            );
        } else {
            $response = $this->client->request(
                'GET',
                'https://ffcyclo.org/ws/rest/Oin/ListerStructuresPourManifestation?token='.$token.'&StructureCode='.$structureCode
            );
        }
        $structures = json_decode($response->getContent(), true);
        return $structures["Response"]["Structures"];
        
    }

    /**
     * @Route("/maj-clubs", name="sync_clubs")
    */
    public function sync_clubs(){
        $slugify = new Slugify();
        //recup les ids des clubs esri
        //$client = HttpClient::create(['headers'=>['Content-Type'=>'application/x-www-form-urlencoded']]);
        $arrEsri=array();
        $arrEsriIdsOnly=array();//attention si republication du service, il faut maj le service definition pour augmenter le maxRecordCount (https://support.esri.com/en/technical-article/000012383)
        $getEsriIds = "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/OIN_TEST2/FeatureServer/1/query?token=".$this->get_esri_token()."&where=1=1&f=json&outFields=OBJECTID,StructureCode,date_modif&returnGeometry=false";
        $response = $this->client->request('GET', $getEsriIds,['headers'=>['Content-Type'=>'application/x-www-form-urlencoded']]);
        $content = $response->getContent() ? json_decode($response->getContent())->features : false;
        if($content){
            foreach ($content as $value) {
                array_push($arrEsri, [$value->attributes->StructureCode, $value->attributes->OBJECTID, $value->attributes->date_modif]);
                array_push($arrEsriIdsOnly, $value->attributes->StructureCode);
            }
        }
        
        //recup les clubs exalto
        $arrExaltoIds = array();
        $arrExaltoIdsOnly = array();
        $getExaltoClubs = array();
        $getExalto = $this->lister_structures($this->get_exalto_token());
        foreach ($getExalto as $value) {
            if(($value["DerniereAffiliation"]==date("Y")-1 || $value["DerniereAffiliation"]==date("Y")) && (floatval($value["Latitude"])>0 && floatval($value["Longitude"])>0)){ //exclut les clubs sans latitude et longitude et non affiliés
                array_push($arrExaltoIds, [$value["StructureCode"],$value["date_modif"]]);
                array_push($arrExaltoIdsOnly, $value["StructureCode"]);
                array_push($getExaltoClubs,$value);
            }
        }

        //clubs a supprimer chez esri
        $deleteFeatures=array();
        $clubsToDelete = array_diff($arrEsriIdsOnly,$arrExaltoIds); //clubs esri qui ne sont pas chez exalto
        foreach ($clubsToDelete as $id) {
            foreach($arrEsri as $value){
                if($id==$value[0]){
                    array_push($deleteFeatures,$value[1]);
                }
            }
        }
        $deleteFeatures=implode(",",$deleteFeatures);
        
        //clubs a mettre a jour chez esri
        $clubsToUpdate=array();
        foreach ($arrExaltoIds as $dateModifExalto) {
            foreach ($arrEsri as $dateModifEsri) {
                $idExalto=$dateModifExalto[0];
                $dateExalto=new DateTime($dateModifExalto[1]);
                $idEsri=$dateModifEsri[0];
                $objectIdEsri=$dateModifEsri[1];
                $dateEsri=new DateTime($dateModifEsri[2]); 
                if(($idExalto == $idEsri)&&($dateExalto > $dateEsri)){
                    array_push($clubsToUpdate,[$idEsri,$objectIdEsri]);
                }
            }
        }
        $updateFeatures=array();
        if(!empty($clubsToUpdate)){
            foreach($clubsToUpdate as $clubToUpdate){
                foreach ($getExaltoClubs as $value) {
                    if($value["StructureCode"]==$clubToUpdate[0]){
                        $getExtraData=$this->lister_structures($this->get_exalto_token(),$value["StructureCode"]);
                        foreach($getExtraData as $data){
                            $pratique=array();
                            foreach($data["Disciplines"] as $discipline){
                                array_push($pratique, $discipline["CodeDiscipline"]);
                            }
                            $nomCorresp = $data["Instances"]["corresp"]["Nom"];
                            $prenomCorresp = $data["Instances"]["corresp"]["Prenom"];
                            $telCorresp = $data["Instances"]["corresp"]["AdrTel"];
                            $mailCorresp = $data["Instances"]["corresp"]["AdrMail"];
                        }
                        $pratiqueRoute=in_array("Route", $pratique) ? 1 : 0;
                        $pratiqueVTT=in_array("VTT / VTC", $pratique) ? 1 : 0;
                        $typeStructure=$value["StructureType"];
                        if($typeStructure=="CLU"){
                            $typeStructure=1;
                        } else if ($typeStructure="CODEP"){
                            $typeStructure=2;
                        } else if ($typeStructure="COREG"){
                            $typeStructure=3;
                        } else {
                            $typeStructure=null;
                        }
                        array_push($updateFeatures, [
                            "geometry"=> [
                                "x" => floatval($value["Longitude"]), //longitude
                                "y" => floatval($value["Latitude"]), //latitude
                                "spatialReference" => ["wkid"=> 4326]
                            ],
                            "attributes" => [
                                "OBJECTID"=> $clubToUpdate[1],
                                "date_creation" => $value["date_creation"],
                                "date_modif" => $value["date_modif"],
                                "StructureType" => $typeStructure,
                                "StructureNom" => $value["StructureNom"],
                                "StructureNomCourt" => $value["StructureNomCourt"],
                                "StructureEtat" => $value["StructureEtat"],
                                "StructureCode" => $value["StructureCode"],
                                "StructureCodeDepartement" => $value["StructureCodeDepartement"],
                                "StructureCodeRegion" => $value["StructureCodeRegion"],
                                "AdrNumVoie" => $value["AdrNumVoie"],
                                "AdrNomVoie" => $value["AdrNomVoie"],
                                "AdrBatiment" => $value["AdrBatiment"],
                                "AdrEscalier" => $value["AdrEscalier"],
                                "AdrLieuDit" => $value["AdrLieuDit"],
                                "AdrCP" => $value["AdrCP"],
                                "AdrVille" => $value["AdrVille"],
                                "AdrTel" => $value["AdrTel"],
                                "AdrMail" => $value["AdrMail"],
                                "AdrWeb" => $value["AdrWeb"],
                                "DerniereAffiliation" => $value["DerniereAffiliation"],
                                "Pratique_Route" => $pratiqueRoute,
                                "Pratique_VTT_VTC" => $pratiqueVTT,
                                "EcoleCyclo" => $value["EcoleCyclo"] == "Oui" ? 1 : 0,
                                "AccueilHandicap" => $value["AccueilHandicap"] == "Oui" ? 1 : 0,
                                "AccueilJeune" => $value["AccueilJeune"] == "Oui" ? 1 : 0,
                                "VeloEcole" => $value["VeloEcole"] == "Oui" ? 1 : 0,
                                "VAE" => $value["StructureVAE"] == "Oui" ? 1 : 0,
                                "Logo" => $value["Logo"], 
                                "Nom_Correspondant" => $nomCorresp,
                                "Prenom_Correspondant" => $prenomCorresp,
                                "Tel_Correspondant" => $telCorresp,
                                "Mail_Correspondant" => $mailCorresp,
                                "slug"=> $slugify->slugify($value["StructureCode"]."-".$value["StructureNom"])
                            ]
                        ]);
                    }
                }
            }
        }
        
        //clubs a ajouter chez esri
        $addFeatures=array();
        $clubsToAdd = array_diff_key($arrExaltoIds,$arrEsri); //clubs exalto qui ne sont pas chez esri
        if(!empty($clubsToAdd)){
            foreach($clubsToAdd as $clubToAdd){
                foreach ($getExaltoClubs as $value) {
                    if($value["StructureCode"]==$clubToAdd[0]){
                        $getExtraData=$this->lister_structures($this->get_exalto_token(),$value["StructureCode"]);
                        foreach($getExtraData as $data){
                            $pratique=array();
                            foreach($data["Disciplines"] as $discipline){
                                array_push($pratique, $discipline["CodeDiscipline"]);
                            }
                            $nomCorresp = $data["Instances"]["corresp"]["Nom"];
                            $prenomCorresp = $data["Instances"]["corresp"]["Prenom"];
                            $telCorresp = $data["Instances"]["corresp"]["AdrTel"];
                            $mailCorresp = $data["Instances"]["corresp"]["AdrMail"];
                        }
                        $pratiqueRoute=in_array("Route", $pratique) ? 1 : 0;
                        $pratiqueVTT=in_array("VTT / VTC", $pratique) ? 1 : 0;
                        $typeStructure=$value["StructureType"];
                        if($typeStructure=="CLU"){
                            $typeStructure=1;
                        } else if ($typeStructure="CODEP"){
                            $typeStructure=2;
                        } else if ($typeStructure="COREG"){
                            $typeStructure=3;
                        } else {
                            $typeStructure=null;
                        }
                        array_push($addFeatures, [
                            "geometry"=> [
                                "x" => floatval($value["Longitude"]), //longitude
                                "y" => floatval($value["Latitude"]), //latitude
                                "spatialReference" => ["wkid"=> 4326]
                            ],
                            "attributes" => [
                                "date_creation" => $value["date_creation"],
                                "date_modif" => $value["date_modif"],
                                "StructureType" => $typeStructure,
                                "StructureNom" => $value["StructureNom"],
                                "StructureNomCourt" => $value["StructureNomCourt"],
                                "StructureEtat" => $value["StructureEtat"],
                                "StructureCode" => $value["StructureCode"],
                                "StructureCodeDepartement" => $value["StructureCodeDepartement"],
                                "StructureCodeRegion" => $value["StructureCodeRegion"],
                                "AdrNumVoie" => $value["AdrNumVoie"],
                                "AdrNomVoie" => $value["AdrNomVoie"],
                                "AdrBatiment" => $value["AdrBatiment"],
                                "AdrEscalier" => $value["AdrEscalier"],
                                "AdrLieuDit" => $value["AdrLieuDit"],
                                "AdrCP" => $value["AdrCP"],
                                "AdrVille" => $value["AdrVille"],
                                "AdrTel" => $value["AdrTel"],
                                "AdrMail" => $value["AdrMail"],
                                "AdrWeb" => $value["AdrWeb"],
                                "DerniereAffiliation" => $value["DerniereAffiliation"],
                                "Pratique_Route" => $pratiqueRoute,
                                "Pratique_VTT_VTC" => $pratiqueVTT,
                                "EcoleCyclo" => $value["EcoleCyclo"] == "Oui" ? 1 : 0,
                                "AccueilHandicap" => $value["AccueilHandicap"] == "Oui" ? 1 : 0,
                                "AccueilJeune" => $value["AccueilJeune"] == "Oui" ? 1 : 0,
                                "VeloEcole" => $value["VeloEcole"] == "Oui" ? 1 : 0,
                                "VAE" => $value["StructureVAE"] == "Oui" ? 1 : 0,
                                "Logo" => $value["Logo"],
                                "Nom_Correspondant" => $nomCorresp,
                                "Prenom_Correspondant" => $prenomCorresp,
                                "Tel_Correspondant" => $telCorresp,
                                "Mail_Correspondant" => $mailCorresp,
                                "slug"=> $slugify->slugify($value["StructureCode"]."-".$value["StructureNom"])
                            ]
                        ]);
                    }
                
                }
                break;
            }
            
        }

        //envoi dans gdb arcgis online
        $url = "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/OIN_TEST2/FeatureServer/1/applyEdits";
        $parameters = [
            'f'=> 'json',
            'token' => $this->get_esri_token(),
            'deletes'=> $deleteFeatures,
            'adds'=> json_encode($addFeatures),
            'updates'=> json_encode($updateFeatures),
        ];
        $post_response = $this->client->request('POST', $url, ['body' => $parameters]);
        dd($post_response->getContent());
    }
    
    
}
