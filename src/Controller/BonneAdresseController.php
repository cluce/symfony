<?php

namespace App\Controller;

use Cocur\Slugify\Slugify;
//use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;



class BonneAdresseController extends AbstractController
{
    private $client;
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }
    
    //token arcgis online esri
    private function get_esri_token(){
        //$client = HttpClient::create();
        $response = $this->client->request(
            'GET',
            'https://www.arcgis.com/sharing/rest/oauth2/token?client_id=&client_secret=&grant_type=client_credentials'
        );
        $token = json_decode($response->getContent())->access_token;
        return $token;
    }

    //api njuko
    private function get_ba_njuko(){
        /*$client = HttpClient::create(
            ['headers'=>[
                'Content-Type'=>'application/json',
                'Edition'=>'19004',
                'Authorization'=>'b08f3a0dfbd11f61905e6f8e5828c530'
            ]
        ]);*/
        $response = $this->client->request(
            'POST',
            'https://api.njuko.net/api-registrations/registrations',[
                'headers' => [
                    'Content-Type'=>'application/json',
                    'Edition'=>'19004',
                    'Authorization'=>''
                ]
            ]
        );
        $bonnesAdresses = json_decode($response->getContent())->results;
        return $bonnesAdresses;
        
    }

    /**
     * @Route("/maj-ba", name="sync_ba")
     */
    public function sync_ba()
    {
        $slugify = new Slugify();
        //recup ba dans agol
        //$client = HttpClient::create(['headers'=>['Content-Type'=>'application/x-www-form-urlencoded']]);
        $arrEsriBa=array();
        $arrEsriBaIdsOnly=array();
        $getEsriBa = "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/BONNES_ADRESSES/FeatureServer/0/query?token=".$this->get_esri_token()."&where=1=1&f=json&outFields=OBJECTID,IDENTIFIANT_BONNE_ADRESSE,DATE_MODIFICATION&returnGeometry=false";
        $response = $this->client->request('GET',$getEsriBa,['headers'=>['Content-Type'=>'application/x-www-form-urlencoded']]);
        $content = $response->getContent() ? json_decode($response->getContent())->features : false;
        if($content){
            foreach ($content as $value) {
                array_push($arrEsriBa, [$value->attributes->IDENTIFIANT_BONNE_ADRESSE, $value->attributes->DATE_MODIFICATION, $value->attributes->OBJECTID]);
                array_push($arrEsriBaIdsOnly, $value->attributes->IDENTIFIANT_BONNE_ADRESSE);
            }
        }

        //recup ba dans njuko
        $arrNjukoBa = array();
        $arrNjukoBaId=array();
        $arrNjukoBaIdsOnly=array();
        $getNjukoBa = $this->get_ba_njuko();
        foreach ($getNjukoBa as $value) {
            if($value->isRegistered==true){
                array_push($arrNjukoBa,$value);
                array_push($arrNjukoBaId, [$value->id, $value->updated]);
                array_push($arrNjukoBaIdsOnly, $value->id);
            }
        }

        //bonnes adresses a supprimer
        $deleteFeatures=array();
        $basToDelete = array_diff($arrEsriBaIdsOnly,$arrNjukoBaIdsOnly); //ba esri qui ne sont pas chez njuko
        foreach ($basToDelete as $value) {
            array_push($deleteFeatures,$value);
        }
        $deleteFeatures=implode(",",$deleteFeatures);

        //bonnes adresses a mettre a jour
        $basToUpdate=array();
        foreach ($arrNjukoBaId as $dateModifNjuko) {
            foreach ($arrEsriBa as $dateModifEsri) {
                $idNjuko=$dateModifNjuko[0];
                $dateNjuko=$dateModifNjuko[1];
                $idEsri=intval($dateModifEsri[0]);
                $objectIdEsri=intval($dateModifEsri[2]);
                $dateEsri=$dateModifEsri[1];
                if(($idNjuko == $idEsri)&&($dateNjuko > $dateEsri)){
                    array_push($basToUpdate,[$idEsri, $objectIdEsri]);
                }
            }
        }
        $arrayUpdate=array();
        $updateFeatures=array();
        if(!empty($basToUpdate)){
            foreach($basToUpdate as $baToUpdate){
                foreach ($arrNjukoBa as $value) {
                    if($value->id==$baToUpdate[0]){
                        array_push($updateFeatures,$this->setBonneAdresse($value, $arrayUpdate, $slugify, $baToUpdate[1]));
                    }
                }
            }
        }
        //dd($updateFeatures);

        //bonnes adresses a ajouter
        $arrayAdd=array();
        $addFeatures=array();
        $basToAdd = array_diff($arrNjukoBaIdsOnly,$arrEsriBaIdsOnly); //ba njuko qui ne sont pas chez esri
        if(!empty($basToAdd)){
            foreach($basToAdd as $baToAdd){
                foreach ($arrNjukoBa as $value) {
                    if($value->id==$baToAdd){
                        array_push($addFeatures,$this->setBonneAdresse($value, $arrayAdd, $slugify, null));
                    }
                }
            }
        }
        //dd($addFeatures);

        //envoi dans gdb arcgis online
        $url = "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/BONNES_ADRESSES/FeatureServer/0/applyEdits";
        $parameters = [
            'f'=> 'json',
            'token' => $this->get_esri_token(),
            'deletes'=> $deleteFeatures,
            'adds'=> json_encode($addFeatures),
            'updates'=> json_encode($updateFeatures),
        ];
        $post_response = $this->client->request('POST', $url, ['body' => $parameters]);
        dd($post_response->getContent());
    }

    private function get_index($answers, $id_field){
        $test=array_search($id_field,  array_column($answers, 'field_id'));
        return $test;
    }

    private function setBonneAdresse($value, $array, $slugify, $objectId){
        $answers=$value->answers;
        $photo1 = $value->permission_slip ? str_replace("jpg", 'png', $value->permission_slip) : false;
        $photo2 = isset($answers[$this->get_index($answers,375530)]->value) ? str_replace("jpg", 'png', $value->answers[$this->get_index($answers,375530)]->value) : null;
        $photo3 = isset($answers[$this->get_index($answers,375531)]->value) ? str_replace("jpg", 'png', $value->answers[$this->get_index($answers,375531)]->value) : null;
        $photo4 = isset($answers[$this->get_index($answers,385526)]->value) ? str_replace("jpg", 'png', $value->answers[$this->get_index($answers,385526)]->value) : null;
        $photo5 = isset($answers[$this->get_index($answers,385527)]->value) ? str_replace("jpg", 'png', $value->answers[$this->get_index($answers,385527)]->value) : null;
        array_push($array, [
            "geometry"=> [
                "x" => isset($answers[$this->get_index($answers, 375519)]->lng) ? floatval($answers[$this->get_index($answers, 375519)]->lng) : 0, //longitude
                "y" => isset($answers[$this->get_index($answers, 375519)]->lat) ? floatval($answers[$this->get_index($answers, 375519)]->lat) : 0, //latitude
                "spatialReference" => ["wkid"=> 4326]
            ],
            "attributes" => [
                "OBJECTID" => $objectId,
                "IDENTIFIANT_BONNE_ADRESSE"=> $value->id,
                "HEBERGEMENT"=> $value->gender=="male" ? 1 : 0, //champ systeme njuko gender utilise pour hebergement -> male == oui, female == non
                "RESTAURANT"=> isset($answers[$this->get_index($answers, 375515)]->value) ? ($answers[$this->get_index($answers, 375515)]->value[0]=="Oui" ? 1 : 0) : 0,
                "VELOCISTE"=> isset($answers[$this->get_index($answers, 379666)]->value) ? ($answers[$this->get_index($answers, 379666)]->value[0]=="Oui" ? 1 : 0) : 0,
                "LOUEUR_VELO"=> isset($answers[$this->get_index($answers, 375516)]->value) ? ( $answers[$this->get_index($answers, 375516)]->value[0]=="Oui" ? 1 : 0) : 0,
                "NOM_ETABLISSEMENT"=> isset($answers[$this->get_index($answers, 375518)]->value) ? $answers[$this->get_index($answers, 375518)]->value : null,
                "ADRESSE_ETABLISSEMENT"=> isset($answers[$this->get_index($answers, 375519)]->value) ? $answers[$this->get_index($answers, 375519)]->value : null,
                "CP_ETABLISSEMENT"=> isset($answers[$this->get_index($answers, 375519)]->postal_code) ? $answers[$this->get_index($answers, 375519)]->postal_code : null,
                "VILLE_ETABLISSEMENT"=> isset($answers[$this->get_index($answers, 375519)]->locality) ? $answers[$this->get_index($answers, 375519)]->locality : null,
                "NOMDEPT_ETABLISSEMENT"=> isset($answers[$this->get_index($answers, 375519)]->aal2) ? $answers[$this->get_index($answers, 375519)]->aal2 : null,
                "PAYS_ETABLISSEMENT"=> isset($answers[$this->get_index($answers, 375519)]->country) ? $answers[$this->get_index($answers, 375519)]->country : null,
                "TELEPHONE_ETABLISSEMENT"=> isset($answers[$this->get_index($answers, 375520)]->value) ? $answers[$this->get_index($answers, 375520)]->value : null,
                "EMAIL_ETABLISSEMENT"=> isset($answers[$this->get_index($answers, 375582)]->value) ? $answers[$this->get_index($answers, 375582)]->value : null,
                "SITEWEB"=> isset($answers[$this->get_index($answers, 375522)]->value) ? $answers[$this->get_index($answers, 375522)]->value : null,
                "CLUB_AVANTAGES"=> $value->group_id ? $value->group_name : null,
                "FERMETURE_HEBDOMADAIRE"=> isset($answers[$this->get_index($answers, 375524)]->value) ? $answers[$this->get_index($answers, 375524)]->value : null,
                "FERMETURE_ANUELLE"=> isset($answers[$this->get_index($answers, 375526)]->value) ? $answers[$this->get_index($answers, 375526)]->value : null,
                "DESCRIPTION"=> isset($answers[$this->get_index($answers, 375527)]->value) ? $answers[$this->get_index($answers, 375527)]->value : null,
                "LIEN_PHOTO1"=> $value->permission_slip ? "https://njuko.s3-eu-west-1.amazonaws.com/thumb_".$photo1 : null,
                "LIEN_PHOTO2"=> $photo2 ? "https://njuko.s3-eu-west-1.amazonaws.com/thumb_".$photo2 : null,
                "LIEN_PHOTO3"=> $photo3 ? "https://njuko.s3-eu-west-1.amazonaws.com/thumb_".$photo3 : null,
                "LIEN_PHOTO4"=> $photo4 ? "https://njuko.s3-eu-west-1.amazonaws.com/thumb_".$photo4 : null,
                "LIEN_PHOTO5"=> $photo5 ? "https://njuko.s3-eu-west-1.amazonaws.com/thumb_".$photo5 : null,
                "LIEN_VIDEO"=> isset($answers[$this->get_index($answers, 375532)]->value) ? $answers[$this->get_index($answers, 375532)]->value : null,
                "HOTEL"=> isset($answers[$this->get_index($answers, 375534)]->value) ? (in_array("Hôtel", $answers[$this->get_index($answers, 375534)]->value) ? 1 : 0) : 0,
                "APPARTHOTEL"=> isset($answers[$this->get_index($answers, 375534)]->value) ? (in_array("Appart'hôtel", $answers[$this->get_index($answers, 375534)]->value) ? 1 : 0) : 0,
                "HOTEL_RESTAURANT"=> isset($answers[$this->get_index($answers, 375534)]->value) ? (in_array("Hôtel-restaurant", $answers[$this->get_index($answers, 375534)]->value) ? 1 : 0) : 0,
                "CAMPING"=> isset($answers[$this->get_index($answers, 375534)]->value) ? (in_array("Camping", $answers[$this->get_index($answers, 375534)]->value) ? 1 : 0) : 0,
                "LODGE"=> isset($answers[$this->get_index($answers, 375534)]->value) ? (in_array("Lodge", $answers[$this->get_index($answers, 375534)]->value) ? 1 : 0) : 0,
                "BUNGALOW"=> isset($answers[$this->get_index($answers, 375534)]->value) ? (in_array("Bungalow", $answers[$this->get_index($answers, 375534)]->value) ? 1 : 0) : 0,
                "GITE"=> isset($answers[$this->get_index($answers, 375534)]->value) ? (in_array("Gîte", $answers[$this->get_index($answers, 375534)]->value) ? 1 : 0) : 0,
                "CHAMBRE_HOTE"=> isset($answers[$this->get_index($answers, 375534)]->value) ? (in_array("Chambre d'hôtes", $answers[$this->get_index($answers, 375534)]->value) ? 1 : 0) : 0,
                "CIS"=> isset($answers[$this->get_index($answers, 375534)]->value) ? (in_array("Centre international de séjour (CIS)", $answers[$this->get_index($answers, 375534)]->value) ? 1 : 0) : 0,
                "VILLAGE_VACANCES"=> isset($answers[$this->get_index($answers, 375534)]->value) ? (in_array("Village vacances", $answers[$this->get_index($answers, 375534)]->value) ? 1 : 0) : 0,
                "AUBERGE_JEUNESSE"=> isset($answers[$this->get_index($answers, 375534)]->value) ? (in_array("Auberge de jeunesse", $answers[$this->get_index($answers, 375534)]->value) ? 1 : 0) : 0,
                "INSOLITE"=> isset($answers[$this->get_index($answers, 375534)]->value) ? (in_array("Hébergement insolite", $answers[$this->get_index($answers, 375534)]->value) ? 1 : 0) : 0,
                "COTTAGE"=> isset($answers[$this->get_index($answers, 375534)]->value) ? (in_array("Cottage", $answers[$this->get_index($answers, 375534)]->value) ? 1 : 0) : 0,//($answers[$this->get_index($answers, 375534)]->value[0] =="Cottage" ? 1 : 0) : 0,
                "CATEGORIE"=> isset($answers[$this->get_index($answers, 375535)]->value) ? $answers[$this->get_index($answers, 375535)]->value : null,
                "NOMBRE_CHAMBRES"=> isset($answers[$this->get_index($answers, 375537)]->value) ? $answers[$this->get_index($answers, 375537)]->value : null,
                "NOMBRE_PERSONNES"=> isset($answers[$this->get_index($answers, 377368)]->value) ? $answers[$this->get_index($answers, 377368)]->value : null,
                "NOMBRE_EMPLACEMENT_CAMPING"=> isset($answers[$this->get_index($answers, 375538)]->value) ? $answers[$this->get_index($answers, 375538)]->value : null,
                "CAPACITE_LOCALVELO"=> isset($answers[$this->get_index($answers, 375572)]->value) ? $answers[$this->get_index($answers, 375572)]->value : null,
                "HEURE_PETITDEJEUNER"=> isset($answers[$this->get_index($answers, 377293)]->value) ? $answers[$this->get_index($answers, 377293)]->value : null,
                "HEURE_FIN_PETIT_DEJEUNER"=> isset($answers[$this->get_index($answers, 381857)]->value) ? $answers[$this->get_index($answers, 381857)]->value : null, //a corriger
                "HEURE_SERVICEMIDI"=> isset($answers[$this->get_index($answers, 377296)]->value) ? $answers[$this->get_index($answers, 377296)]->value : null,
                "HEURE_FIN_SERVICE_MIDI"=> isset($answers[$this->get_index($answers, 381855)]->value) ? $answers[$this->get_index($answers, 381855)]->value : null, //a corriger
                "HEURE_SERVICESOIR"=> isset($answers[$this->get_index($answers, 377298)]->value) ? $answers[$this->get_index($answers, 377298)]->value : null,
                "HEURE_FIN_SERVICE_SOIR"=> isset($answers[$this->get_index($answers, 381856)]->value) ? $answers[$this->get_index($answers, 381856)]->value : null, //a corriger
                "ARTISAN_CONSTRUCTEUR"=> isset($answers[$this->get_index($answers, 375560)]->value) ? ($answers[$this->get_index($answers, 375560)]->value[0]=="Oui" ? 1 : 0) : 0,
                "REPARATION_URGENTE"=> isset($answers[$this->get_index($answers, 375561)]->value) ? ($answers[$this->get_index($answers, 375561)]->value[0]=="Oui" ? 1 : 0) : 0,
                "HEURE_OUVERTURE_MATIN_VELOCISTE"=> isset($answers[$this->get_index($answers, 377299)]->value) ? $answers[$this->get_index($answers, 377299)]->value : null,
                "HEURE_FERMETURE_MIDI_VELOCISTE"=> isset($answers[$this->get_index($answers, 377358)]->value) ? $answers[$this->get_index($answers, 377358)]->value : null,
                "HEURE_OUVERTURE_APREM_VELOCISTE"=> isset($answers[$this->get_index($answers, 377301)]->value) ? $answers[$this->get_index($answers, 377301)]->value : null,
                "HEURE_FERMETURE_SOIR_VELOCISTE"=> isset($answers[$this->get_index($answers, 377359)]->value) ? $answers[$this->get_index($answers, 377359)]->value : null,
                "LOCATION_ROUTE"=> isset($answers[$this->get_index($answers, 375564)]->value) ? (in_array("Route", $answers[$this->get_index($answers, 375564)]->value) ? 1 : 0) : 0,
                "LOCATION_ROUTE_VAE"=> isset($answers[$this->get_index($answers, 375564)]->value) ?  (in_array("Route à assistance électrique", $answers[$this->get_index($answers, 375564)]->value) ? 1 : 0) : 0,
                "LOCATION_VTT"=> isset($answers[$this->get_index($answers, 375564)]->value) ?  (in_array("VTT", $answers[$this->get_index($answers, 375564)]->value) ? 1 : 0) : 0,
                "LOCATION_VTTAE"=> isset($answers[$this->get_index($answers, 375564)]->value) ?  (in_array("VTT à assistance électrique", $answers[$this->get_index($answers, 375564)]->value) ? 1 : 0) : 0,
                "LOCATION_VTC"=> isset($answers[$this->get_index($answers, 375564)]->value) ?  (in_array("VTC", $answers[$this->get_index($answers, 375564)]->value) ? 1 : 0) : 0,
                "LOCATION_VTCAE"=> isset($answers[$this->get_index($answers, 375564)]->value) ?  (in_array("VTC à assistance électrique", $answers[$this->get_index($answers, 375564)]->value) ? 1 : 0) : 0,
                "LOCATION_GRAVEL"=> isset($answers[$this->get_index($answers, 375564)]->value) ?  (in_array("Gravel", $answers[$this->get_index($answers, 375564)]->value) ? 1 : 0) : 0,
                "LOCATION_GRAVEL_VAE"=> isset($answers[$this->get_index($answers, 375564)]->value) ?  (in_array("Gravel à assistance électrique", $answers[$this->get_index($answers, 375564)]->value) ? 1 : 0) : 0,
                "LOCATION_VILLE"=> isset($answers[$this->get_index($answers, 375564)]->value) ?  (in_array("Ville", $answers[$this->get_index($answers, 375564)]->value) ? 1 : 0) : 0,
                "LOCATION_VILLE_VAE"=> isset($answers[$this->get_index($answers, 375564)]->value) ?  (in_array("Ville à assistance électrique", $answers[$this->get_index($answers, 375564)]->value) ? 1 : 0) : 0,
                "LOCATION_ENFANT"=> isset($answers[$this->get_index($answers, 375564)]->value) ?  (in_array("Vélos pour enfants", $answers[$this->get_index($answers, 375564)]->value) ? 1 : 0) : 0,
                "LOCATION_ENFANT_VAE"=> isset($answers[$this->get_index($answers, 375564)]->value) ?  (in_array("Vélos pour enfants à assistance électrique", $answers[$this->get_index($answers, 375564)]->value) ? 1 : 0) : 0,
                "OFFRE_5POURCENT"=> isset($answers[$this->get_index($answers, 375573)]->value) ? (in_array("une remise de 5%", $answers[$this->get_index($answers, 375573)]->value) ? 1 : 0) : 0,
                "OFFRE_APERO"=> isset($answers[$this->get_index($answers, 375573)]->value) ? (in_array("un apéritif offert", $answers[$this->get_index($answers, 375573)]->value) ? 1 : 0) : 0,
                "OFFRE_ADHESION"=> isset($answers[$this->get_index($answers, 375573)]->value) ?  (in_array("adhésion offerte à votre établissement", $answers[$this->get_index($answers, 375573)]->value) ? 1 : 0) : 0, //($answers[$this->get_index($answers, 375573)]->value[0]=="adhésion offerte à votre établissement" ? 1 : 0) : 0,
                "REPAS_EMPORTER"=> isset($answers[$this->get_index($answers, 375576)]->value) ? ($answers[$this->get_index($answers, 375576)]->value[0]=="Oui" ? 1 : 0) : 0,
                "LAVE_LINGE"=> isset($answers[$this->get_index($answers, 375577)]->value) ? ($answers[$this->get_index($answers, 375577)]->value[0]=="Oui" ? 1 : 0) : 0,
                "WIFI"=> isset($answers[$this->get_index($answers, 375578)]->value) ? ($answers[$this->get_index($answers, 375578)]->value[0]=="Oui" ? 1 : 0) : 0,
                "DATE_CREATION" => $value->created,
                "DATE_MODIFICATION"=> $value->updated,
                "TARIF_MINIMUM"=> isset($answers[$this->get_index($answers, 379607)]->value) ? $answers[$this->get_index($answers, 379607)]->value : null,
                "SLUG"=> isset($answers[$this->get_index($answers, 375518)]->value) ? $slugify->slugify($value->id."-".$answers[$this->get_index($answers, 375518)]->value) : $slugify->slugify($value->id)
            ]
        ]);
        if($objectId==null){
            unset($array[0]["attributes"]["OBJECTID"]);
        }
        return $array[0];
    }
    

}
