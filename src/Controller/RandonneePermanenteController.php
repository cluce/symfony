<?php

namespace App\Controller;

use DateTime;
use Cocur\Slugify\Slugify;
//use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RandonneePermanenteController extends AbstractController
{

    private $client;
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }
    
    //token arcgis online esri
    private function get_esri_token(){
        //$client = HttpClient::create();
        $response = $this->client->request(
            'GET',
            'https://www.arcgis.com/sharing/rest/oauth2/token?client_id=&client_secret=&grant_type=client_credentials'
        );
        $token = json_decode($response->getContent())->access_token;
        return $token;
    }

    //token api ffcyclo.org - exalto
    private function get_exalto_token() {
        date_default_timezone_set('Europe/Paris');
        //$client = HttpClient::create();
        $response = $this->client->request(
            'GET',
        'http://ffcyclo.org/ws/rest/Oin/GetToken?sessionIdentite=&password='.date('YmdHi'), 
        );
        $token = json_decode($response->getContent())->Response->token;
        return $token;
    }

    private function lister_manifestations($token, $modeleId){
        //$client = HttpClient::create();
        $response = $this->client->request(
            'GET',
            'http://ffcyclo.org/ws/rest/Oin/ListerManifestations?token='.$token.'&ModeleId='.$modeleId, 
        );
        $rps = json_decode($response->getContent(), true);
        return $rps["Response"]["manifestationsListe"];
    }

    private function voir_manifestation($token, $id){
        //$client = HttpClient::create();
        $response = $this->client->request(
            'GET',
            'http://ffcyclo.org/ws/rest/Oin/VoirManifestation?token='.$token.'&ManifestationId='.$id,
        );
        $manif = json_decode($response->getContent(), true);
        return $manif["Response"]["manifestationModel"];
    }

    /**
     * @Route("/maj-rp", name="sync_rp")
    */
    public function sync_rp(){
        $slugify = new Slugify();
        //$client = HttpClient::create(['headers'=>['Content-Type'=>'application/x-www-form-urlencoded']]);
        $arrEsriIdsOnly=array();
        $arrEsri=array();//attention si republication du service, il faut maj le service definition pour augmenter le maxRecordCount (https://support.esri.com/en/technical-article/000012383)
        $getEsrIds = "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/OIN_TEST2/FeatureServer/2/query?token=".$this->get_esri_token()."&where=1=1&f=json&outFields=OBJECTID,ManifId,date_modif&returnGeometry=false";
        $response = $this->client->request('GET', $getEsrIds,['headers'=>['Content-Type'=>'application/x-www-form-urlencoded']]);
        $content = $response->getContent() ? json_decode($response->getContent())->features : false;
        if($content){
            foreach ($content as $value) {
                array_push($arrEsri, [$value->attributes->ManifId, $value->attributes->OBJECTID, $value->attributes->date_modif]);
                array_push($arrEsriIdsOnly, $value->attributes->ManifId);
            }
        }

        //rp route et vtt exalto
        $arrExaltoIds = array();
        $arrExaltoIdsOnly = array();
        $getExaltoRpVTT = $this->lister_manifestations($this->get_exalto_token(), "26");
        foreach ($getExaltoRpVTT as $value) {
            array_push($arrExaltoIds, [$value["ManifId"],$value["date_modif"]]);
            array_push($arrExaltoIdsOnly, $value["ManifId"]);
        }
        $getExaltoRpRoute = $this->lister_manifestations($this->get_exalto_token(), "25");
        foreach ($getExaltoRpRoute as $value) {
            array_push($arrExaltoIds, [$value["ManifId"],$value["date_modif"]]);
            array_push($arrExaltoIdsOnly, $value["ManifId"]);
        }

        //rp a supprimer esri
        $deleteFeatures=array();
        $rpsToDelete = array_diff($arrEsriIdsOnly,$arrExaltoIdsOnly); //manifs esri qui ne sont pas chez exalto
        foreach ($rpsToDelete as $id) {
            foreach($arrEsri as $value){
                if($id==$value[0]){
                    array_push($deleteFeatures,$value[1]);
                }
            } 
        }
        $deleteFeatures=implode(",",$deleteFeatures);
        
        //rp a mettre a jour esri
        $rpsToUpdate=array();
        foreach ($arrExaltoIds as $dateModifExalto) {
            foreach ($arrEsri as $dateModifEsri) {
                $idExalto=$dateModifExalto[0];
                $dateExalto=new DateTime($dateModifExalto[1]);
                $idEsri=$dateModifEsri[0];
                $objectIdEsri=$dateModifEsri[1];
                $dateEsri=new DateTime($dateModifEsri[2]); 
                if(($idExalto == $idEsri)&&($dateExalto > $dateEsri)){
                    array_push($rpsToUpdate,[$idEsri,$objectIdEsri]);
                }
            }
        }
        $arrayUpdate=array();
        $updateFeatures=array();
        foreach ($rpsToUpdate as $value) {
            array_push($updateFeatures,$this->setRandonneePermanente($value,$arrayUpdate,$slugify));
        }

        //rp a ajouter esri
        $arrayAdd=array();
        $addFeatures=array();
        $rpsToAdd = array_diff($arrExaltoIdsOnly, $arrEsriIdsOnly); //rp exalto qui ne sont pas chez esri
        foreach ($rpsToAdd as $value) {
            array_push($addFeatures,$this->setRandonneePermanente($value,$arrayAdd,$slugify));
        }

        //chargement gdb arcgis online
        $url = "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/OIN_TEST2/FeatureServer/2/applyEdits";
        $parameters = [
            'f'=> 'json',
            'token' => $this->get_esri_token(),
            'deletes'=> $deleteFeatures,
            'adds'=> json_encode($addFeatures),
            'updates'=> json_encode($updateFeatures),
        ];
        $post_response = $this->client->request('POST', $url, ['body' => $parameters]);
        dd($post_response->getContent());
    }

    private function setRandonneePermanente($value,$array,$slugify){
        $id = is_array($value)==true ? /*id update*/$value[0] : /*id add*/$value;
        $objectId=  is_array($value)==true ? $value[1] : null;
        $getExaltoVoirRps = $this->voir_manifestation($this->get_exalto_token(),$id);
        $OinManifObservation=$getExaltoVoirRps["OinManifObservation"];
        $OinManifObservation = str_replace(array("\n", "\t", "\r"), '', $OinManifObservation);
        $Nom=$getExaltoVoirRps["Nom"];
        $Nom = str_replace(array("<", "/", ">"), ' ', $Nom);
        array_push($array, [
            "attributes" => [
                "OBJECTID"=> $objectId,
                "date_creation" => $getExaltoVoirRps["date_creation"],
                "date_modif" => $getExaltoVoirRps["date_modif"],
                "ManifId" => $getExaltoVoirRps["ManifId"],
                "Nom" => $Nom,
                "Accueil_DateDbt" => $getExaltoVoirRps["Accueil_DateDbt"],
                "Arrivee_DateFin" => $getExaltoVoirRps["Arrivee_DateFin"],
                "Accueil_Adresse" => $getExaltoVoirRps["Accueil_Adresse"],
                "Accueil_Commune" => $getExaltoVoirRps["Accueil_Commune"],
                "Accueil_CP" => $getExaltoVoirRps["Accueil_CP"],
                "heures" => $getExaltoVoirRps["heures"],
                "Accueil_Lieu" => $getExaltoVoirRps["Accueil_Lieu"],
                "heureDeCloture" => $getExaltoVoirRps["heureDeCloture"],
                "InfoLabel" => $getExaltoVoirRps["InfoLabel"],
                "NumeroLabel" => $getExaltoVoirRps["NumeroLabel"],
                "CategorieLabel" => $getExaltoVoirRps["CategorieLabel"],
                "SiteWebRip" => $getExaltoVoirRps["SiteWebRip"],
                "OinManifLicTarifAdulte" => $getExaltoVoirRps["OinManifLicTarifAdulte"],
                "OinManifLicTarifJeune" => $getExaltoVoirRps["OinManifLicTarifJeune"],
                "OinManifNonLicTarifAdulte" => $getExaltoVoirRps["OinManifNonLicTarifAdulte"],
                "OinManifNonLicTarifJeune" => $getExaltoVoirRps["OinManifNonLicTarifJeune"],
                "Structure" => $getExaltoVoirRps["Structure"],
                "StructureDep" => $getExaltoVoirRps["StructureDep"],
                "StructureReg" => $getExaltoVoirRps["StructureReg"],
                "Type" => $getExaltoVoirRps["Type"],
                "ModeleId" => $getExaltoVoirRps["ModeleId"],
                "OinManifObservation" => $OinManifObservation,
                "OinManifReserveLicencie" => $getExaltoVoirRps["OinManifReserveLicencie"] == "Oui" ? 1 : 0,
                "Contact_Nom" => $getExaltoVoirRps["Contact_Nom"],
                "Contact_Prenom" => $getExaltoVoirRps["Contact_Prenom"],
                "AdresseMail" => $getExaltoVoirRps["AdresseMail"],
                "AdresseWeb" => $getExaltoVoirRps["AdresseWeb"],
                "AdresseTel" => $getExaltoVoirRps["AdresseTel"],
                "Arrivee_Lieu" => $getExaltoVoirRps["Arrivee_Lieu"],
                "Arrivee_CP" => $getExaltoVoirRps["Arrivee_CP"],
                "Arrivee_Commune" => $getExaltoVoirRps["Arrivee_Commune"],
                "Pratique_VTT" => $getExaltoVoirRps["Pratique_VTT"] == "Oui" ? 1 : 0,
                "Pratique_Route" => $getExaltoVoirRps["Pratique_Route"] == "Oui" ? 1 : 0,
                "Pratique_Marche" => $getExaltoVoirRps["Pratique_Marche"] == "Oui" ? 1 : 0,
                "Pratique_Gravel" => $getExaltoVoirRps["Pratique_Gravel"] == "Oui" ? 1 : 0,
                "Accueil_Lat" => $getExaltoVoirRps["Accueil_Lat"],
                "Accueil_Long" => $getExaltoVoirRps["Accueil_Long"],
                "Arrivee_Lat" => $getExaltoVoirRps["Arrivee_Lat"],
                "Arrivee_Long" => $getExaltoVoirRps["Arrivee_Long"],
                "FichierGpx" => $getExaltoVoirRps["FichierGpx"],
                "FichierFlyer" => $getExaltoVoirRps["FichierFlyer"],
                "AccesPSH" => $getExaltoVoirRps["AccesPSH"] == "Oui" ? 1 : 0,
                "DevDurable" => $getExaltoVoirRps["DevDurable"] == "Oui" ? 1 : 0,
                "SpeFem" => $getExaltoVoirRps["SpeFem"] == "Oui" ? 1 : 0,
                "InscriptionLigne" => $getExaltoVoirRps["InscriptionLigne"] == "1" ? 1 : 0,
                //marche
                "OinMarcheDist_1" => intval($getExaltoVoirRps["OinMarcheDist_1"]),
                "OinMarcheDist_2" => intval($getExaltoVoirRps["OinMarcheDist_2"]),
                "OinMarcheDist_3" => intval($getExaltoVoirRps["OinMarcheDist_3"]),
                "OinMarcheDist_4" => intval($getExaltoVoirRps["OinMarcheDist_4"]),
                "OinMarcheDist_5" => intval($getExaltoVoirRps["OinMarcheDist_5"]),
                "OinMarcheDist_6" => intval($getExaltoVoirRps["OinMarcheDist_6"]),
                //route
                "OinRouteDeniv_1" => intval($getExaltoVoirRps["OinRouteDeniv_1"]),
                "OinRouteDeniv_2" => intval($getExaltoVoirRps["OinRouteDeniv_2"]),
                "OinRouteDeniv_3" => intval($getExaltoVoirRps["OinRouteDeniv_3"]),
                "OinRouteDeniv_4" => intval($getExaltoVoirRps["OinRouteDeniv_4"]),
                "OinRouteDeniv_5" => intval($getExaltoVoirRps["OinRouteDeniv_5"]),
                "OinRouteDeniv_6" => intval($getExaltoVoirRps["OinRouteDeniv_6"]),
                "OinRouteDiff_1" => intval($getExaltoVoirRps["OinRouteDiff_1"]),
                "OinRouteDiff_2" => intval($getExaltoVoirRps["OinRouteDiff_2"]),
                "OinRouteDiff_3" => intval($getExaltoVoirRps["OinRouteDiff_3"]),
                "OinRouteDiff_4" => intval($getExaltoVoirRps["OinRouteDiff_4"]),
                "OinRouteDiff_5" => intval($getExaltoVoirRps["OinRouteDiff_5"]),
                "OinRouteDiff_6" => intval($getExaltoVoirRps["OinRouteDiff_6"]),
                "OinRouteDist_1" => intval($getExaltoVoirRps["OinRouteDist_1"]),
                "OinRouteDist_2" => intval($getExaltoVoirRps["OinRouteDist_2"]),
                "OinRouteDist_3" => intval($getExaltoVoirRps["OinRouteDist_3"]),
                "OinRouteDist_4" => intval($getExaltoVoirRps["OinRouteDist_4"]),
                "OinRouteDist_5" => intval($getExaltoVoirRps["OinRouteDist_5"]),
                "OinRouteDist_6" => intval($getExaltoVoirRps["OinRouteDist_6"]),
                //vtt
                "OinVTTDeniv_1" => intval($getExaltoVoirRps["OinVTTDeniv_1"]),
                "OinVTTDeniv_2" => intval($getExaltoVoirRps["OinVTTDeniv_2"]),
                "OinVTTDeniv_3" => intval($getExaltoVoirRps["OinVTTDeniv_3"]),
                "OinVTTDeniv_4" => intval($getExaltoVoirRps["OinVTTDeniv_4"]),
                "OinVTTDeniv_5" => intval($getExaltoVoirRps["OinVTTDeniv_5"]),
                "OinVTTDeniv_6" => intval($getExaltoVoirRps["OinVTTDeniv_6"]),
                "OinVTTDiff_1" => intval($getExaltoVoirRps["OinVTTDiff_1"]),
                "OinVTTDiff_2" => intval($getExaltoVoirRps["OinVTTDiff_2"]),
                "OinVTTDiff_3" => intval($getExaltoVoirRps["OinVTTDiff_3"]),
                "OinVTTDiff_4" => intval($getExaltoVoirRps["OinVTTDiff_4"]),
                "OinVTTDiff_5" => intval($getExaltoVoirRps["OinVTTDiff_5"]),
                "OinVTTDiff_6" => intval($getExaltoVoirRps["OinVTTDiff_6"]),
                "OinVTTDist_1" => intval($getExaltoVoirRps["OinVTTDist_1"]),
                "OinVTTDist_2" => intval($getExaltoVoirRps["OinVTTDist_2"]),
                "OinVTTDist_3" => intval($getExaltoVoirRps["OinVTTDist_3"]),
                "OinVTTDist_4" => intval($getExaltoVoirRps["OinVTTDist_4"]),
                "OinVTTDist_5" => intval($getExaltoVoirRps["OinVTTDist_5"]),
                "OinVTTDist_6" => intval($getExaltoVoirRps["OinVTTDist_6"]),
                //gravel
                "OinGravelDeniv_1" => intval($getExaltoVoirRps["OinGravelDeniv_1"]),
                "OinGravelDeniv_2" => intval($getExaltoVoirRps["OinGravelDeniv_2"]),
                "OinGravelDeniv_3" => intval($getExaltoVoirRps["OinGravelDeniv_3"]),
                "OinGravelDeniv_4" => intval($getExaltoVoirRps["OinGravelDeniv_4"]),
                "OinGravelDeniv_5" => intval($getExaltoVoirRps["OinGravelDeniv_5"]),
                "OinGravelDeniv_6" => intval($getExaltoVoirRps["OinGravelDeniv_6"]),
                "OinGravelDiff_1" => intval($getExaltoVoirRps["OinGravelDiff_1"]),
                "OinGravelDiff_2" => intval($getExaltoVoirRps["OinGravelDiff_2"]),
                "OinGravelDiff_3" => intval($getExaltoVoirRps["OinGravelDiff_3"]),
                "OinGravelDiff_4" => intval($getExaltoVoirRps["OinGravelDiff_4"]),
                "OinGravelDiff_5" => intval($getExaltoVoirRps["OinGravelDiff_5"]),
                "OinGravelDiff_6" => intval($getExaltoVoirRps["OinGravelDiff_6"]),
                "OinGravelDist_1" => intval($getExaltoVoirRps["OinGravelDist_1"]),
                "OinGravelDist_2" => intval($getExaltoVoirRps["OinGravelDist_2"]),
                "OinGravelDist_3" => intval($getExaltoVoirRps["OinGravelDist_3"]),
                "OinGravelDist_4" => intval($getExaltoVoirRps["OinGravelDist_4"]),
                "OinGravelDist_5" => intval($getExaltoVoirRps["OinGravelDist_5"]),
                "OinGravelDist_6" => intval($getExaltoVoirRps["OinGravelDist_6"]),
                "slug"=> $slugify->slugify($getExaltoVoirRps["ManifId"]."-".$Nom) 
            ]
        ]);
        if($objectId==null){
            unset($array[0]["attributes"]["OBJECTID"]);
        }
        //dd($array[0]);
        return $array[0];
    }
    
}
