<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MajBaCommand extends Command
{
    protected static $defaultName = 'app:maj-ba';

    protected function configure()
    {
        $this
            ->setDescription('Mise à jour des bonnes adresses de Njuko vers Esri')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $service = $this->getApplication()->getKernel()->getContainer()->get('MajBaCommandService');
        $service->sync_ba();
        return Command::SUCCESS;
    }
}
