<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MajRpCommand extends Command
{
    protected static $defaultName = 'app:maj-rp';

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $service = $this->getApplication()->getKernel()->getContainer()->get('MajRpCommandService');
        $service->sync_rp();
        return Command::SUCCESS;
    }
}
