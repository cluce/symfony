<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MajManifsCommand extends Command
{
    protected static $defaultName = 'app:maj-manifs';

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $service = $this->getApplication()->getKernel()->getContainer()->get('MajManifsCommandService');
        $service->sync_manifs();
        return Command::SUCCESS;
    }
}
