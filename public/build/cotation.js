(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cotation"],{

/***/ "./assets/js/pages/cotation/cotation.js":
/*!**********************************************!*\
  !*** ./assets/js/pages/cotation/cotation.js ***!
  \**********************************************/
/*! exports provided: convertMinsToHrsMins, addValueToInput */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "convertMinsToHrsMins", function() { return convertMinsToHrsMins; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addValueToInput", function() { return addValueToInput; });
/* harmony import */ var core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.parse-float.js */ "./node_modules/core-js/modules/es.parse-float.js");
/* harmony import */ var core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.regexp.exec.js */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.string.replace.js */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2__);



//conversion minutes en heures
var convertMinsToHrsMins = function convertMinsToHrsMins(minutes) {
  var h = Math.floor(minutes / 60);
  var m = minutes % 60;
  h = h < 10 ? '0' + h : h;
  m = m < 10 ? '0' + m : m;
  return h + ':' + m;
}; //remplissage inputs

function addValueToInput(inputVal, id) {
  //document.getElementById(id).value = inputVal;
  $(id).val(inputVal);
} //recup coordonnees points departs

/*const getCoordsDepart = (xml) => {
    parser = new DOMParser();
    xmlDoc = parser.parseFromString(xml, "text/xml");
    var lat = xmlDoc.getElementsByTagName("trkpt")[0].getAttribute('lat');
    var lon = xmlDoc.getElementsByTagName("trkpt")[0].getAttribute('lon');
    return [lon, lat];
}*/
//calcul de la difficulte avec radios buttons

$(":radio").change(function () {
  var names = {};
  $(':radio').each(function () {
    names[$(this).attr('name')] = true;
  });
  var count = 0;
  $.each(names, function () {
    count++;
  });

  if ($(':radio:checked').length === count) {
    var total = 0;
    $("input[type=radio]:checked").each(function () {
      total += parseFloat($(this).val());
    });
    outputDiff(total);
  }
}); //input difficulte

function outputDiff(total) {
  var diff, color;

  if (total >= 4 && total <= 5) {
    diff = total + " - Très facile";
    color = "green";
  } else if (total >= 6 && total <= 8) {
    diff = total + " - Facile";
    color = "blue";
  } else if (total >= 9 && total <= 12) {
    diff = total + " - Difficile";
    color = "red";
  } else {
    diff = total + " - Très Difficile";
    color = "black";
  }

  $("#cotation_form_difficulte").val(diff);
  $("#cotation_form_difficulte").css({
    "color": color,
    "font-size": "25px"
  });
} //compte le nombre de caracteres dans le textarea


var textInit = 0;
$('#count_text').html(textInit + ' caractère /500');
$('#cotation_form_textDescr').keyup(function () {
  var text_length = $('#cotation_form_textDescr').val().length;
  $('#count_text').html(text_length + ' caractères /500');

  if (text_length <= 1) {
    $('#count_text').html(text_length + ' caractère /500');
  }

  if (text_length > 500) {
    document.getElementById("count_text").style.color = 'red';
  } else {
    document.getElementById("count_text").style.color = 'black';
  }
}); //empecher les retours charriots du textarea

$('#cotation_form_textDescr').on('keyup', function () {
  $(this).val($(this).val().replace(/[\r\n\v]+/g, ''));
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./node_modules/core-js/internals/get-substitution.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/internals/get-substitution.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var toObject = __webpack_require__(/*! ../internals/to-object */ "./node_modules/core-js/internals/to-object.js");

var floor = Math.floor;
var replace = ''.replace;
var SUBSTITUTION_SYMBOLS = /\$([$&'`]|\d\d?|<[^>]*>)/g;
var SUBSTITUTION_SYMBOLS_NO_NAMED = /\$([$&'`]|\d\d?)/g;

// https://tc39.es/ecma262/#sec-getsubstitution
module.exports = function (matched, str, position, captures, namedCaptures, replacement) {
  var tailPos = position + matched.length;
  var m = captures.length;
  var symbols = SUBSTITUTION_SYMBOLS_NO_NAMED;
  if (namedCaptures !== undefined) {
    namedCaptures = toObject(namedCaptures);
    symbols = SUBSTITUTION_SYMBOLS;
  }
  return replace.call(replacement, symbols, function (match, ch) {
    var capture;
    switch (ch.charAt(0)) {
      case '$': return '$';
      case '&': return matched;
      case '`': return str.slice(0, position);
      case "'": return str.slice(tailPos);
      case '<':
        capture = namedCaptures[ch.slice(1, -1)];
        break;
      default: // \d\d?
        var n = +ch;
        if (n === 0) return match;
        if (n > m) {
          var f = floor(n / 10);
          if (f === 0) return match;
          if (f <= m) return captures[f - 1] === undefined ? ch.charAt(1) : captures[f - 1] + ch.charAt(1);
          return match;
        }
        capture = captures[n - 1];
    }
    return capture === undefined ? '' : capture;
  });
};


/***/ }),

/***/ "./node_modules/core-js/modules/es.string.replace.js":
/*!***********************************************************!*\
  !*** ./node_modules/core-js/modules/es.string.replace.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var fixRegExpWellKnownSymbolLogic = __webpack_require__(/*! ../internals/fix-regexp-well-known-symbol-logic */ "./node_modules/core-js/internals/fix-regexp-well-known-symbol-logic.js");
var anObject = __webpack_require__(/*! ../internals/an-object */ "./node_modules/core-js/internals/an-object.js");
var toLength = __webpack_require__(/*! ../internals/to-length */ "./node_modules/core-js/internals/to-length.js");
var toInteger = __webpack_require__(/*! ../internals/to-integer */ "./node_modules/core-js/internals/to-integer.js");
var requireObjectCoercible = __webpack_require__(/*! ../internals/require-object-coercible */ "./node_modules/core-js/internals/require-object-coercible.js");
var advanceStringIndex = __webpack_require__(/*! ../internals/advance-string-index */ "./node_modules/core-js/internals/advance-string-index.js");
var getSubstitution = __webpack_require__(/*! ../internals/get-substitution */ "./node_modules/core-js/internals/get-substitution.js");
var regExpExec = __webpack_require__(/*! ../internals/regexp-exec-abstract */ "./node_modules/core-js/internals/regexp-exec-abstract.js");

var max = Math.max;
var min = Math.min;

var maybeToString = function (it) {
  return it === undefined ? it : String(it);
};

// @@replace logic
fixRegExpWellKnownSymbolLogic('replace', 2, function (REPLACE, nativeReplace, maybeCallNative, reason) {
  var REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE = reason.REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE;
  var REPLACE_KEEPS_$0 = reason.REPLACE_KEEPS_$0;
  var UNSAFE_SUBSTITUTE = REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE ? '$' : '$0';

  return [
    // `String.prototype.replace` method
    // https://tc39.es/ecma262/#sec-string.prototype.replace
    function replace(searchValue, replaceValue) {
      var O = requireObjectCoercible(this);
      var replacer = searchValue == undefined ? undefined : searchValue[REPLACE];
      return replacer !== undefined
        ? replacer.call(searchValue, O, replaceValue)
        : nativeReplace.call(String(O), searchValue, replaceValue);
    },
    // `RegExp.prototype[@@replace]` method
    // https://tc39.es/ecma262/#sec-regexp.prototype-@@replace
    function (regexp, replaceValue) {
      if (
        (!REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE && REPLACE_KEEPS_$0) ||
        (typeof replaceValue === 'string' && replaceValue.indexOf(UNSAFE_SUBSTITUTE) === -1)
      ) {
        var res = maybeCallNative(nativeReplace, regexp, this, replaceValue);
        if (res.done) return res.value;
      }

      var rx = anObject(regexp);
      var S = String(this);

      var functionalReplace = typeof replaceValue === 'function';
      if (!functionalReplace) replaceValue = String(replaceValue);

      var global = rx.global;
      if (global) {
        var fullUnicode = rx.unicode;
        rx.lastIndex = 0;
      }
      var results = [];
      while (true) {
        var result = regExpExec(rx, S);
        if (result === null) break;

        results.push(result);
        if (!global) break;

        var matchStr = String(result[0]);
        if (matchStr === '') rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
      }

      var accumulatedResult = '';
      var nextSourcePosition = 0;
      for (var i = 0; i < results.length; i++) {
        result = results[i];

        var matched = String(result[0]);
        var position = max(min(toInteger(result.index), S.length), 0);
        var captures = [];
        // NOTE: This is equivalent to
        //   captures = result.slice(1).map(maybeToString)
        // but for some reason `nativeSlice.call(result, 1, result.length)` (called in
        // the slice polyfill when slicing native arrays) "doesn't work" in safari 9 and
        // causes a crash (https://pastebin.com/N21QzeQA) when trying to debug it.
        for (var j = 1; j < result.length; j++) captures.push(maybeToString(result[j]));
        var namedCaptures = result.groups;
        if (functionalReplace) {
          var replacerArgs = [matched].concat(captures, position, S);
          if (namedCaptures !== undefined) replacerArgs.push(namedCaptures);
          var replacement = String(replaceValue.apply(undefined, replacerArgs));
        } else {
          replacement = getSubstitution(matched, S, position, captures, namedCaptures, replaceValue);
        }
        if (position >= nextSourcePosition) {
          accumulatedResult += S.slice(nextSourcePosition, position) + replacement;
          nextSourcePosition = position + matched.length;
        }
      }
      return accumulatedResult + S.slice(nextSourcePosition);
    }
  ];
});


/***/ })

},[["./assets/js/pages/cotation/cotation.js","runtime","vendors~app~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_velor~10347b53","vendors~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~61364f2f","vendors~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~cotatio~d7e263c1"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvcGFnZXMvY290YXRpb24vY290YXRpb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvaW50ZXJuYWxzL2dldC1zdWJzdGl0dXRpb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbW9kdWxlcy9lcy5zdHJpbmcucmVwbGFjZS5qcyJdLCJuYW1lcyI6WyJjb252ZXJ0TWluc1RvSHJzTWlucyIsIm1pbnV0ZXMiLCJoIiwiTWF0aCIsImZsb29yIiwibSIsImFkZFZhbHVlVG9JbnB1dCIsImlucHV0VmFsIiwiaWQiLCIkIiwidmFsIiwiY2hhbmdlIiwibmFtZXMiLCJlYWNoIiwiYXR0ciIsImNvdW50IiwibGVuZ3RoIiwidG90YWwiLCJwYXJzZUZsb2F0Iiwib3V0cHV0RGlmZiIsImRpZmYiLCJjb2xvciIsImNzcyIsInRleHRJbml0IiwiaHRtbCIsImtleXVwIiwidGV4dF9sZW5ndGgiLCJkb2N1bWVudCIsImdldEVsZW1lbnRCeUlkIiwic3R5bGUiLCJvbiIsInJlcGxhY2UiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNPLElBQU1BLG9CQUFvQixHQUFHLFNBQXZCQSxvQkFBdUIsQ0FBQ0MsT0FBRCxFQUFhO0FBQzdDLE1BQUlDLENBQUMsR0FBR0MsSUFBSSxDQUFDQyxLQUFMLENBQVdILE9BQU8sR0FBRyxFQUFyQixDQUFSO0FBQ0EsTUFBSUksQ0FBQyxHQUFHSixPQUFPLEdBQUcsRUFBbEI7QUFDQUMsR0FBQyxHQUFHQSxDQUFDLEdBQUcsRUFBSixHQUFTLE1BQU1BLENBQWYsR0FBbUJBLENBQXZCO0FBQ0FHLEdBQUMsR0FBR0EsQ0FBQyxHQUFHLEVBQUosR0FBUyxNQUFNQSxDQUFmLEdBQW1CQSxDQUF2QjtBQUNBLFNBQU9ILENBQUMsR0FBRyxHQUFKLEdBQVVHLENBQWpCO0FBQ0gsQ0FOTSxDLENBUVA7O0FBQ08sU0FBU0MsZUFBVCxDQUF5QkMsUUFBekIsRUFBbUNDLEVBQW5DLEVBQXNDO0FBQ3pDO0FBQ0FDLEdBQUMsQ0FBQ0QsRUFBRCxDQUFELENBQU1FLEdBQU4sQ0FBVUgsUUFBVjtBQUNILEMsQ0FFRDs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOztBQUNBRSxDQUFDLENBQUMsUUFBRCxDQUFELENBQVlFLE1BQVosQ0FBbUIsWUFBWTtBQUMzQixNQUFJQyxLQUFLLEdBQUcsRUFBWjtBQUNBSCxHQUFDLENBQUMsUUFBRCxDQUFELENBQVlJLElBQVosQ0FBaUIsWUFBWTtBQUN6QkQsU0FBSyxDQUFDSCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFLLElBQVIsQ0FBYSxNQUFiLENBQUQsQ0FBTCxHQUE4QixJQUE5QjtBQUNILEdBRkQ7QUFHQSxNQUFJQyxLQUFLLEdBQUcsQ0FBWjtBQUNBTixHQUFDLENBQUNJLElBQUYsQ0FBT0QsS0FBUCxFQUFjLFlBQVk7QUFDdEJHLFNBQUs7QUFDUixHQUZEOztBQUdBLE1BQUlOLENBQUMsQ0FBQyxnQkFBRCxDQUFELENBQW9CTyxNQUFwQixLQUErQkQsS0FBbkMsRUFBMEM7QUFDdEMsUUFBSUUsS0FBSyxHQUFHLENBQVo7QUFDQVIsS0FBQyxDQUFDLDJCQUFELENBQUQsQ0FBK0JJLElBQS9CLENBQW9DLFlBQVk7QUFDNUNJLFdBQUssSUFBSUMsVUFBVSxDQUFDVCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFDLEdBQVIsRUFBRCxDQUFuQjtBQUNILEtBRkQ7QUFHQVMsY0FBVSxDQUFDRixLQUFELENBQVY7QUFDSDtBQUNKLENBaEJELEUsQ0FtQkE7O0FBQ0EsU0FBU0UsVUFBVCxDQUFvQkYsS0FBcEIsRUFBMEI7QUFDdEIsTUFBSUcsSUFBSixFQUFVQyxLQUFWOztBQUNBLE1BQUlKLEtBQUssSUFBSSxDQUFULElBQWNBLEtBQUssSUFBSSxDQUEzQixFQUE4QjtBQUMxQkcsUUFBSSxHQUFHSCxLQUFLLEdBQUcsZ0JBQWY7QUFDQUksU0FBSyxHQUFHLE9BQVI7QUFDSCxHQUhELE1BR08sSUFBSUosS0FBSyxJQUFJLENBQVQsSUFBY0EsS0FBSyxJQUFJLENBQTNCLEVBQThCO0FBQ2pDRyxRQUFJLEdBQUdILEtBQUssR0FBRyxXQUFmO0FBQ0FJLFNBQUssR0FBRyxNQUFSO0FBQ0gsR0FITSxNQUdBLElBQUlKLEtBQUssSUFBSSxDQUFULElBQWNBLEtBQUssSUFBSSxFQUEzQixFQUErQjtBQUNsQ0csUUFBSSxHQUFHSCxLQUFLLEdBQUcsY0FBZjtBQUNBSSxTQUFLLEdBQUcsS0FBUjtBQUNILEdBSE0sTUFHQTtBQUNIRCxRQUFJLEdBQUdILEtBQUssR0FBRyxtQkFBZjtBQUNBSSxTQUFLLEdBQUcsT0FBUjtBQUNIOztBQUNEWixHQUFDLENBQUMsMkJBQUQsQ0FBRCxDQUErQkMsR0FBL0IsQ0FBbUNVLElBQW5DO0FBQ0FYLEdBQUMsQ0FBQywyQkFBRCxDQUFELENBQStCYSxHQUEvQixDQUFtQztBQUFDLGFBQVNELEtBQVY7QUFBaUIsaUJBQWE7QUFBOUIsR0FBbkM7QUFDSCxDLENBRUQ7OztBQUNBLElBQUlFLFFBQVEsR0FBRyxDQUFmO0FBQ0FkLENBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJlLElBQWpCLENBQXNCRCxRQUFRLEdBQUcsaUJBQWpDO0FBQ0FkLENBQUMsQ0FBQywwQkFBRCxDQUFELENBQThCZ0IsS0FBOUIsQ0FBb0MsWUFBWTtBQUM1QyxNQUFJQyxXQUFXLEdBQUdqQixDQUFDLENBQUMsMEJBQUQsQ0FBRCxDQUE4QkMsR0FBOUIsR0FBb0NNLE1BQXREO0FBQ0FQLEdBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJlLElBQWpCLENBQXNCRSxXQUFXLEdBQUcsa0JBQXBDOztBQUNBLE1BQUlBLFdBQVcsSUFBSSxDQUFuQixFQUFzQjtBQUNsQmpCLEtBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJlLElBQWpCLENBQXNCRSxXQUFXLEdBQUcsaUJBQXBDO0FBQ0g7O0FBQ0QsTUFBSUEsV0FBVyxHQUFHLEdBQWxCLEVBQXVCO0FBQ25CQyxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0NDLEtBQXRDLENBQTRDUixLQUE1QyxHQUFvRCxLQUFwRDtBQUNILEdBRkQsTUFFTztBQUNITSxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0NDLEtBQXRDLENBQTRDUixLQUE1QyxHQUFvRCxPQUFwRDtBQUNIO0FBQ0osQ0FYRCxFLENBWUE7O0FBQ0FaLENBQUMsQ0FBQywwQkFBRCxDQUFELENBQThCcUIsRUFBOUIsQ0FBaUMsT0FBakMsRUFBMEMsWUFBWTtBQUNsRHJCLEdBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUUMsR0FBUixDQUFZRCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFDLEdBQVIsR0FBY3FCLE9BQWQsQ0FBc0IsWUFBdEIsRUFBb0MsRUFBcEMsQ0FBWjtBQUNILENBRkQsRTs7Ozs7Ozs7Ozs7O0FDaEZBLGVBQWUsbUJBQU8sQ0FBQyw2RUFBd0I7O0FBRS9DO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOzs7Ozs7Ozs7Ozs7O0FDdkNhO0FBQ2Isb0NBQW9DLG1CQUFPLENBQUMsK0hBQWlEO0FBQzdGLGVBQWUsbUJBQU8sQ0FBQyw2RUFBd0I7QUFDL0MsZUFBZSxtQkFBTyxDQUFDLDZFQUF3QjtBQUMvQyxnQkFBZ0IsbUJBQU8sQ0FBQywrRUFBeUI7QUFDakQsNkJBQTZCLG1CQUFPLENBQUMsMkdBQXVDO0FBQzVFLHlCQUF5QixtQkFBTyxDQUFDLG1HQUFtQztBQUNwRSxzQkFBc0IsbUJBQU8sQ0FBQywyRkFBK0I7QUFDN0QsaUJBQWlCLG1CQUFPLENBQUMsbUdBQW1DOztBQUU1RDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxxQkFBcUIsb0JBQW9CO0FBQ3pDOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsbUJBQW1CO0FBQzFDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDIiwiZmlsZSI6ImNvdGF0aW9uLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy9jb252ZXJzaW9uIG1pbnV0ZXMgZW4gaGV1cmVzXHJcbmV4cG9ydCBjb25zdCBjb252ZXJ0TWluc1RvSHJzTWlucyA9IChtaW51dGVzKSA9PiB7XHJcbiAgICB2YXIgaCA9IE1hdGguZmxvb3IobWludXRlcyAvIDYwKTtcclxuICAgIHZhciBtID0gbWludXRlcyAlIDYwO1xyXG4gICAgaCA9IGggPCAxMCA/ICcwJyArIGggOiBoO1xyXG4gICAgbSA9IG0gPCAxMCA/ICcwJyArIG0gOiBtO1xyXG4gICAgcmV0dXJuIGggKyAnOicgKyBtO1xyXG59XHJcblxyXG4vL3JlbXBsaXNzYWdlIGlucHV0c1xyXG5leHBvcnQgZnVuY3Rpb24gYWRkVmFsdWVUb0lucHV0KGlucHV0VmFsLCBpZCl7XHJcbiAgICAvL2RvY3VtZW50LmdldEVsZW1lbnRCeUlkKGlkKS52YWx1ZSA9IGlucHV0VmFsO1xyXG4gICAgJChpZCkudmFsKGlucHV0VmFsKTtcclxufVxyXG5cclxuLy9yZWN1cCBjb29yZG9ubmVlcyBwb2ludHMgZGVwYXJ0c1xyXG4vKmNvbnN0IGdldENvb3Jkc0RlcGFydCA9ICh4bWwpID0+IHtcclxuICAgIHBhcnNlciA9IG5ldyBET01QYXJzZXIoKTtcclxuICAgIHhtbERvYyA9IHBhcnNlci5wYXJzZUZyb21TdHJpbmcoeG1sLCBcInRleHQveG1sXCIpO1xyXG4gICAgdmFyIGxhdCA9IHhtbERvYy5nZXRFbGVtZW50c0J5VGFnTmFtZShcInRya3B0XCIpWzBdLmdldEF0dHJpYnV0ZSgnbGF0Jyk7XHJcbiAgICB2YXIgbG9uID0geG1sRG9jLmdldEVsZW1lbnRzQnlUYWdOYW1lKFwidHJrcHRcIilbMF0uZ2V0QXR0cmlidXRlKCdsb24nKTtcclxuICAgIHJldHVybiBbbG9uLCBsYXRdO1xyXG59Ki9cclxuXHJcbi8vY2FsY3VsIGRlIGxhIGRpZmZpY3VsdGUgYXZlYyByYWRpb3MgYnV0dG9uc1xyXG4kKFwiOnJhZGlvXCIpLmNoYW5nZShmdW5jdGlvbiAoKSB7XHJcbiAgICBsZXQgbmFtZXMgPSB7fTtcclxuICAgICQoJzpyYWRpbycpLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIG5hbWVzWyQodGhpcykuYXR0cignbmFtZScpXSA9IHRydWU7XHJcbiAgICB9KTtcclxuICAgIGxldCBjb3VudCA9IDA7XHJcbiAgICAkLmVhY2gobmFtZXMsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBjb3VudCsrO1xyXG4gICAgfSk7XHJcbiAgICBpZiAoJCgnOnJhZGlvOmNoZWNrZWQnKS5sZW5ndGggPT09IGNvdW50KSB7XHJcbiAgICAgICAgbGV0IHRvdGFsID0gMDtcclxuICAgICAgICAkKFwiaW5wdXRbdHlwZT1yYWRpb106Y2hlY2tlZFwiKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdG90YWwgKz0gcGFyc2VGbG9hdCgkKHRoaXMpLnZhbCgpKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICBvdXRwdXREaWZmKHRvdGFsKTtcclxuICAgIH1cclxufSk7XHJcblxyXG5cclxuLy9pbnB1dCBkaWZmaWN1bHRlXHJcbmZ1bmN0aW9uIG91dHB1dERpZmYodG90YWwpe1xyXG4gICAgdmFyIGRpZmYsIGNvbG9yO1xyXG4gICAgaWYgKHRvdGFsID49IDQgJiYgdG90YWwgPD0gNSkge1xyXG4gICAgICAgIGRpZmYgPSB0b3RhbCArIFwiIC0gVHLDqHMgZmFjaWxlXCI7XHJcbiAgICAgICAgY29sb3IgPSBcImdyZWVuXCI7XHJcbiAgICB9IGVsc2UgaWYgKHRvdGFsID49IDYgJiYgdG90YWwgPD0gOCkge1xyXG4gICAgICAgIGRpZmYgPSB0b3RhbCArIFwiIC0gRmFjaWxlXCI7XHJcbiAgICAgICAgY29sb3IgPSBcImJsdWVcIjtcclxuICAgIH0gZWxzZSBpZiAodG90YWwgPj0gOSAmJiB0b3RhbCA8PSAxMikge1xyXG4gICAgICAgIGRpZmYgPSB0b3RhbCArIFwiIC0gRGlmZmljaWxlXCI7XHJcbiAgICAgICAgY29sb3IgPSBcInJlZFwiO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBkaWZmID0gdG90YWwgKyBcIiAtIFRyw6hzIERpZmZpY2lsZVwiO1xyXG4gICAgICAgIGNvbG9yID0gXCJibGFja1wiO1xyXG4gICAgfVxyXG4gICAgJChcIiNjb3RhdGlvbl9mb3JtX2RpZmZpY3VsdGVcIikudmFsKGRpZmYpO1xyXG4gICAgJChcIiNjb3RhdGlvbl9mb3JtX2RpZmZpY3VsdGVcIikuY3NzKHtcImNvbG9yXCI6IGNvbG9yLCBcImZvbnQtc2l6ZVwiOiBcIjI1cHhcIn0pO1xyXG59XHJcblxyXG4vL2NvbXB0ZSBsZSBub21icmUgZGUgY2FyYWN0ZXJlcyBkYW5zIGxlIHRleHRhcmVhXHJcbnZhciB0ZXh0SW5pdCA9IDA7XHJcbiQoJyNjb3VudF90ZXh0JykuaHRtbCh0ZXh0SW5pdCArICcgY2FyYWN0w6hyZSAvNTAwJyk7XHJcbiQoJyNjb3RhdGlvbl9mb3JtX3RleHREZXNjcicpLmtleXVwKGZ1bmN0aW9uICgpIHtcclxuICAgIGxldCB0ZXh0X2xlbmd0aCA9ICQoJyNjb3RhdGlvbl9mb3JtX3RleHREZXNjcicpLnZhbCgpLmxlbmd0aDtcclxuICAgICQoJyNjb3VudF90ZXh0JykuaHRtbCh0ZXh0X2xlbmd0aCArICcgY2FyYWN0w6hyZXMgLzUwMCcpO1xyXG4gICAgaWYgKHRleHRfbGVuZ3RoIDw9IDEpIHtcclxuICAgICAgICAkKCcjY291bnRfdGV4dCcpLmh0bWwodGV4dF9sZW5ndGggKyAnIGNhcmFjdMOocmUgLzUwMCcpO1xyXG4gICAgfVxyXG4gICAgaWYgKHRleHRfbGVuZ3RoID4gNTAwKSB7XHJcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjb3VudF90ZXh0XCIpLnN0eWxlLmNvbG9yID0gJ3JlZCc7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY291bnRfdGV4dFwiKS5zdHlsZS5jb2xvciA9ICdibGFjayc7XHJcbiAgICB9XHJcbn0pO1xyXG4vL2VtcGVjaGVyIGxlcyByZXRvdXJzIGNoYXJyaW90cyBkdSB0ZXh0YXJlYVxyXG4kKCcjY290YXRpb25fZm9ybV90ZXh0RGVzY3InKS5vbigna2V5dXAnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAkKHRoaXMpLnZhbCgkKHRoaXMpLnZhbCgpLnJlcGxhY2UoL1tcXHJcXG5cXHZdKy9nLCAnJykpO1xyXG59KTtcclxuIiwidmFyIHRvT2JqZWN0ID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL3RvLW9iamVjdCcpO1xuXG52YXIgZmxvb3IgPSBNYXRoLmZsb29yO1xudmFyIHJlcGxhY2UgPSAnJy5yZXBsYWNlO1xudmFyIFNVQlNUSVRVVElPTl9TWU1CT0xTID0gL1xcJChbJCYnYF18XFxkXFxkP3w8W14+XSo+KS9nO1xudmFyIFNVQlNUSVRVVElPTl9TWU1CT0xTX05PX05BTUVEID0gL1xcJChbJCYnYF18XFxkXFxkPykvZztcblxuLy8gaHR0cHM6Ly90YzM5LmVzL2VjbWEyNjIvI3NlYy1nZXRzdWJzdGl0dXRpb25cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKG1hdGNoZWQsIHN0ciwgcG9zaXRpb24sIGNhcHR1cmVzLCBuYW1lZENhcHR1cmVzLCByZXBsYWNlbWVudCkge1xuICB2YXIgdGFpbFBvcyA9IHBvc2l0aW9uICsgbWF0Y2hlZC5sZW5ndGg7XG4gIHZhciBtID0gY2FwdHVyZXMubGVuZ3RoO1xuICB2YXIgc3ltYm9scyA9IFNVQlNUSVRVVElPTl9TWU1CT0xTX05PX05BTUVEO1xuICBpZiAobmFtZWRDYXB0dXJlcyAhPT0gdW5kZWZpbmVkKSB7XG4gICAgbmFtZWRDYXB0dXJlcyA9IHRvT2JqZWN0KG5hbWVkQ2FwdHVyZXMpO1xuICAgIHN5bWJvbHMgPSBTVUJTVElUVVRJT05fU1lNQk9MUztcbiAgfVxuICByZXR1cm4gcmVwbGFjZS5jYWxsKHJlcGxhY2VtZW50LCBzeW1ib2xzLCBmdW5jdGlvbiAobWF0Y2gsIGNoKSB7XG4gICAgdmFyIGNhcHR1cmU7XG4gICAgc3dpdGNoIChjaC5jaGFyQXQoMCkpIHtcbiAgICAgIGNhc2UgJyQnOiByZXR1cm4gJyQnO1xuICAgICAgY2FzZSAnJic6IHJldHVybiBtYXRjaGVkO1xuICAgICAgY2FzZSAnYCc6IHJldHVybiBzdHIuc2xpY2UoMCwgcG9zaXRpb24pO1xuICAgICAgY2FzZSBcIidcIjogcmV0dXJuIHN0ci5zbGljZSh0YWlsUG9zKTtcbiAgICAgIGNhc2UgJzwnOlxuICAgICAgICBjYXB0dXJlID0gbmFtZWRDYXB0dXJlc1tjaC5zbGljZSgxLCAtMSldO1xuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6IC8vIFxcZFxcZD9cbiAgICAgICAgdmFyIG4gPSArY2g7XG4gICAgICAgIGlmIChuID09PSAwKSByZXR1cm4gbWF0Y2g7XG4gICAgICAgIGlmIChuID4gbSkge1xuICAgICAgICAgIHZhciBmID0gZmxvb3IobiAvIDEwKTtcbiAgICAgICAgICBpZiAoZiA9PT0gMCkgcmV0dXJuIG1hdGNoO1xuICAgICAgICAgIGlmIChmIDw9IG0pIHJldHVybiBjYXB0dXJlc1tmIC0gMV0gPT09IHVuZGVmaW5lZCA/IGNoLmNoYXJBdCgxKSA6IGNhcHR1cmVzW2YgLSAxXSArIGNoLmNoYXJBdCgxKTtcbiAgICAgICAgICByZXR1cm4gbWF0Y2g7XG4gICAgICAgIH1cbiAgICAgICAgY2FwdHVyZSA9IGNhcHR1cmVzW24gLSAxXTtcbiAgICB9XG4gICAgcmV0dXJuIGNhcHR1cmUgPT09IHVuZGVmaW5lZCA/ICcnIDogY2FwdHVyZTtcbiAgfSk7XG59O1xuIiwiJ3VzZSBzdHJpY3QnO1xudmFyIGZpeFJlZ0V4cFdlbGxLbm93blN5bWJvbExvZ2ljID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2ZpeC1yZWdleHAtd2VsbC1rbm93bi1zeW1ib2wtbG9naWMnKTtcbnZhciBhbk9iamVjdCA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9hbi1vYmplY3QnKTtcbnZhciB0b0xlbmd0aCA9IHJlcXVpcmUoJy4uL2ludGVybmFscy90by1sZW5ndGgnKTtcbnZhciB0b0ludGVnZXIgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvdG8taW50ZWdlcicpO1xudmFyIHJlcXVpcmVPYmplY3RDb2VyY2libGUgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvcmVxdWlyZS1vYmplY3QtY29lcmNpYmxlJyk7XG52YXIgYWR2YW5jZVN0cmluZ0luZGV4ID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2FkdmFuY2Utc3RyaW5nLWluZGV4Jyk7XG52YXIgZ2V0U3Vic3RpdHV0aW9uID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2dldC1zdWJzdGl0dXRpb24nKTtcbnZhciByZWdFeHBFeGVjID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL3JlZ2V4cC1leGVjLWFic3RyYWN0Jyk7XG5cbnZhciBtYXggPSBNYXRoLm1heDtcbnZhciBtaW4gPSBNYXRoLm1pbjtcblxudmFyIG1heWJlVG9TdHJpbmcgPSBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIGl0ID09PSB1bmRlZmluZWQgPyBpdCA6IFN0cmluZyhpdCk7XG59O1xuXG4vLyBAQHJlcGxhY2UgbG9naWNcbmZpeFJlZ0V4cFdlbGxLbm93blN5bWJvbExvZ2ljKCdyZXBsYWNlJywgMiwgZnVuY3Rpb24gKFJFUExBQ0UsIG5hdGl2ZVJlcGxhY2UsIG1heWJlQ2FsbE5hdGl2ZSwgcmVhc29uKSB7XG4gIHZhciBSRUdFWFBfUkVQTEFDRV9TVUJTVElUVVRFU19VTkRFRklORURfQ0FQVFVSRSA9IHJlYXNvbi5SRUdFWFBfUkVQTEFDRV9TVUJTVElUVVRFU19VTkRFRklORURfQ0FQVFVSRTtcbiAgdmFyIFJFUExBQ0VfS0VFUFNfJDAgPSByZWFzb24uUkVQTEFDRV9LRUVQU18kMDtcbiAgdmFyIFVOU0FGRV9TVUJTVElUVVRFID0gUkVHRVhQX1JFUExBQ0VfU1VCU1RJVFVURVNfVU5ERUZJTkVEX0NBUFRVUkUgPyAnJCcgOiAnJDAnO1xuXG4gIHJldHVybiBbXG4gICAgLy8gYFN0cmluZy5wcm90b3R5cGUucmVwbGFjZWAgbWV0aG9kXG4gICAgLy8gaHR0cHM6Ly90YzM5LmVzL2VjbWEyNjIvI3NlYy1zdHJpbmcucHJvdG90eXBlLnJlcGxhY2VcbiAgICBmdW5jdGlvbiByZXBsYWNlKHNlYXJjaFZhbHVlLCByZXBsYWNlVmFsdWUpIHtcbiAgICAgIHZhciBPID0gcmVxdWlyZU9iamVjdENvZXJjaWJsZSh0aGlzKTtcbiAgICAgIHZhciByZXBsYWNlciA9IHNlYXJjaFZhbHVlID09IHVuZGVmaW5lZCA/IHVuZGVmaW5lZCA6IHNlYXJjaFZhbHVlW1JFUExBQ0VdO1xuICAgICAgcmV0dXJuIHJlcGxhY2VyICE9PSB1bmRlZmluZWRcbiAgICAgICAgPyByZXBsYWNlci5jYWxsKHNlYXJjaFZhbHVlLCBPLCByZXBsYWNlVmFsdWUpXG4gICAgICAgIDogbmF0aXZlUmVwbGFjZS5jYWxsKFN0cmluZyhPKSwgc2VhcmNoVmFsdWUsIHJlcGxhY2VWYWx1ZSk7XG4gICAgfSxcbiAgICAvLyBgUmVnRXhwLnByb3RvdHlwZVtAQHJlcGxhY2VdYCBtZXRob2RcbiAgICAvLyBodHRwczovL3RjMzkuZXMvZWNtYTI2Mi8jc2VjLXJlZ2V4cC5wcm90b3R5cGUtQEByZXBsYWNlXG4gICAgZnVuY3Rpb24gKHJlZ2V4cCwgcmVwbGFjZVZhbHVlKSB7XG4gICAgICBpZiAoXG4gICAgICAgICghUkVHRVhQX1JFUExBQ0VfU1VCU1RJVFVURVNfVU5ERUZJTkVEX0NBUFRVUkUgJiYgUkVQTEFDRV9LRUVQU18kMCkgfHxcbiAgICAgICAgKHR5cGVvZiByZXBsYWNlVmFsdWUgPT09ICdzdHJpbmcnICYmIHJlcGxhY2VWYWx1ZS5pbmRleE9mKFVOU0FGRV9TVUJTVElUVVRFKSA9PT0gLTEpXG4gICAgICApIHtcbiAgICAgICAgdmFyIHJlcyA9IG1heWJlQ2FsbE5hdGl2ZShuYXRpdmVSZXBsYWNlLCByZWdleHAsIHRoaXMsIHJlcGxhY2VWYWx1ZSk7XG4gICAgICAgIGlmIChyZXMuZG9uZSkgcmV0dXJuIHJlcy52YWx1ZTtcbiAgICAgIH1cblxuICAgICAgdmFyIHJ4ID0gYW5PYmplY3QocmVnZXhwKTtcbiAgICAgIHZhciBTID0gU3RyaW5nKHRoaXMpO1xuXG4gICAgICB2YXIgZnVuY3Rpb25hbFJlcGxhY2UgPSB0eXBlb2YgcmVwbGFjZVZhbHVlID09PSAnZnVuY3Rpb24nO1xuICAgICAgaWYgKCFmdW5jdGlvbmFsUmVwbGFjZSkgcmVwbGFjZVZhbHVlID0gU3RyaW5nKHJlcGxhY2VWYWx1ZSk7XG5cbiAgICAgIHZhciBnbG9iYWwgPSByeC5nbG9iYWw7XG4gICAgICBpZiAoZ2xvYmFsKSB7XG4gICAgICAgIHZhciBmdWxsVW5pY29kZSA9IHJ4LnVuaWNvZGU7XG4gICAgICAgIHJ4Lmxhc3RJbmRleCA9IDA7XG4gICAgICB9XG4gICAgICB2YXIgcmVzdWx0cyA9IFtdO1xuICAgICAgd2hpbGUgKHRydWUpIHtcbiAgICAgICAgdmFyIHJlc3VsdCA9IHJlZ0V4cEV4ZWMocngsIFMpO1xuICAgICAgICBpZiAocmVzdWx0ID09PSBudWxsKSBicmVhaztcblxuICAgICAgICByZXN1bHRzLnB1c2gocmVzdWx0KTtcbiAgICAgICAgaWYgKCFnbG9iYWwpIGJyZWFrO1xuXG4gICAgICAgIHZhciBtYXRjaFN0ciA9IFN0cmluZyhyZXN1bHRbMF0pO1xuICAgICAgICBpZiAobWF0Y2hTdHIgPT09ICcnKSByeC5sYXN0SW5kZXggPSBhZHZhbmNlU3RyaW5nSW5kZXgoUywgdG9MZW5ndGgocngubGFzdEluZGV4KSwgZnVsbFVuaWNvZGUpO1xuICAgICAgfVxuXG4gICAgICB2YXIgYWNjdW11bGF0ZWRSZXN1bHQgPSAnJztcbiAgICAgIHZhciBuZXh0U291cmNlUG9zaXRpb24gPSAwO1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCByZXN1bHRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHJlc3VsdCA9IHJlc3VsdHNbaV07XG5cbiAgICAgICAgdmFyIG1hdGNoZWQgPSBTdHJpbmcocmVzdWx0WzBdKTtcbiAgICAgICAgdmFyIHBvc2l0aW9uID0gbWF4KG1pbih0b0ludGVnZXIocmVzdWx0LmluZGV4KSwgUy5sZW5ndGgpLCAwKTtcbiAgICAgICAgdmFyIGNhcHR1cmVzID0gW107XG4gICAgICAgIC8vIE5PVEU6IFRoaXMgaXMgZXF1aXZhbGVudCB0b1xuICAgICAgICAvLyAgIGNhcHR1cmVzID0gcmVzdWx0LnNsaWNlKDEpLm1hcChtYXliZVRvU3RyaW5nKVxuICAgICAgICAvLyBidXQgZm9yIHNvbWUgcmVhc29uIGBuYXRpdmVTbGljZS5jYWxsKHJlc3VsdCwgMSwgcmVzdWx0Lmxlbmd0aClgIChjYWxsZWQgaW5cbiAgICAgICAgLy8gdGhlIHNsaWNlIHBvbHlmaWxsIHdoZW4gc2xpY2luZyBuYXRpdmUgYXJyYXlzKSBcImRvZXNuJ3Qgd29ya1wiIGluIHNhZmFyaSA5IGFuZFxuICAgICAgICAvLyBjYXVzZXMgYSBjcmFzaCAoaHR0cHM6Ly9wYXN0ZWJpbi5jb20vTjIxUXplUUEpIHdoZW4gdHJ5aW5nIHRvIGRlYnVnIGl0LlxuICAgICAgICBmb3IgKHZhciBqID0gMTsgaiA8IHJlc3VsdC5sZW5ndGg7IGorKykgY2FwdHVyZXMucHVzaChtYXliZVRvU3RyaW5nKHJlc3VsdFtqXSkpO1xuICAgICAgICB2YXIgbmFtZWRDYXB0dXJlcyA9IHJlc3VsdC5ncm91cHM7XG4gICAgICAgIGlmIChmdW5jdGlvbmFsUmVwbGFjZSkge1xuICAgICAgICAgIHZhciByZXBsYWNlckFyZ3MgPSBbbWF0Y2hlZF0uY29uY2F0KGNhcHR1cmVzLCBwb3NpdGlvbiwgUyk7XG4gICAgICAgICAgaWYgKG5hbWVkQ2FwdHVyZXMgIT09IHVuZGVmaW5lZCkgcmVwbGFjZXJBcmdzLnB1c2gobmFtZWRDYXB0dXJlcyk7XG4gICAgICAgICAgdmFyIHJlcGxhY2VtZW50ID0gU3RyaW5nKHJlcGxhY2VWYWx1ZS5hcHBseSh1bmRlZmluZWQsIHJlcGxhY2VyQXJncykpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJlcGxhY2VtZW50ID0gZ2V0U3Vic3RpdHV0aW9uKG1hdGNoZWQsIFMsIHBvc2l0aW9uLCBjYXB0dXJlcywgbmFtZWRDYXB0dXJlcywgcmVwbGFjZVZhbHVlKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAocG9zaXRpb24gPj0gbmV4dFNvdXJjZVBvc2l0aW9uKSB7XG4gICAgICAgICAgYWNjdW11bGF0ZWRSZXN1bHQgKz0gUy5zbGljZShuZXh0U291cmNlUG9zaXRpb24sIHBvc2l0aW9uKSArIHJlcGxhY2VtZW50O1xuICAgICAgICAgIG5leHRTb3VyY2VQb3NpdGlvbiA9IHBvc2l0aW9uICsgbWF0Y2hlZC5sZW5ndGg7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiBhY2N1bXVsYXRlZFJlc3VsdCArIFMuc2xpY2UobmV4dFNvdXJjZVBvc2l0aW9uKTtcbiAgICB9XG4gIF07XG59KTtcbiJdLCJzb3VyY2VSb290IjoiIn0=