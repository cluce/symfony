(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["calendrier"],{

/***/ "./assets/js/pages/calendrier.js":
/*!***************************************!*\
  !*** ./assets/js/pages/calendrier.js ***!
  \***************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.concat.js */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.filter.js */ "./node_modules/core-js/modules/es.array.filter.js");
/* harmony import */ var core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_array_includes_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.array.includes.js */ "./node_modules/core-js/modules/es.array.includes.js");
/* harmony import */ var core_js_modules_es_array_includes_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_includes_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_index_of_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.index-of.js */ "./node_modules/core-js/modules/es.array.index-of.js");
/* harmony import */ var core_js_modules_es_array_index_of_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_index_of_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_join_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.join.js */ "./node_modules/core-js/modules/es.array.join.js");
/* harmony import */ var core_js_modules_es_array_join_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_join_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_array_splice_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.array.splice.js */ "./node_modules/core-js/modules/es.array.splice.js");
/* harmony import */ var core_js_modules_es_array_splice_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_splice_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.date.to-string.js */ "./node_modules/core-js/modules/es.date.to-string.js");
/* harmony import */ var core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_object_define_property_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.object.define-property.js */ "./node_modules/core-js/modules/es.object.define-property.js");
/* harmony import */ var core_js_modules_es_object_define_property_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_define_property_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.regexp.exec.js */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_string_includes_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.string.includes.js */ "./node_modules/core-js/modules/es.string.includes.js");
/* harmony import */ var core_js_modules_es_string_includes_js__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_includes_js__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.string.split.js */ "./node_modules/core-js/modules/es.string.split.js");
/* harmony import */ var core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../services/mapConfig */ "./assets/js/services/mapConfig.js");
/* harmony import */ var _components_popup__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../components/popup */ "./assets/js/components/popup.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _components_InfoFeature__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../components/InfoFeature */ "./assets/js/components/InfoFeature.jsx");
/* harmony import */ var _yaireo_tagify__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @yaireo/tagify */ "./node_modules/@yaireo/tagify/dist/tagify.min.js");
/* harmony import */ var _yaireo_tagify__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_yaireo_tagify__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var flatpickr__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! flatpickr */ "./node_modules/flatpickr/dist/esm/index.js");
/* harmony import */ var flatpickr_dist_l10n_fr_js__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! flatpickr/dist/l10n/fr.js */ "./node_modules/flatpickr/dist/l10n/fr.js");
/* harmony import */ var flatpickr_dist_l10n_fr_js__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(flatpickr_dist_l10n_fr_js__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var nouislider__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! nouislider */ "./node_modules/nouislider/distribute/nouislider.js");
/* harmony import */ var nouislider__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(nouislider__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var wnumb__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! wnumb */ "./node_modules/wnumb/wNumb.js");
/* harmony import */ var wnumb__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(wnumb__WEBPACK_IMPORTED_MODULE_21__);













function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }












(function mapCalendrier() {
  //map
  var map = L.map('map', {
    center: [47, 0],
    zoom: 5,
    minZoom: 3,
    maxZoom: 16,
    zoomControl: true,
    layers: [L.esri.basemapLayer('Topographic')]
  });
  L.control.scale().addTo(map); //barre echelle

  var defaultLayer = L.esri.basemapLayer("Topographic").addTo(map);
  map.attributionControl.addAttribution('<a target="_blank" href="https://ffvelo.fr">Fédération française de cyclotourisme</a>'); //variables globales

  var mapElement = document.getElementById("map");
  var sidebarElement = document.getElementById("sidebar");
  var mapWidth = document.getElementById("map").offsetWidth; //bouton toggle sidebar

  Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["sidebarToggleControl"])(L.Control, map, sidebarElement, mapElement, mapWidth); // controle des couches et changement de fond de carte

  var baseLayers = {
    'Topographique (Esri)': defaultLayer,
    'Satellite (Esri)': L.esri.basemapLayer('Imagery'),
    'OpenStreetMap': L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    })
  };
  var overlayMaps = {
    "Cols": Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["colsLayer"])()
  };
  L.control.layers(baseLayers, overlayMaps, {
    position: "bottomleft"
  }).addTo(map);
  Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["layerControlTitles"])();
  map.addControl(new _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["geolocationButton"](map)); //layer manifestations

  var manifsLayer = L.markerClusterGroup({
    chunkedLoading: true,
    iconCreateFunction: function iconCreateFunction(cluster) {
      var count = cluster.getChildCount();
      var digits = (count + '').length;
      return L.divIcon({
        html: count,
        className: 'cluster digits-' + digits,
        iconSize: null
      });
    }
  });
  var manifs = L.esri.Cluster.featureLayer({
    url: _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["agolFeatureServices"].manifs,
    token: token,
    pointToLayer: function pointToLayer(feature, latlng) {
      return L.marker(latlng, {
        icon: _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["iconOin"][1]
      });
    },
    iconCreateFunction: function iconCreateFunction(cluster) {
      var count = cluster.getChildCount();
      var digits = (count + '').length;
      return L.divIcon({
        html: count,
        className: 'cluster digits-' + digits,
        iconSize: null
      });
    }
  }).addTo(manifsLayer).addTo(map); //popups manifs

  manifs.bindPopup(function (layer) {
    var nom = layer.feature.properties.Nom;
    var id = layer.feature.properties.ManifId;
    var type = layer.feature.properties.Type;
    var date_debut = layer.feature.properties.Accueil_DateDbt;
    var proute = layer.feature.properties.Pratique_Route ? "Route" : false;
    var pvtt = layer.feature.properties.Pratique_VTT ? "VTT" : false;
    var pgravel = layer.feature.properties.Pratique_Gravel ? "Gravel" : false;
    var pmarche = layer.feature.properties.Pratique_Marche ? "Marche" : false;
    var durable = layer.feature.properties.DevDurable ? "Éco-responsable" : false;
    var psh = layer.feature.properties.AccesPSH ? "Accueil PSH" : false;
    var spefem = layer.feature.properties.SpeFem ? "Spécifique femmes" : false;
    var email = layer.feature.properties.AdresseMail;
    var siteweb = layer.feature.properties.AdresseWeb;
    return L.Util.template(Object(_components_popup__WEBPACK_IMPORTED_MODULE_13__["popupTemplateManifs"])(nom, id, type, date_debut, proute, pvtt, pgravel, pmarche, durable, psh, spefem, email, siteweb));
  });
  /*evenements carte et layer point depart*/

  manifs.once('load', function () {
    if (location.pathname != menuUrl) {
      var parts = location.href.split(menuUrl + "/");
      var urlid = parts[1].split("-");
      renderInfoFeature(manifs, urlid[0]);
    } else {
      Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["zoomToMarkersAfterLoad"])(map, manifs);
    }

    ; //syncSidebar();
  });
  manifs.on('popupopen', function () {
    document.getElementById("buttonPopup").addEventListener('click', function () {
      renderInfoFeature(manifs, document.getElementById("buttonPopup").getAttribute('value'));
    }, true);
  });
  manifs.on('click', function (e) {
    console.log(e);
  });
  map.on("moveend", function () {
    manifs.setWhere(manifs.getWhere());
    syncSidebar();
  });
  map.on('overlayremove', function () {
    manifsLayer.clearLayers();
  });

  window.onpopstate = function () {
    Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["backButtonNavigation"])(menuUrl, map, mapElement, mapWidth, manifs, renderInfoFeature);
  };
  /*fin evenements carte et layer point depart*/
  //barre de recherche geocodeur


  var searchControl = L.esri.Geocoding.geosearch({
    useMapBounds: false,
    title: 'Rechercher un lieu / une adresse',
    placeholder: 'Où ?',
    expanded: true,
    collapseAfterResult: false,
    position: 'topright',
    providers: [_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["arcgisOnlineProvider"], L.esri.Geocoding.featureLayerProvider({
      //recherche des couches dans la gdb oin
      url: _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["agolFeatureServices"].manifs,
      where: manifs.getWhere(),
      token: token,
      searchFields: ['ManifId', 'Nom', 'Structure'],
      label: 'MANIFESTATIONS FFVELO:',
      maxResults: 7,
      bufferRadius: 1000,
      formatSuggestion: function formatSuggestion(feature) {
        return "".concat(feature.properties.Nom, " - ").concat(feature.properties.Type);
      }
    })]
  }).addTo(map); //geocodeur en dehors de la carte

  var esriGeocoder = searchControl.getContainer();
  Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["setEsriGeocoder"])(esriGeocoder, document.getElementById('geocoder')); //recup la requete en cours et applique aux resultats du geocodeur

  searchControl.on("requestend", function () {
    searchControl.options.providers[1].options.where = manifs.getWhere();
  }); //format de la date des manifs

  var handleDate = function handleDate(str) {
    var date = str.split('-');
    return date[2] + "/" + date[1] + "/" + date[0];
  };

  var syncSidebar = function syncSidebar() {
    var arrResultsSidebar = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    $("#features").empty();
    manifs.eachActiveFeature(function (layer) {
      //eachActiveFeature = fonction a ajouter dans esri-leaflet-cluster.js - attention si maj de la librairie
      if (map.hasLayer(manifs)) {
        if (map.getBounds().contains(layer.getLatLng())) {
          arrResultsSidebar.push(sidebarTemplateManifs(layer.feature.properties.ManifId, handleDate(layer.feature.properties.Accueil_DateDbt), layer.feature.properties.Nom, layer.feature.properties.Type, layer.feature.properties.Pratique_Route, layer.feature.properties.Pratique_VTT, layer.feature.properties.Pratique_Gravel, layer.feature.properties.Pratique_Marche, layer.feature.properties.Accueil_CP, layer.feature.properties.Accueil_Commune, layer.feature.properties.DevDurable, layer.feature.properties.AccesPSH, layer.feature.properties.SpeFem));
        }
      }
    });
    Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["addResultsToSidebar"])(arrResultsSidebar, manifs, renderInfoFeature);
  }; // sidebar card html


  var sidebarTemplateManifs = function sidebarTemplateManifs(id, date, nom, type, route, vtt, gravel, marche, cp, ville, durable, psh, spefem) {
    return "<div class=\"card feature-card my-2 p-2\" id=\"".concat(id, "\" data-lat=\"\" data-lon=\"\"> \n                <div class=\"col align-self-center rounded-right\">\n                    <h5 class=\"d-block text-uppercase\"><i class=\"far fa-calendar-check\"></i> ").concat(date, "</h5>\n                    <h6 class=\"d-block text-uppercase\">").concat(nom, "</h6>\n                    ").concat(route ? "<span class=\"badge badge-pill badge-light my-1 mr-1\">Route</span>" : "", "\n                    ").concat(vtt ? "<span class=\"badge badge-pill badge-light my-1 mr-1\">VTT</span>" : "", "\n                    ").concat(gravel ? "<span class=\"badge badge-pill badge-light my-1 mr-1\">Gravel</span>" : "", "\n                    ").concat(marche ? "<span class=\"badge badge-pill badge-light my-1 mr-1\">Marche</span>" : "", "\n                    ").concat(durable ? "<span class=\"badge badge-pill badge-light my-1 mr-1\">\xC9co-responsable</span>" : "", "\n                    ").concat(psh ? "<span class=\"badge badge-pill badge-light my-1 mr-1\">Acc\xE8s PSH</span>" : "", "\n                    ").concat(spefem ? "<span class=\"badge badge-pill badge-light my-1 mr-1\">Sp\xE9cifique femmes</span>" : "", "\n                    <span class=\"badge badge-pill badge-light my-1 mr-1\">").concat(type, "</span>\n                    <span class=\"my-1 mr-1\"><br/><i class=\"fas fa-map-marked-alt\"></i> ").concat(cp, ", ").concat(ville, "</span>\n                </div>\n            </div>");
  }; //composant fiche descriptive


  var renderInfoFeature = function renderInfoFeature(Layer, id) {
    Layer.eachFeature(function (layer) {
      if (layer.feature.properties.ManifId == id) {
        var handleGpxManifs = function handleGpxManifs() {
          var gpxFiles = layer.feature.properties.FichierGpx ? layer.feature.properties.FichierGpx.split(',') : false;
          return gpxFiles;
        };

        var infofeatureElement = document.getElementById('infofeature');
        Object(react_dom__WEBPACK_IMPORTED_MODULE_15__["unmountComponentAtNode"])(infofeatureElement);
        Object(react_dom__WEBPACK_IMPORTED_MODULE_15__["render"])( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_14___default.a.createElement(_components_InfoFeature__WEBPACK_IMPORTED_MODULE_16__["default"], {
          idManif: id,
          nom: layer.feature.properties.Nom,
          AccesPSH: layer.feature.properties.AccesPSH,
          Accueil_CP: layer.feature.properties.Accueil_CP,
          Accueil_Commune: layer.feature.properties.Accueil_Commune,
          Accueil_DateDbt: layer.feature.properties.Accueil_DateDbt,
          Accueil_Lieu: layer.feature.properties.Accueil_Lieu,
          Accueil_Adresse: layer.feature.properties.Accueil_Adresse,
          AdresseMail: layer.feature.properties.AdresseMail,
          AdresseWeb: layer.feature.properties.AdresseWeb,
          Arrivee_CP: layer.feature.properties.Arrivee_CP,
          Arrivee_Commune: layer.feature.properties.Arrivee_Commune,
          Arrivee_DateFin: layer.feature.properties.Arrivee_DateFin,
          Arrivee_Lieu: layer.feature.properties.Arrivee_Lieu,
          CategorieLabel: layer.feature.properties.CategorieLabel,
          Contact_Nom: layer.feature.properties.Contact_Nom,
          Contact_Prenom: layer.feature.properties.Contact_Prenom,
          DevDurable: layer.feature.properties.DevDurable,
          FichierFlyer: layer.feature.properties.FichierFlyer,
          FichierGpx: handleGpxManifs(),
          InscriptionLigne: layer.feature.properties.InscriptionLigne,
          ModeleId: layer.feature.properties.ModeleId,
          NumeroLabel: layer.feature.properties.NumeroLabel,
          OinManifLicTarifAdulte: layer.feature.properties.OinManifLicTarifAdulte,
          OinManifLicTarifJeune: layer.feature.properties.OinManifLicTarifJeune,
          OinManifNonLicTarifAdulte: layer.feature.properties.OinManifNonLicTarifAdulte,
          OinManifNonLicTarifJeune: layer.feature.properties.OinManifNonLicTarifJeune,
          OinManifReserveLicencie: layer.feature.properties.OinManifReserveLicencie,
          descr: layer.feature.properties.OinManifObservation,
          OinMarcheDist_1: layer.feature.properties.OinMarcheDist_1,
          OinMarcheDist_2: layer.feature.properties.OinMarcheDist_2,
          OinMarcheDist_3: layer.feature.properties.OinMarcheDist_3,
          OinMarcheDist_4: layer.feature.properties.OinMarcheDist_4,
          OinMarcheDist_5: layer.feature.properties.OinMarcheDist_5,
          OinMarcheDist_6: layer.feature.properties.OinMarcheDist_6,
          OinRouteDeniv_1: layer.feature.properties.OinRouteDeniv_1,
          OinRouteDeniv_2: layer.feature.properties.OinRouteDeniv_2,
          OinRouteDeniv_3: layer.feature.properties.OinRouteDeniv_3,
          OinRouteDeniv_4: layer.feature.properties.OinRouteDeniv_4,
          OinRouteDeniv_5: layer.feature.properties.OinRouteDeniv_5,
          OinRouteDeniv_6: layer.feature.properties.OinRouteDeniv_6,
          OinRouteDiff_1: layer.feature.properties.OinRouteDiff_1,
          OinRouteDiff_2: layer.feature.properties.OinRouteDiff_2,
          OinRouteDiff_3: layer.feature.properties.OinRouteDiff_3,
          OinRouteDiff_4: layer.feature.properties.OinRouteDiff_4,
          OinRouteDiff_5: layer.feature.properties.OinRouteDiff_5,
          OinRouteDiff_6: layer.feature.properties.OinRouteDiff_6,
          OinRouteDist_1: layer.feature.properties.OinRouteDist_1,
          OinRouteDist_2: layer.feature.properties.OinRouteDist_2,
          OinRouteDist_3: layer.feature.properties.OinRouteDist_3,
          OinRouteDist_4: layer.feature.properties.OinRouteDist_4,
          OinRouteDist_5: layer.feature.properties.OinRouteDist_5,
          OinRouteDist_6: layer.feature.properties.OinRouteDist_6,
          OinVTTDeniv_1: layer.feature.properties.OinVTTDeniv_1,
          OinVTTDeniv_2: layer.feature.properties.OinVTTDeniv_2,
          OinVTTDeniv_3: layer.feature.properties.OinVTTDeniv_3,
          OinVTTDeniv_4: layer.feature.properties.OinVTTDeniv_4,
          OinVTTDeniv_5: layer.feature.properties.OinVTTDeniv_5,
          OinVTTDeniv_6: layer.feature.properties.OinVTTDeniv_6,
          OinVTTDiff_1: layer.feature.properties.OinVTTDiff_1,
          OinVTTDiff_2: layer.feature.properties.OinVTTDiff_2,
          OinVTTDiff_3: layer.feature.properties.OinVTTDiff_3,
          OinVTTDiff_4: layer.feature.properties.OinVTTDiff_4,
          OinVTTDiff_5: layer.feature.properties.OinVTTDiff_5,
          OinVTTDiff_6: layer.feature.properties.OinVTTDiff_6,
          OinVTTDist_1: layer.feature.properties.OinVTTDist_1,
          OinVTTDist_2: layer.feature.properties.OinVTTDist_2,
          OinVTTDist_3: layer.feature.properties.OinVTTDist_3,
          OinVTTDist_4: layer.feature.properties.OinVTTDist_4,
          OinVTTDist_5: layer.feature.properties.OinVTTDist_5,
          OinVTTDist_6: layer.feature.properties.OinVTTDist_6,
          OinGravelDeniv_1: layer.feature.properties.OinGravelDeniv_1,
          OinGravelDeniv_2: layer.feature.properties.OinGravelDeniv_2,
          OinGravelDeniv_3: layer.feature.properties.OinGravelDeniv_3,
          OinGravelDeniv_4: layer.feature.properties.OinGravelDeniv_4,
          OinGravelDeniv_5: layer.feature.properties.OinGravelDeniv_5,
          OinGravelDeniv_6: layer.feature.properties.OinGravelDeniv_6,
          OinGravelDiff_1: layer.feature.properties.OinGravelDiff_1,
          OinGravelDiff_2: layer.feature.properties.OinGravelDiff_2,
          OinGravelDiff_3: layer.feature.properties.OinGravelDiff_3,
          OinGravelDiff_4: layer.feature.properties.OinGravelDiff_4,
          OinGravelDiff_5: layer.feature.properties.OinGravelDiff_5,
          OinGravelDiff_6: layer.feature.properties.OinGravelDiff_6,
          OinGravelDist_1: layer.feature.properties.OinGravelDist_1,
          OinGravelDist_2: layer.feature.properties.OinGravelDist_2,
          OinGravelDist_3: layer.feature.properties.OinGravelDist_3,
          OinGravelDist_4: layer.feature.properties.OinGravelDist_4,
          OinGravelDist_5: layer.feature.properties.OinGravelDist_5,
          OinGravelDist_6: layer.feature.properties.OinGravelDist_6,
          Pratique_Gravel: layer.feature.properties.Pratique_Gravel,
          Pratique_Marche: layer.feature.properties.Pratique_Marche,
          Pratique_Route: layer.feature.properties.Pratique_Route,
          Pratique_VTT: layer.feature.properties.Pratique_VTT,
          SpeFem: layer.feature.properties.SpeFem,
          Structure: layer.feature.properties.Structure,
          StructureDep: layer.feature.properties.StructureDep,
          StructureReg: layer.feature.properties.StructureReg,
          Type: layer.feature.properties.Type,
          heureDeCloture: layer.feature.properties.heureDeCloture,
          heures: layer.feature.properties.heures,
          rootContainer: infofeatureElement,
          map: map,
          mapWidth: mapWidth,
          Layer: Layer,
          layer: layer,
          menuUrl: menuUrl
        }), infofeatureElement);
        Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["navigationHistory"])(id, menuUrl, layer.feature.properties.slug); //gestion de la carte

        var resize = mapWidth - document.getElementById("info-feature-content").offsetWidth;
        mapElement.style.width = resize + "px";
        map.invalidateSize();
        map.setView([layer.feature.geometry.coordinates[1], layer.feature.geometry.coordinates[0]], 12);
      }
    });
  };
  /*filtres de recherches */


  var dateFilter = function dateFilter() {
    var _flatpickr;

    Object(flatpickr__WEBPACK_IMPORTED_MODULE_18__["default"])("#flatpickr", (_flatpickr = {
      locale: {
        firstDayOfWeek: 1
      }
    }, _defineProperty(_flatpickr, "locale", "fr"), _defineProperty(_flatpickr, "showMonths", 2), _defineProperty(_flatpickr, "dateFormat", "d-m-Y"), _defineProperty(_flatpickr, "minDate", "today"), _defineProperty(_flatpickr, "mode", "range"), _defineProperty(_flatpickr, "altInput", true), _defineProperty(_flatpickr, "disableMobile", "true"), _defineProperty(_flatpickr, "altFormat", "d-m-Y"), _defineProperty(_flatpickr, "onChange", function onChange(selectedDates, dateStr, instance) {
      var datestart = selectedDates[0];
      var dateend = new Date(selectedDates[1]);
      console.log(datestart, dateend);
    }), _flatpickr));
  };

  dateFilter();
  var distSlider = document.getElementById('distSlider');
  nouislider__WEBPACK_IMPORTED_MODULE_20___default.a.create(distSlider, {
    start: [0, 250],
    connect: true,
    range: {
      'min': 0,
      'max': 250
    },
    format: wnumb__WEBPACK_IMPORTED_MODULE_21___default()({
      decimals: 0 // default is 2

    }),
    pips: {
      mode: 'count',
      values: 5
    }
  });
  var denivSlider = document.getElementById('denivSlider');
  nouislider__WEBPACK_IMPORTED_MODULE_20___default.a.create(denivSlider, {
    start: [0, 1000],
    connect: true,
    range: {
      'min': 0,
      'max': 1000
    },
    format: wnumb__WEBPACK_IMPORTED_MODULE_21___default()({
      decimals: 0 // default is 2

    }),
    pips: {
      mode: 'count',
      values: 3
    }
  });
  var tagifyClubs = new _yaireo_tagify__WEBPACK_IMPORTED_MODULE_17___default.a(document.querySelector("textarea[name=tagsClubs]"), {
    enforeWhitelist: true,
    whitelist: [],
    duplicates: false
  });

  var getFilters = function getFilters() {
    var filters = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var elements = document.querySelectorAll(".typeVeloFilter");

    _toConsumableArray(elements).map(function (item) {
      item.addEventListener('change', function (e) {
        if (item.checked === true) {
          filters.push(item.value);
        } else {
          filters.splice(filters.indexOf(item.value), 1);
        }

        setFilters(filters);
      });
    });

    tagifyClubs.on('add', function (e) {
      filters.push(e.detail.data.value);
      filters = filters.join("");
    }).on('remove', function (e) {
      filters.splice(filters.indexOf(e.detail.data.value), 1);
      filters = filters.join("");
    });
    distSlider.noUiSlider.on("change", function (data) {
      var distList = ["OinRouteDist_1", "OinRouteDist_2", "OinRouteDist_3", "OinRouteDist_4", "OinRouteDist_5", "OinRouteDist_6", "OinVTTDist_1", "OinVTTDist_2", "OinVTTDist_3", "OinVTTDist_4", "OinVTTDist_5", "OinVTTDist_6", "OinGravelDist_1", "OinGravelDist_2", "OinGravelDist_3", "OinGravelDist_4", "OinGravelDist_5", "OinGravelDist_6", "OinMarcheDist_1", "OinMarcheDist_2", "OinMarcheDist_3", "OinMarcheDist_4", "OinMarcheDist_5", "OinMarcheDist_6"];
      distList.map(function (dist) {
        filters.splice(filters.indexOf(dist), 1);
      });
      distList.map(function (dist) {
        if (data[0] > 0 && data[1] < 250) {
          filters.push("".concat(dist, " >= ").concat(data[0], " AND ").concat(dist, " <= ").concat(data[1]));
        } else if (data[0] > 0 && data[1] == 250) {
          filters.push("".concat(dist, " >= ").concat(data[0]));
        } else if (data[0] == 0 && data[1] < 250) {
          filters.push("".concat(dist, " > 0 AND ").concat(dist, " <= ").concat(data[1]));
        }
      });
      setFilters(filters);
    });
    denivSlider.noUiSlider.on("change", function (data) {
      var denivList = ["OinRouteDeniv_1", "OinRouteDeniv_2", "OinRouteDeniv_3", "OinRouteDeniv_4", "OinRouteDeniv_5", "OinRouteDeniv_6", "OinVTTDeniv_1", "OinVTTDeniv_2", "OinVTTDeniv_3", "OinVTTDeniv_4", "OinVTTDeniv_5", "OinVTTDeniv_6", "OinGravelDeniv_1", "OinGravelDeniv_2", "OinGravelDeniv_3", "OinGravelDeniv_4", "OinGravelDeniv_5", "OinGravelDeniv_6", "OinMarcheDeniv_1", "OinMarcheDeniv_2", "OinMarcheDeniv_3", "OinMarcheDeniv_4", "OinMarcheDeniv_5", "OinMarcheDeniv_6"]; //denivList.map((deniv)=>{filters.splice(filters.indexOf(deniv),1)});

      denivList.map(function (deniv) {
        if (data[0] > 0 && data[1] <= 1000) {
          filters.push("".concat(deniv, " >= ").concat(data[0], " AND ").concat(deniv, " <= ").concat(data[1]));
        } else if (data[0] > 0 && data[1] == 1000) {
          filters.push("".concat(deniv, " >= ").concat(data[0]));
        } else if (data[0] == 0 && data[1] < 1000) {
          filters.push("".concat(deniv, " > 0 AND ").concat(deniv, " <= ").concat(data[1]));
        }
      });
      setFilters(filters);
    });
  };

  getFilters();

  var setFilters = function setFilters(filters) {
    var arrResult = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    console.log(filters);
    var type = filters.filter(function (f) {
      return f.includes("Pratique_");
    }).join(" OR ");
    var distance = filters.filter(function (f) {
      return f.includes("Dist_");
    }).join(" OR ");
    var denivele = filters.filter(function (f) {
      return f.includes("Deniv_");
    }).join(" OR ");
    distance.length > 0 ? arrResult.push("(".concat(distance, ")")) : false;
    type.length > 0 ? arrResult.push("(".concat(type, ")")) : false;
    denivele.length > 0 ? arrResult.push("(".concat(denivele, ")")) : false;
    var result = arrResult.join(" AND "); //console.log(result);

    manifs.setWhere(result);
    Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["zoomToMarkersAfterLoad"])(map, manifs);
  };
  /*fin des filtres de recherches */

})();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ })

},[["./assets/js/pages/calendrier.js","runtime","vendors~app~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_velor~10347b53","vendors~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~61364f2f","vendors~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~cotatio~d7e263c1","vendors~bonnes_adresses~calendrier~circuits~clubs~labels_velo~login_licencie","vendors~bonnes_adresses~calendrier~circuits~clubs~labels_velo","vendors~calendrier~circuits~clubs~labels_velo","vendors~calendrier~circuits","vendors~calendrier","bonnes_adresses~calendrier~circuits~clubs~labels_velo"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvcGFnZXMvY2FsZW5kcmllci5qcyJdLCJuYW1lcyI6WyJtYXBDYWxlbmRyaWVyIiwibWFwIiwiTCIsImNlbnRlciIsInpvb20iLCJtaW5ab29tIiwibWF4Wm9vbSIsInpvb21Db250cm9sIiwibGF5ZXJzIiwiZXNyaSIsImJhc2VtYXBMYXllciIsImNvbnRyb2wiLCJzY2FsZSIsImFkZFRvIiwiZGVmYXVsdExheWVyIiwiYXR0cmlidXRpb25Db250cm9sIiwiYWRkQXR0cmlidXRpb24iLCJtYXBFbGVtZW50IiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50QnlJZCIsInNpZGViYXJFbGVtZW50IiwibWFwV2lkdGgiLCJvZmZzZXRXaWR0aCIsInNpZGViYXJUb2dnbGVDb250cm9sIiwiQ29udHJvbCIsImJhc2VMYXllcnMiLCJ0aWxlTGF5ZXIiLCJhdHRyaWJ1dGlvbiIsIm92ZXJsYXlNYXBzIiwiY29sc0xheWVyIiwicG9zaXRpb24iLCJsYXllckNvbnRyb2xUaXRsZXMiLCJhZGRDb250cm9sIiwiZ2VvbG9jYXRpb25CdXR0b24iLCJtYW5pZnNMYXllciIsIm1hcmtlckNsdXN0ZXJHcm91cCIsImNodW5rZWRMb2FkaW5nIiwiaWNvbkNyZWF0ZUZ1bmN0aW9uIiwiY2x1c3RlciIsImNvdW50IiwiZ2V0Q2hpbGRDb3VudCIsImRpZ2l0cyIsImxlbmd0aCIsImRpdkljb24iLCJodG1sIiwiY2xhc3NOYW1lIiwiaWNvblNpemUiLCJtYW5pZnMiLCJDbHVzdGVyIiwiZmVhdHVyZUxheWVyIiwidXJsIiwiYWdvbEZlYXR1cmVTZXJ2aWNlcyIsInRva2VuIiwicG9pbnRUb0xheWVyIiwiZmVhdHVyZSIsImxhdGxuZyIsIm1hcmtlciIsImljb24iLCJpY29uT2luIiwiYmluZFBvcHVwIiwibGF5ZXIiLCJub20iLCJwcm9wZXJ0aWVzIiwiTm9tIiwiaWQiLCJNYW5pZklkIiwidHlwZSIsIlR5cGUiLCJkYXRlX2RlYnV0IiwiQWNjdWVpbF9EYXRlRGJ0IiwicHJvdXRlIiwiUHJhdGlxdWVfUm91dGUiLCJwdnR0IiwiUHJhdGlxdWVfVlRUIiwicGdyYXZlbCIsIlByYXRpcXVlX0dyYXZlbCIsInBtYXJjaGUiLCJQcmF0aXF1ZV9NYXJjaGUiLCJkdXJhYmxlIiwiRGV2RHVyYWJsZSIsInBzaCIsIkFjY2VzUFNIIiwic3BlZmVtIiwiU3BlRmVtIiwiZW1haWwiLCJBZHJlc3NlTWFpbCIsInNpdGV3ZWIiLCJBZHJlc3NlV2ViIiwiVXRpbCIsInRlbXBsYXRlIiwicG9wdXBUZW1wbGF0ZU1hbmlmcyIsIm9uY2UiLCJsb2NhdGlvbiIsInBhdGhuYW1lIiwibWVudVVybCIsInBhcnRzIiwiaHJlZiIsInNwbGl0IiwidXJsaWQiLCJyZW5kZXJJbmZvRmVhdHVyZSIsInpvb21Ub01hcmtlcnNBZnRlckxvYWQiLCJvbiIsImFkZEV2ZW50TGlzdGVuZXIiLCJnZXRBdHRyaWJ1dGUiLCJlIiwiY29uc29sZSIsImxvZyIsInNldFdoZXJlIiwiZ2V0V2hlcmUiLCJzeW5jU2lkZWJhciIsImNsZWFyTGF5ZXJzIiwid2luZG93Iiwib25wb3BzdGF0ZSIsImJhY2tCdXR0b25OYXZpZ2F0aW9uIiwic2VhcmNoQ29udHJvbCIsIkdlb2NvZGluZyIsImdlb3NlYXJjaCIsInVzZU1hcEJvdW5kcyIsInRpdGxlIiwicGxhY2Vob2xkZXIiLCJleHBhbmRlZCIsImNvbGxhcHNlQWZ0ZXJSZXN1bHQiLCJwcm92aWRlcnMiLCJhcmNnaXNPbmxpbmVQcm92aWRlciIsImZlYXR1cmVMYXllclByb3ZpZGVyIiwid2hlcmUiLCJzZWFyY2hGaWVsZHMiLCJsYWJlbCIsIm1heFJlc3VsdHMiLCJidWZmZXJSYWRpdXMiLCJmb3JtYXRTdWdnZXN0aW9uIiwiZXNyaUdlb2NvZGVyIiwiZ2V0Q29udGFpbmVyIiwic2V0RXNyaUdlb2NvZGVyIiwib3B0aW9ucyIsImhhbmRsZURhdGUiLCJzdHIiLCJkYXRlIiwiYXJyUmVzdWx0c1NpZGViYXIiLCIkIiwiZW1wdHkiLCJlYWNoQWN0aXZlRmVhdHVyZSIsImhhc0xheWVyIiwiZ2V0Qm91bmRzIiwiY29udGFpbnMiLCJnZXRMYXRMbmciLCJwdXNoIiwic2lkZWJhclRlbXBsYXRlTWFuaWZzIiwiQWNjdWVpbF9DUCIsIkFjY3VlaWxfQ29tbXVuZSIsImFkZFJlc3VsdHNUb1NpZGViYXIiLCJyb3V0ZSIsInZ0dCIsImdyYXZlbCIsIm1hcmNoZSIsImNwIiwidmlsbGUiLCJMYXllciIsImVhY2hGZWF0dXJlIiwiaGFuZGxlR3B4TWFuaWZzIiwiZ3B4RmlsZXMiLCJGaWNoaWVyR3B4IiwiaW5mb2ZlYXR1cmVFbGVtZW50IiwidW5tb3VudENvbXBvbmVudEF0Tm9kZSIsInJlbmRlciIsIkFjY3VlaWxfTGlldSIsIkFjY3VlaWxfQWRyZXNzZSIsIkFycml2ZWVfQ1AiLCJBcnJpdmVlX0NvbW11bmUiLCJBcnJpdmVlX0RhdGVGaW4iLCJBcnJpdmVlX0xpZXUiLCJDYXRlZ29yaWVMYWJlbCIsIkNvbnRhY3RfTm9tIiwiQ29udGFjdF9QcmVub20iLCJGaWNoaWVyRmx5ZXIiLCJJbnNjcmlwdGlvbkxpZ25lIiwiTW9kZWxlSWQiLCJOdW1lcm9MYWJlbCIsIk9pbk1hbmlmTGljVGFyaWZBZHVsdGUiLCJPaW5NYW5pZkxpY1RhcmlmSmV1bmUiLCJPaW5NYW5pZk5vbkxpY1RhcmlmQWR1bHRlIiwiT2luTWFuaWZOb25MaWNUYXJpZkpldW5lIiwiT2luTWFuaWZSZXNlcnZlTGljZW5jaWUiLCJPaW5NYW5pZk9ic2VydmF0aW9uIiwiT2luTWFyY2hlRGlzdF8xIiwiT2luTWFyY2hlRGlzdF8yIiwiT2luTWFyY2hlRGlzdF8zIiwiT2luTWFyY2hlRGlzdF80IiwiT2luTWFyY2hlRGlzdF81IiwiT2luTWFyY2hlRGlzdF82IiwiT2luUm91dGVEZW5pdl8xIiwiT2luUm91dGVEZW5pdl8yIiwiT2luUm91dGVEZW5pdl8zIiwiT2luUm91dGVEZW5pdl80IiwiT2luUm91dGVEZW5pdl81IiwiT2luUm91dGVEZW5pdl82IiwiT2luUm91dGVEaWZmXzEiLCJPaW5Sb3V0ZURpZmZfMiIsIk9pblJvdXRlRGlmZl8zIiwiT2luUm91dGVEaWZmXzQiLCJPaW5Sb3V0ZURpZmZfNSIsIk9pblJvdXRlRGlmZl82IiwiT2luUm91dGVEaXN0XzEiLCJPaW5Sb3V0ZURpc3RfMiIsIk9pblJvdXRlRGlzdF8zIiwiT2luUm91dGVEaXN0XzQiLCJPaW5Sb3V0ZURpc3RfNSIsIk9pblJvdXRlRGlzdF82IiwiT2luVlRURGVuaXZfMSIsIk9pblZUVERlbml2XzIiLCJPaW5WVFREZW5pdl8zIiwiT2luVlRURGVuaXZfNCIsIk9pblZUVERlbml2XzUiLCJPaW5WVFREZW5pdl82IiwiT2luVlRURGlmZl8xIiwiT2luVlRURGlmZl8yIiwiT2luVlRURGlmZl8zIiwiT2luVlRURGlmZl80IiwiT2luVlRURGlmZl81IiwiT2luVlRURGlmZl82IiwiT2luVlRURGlzdF8xIiwiT2luVlRURGlzdF8yIiwiT2luVlRURGlzdF8zIiwiT2luVlRURGlzdF80IiwiT2luVlRURGlzdF81IiwiT2luVlRURGlzdF82IiwiT2luR3JhdmVsRGVuaXZfMSIsIk9pbkdyYXZlbERlbml2XzIiLCJPaW5HcmF2ZWxEZW5pdl8zIiwiT2luR3JhdmVsRGVuaXZfNCIsIk9pbkdyYXZlbERlbml2XzUiLCJPaW5HcmF2ZWxEZW5pdl82IiwiT2luR3JhdmVsRGlmZl8xIiwiT2luR3JhdmVsRGlmZl8yIiwiT2luR3JhdmVsRGlmZl8zIiwiT2luR3JhdmVsRGlmZl80IiwiT2luR3JhdmVsRGlmZl81IiwiT2luR3JhdmVsRGlmZl82IiwiT2luR3JhdmVsRGlzdF8xIiwiT2luR3JhdmVsRGlzdF8yIiwiT2luR3JhdmVsRGlzdF8zIiwiT2luR3JhdmVsRGlzdF80IiwiT2luR3JhdmVsRGlzdF81IiwiT2luR3JhdmVsRGlzdF82IiwiU3RydWN0dXJlIiwiU3RydWN0dXJlRGVwIiwiU3RydWN0dXJlUmVnIiwiaGV1cmVEZUNsb3R1cmUiLCJoZXVyZXMiLCJuYXZpZ2F0aW9uSGlzdG9yeSIsInNsdWciLCJyZXNpemUiLCJzdHlsZSIsIndpZHRoIiwiaW52YWxpZGF0ZVNpemUiLCJzZXRWaWV3IiwiZ2VvbWV0cnkiLCJjb29yZGluYXRlcyIsImRhdGVGaWx0ZXIiLCJmbGF0cGlja3IiLCJsb2NhbGUiLCJmaXJzdERheU9mV2VlayIsInNlbGVjdGVkRGF0ZXMiLCJkYXRlU3RyIiwiaW5zdGFuY2UiLCJkYXRlc3RhcnQiLCJkYXRlZW5kIiwiRGF0ZSIsImRpc3RTbGlkZXIiLCJub1VpU2xpZGVyIiwiY3JlYXRlIiwic3RhcnQiLCJjb25uZWN0IiwicmFuZ2UiLCJmb3JtYXQiLCJ3TnVtYiIsImRlY2ltYWxzIiwicGlwcyIsIm1vZGUiLCJ2YWx1ZXMiLCJkZW5pdlNsaWRlciIsInRhZ2lmeUNsdWJzIiwiVGFnaWZ5IiwicXVlcnlTZWxlY3RvciIsImVuZm9yZVdoaXRlbGlzdCIsIndoaXRlbGlzdCIsImR1cGxpY2F0ZXMiLCJnZXRGaWx0ZXJzIiwiZmlsdGVycyIsImVsZW1lbnRzIiwicXVlcnlTZWxlY3RvckFsbCIsIml0ZW0iLCJjaGVja2VkIiwidmFsdWUiLCJzcGxpY2UiLCJpbmRleE9mIiwic2V0RmlsdGVycyIsImRldGFpbCIsImRhdGEiLCJqb2luIiwiZGlzdExpc3QiLCJkaXN0IiwiZGVuaXZMaXN0IiwiZGVuaXYiLCJhcnJSZXN1bHQiLCJmaWx0ZXIiLCJmIiwiaW5jbHVkZXMiLCJkaXN0YW5jZSIsImRlbml2ZWxlIiwicmVzdWx0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQWVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxDQUFDLFNBQVNBLGFBQVQsR0FBMEI7QUFFdkI7QUFDQSxNQUFNQyxHQUFHLEdBQUdDLENBQUMsQ0FBQ0QsR0FBRixDQUFNLEtBQU4sRUFBYTtBQUNyQkUsVUFBTSxFQUFFLENBQUMsRUFBRCxFQUFLLENBQUwsQ0FEYTtBQUVyQkMsUUFBSSxFQUFFLENBRmU7QUFHckJDLFdBQU8sRUFBRSxDQUhZO0FBSXJCQyxXQUFPLEVBQUUsRUFKWTtBQUtyQkMsZUFBVyxFQUFFLElBTFE7QUFNckJDLFVBQU0sRUFBRSxDQUFDTixDQUFDLENBQUNPLElBQUYsQ0FBT0MsWUFBUCxDQUFvQixhQUFwQixDQUFEO0FBTmEsR0FBYixDQUFaO0FBUUFSLEdBQUMsQ0FBQ1MsT0FBRixDQUFVQyxLQUFWLEdBQWtCQyxLQUFsQixDQUF3QlosR0FBeEIsRUFYdUIsQ0FXTTs7QUFDaEMsTUFBTWEsWUFBWSxHQUFHWixDQUFDLENBQUNPLElBQUYsQ0FBT0MsWUFBUCxDQUFvQixhQUFwQixFQUFtQ0csS0FBbkMsQ0FBeUNaLEdBQXpDLENBQXJCO0FBQ0dBLEtBQUcsQ0FBQ2Msa0JBQUosQ0FBdUJDLGNBQXZCLENBQXNDLHVGQUF0QyxFQWJ1QixDQWV2Qjs7QUFDQSxNQUFNQyxVQUFVLEdBQUdDLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixLQUF4QixDQUFuQjtBQUNBLE1BQU1DLGNBQWMsR0FBR0YsUUFBUSxDQUFDQyxjQUFULENBQXdCLFNBQXhCLENBQXZCO0FBQ0EsTUFBTUUsUUFBUSxHQUFHSCxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsS0FBeEIsRUFBK0JHLFdBQWhELENBbEJ1QixDQW9CdkI7O0FBQ0FDLG1GQUFvQixDQUFDckIsQ0FBQyxDQUFDc0IsT0FBSCxFQUFZdkIsR0FBWixFQUFpQm1CLGNBQWpCLEVBQWlDSCxVQUFqQyxFQUE2Q0ksUUFBN0MsQ0FBcEIsQ0FyQnVCLENBdUJ2Qjs7QUFDSCxNQUFNSSxVQUFVLEdBQUc7QUFDbEIsNEJBQXdCWCxZQUROO0FBRWxCLHdCQUFvQlosQ0FBQyxDQUFDTyxJQUFGLENBQU9DLFlBQVAsQ0FBb0IsU0FBcEIsQ0FGRjtBQUdsQixxQkFBa0JSLENBQUMsQ0FBQ3dCLFNBQUYsQ0FBWSxvREFBWixFQUFrRTtBQUNuRkMsaUJBQVcsRUFBRTtBQURzRSxLQUFsRTtBQUhBLEdBQW5CO0FBT0EsTUFBTUMsV0FBVyxHQUFHO0FBQ25CLFlBQVFDLHNFQUFTO0FBREUsR0FBcEI7QUFHQTNCLEdBQUMsQ0FBQ1MsT0FBRixDQUFVSCxNQUFWLENBQWlCaUIsVUFBakIsRUFBNkJHLFdBQTdCLEVBQTBDO0FBQ3pDRSxZQUFRLEVBQUU7QUFEK0IsR0FBMUMsRUFFR2pCLEtBRkgsQ0FFU1osR0FGVDtBQUdBOEIsaUZBQWtCO0FBQ2xCOUIsS0FBRyxDQUFDK0IsVUFBSixDQUFlLElBQUlDLHNFQUFKLENBQXNCaEMsR0FBdEIsQ0FBZixFQXRDMEIsQ0F3Q3ZCOztBQUNBLE1BQU1pQyxXQUFXLEdBQUdoQyxDQUFDLENBQUNpQyxrQkFBRixDQUFxQjtBQUNyQ0Msa0JBQWMsRUFBRSxJQURxQjtBQUVyQ0Msc0JBQWtCLEVBQUUsNEJBQVVDLE9BQVYsRUFBbUI7QUFDbkMsVUFBSUMsS0FBSyxHQUFHRCxPQUFPLENBQUNFLGFBQVIsRUFBWjtBQUNBLFVBQUlDLE1BQU0sR0FBRyxDQUFDRixLQUFLLEdBQUcsRUFBVCxFQUFhRyxNQUExQjtBQUNBLGFBQU94QyxDQUFDLENBQUN5QyxPQUFGLENBQVU7QUFDZkMsWUFBSSxFQUFFTCxLQURTO0FBRWZNLGlCQUFTLEVBQUUsb0JBQW9CSixNQUZoQjtBQUdmSyxnQkFBUSxFQUFFO0FBSEssT0FBVixDQUFQO0FBS0g7QUFWb0MsR0FBckIsQ0FBcEI7QUFZSCxNQUFNQyxNQUFNLEdBQUc3QyxDQUFDLENBQUNPLElBQUYsQ0FBT3VDLE9BQVAsQ0FBZUMsWUFBZixDQUE0QjtBQUNwQ0MsT0FBRyxFQUFFQyx3RUFBbUIsQ0FBQ0osTUFEVztBQUVwQ0ssU0FBSyxFQUFFQSxLQUY2QjtBQUcxQ0MsZ0JBQVksRUFBRSxzQkFBQ0MsT0FBRCxFQUFVQyxNQUFWLEVBQXFCO0FBQ2xDLGFBQU9yRCxDQUFDLENBQUNzRCxNQUFGLENBQVNELE1BQVQsRUFBaUI7QUFDdkJFLFlBQUksRUFBRUMsNERBQU8sQ0FBQyxDQUFEO0FBRFUsT0FBakIsQ0FBUDtBQUdBLEtBUHlDO0FBUTFDckIsc0JBQWtCLEVBQUUsNEJBQUNDLE9BQUQsRUFBYTtBQUNoQyxVQUFJQyxLQUFLLEdBQUdELE9BQU8sQ0FBQ0UsYUFBUixFQUFaO0FBQ0EsVUFBSUMsTUFBTSxHQUFHLENBQUNGLEtBQUssR0FBRyxFQUFULEVBQWFHLE1BQTFCO0FBQ0EsYUFBT3hDLENBQUMsQ0FBQ3lDLE9BQUYsQ0FBVTtBQUNmQyxZQUFJLEVBQUVMLEtBRFM7QUFFZk0saUJBQVMsRUFBRSxvQkFBb0JKLE1BRmhCO0FBR2ZLLGdCQUFRLEVBQUU7QUFISyxPQUFWLENBQVA7QUFLRTtBQWhCdUMsR0FBNUIsRUFpQlRqQyxLQWpCUyxDQWlCSHFCLFdBakJHLEVBaUJVckIsS0FqQlYsQ0FpQmdCWixHQWpCaEIsQ0FBZixDQXJEMEIsQ0F3RTFCOztBQUNBOEMsUUFBTSxDQUFDWSxTQUFQLENBQWlCLFVBQUNDLEtBQUQsRUFBUztBQUNuQixRQUFJQyxHQUFHLEdBQUdELEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCQyxHQUFuQztBQUNBLFFBQUlDLEVBQUUsR0FBR0osS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJHLE9BQWxDO0FBQ0EsUUFBSUMsSUFBSSxHQUFHTixLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QkssSUFBcEM7QUFDQSxRQUFJQyxVQUFVLEdBQUdSLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCTyxlQUExQztBQUNBLFFBQUlDLE1BQU0sR0FBR1YsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJTLGNBQXpCLEdBQTBDLE9BQTFDLEdBQW9ELEtBQWpFO0FBQ0EsUUFBSUMsSUFBSSxHQUFHWixLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QlcsWUFBekIsR0FBd0MsS0FBeEMsR0FBZ0QsS0FBM0Q7QUFDQSxRQUFJQyxPQUFPLEdBQUdkLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCYSxlQUF6QixHQUEyQyxRQUEzQyxHQUFzRCxLQUFwRTtBQUNBLFFBQUlDLE9BQU8sR0FBR2hCLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCZSxlQUF6QixHQUEyQyxRQUEzQyxHQUFzRCxLQUFwRTtBQUNBLFFBQUlDLE9BQU8sR0FBR2xCLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCaUIsVUFBekIsR0FBc0MsaUJBQXRDLEdBQTBELEtBQXhFO0FBQ0EsUUFBSUMsR0FBRyxHQUFHcEIsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJtQixRQUF6QixHQUFvQyxhQUFwQyxHQUFvRCxLQUE5RDtBQUNBLFFBQUlDLE1BQU0sR0FBR3RCLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCcUIsTUFBekIsR0FBa0MsbUJBQWxDLEdBQXdELEtBQXJFO0FBQ0EsUUFBSUMsS0FBSyxHQUFHeEIsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJ1QixXQUFyQztBQUNBLFFBQUlDLE9BQU8sR0FBRzFCLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCeUIsVUFBdkM7QUFDQSxXQUFPckYsQ0FBQyxDQUFDc0YsSUFBRixDQUFPQyxRQUFQLENBQ0hDLDhFQUFtQixDQUFDN0IsR0FBRCxFQUFNRyxFQUFOLEVBQVVFLElBQVYsRUFBZ0JFLFVBQWhCLEVBQTRCRSxNQUE1QixFQUFvQ0UsSUFBcEMsRUFBMENFLE9BQTFDLEVBQW1ERSxPQUFuRCxFQUE0REUsT0FBNUQsRUFBcUVFLEdBQXJFLEVBQTBFRSxNQUExRSxFQUFrRkUsS0FBbEYsRUFBeUZFLE9BQXpGLENBRGhCLENBQVA7QUFHSCxHQWpCSjtBQW1CRzs7QUFDSHZDLFFBQU0sQ0FBQzRDLElBQVAsQ0FBWSxNQUFaLEVBQW9CLFlBQU07QUFDekIsUUFBR0MsUUFBUSxDQUFDQyxRQUFULElBQXFCQyxPQUF4QixFQUFnQztBQUMvQixVQUFJQyxLQUFLLEdBQUdILFFBQVEsQ0FBQ0ksSUFBVCxDQUFjQyxLQUFkLENBQW9CSCxPQUFPLEdBQUMsR0FBNUIsQ0FBWjtBQUNBLFVBQUlJLEtBQUssR0FBR0gsS0FBSyxDQUFDLENBQUQsQ0FBTCxDQUFTRSxLQUFULENBQWUsR0FBZixDQUFaO0FBQ0FFLHVCQUFpQixDQUFDcEQsTUFBRCxFQUFTbUQsS0FBSyxDQUFDLENBQUQsQ0FBZCxDQUFqQjtBQUNBLEtBSkQsTUFJTztBQUNORSx5RkFBc0IsQ0FBQ25HLEdBQUQsRUFBTThDLE1BQU4sQ0FBdEI7QUFDQTs7QUFBQSxLQVB3QixDQVF6QjtBQUNBLEdBVEQ7QUFVQUEsUUFBTSxDQUFDc0QsRUFBUCxDQUFVLFdBQVYsRUFBdUIsWUFBTTtBQUM1Qm5GLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3QixhQUF4QixFQUF1Q21GLGdCQUF2QyxDQUF3RCxPQUF4RCxFQUFpRSxZQUFNO0FBQ3RFSCx1QkFBaUIsQ0FBQ3BELE1BQUQsRUFBUzdCLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixhQUF4QixFQUF1Q29GLFlBQXZDLENBQW9ELE9BQXBELENBQVQsQ0FBakI7QUFDQSxLQUZELEVBRUcsSUFGSDtBQUdBLEdBSkQ7QUFLQXhELFFBQU0sQ0FBQ3NELEVBQVAsQ0FBVSxPQUFWLEVBQW1CLFVBQUNHLENBQUQsRUFBTztBQUN6QkMsV0FBTyxDQUFDQyxHQUFSLENBQVlGLENBQVo7QUFDQSxHQUZEO0FBR0F2RyxLQUFHLENBQUNvRyxFQUFKLENBQU8sU0FBUCxFQUFrQixZQUFNO0FBQ3ZCdEQsVUFBTSxDQUFDNEQsUUFBUCxDQUFnQjVELE1BQU0sQ0FBQzZELFFBQVAsRUFBaEI7QUFDQUMsZUFBVztBQUNYLEdBSEQ7QUFJQTVHLEtBQUcsQ0FBQ29HLEVBQUosQ0FBTyxlQUFQLEVBQXdCLFlBQU07QUFDN0JuRSxlQUFXLENBQUM0RSxXQUFaO0FBQ0csR0FGSjs7QUFHR0MsUUFBTSxDQUFDQyxVQUFQLEdBQW9CLFlBQU07QUFDdEJDLHFGQUFvQixDQUFDbkIsT0FBRCxFQUFVN0YsR0FBVixFQUFlZ0IsVUFBZixFQUEyQkksUUFBM0IsRUFBcUMwQixNQUFyQyxFQUE2Q29ELGlCQUE3QyxDQUFwQjtBQUNILEdBRkQ7QUFHSDtBQUVHOzs7QUFDQSxNQUFNZSxhQUFhLEdBQUdoSCxDQUFDLENBQUNPLElBQUYsQ0FBTzBHLFNBQVAsQ0FBaUJDLFNBQWpCLENBQTJCO0FBQzdDQyxnQkFBWSxFQUFDLEtBRGdDO0FBRTdDQyxTQUFLLEVBQUUsa0NBRnNDO0FBRzdDQyxlQUFXLEVBQUUsTUFIZ0M7QUFJN0NDLFlBQVEsRUFBQyxJQUpvQztBQUs3Q0MsdUJBQW1CLEVBQUUsS0FMd0I7QUFNN0MzRixZQUFRLEVBQUUsVUFObUM7QUFPN0M0RixhQUFTLEVBQUUsQ0FDUEMseUVBRE8sRUFFUHpILENBQUMsQ0FBQ08sSUFBRixDQUFPMEcsU0FBUCxDQUFpQlMsb0JBQWpCLENBQXNDO0FBQUU7QUFDaEQxRSxTQUFHLEVBQUVDLHdFQUFtQixDQUFDSixNQURxQjtBQUVsQzhFLFdBQUssRUFBRTlFLE1BQU0sQ0FBQzZELFFBQVAsRUFGMkI7QUFHbEN4RCxXQUFLLEVBQUVBLEtBSDJCO0FBSTlDMEUsa0JBQVksRUFBRSxDQUFDLFNBQUQsRUFBVyxLQUFYLEVBQWlCLFdBQWpCLENBSmdDO0FBSzlDQyxXQUFLLEVBQUUsd0JBTHVDO0FBTTlDQyxnQkFBVSxFQUFFLENBTmtDO0FBTzlDQyxrQkFBWSxFQUFFLElBUGdDO0FBUTlDQyxzQkFBZ0IsRUFBRSwwQkFBQzVFLE9BQUQsRUFBYTtBQUM5Qix5QkFBVUEsT0FBTyxDQUFDUSxVQUFSLENBQW1CQyxHQUE3QixnQkFBc0NULE9BQU8sQ0FBQ1EsVUFBUixDQUFtQkssSUFBekQ7QUFDQTtBQVY2QyxLQUF0QyxDQUZPO0FBUGtDLEdBQTNCLEVBc0JuQnRELEtBdEJtQixDQXNCYlosR0F0QmEsQ0FBdEIsQ0E1SHVCLENBbUp2Qjs7QUFDQSxNQUFNa0ksWUFBWSxHQUFHakIsYUFBYSxDQUFDa0IsWUFBZCxFQUFyQjtBQUNBQyw4RUFBZSxDQUFDRixZQUFELEVBQWVqSCxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsVUFBeEIsQ0FBZixDQUFmLENBckp1QixDQXNKdkI7O0FBQ0grRixlQUFhLENBQUNiLEVBQWQsQ0FBaUIsWUFBakIsRUFBK0IsWUFBWTtBQUMxQ2EsaUJBQWEsQ0FBQ29CLE9BQWQsQ0FBc0JaLFNBQXRCLENBQWdDLENBQWhDLEVBQW1DWSxPQUFuQyxDQUEyQ1QsS0FBM0MsR0FBbUQ5RSxNQUFNLENBQUM2RCxRQUFQLEVBQW5EO0FBQ0csR0FGSixFQXZKMEIsQ0EySnRCOztBQUNBLE1BQU0yQixVQUFVLEdBQUcsU0FBYkEsVUFBYSxDQUFDQyxHQUFELEVBQVM7QUFDekIsUUFBSUMsSUFBSSxHQUFHRCxHQUFHLENBQUN2QyxLQUFKLENBQVUsR0FBVixDQUFYO0FBQ0EsV0FBT3dDLElBQUksQ0FBQyxDQUFELENBQUosR0FBVSxHQUFWLEdBQWdCQSxJQUFJLENBQUMsQ0FBRCxDQUFwQixHQUEwQixHQUExQixHQUFnQ0EsSUFBSSxDQUFDLENBQUQsQ0FBM0M7QUFDSCxHQUhBOztBQUtELE1BQU01QixXQUFXLEdBQUcsU0FBZEEsV0FBYyxHQUEwQjtBQUFBLFFBQXpCNkIsaUJBQXlCLHVFQUFQLEVBQU87QUFDMUNDLEtBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZUMsS0FBZjtBQUNBN0YsVUFBTSxDQUFDOEYsaUJBQVAsQ0FBeUIsVUFBVWpGLEtBQVYsRUFBaUI7QUFBRTtBQUN4QyxVQUFJM0QsR0FBRyxDQUFDNkksUUFBSixDQUFhL0YsTUFBYixDQUFKLEVBQTBCO0FBQ3RCLFlBQUk5QyxHQUFHLENBQUM4SSxTQUFKLEdBQWdCQyxRQUFoQixDQUF5QnBGLEtBQUssQ0FBQ3FGLFNBQU4sRUFBekIsQ0FBSixFQUFpRDtBQUM3Q1AsMkJBQWlCLENBQUNRLElBQWxCLENBQ0lDLHFCQUFxQixDQUNqQnZGLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCRyxPQURSLEVBRWpCc0UsVUFBVSxDQUFDM0UsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJPLGVBQTFCLENBRk8sRUFHakJULEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCQyxHQUhSLEVBSWpCSCxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QkssSUFKUixFQUtqQlAsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJTLGNBTFIsRUFNakJYLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCVyxZQU5SLEVBT2pCYixLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QmEsZUFQUixFQVFqQmYsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJlLGVBUlIsRUFTakJqQixLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QnNGLFVBVFIsRUFVakJ4RixLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QnVGLGVBVlIsRUFXakJ6RixLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QmlCLFVBWFIsRUFZakJuQixLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5Qm1CLFFBWlIsRUFhakJyQixLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QnFCLE1BYlIsQ0FEekI7QUFpQkg7QUFDSjtBQUNKLEtBdEJEO0FBdUJBbUUsb0ZBQW1CLENBQUNaLGlCQUFELEVBQW9CM0YsTUFBcEIsRUFBNEJvRCxpQkFBNUIsQ0FBbkI7QUFDSCxHQTFCRCxDQWpLdUIsQ0E2THZCOzs7QUFDQSxNQUFNZ0QscUJBQXFCLEdBQUcsU0FBeEJBLHFCQUF3QixDQUFDbkYsRUFBRCxFQUFLeUUsSUFBTCxFQUFXNUUsR0FBWCxFQUFnQkssSUFBaEIsRUFBc0JxRixLQUF0QixFQUE2QkMsR0FBN0IsRUFBa0NDLE1BQWxDLEVBQTBDQyxNQUExQyxFQUFrREMsRUFBbEQsRUFBc0RDLEtBQXRELEVBQTZEOUUsT0FBN0QsRUFBc0VFLEdBQXRFLEVBQTJFRSxNQUEzRSxFQUFzRjtBQUNoSCxvRUFDbURsQixFQURuRCxxTkFHdUZ5RSxJQUh2Riw2RUFJaUQ1RSxHQUpqRCx3Q0FLYzBGLEtBQUssNkVBTG5CLG1DQU1jQyxHQUFHLDJFQU5qQixtQ0FPY0MsTUFBTSw4RUFQcEIsbUNBUWNDLE1BQU0sOEVBUnBCLG1DQVNjNUUsT0FBTywwRkFUckIsbUNBVWNFLEdBQUcsb0ZBVmpCLG1DQVdjRSxNQUFNLDRGQVhwQiwwRkFZbUVoQixJQVpuRSxpSEFhaUZ5RixFQWJqRixlQWF3RkMsS0FieEY7QUFpQkgsR0FsQkQsQ0E5THVCLENBa052Qjs7O0FBQ0EsTUFBTXpELGlCQUFpQixHQUFHLFNBQXBCQSxpQkFBb0IsQ0FBQzBELEtBQUQsRUFBUTdGLEVBQVIsRUFBZTtBQUNyQzZGLFNBQUssQ0FBQ0MsV0FBTixDQUFrQixVQUFVbEcsS0FBVixFQUFpQjtBQUMvQixVQUFHQSxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QkcsT0FBekIsSUFBb0NELEVBQXZDLEVBQTBDO0FBQ3RDLFlBQU0rRixlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLEdBQU07QUFDMUIsY0FBSUMsUUFBUSxHQUFHcEcsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJtRyxVQUF6QixHQUFzQ3JHLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCbUcsVUFBekIsQ0FBb0NoRSxLQUFwQyxDQUEwQyxHQUExQyxDQUF0QyxHQUF1RixLQUF0RztBQUNBLGlCQUFPK0QsUUFBUDtBQUNILFNBSEQ7O0FBSUEsWUFBSUUsa0JBQWtCLEdBQUdoSixRQUFRLENBQUNDLGNBQVQsQ0FBd0IsYUFBeEIsQ0FBekI7QUFDQWdKLGlGQUFzQixDQUFDRCxrQkFBRCxDQUF0QjtBQUNBRSxpRUFBTSxlQUNGLDREQUFDLGdFQUFEO0FBQ0ksaUJBQU8sRUFBRXBHLEVBRGI7QUFFSSxhQUFHLEVBQUVKLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCQyxHQUZsQztBQUdJLGtCQUFRLEVBQUVILEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCbUIsUUFIdkM7QUFJSSxvQkFBVSxFQUFFckIsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJzRixVQUp6QztBQUtJLHlCQUFlLEVBQUV4RixLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QnVGLGVBTDlDO0FBTUkseUJBQWUsRUFBRXpGLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCTyxlQU45QztBQU9JLHNCQUFZLEVBQUVULEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCdUcsWUFQM0M7QUFRSSx5QkFBZSxFQUFFekcsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJ3RyxlQVI5QztBQVNJLHFCQUFXLEVBQUUxRyxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QnVCLFdBVDFDO0FBVUksb0JBQVUsRUFBRXpCLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCeUIsVUFWekM7QUFXSSxvQkFBVSxFQUFFM0IsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJ5RyxVQVh6QztBQVlJLHlCQUFlLEVBQUUzRyxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QjBHLGVBWjlDO0FBYUkseUJBQWUsRUFBRTVHLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCMkcsZUFiOUM7QUFjSSxzQkFBWSxFQUFFN0csS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUI0RyxZQWQzQztBQWVJLHdCQUFjLEVBQUU5RyxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QjZHLGNBZjdDO0FBZ0JJLHFCQUFXLEVBQUUvRyxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QjhHLFdBaEIxQztBQWlCSSx3QkFBYyxFQUFFaEgsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUIrRyxjQWpCN0M7QUFrQkksb0JBQVUsRUFBRWpILEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCaUIsVUFsQnpDO0FBbUJJLHNCQUFZLEVBQUVuQixLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QmdILFlBbkIzQztBQW9CSSxvQkFBVSxFQUFFZixlQUFlLEVBcEIvQjtBQXFCSSwwQkFBZ0IsRUFBRW5HLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCaUgsZ0JBckIvQztBQXNCSSxrQkFBUSxFQUFFbkgsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJrSCxRQXRCdkM7QUF1QkkscUJBQVcsRUFBRXBILEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCbUgsV0F2QjFDO0FBd0JJLGdDQUFzQixFQUFFckgsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJvSCxzQkF4QnJEO0FBeUJJLCtCQUFxQixFQUFFdEgsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJxSCxxQkF6QnBEO0FBMEJJLG1DQUF5QixFQUFFdkgsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJzSCx5QkExQnhEO0FBMkJJLGtDQUF3QixFQUFFeEgsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJ1SCx3QkEzQnZEO0FBNEJJLGlDQUF1QixFQUFFekgsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJ3SCx1QkE1QnREO0FBNkJJLGVBQUssRUFBRTFILEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCeUgsbUJBN0JwQztBQThCSSx5QkFBZSxFQUFFM0gsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUIwSCxlQTlCOUM7QUErQkkseUJBQWUsRUFBRTVILEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCMkgsZUEvQjlDO0FBZ0NJLHlCQUFlLEVBQUU3SCxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QjRILGVBaEM5QztBQWlDSSx5QkFBZSxFQUFFOUgsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUI2SCxlQWpDOUM7QUFrQ0kseUJBQWUsRUFBRS9ILEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCOEgsZUFsQzlDO0FBbUNJLHlCQUFlLEVBQUVoSSxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QitILGVBbkM5QztBQW9DSSx5QkFBZSxFQUFFakksS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJnSSxlQXBDOUM7QUFxQ0kseUJBQWUsRUFBRWxJLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCaUksZUFyQzlDO0FBc0NJLHlCQUFlLEVBQUVuSSxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QmtJLGVBdEM5QztBQXVDSSx5QkFBZSxFQUFFcEksS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJtSSxlQXZDOUM7QUF3Q0kseUJBQWUsRUFBRXJJLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCb0ksZUF4QzlDO0FBeUNJLHlCQUFlLEVBQUV0SSxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QnFJLGVBekM5QztBQTBDSSx3QkFBYyxFQUFFdkksS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJzSSxjQTFDN0M7QUEyQ0ksd0JBQWMsRUFBRXhJLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCdUksY0EzQzdDO0FBNENJLHdCQUFjLEVBQUV6SSxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QndJLGNBNUM3QztBQTZDSSx3QkFBYyxFQUFFMUksS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJ5SSxjQTdDN0M7QUE4Q0ksd0JBQWMsRUFBRTNJLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCMEksY0E5QzdDO0FBK0NJLHdCQUFjLEVBQUU1SSxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QjJJLGNBL0M3QztBQWdESSx3QkFBYyxFQUFFN0ksS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUI0SSxjQWhEN0M7QUFpREksd0JBQWMsRUFBRTlJLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCNkksY0FqRDdDO0FBa0RJLHdCQUFjLEVBQUUvSSxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QjhJLGNBbEQ3QztBQW1ESSx3QkFBYyxFQUFFaEosS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUIrSSxjQW5EN0M7QUFvREksd0JBQWMsRUFBRWpKLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCZ0osY0FwRDdDO0FBcURJLHdCQUFjLEVBQUVsSixLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QmlKLGNBckQ3QztBQXNESSx1QkFBYSxFQUFFbkosS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJrSixhQXRENUM7QUF1REksdUJBQWEsRUFBRXBKLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCbUosYUF2RDVDO0FBd0RJLHVCQUFhLEVBQUVySixLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5Qm9KLGFBeEQ1QztBQXlESSx1QkFBYSxFQUFFdEosS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJxSixhQXpENUM7QUEwREksdUJBQWEsRUFBRXZKLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCc0osYUExRDVDO0FBMkRJLHVCQUFhLEVBQUV4SixLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QnVKLGFBM0Q1QztBQTRESSxzQkFBWSxFQUFFekosS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJ3SixZQTVEM0M7QUE2REksc0JBQVksRUFBRTFKLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCeUosWUE3RDNDO0FBOERJLHNCQUFZLEVBQUUzSixLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QjBKLFlBOUQzQztBQStESSxzQkFBWSxFQUFFNUosS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUIySixZQS9EM0M7QUFnRUksc0JBQVksRUFBRTdKLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCNEosWUFoRTNDO0FBaUVJLHNCQUFZLEVBQUU5SixLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QjZKLFlBakUzQztBQWtFSSxzQkFBWSxFQUFFL0osS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUI4SixZQWxFM0M7QUFtRUksc0JBQVksRUFBRWhLLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCK0osWUFuRTNDO0FBb0VJLHNCQUFZLEVBQUVqSyxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QmdLLFlBcEUzQztBQXFFSSxzQkFBWSxFQUFFbEssS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJpSyxZQXJFM0M7QUFzRUksc0JBQVksRUFBRW5LLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCa0ssWUF0RTNDO0FBdUVJLHNCQUFZLEVBQUVwSyxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5Qm1LLFlBdkUzQztBQXdFSSwwQkFBZ0IsRUFBRXJLLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCb0ssZ0JBeEUvQztBQXlFSSwwQkFBZ0IsRUFBRXRLLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCcUssZ0JBekUvQztBQTBFSSwwQkFBZ0IsRUFBRXZLLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCc0ssZ0JBMUUvQztBQTJFSSwwQkFBZ0IsRUFBRXhLLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCdUssZ0JBM0UvQztBQTRFSSwwQkFBZ0IsRUFBRXpLLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCd0ssZ0JBNUUvQztBQTZFSSwwQkFBZ0IsRUFBRTFLLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCeUssZ0JBN0UvQztBQThFSSx5QkFBZSxFQUFFM0ssS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUIwSyxlQTlFOUM7QUErRUkseUJBQWUsRUFBRTVLLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCMkssZUEvRTlDO0FBZ0ZJLHlCQUFlLEVBQUU3SyxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QjRLLGVBaEY5QztBQWlGSSx5QkFBZSxFQUFFOUssS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUI2SyxlQWpGOUM7QUFrRkkseUJBQWUsRUFBRS9LLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCOEssZUFsRjlDO0FBbUZJLHlCQUFlLEVBQUVoTCxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QitLLGVBbkY5QztBQW9GSSx5QkFBZSxFQUFFakwsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJnTCxlQXBGOUM7QUFxRkkseUJBQWUsRUFBRWxMLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCaUwsZUFyRjlDO0FBc0ZJLHlCQUFlLEVBQUVuTCxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QmtMLGVBdEY5QztBQXVGSSx5QkFBZSxFQUFFcEwsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJtTCxlQXZGOUM7QUF3RkkseUJBQWUsRUFBRXJMLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCb0wsZUF4RjlDO0FBeUZJLHlCQUFlLEVBQUV0TCxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QnFMLGVBekY5QztBQTBGSSx5QkFBZSxFQUFFdkwsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJhLGVBMUY5QztBQTJGSSx5QkFBZSxFQUFFZixLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QmUsZUEzRjlDO0FBNEZJLHdCQUFjLEVBQUVqQixLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QlMsY0E1RjdDO0FBNkZJLHNCQUFZLEVBQUVYLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCVyxZQTdGM0M7QUE4RkksZ0JBQU0sRUFBRWIsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJxQixNQTlGckM7QUErRkksbUJBQVMsRUFBRXZCLEtBQUssQ0FBQ04sT0FBTixDQUFjUSxVQUFkLENBQXlCc0wsU0EvRnhDO0FBZ0dJLHNCQUFZLEVBQUV4TCxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QnVMLFlBaEczQztBQWlHSSxzQkFBWSxFQUFFekwsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJ3TCxZQWpHM0M7QUFrR0ksY0FBSSxFQUFFMUwsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUJLLElBbEduQztBQW1HSSx3QkFBYyxFQUFFUCxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QnlMLGNBbkc3QztBQW9HSSxnQkFBTSxFQUFFM0wsS0FBSyxDQUFDTixPQUFOLENBQWNRLFVBQWQsQ0FBeUIwTCxNQXBHckM7QUFxR0ksdUJBQWEsRUFBRXRGLGtCQXJHbkI7QUFzR0ksYUFBRyxFQUFFakssR0F0R1Q7QUF1R0ksa0JBQVEsRUFBSW9CLFFBdkdoQjtBQXdHSSxlQUFLLEVBQUV3SSxLQXhHWDtBQXlHSSxlQUFLLEVBQUVqRyxLQXpHWDtBQTBHSSxpQkFBTyxFQUFFa0M7QUExR2IsVUFERSxFQTRHRW9FLGtCQTVHRixDQUFOO0FBOEdBdUYsc0ZBQWlCLENBQUN6TCxFQUFELEVBQUs4QixPQUFMLEVBQWFsQyxLQUFLLENBQUNOLE9BQU4sQ0FBY1EsVUFBZCxDQUF5QjRMLElBQXRDLENBQWpCLENBckhzQyxDQXNIdEM7O0FBQ0EsWUFBSUMsTUFBTSxHQUFHdE8sUUFBUSxHQUFDSCxRQUFRLENBQUNDLGNBQVQsQ0FBd0Isc0JBQXhCLEVBQWdERyxXQUF0RTtBQUNBTCxrQkFBVSxDQUFDMk8sS0FBWCxDQUFpQkMsS0FBakIsR0FBdUJGLE1BQU0sR0FBQyxJQUE5QjtBQUNBMVAsV0FBRyxDQUFDNlAsY0FBSjtBQUNBN1AsV0FBRyxDQUFDOFAsT0FBSixDQUFZLENBQUNuTSxLQUFLLENBQUNOLE9BQU4sQ0FBYzBNLFFBQWQsQ0FBdUJDLFdBQXZCLENBQW1DLENBQW5DLENBQUQsRUFBd0NyTSxLQUFLLENBQUNOLE9BQU4sQ0FBYzBNLFFBQWQsQ0FBdUJDLFdBQXZCLENBQW1DLENBQW5DLENBQXhDLENBQVosRUFBNEYsRUFBNUY7QUFDSDtBQUNKLEtBN0hEO0FBOEhILEdBL0hEO0FBaUlBOzs7QUFDQSxNQUFNQyxVQUFVLEdBQUcsU0FBYkEsVUFBYSxHQUFNO0FBQUE7O0FBQ3JCQyw4REFBUyxDQUFDLFlBQUQ7QUFDTEMsWUFBTSxFQUFFO0FBQ0pDLHNCQUFjLEVBQUU7QUFEWjtBQURILDZDQUlLLElBSkwsNkNBS08sQ0FMUCw2Q0FNTyxPQU5QLDBDQVFJLE9BUkosdUNBU0MsT0FURCwyQ0FVSyxJQVZMLGdEQVdVLE1BWFYsNENBWU0sT0FaTiwyQ0FhSyxrQkFBVUMsYUFBVixFQUF5QkMsT0FBekIsRUFBa0NDLFFBQWxDLEVBQTRDO0FBQ2xELFVBQUlDLFNBQVMsR0FBR0gsYUFBYSxDQUFDLENBQUQsQ0FBN0I7QUFDQSxVQUFJSSxPQUFPLEdBQUcsSUFBSUMsSUFBSixDQUFTTCxhQUFhLENBQUMsQ0FBRCxDQUF0QixDQUFkO0FBQ0E3SixhQUFPLENBQUNDLEdBQVIsQ0FBWStKLFNBQVosRUFBdUJDLE9BQXZCO0FBQ0gsS0FqQkksZUFBVDtBQW1CSCxHQXBCRDs7QUFvQkVSLFlBQVU7QUFDZixNQUFNVSxVQUFVLEdBQUcxUCxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsQ0FBbkI7QUFDQTBQLG9EQUFVLENBQUNDLE1BQVgsQ0FBa0JGLFVBQWxCLEVBQThCO0FBQzdCRyxTQUFLLEVBQUUsQ0FBQyxDQUFELEVBQUksR0FBSixDQURzQjtBQUU3QkMsV0FBTyxFQUFFLElBRm9CO0FBRzdCQyxTQUFLLEVBQUU7QUFDTixhQUFPLENBREQ7QUFFTixhQUFPO0FBRkQsS0FIc0I7QUFPN0JDLFVBQU0sRUFBRUMsNkNBQUssQ0FBQztBQUNiQyxjQUFRLEVBQUUsQ0FERyxDQUNBOztBQURBLEtBQUQsQ0FQZ0I7QUFVN0JDLFFBQUksRUFBRTtBQUFDQyxVQUFJLEVBQUUsT0FBUDtBQUFnQkMsWUFBTSxFQUFFO0FBQXhCO0FBVnVCLEdBQTlCO0FBWUEsTUFBTUMsV0FBVyxHQUFHdFEsUUFBUSxDQUFDQyxjQUFULENBQXdCLGFBQXhCLENBQXBCO0FBQ0EwUCxvREFBVSxDQUFDQyxNQUFYLENBQWtCVSxXQUFsQixFQUErQjtBQUM5QlQsU0FBSyxFQUFFLENBQUMsQ0FBRCxFQUFJLElBQUosQ0FEdUI7QUFFOUJDLFdBQU8sRUFBRSxJQUZxQjtBQUc5QkMsU0FBSyxFQUFFO0FBQ04sYUFBTyxDQUREO0FBRU4sYUFBTztBQUZELEtBSHVCO0FBTzlCQyxVQUFNLEVBQUVDLDZDQUFLLENBQUM7QUFDYkMsY0FBUSxFQUFFLENBREcsQ0FDQTs7QUFEQSxLQUFELENBUGlCO0FBVTlCQyxRQUFJLEVBQUU7QUFBQ0MsVUFBSSxFQUFFLE9BQVA7QUFBZ0JDLFlBQU0sRUFBRTtBQUF4QjtBQVZ3QixHQUEvQjtBQVlBLE1BQU1FLFdBQVcsR0FBRyxJQUFJQyxzREFBSixDQUFXeFEsUUFBUSxDQUFDeVEsYUFBVCxDQUF1QiwwQkFBdkIsQ0FBWCxFQUErRDtBQUM1RUMsbUJBQWUsRUFBRSxJQUQyRDtBQUU1RUMsYUFBUyxFQUFHLEVBRmdFO0FBRzVFQyxjQUFVLEVBQUU7QUFIZ0UsR0FBL0QsQ0FBcEI7O0FBTUEsTUFBTUMsVUFBVSxHQUFDLFNBQVhBLFVBQVcsR0FBYztBQUFBLFFBQWJDLE9BQWEsdUVBQUwsRUFBSztBQUM5QixRQUFNQyxRQUFRLEdBQUcvUSxRQUFRLENBQUNnUixnQkFBVCxDQUEwQixpQkFBMUIsQ0FBakI7O0FBQ0EsdUJBQUlELFFBQUosRUFBY2hTLEdBQWQsQ0FBa0IsVUFBQWtTLElBQUksRUFBSTtBQUN6QkEsVUFBSSxDQUFDN0wsZ0JBQUwsQ0FBc0IsUUFBdEIsRUFBZ0MsVUFBQ0UsQ0FBRCxFQUFPO0FBQ3RDLFlBQUkyTCxJQUFJLENBQUNDLE9BQUwsS0FBaUIsSUFBckIsRUFBMkI7QUFDMUJKLGlCQUFPLENBQUM5SSxJQUFSLENBQWFpSixJQUFJLENBQUNFLEtBQWxCO0FBQ0EsU0FGRCxNQUVPO0FBQ05MLGlCQUFPLENBQUNNLE1BQVIsQ0FBZU4sT0FBTyxDQUFDTyxPQUFSLENBQWdCSixJQUFJLENBQUNFLEtBQXJCLENBQWYsRUFBMkMsQ0FBM0M7QUFDQTs7QUFDREcsa0JBQVUsQ0FBQ1IsT0FBRCxDQUFWO0FBQ0EsT0FQRDtBQVFBLEtBVEQ7O0FBVUFQLGVBQVcsQ0FDQXBMLEVBRFgsQ0FDYyxLQURkLEVBQ3FCLFVBQUNHLENBQUQsRUFBTztBQUNkd0wsYUFBTyxDQUFDOUksSUFBUixDQUFhMUMsQ0FBQyxDQUFDaU0sTUFBRixDQUFTQyxJQUFULENBQWNMLEtBQTNCO0FBQ0FMLGFBQU8sR0FBRUEsT0FBTyxDQUFDVyxJQUFSLENBQWEsRUFBYixDQUFUO0FBQ0gsS0FKWCxFQUtXdE0sRUFMWCxDQUtjLFFBTGQsRUFLd0IsVUFBQ0csQ0FBRCxFQUFPO0FBQ2pCd0wsYUFBTyxDQUFDTSxNQUFSLENBQWVOLE9BQU8sQ0FBQ08sT0FBUixDQUFnQi9MLENBQUMsQ0FBQ2lNLE1BQUYsQ0FBU0MsSUFBVCxDQUFjTCxLQUE5QixDQUFmLEVBQW9ELENBQXBEO0FBQ0FMLGFBQU8sR0FBRUEsT0FBTyxDQUFDVyxJQUFSLENBQWEsRUFBYixDQUFUO0FBQ0gsS0FSWDtBQVNBL0IsY0FBVSxDQUFDQyxVQUFYLENBQXNCeEssRUFBdEIsQ0FBeUIsUUFBekIsRUFBbUMsVUFBQ3FNLElBQUQsRUFBVTtBQUNuQyxVQUFJRSxRQUFRLEdBQUMsQ0FDVCxnQkFEUyxFQUNTLGdCQURULEVBQzJCLGdCQUQzQixFQUM2QyxnQkFEN0MsRUFDK0QsZ0JBRC9ELEVBQ2lGLGdCQURqRixFQUVULGNBRlMsRUFFTyxjQUZQLEVBRXVCLGNBRnZCLEVBRXVDLGNBRnZDLEVBRXVELGNBRnZELEVBRXVFLGNBRnZFLEVBR1QsaUJBSFMsRUFHVSxpQkFIVixFQUc2QixpQkFIN0IsRUFHZ0QsaUJBSGhELEVBR21FLGlCQUhuRSxFQUdzRixpQkFIdEYsRUFJVCxpQkFKUyxFQUlVLGlCQUpWLEVBSTZCLGlCQUo3QixFQUlnRCxpQkFKaEQsRUFJbUUsaUJBSm5FLEVBSXNGLGlCQUp0RixDQUFiO0FBTUFBLGNBQVEsQ0FBQzNTLEdBQVQsQ0FBYSxVQUFDNFMsSUFBRCxFQUFRO0FBQUNiLGVBQU8sQ0FBQ00sTUFBUixDQUFlTixPQUFPLENBQUNPLE9BQVIsQ0FBZ0JNLElBQWhCLENBQWYsRUFBcUMsQ0FBckM7QUFBd0MsT0FBOUQ7QUFDQUQsY0FBUSxDQUFDM1MsR0FBVCxDQUFhLFVBQUM0UyxJQUFELEVBQVE7QUFDakIsWUFBS0gsSUFBSSxDQUFDLENBQUQsQ0FBSixHQUFVLENBQVgsSUFBa0JBLElBQUksQ0FBQyxDQUFELENBQUosR0FBVSxHQUFoQyxFQUFzQztBQUNsQ1YsaUJBQU8sQ0FBQzlJLElBQVIsV0FBZ0IySixJQUFoQixpQkFBMkJILElBQUksQ0FBQyxDQUFELENBQS9CLGtCQUEwQ0csSUFBMUMsaUJBQXFESCxJQUFJLENBQUMsQ0FBRCxDQUF6RDtBQUNILFNBRkQsTUFFTyxJQUFLQSxJQUFJLENBQUMsQ0FBRCxDQUFKLEdBQVUsQ0FBWCxJQUFrQkEsSUFBSSxDQUFDLENBQUQsQ0FBSixJQUFXLEdBQWpDLEVBQXVDO0FBQzFDVixpQkFBTyxDQUFDOUksSUFBUixXQUFnQjJKLElBQWhCLGlCQUEyQkgsSUFBSSxDQUFDLENBQUQsQ0FBL0I7QUFDSCxTQUZNLE1BRUEsSUFBS0EsSUFBSSxDQUFDLENBQUQsQ0FBSixJQUFXLENBQVosSUFBbUJBLElBQUksQ0FBQyxDQUFELENBQUosR0FBVSxHQUFqQyxFQUF1QztBQUMxQ1YsaUJBQU8sQ0FBQzlJLElBQVIsV0FBZ0IySixJQUFoQixzQkFBZ0NBLElBQWhDLGlCQUEyQ0gsSUFBSSxDQUFDLENBQUQsQ0FBL0M7QUFDSDtBQUNKLE9BUkQ7QUFTQUYsZ0JBQVUsQ0FBQ1IsT0FBRCxDQUFWO0FBQ1QsS0FsQkQ7QUFtQkFSLGVBQVcsQ0FBQ1gsVUFBWixDQUF1QnhLLEVBQXZCLENBQTBCLFFBQTFCLEVBQW9DLFVBQUNxTSxJQUFELEVBQVU7QUFDcEMsVUFBSUksU0FBUyxHQUFDLENBQ1YsaUJBRFUsRUFDUyxpQkFEVCxFQUM0QixpQkFENUIsRUFDK0MsaUJBRC9DLEVBQ2tFLGlCQURsRSxFQUNxRixpQkFEckYsRUFFVixlQUZVLEVBRU8sZUFGUCxFQUV3QixlQUZ4QixFQUV5QyxlQUZ6QyxFQUUwRCxlQUYxRCxFQUUyRSxlQUYzRSxFQUdWLGtCQUhVLEVBR1Usa0JBSFYsRUFHOEIsa0JBSDlCLEVBR2tELGtCQUhsRCxFQUdzRSxrQkFIdEUsRUFHMEYsa0JBSDFGLEVBSVYsa0JBSlUsRUFJVSxrQkFKVixFQUk4QixrQkFKOUIsRUFJa0Qsa0JBSmxELEVBSXNFLGtCQUp0RSxFQUkwRixrQkFKMUYsQ0FBZCxDQURvQyxDQU9wQzs7QUFDQUEsZUFBUyxDQUFDN1MsR0FBVixDQUFjLFVBQUM4UyxLQUFELEVBQVM7QUFDbkIsWUFBS0wsSUFBSSxDQUFDLENBQUQsQ0FBSixHQUFVLENBQVgsSUFBa0JBLElBQUksQ0FBQyxDQUFELENBQUosSUFBVyxJQUFqQyxFQUF3QztBQUNwQ1YsaUJBQU8sQ0FBQzlJLElBQVIsV0FBZ0I2SixLQUFoQixpQkFBNEJMLElBQUksQ0FBQyxDQUFELENBQWhDLGtCQUEyQ0ssS0FBM0MsaUJBQXVETCxJQUFJLENBQUMsQ0FBRCxDQUEzRDtBQUNILFNBRkQsTUFFTyxJQUFLQSxJQUFJLENBQUMsQ0FBRCxDQUFKLEdBQVUsQ0FBWCxJQUFrQkEsSUFBSSxDQUFDLENBQUQsQ0FBSixJQUFXLElBQWpDLEVBQXdDO0FBQzNDVixpQkFBTyxDQUFDOUksSUFBUixXQUFnQjZKLEtBQWhCLGlCQUE0QkwsSUFBSSxDQUFDLENBQUQsQ0FBaEM7QUFDSCxTQUZNLE1BRUEsSUFBS0EsSUFBSSxDQUFDLENBQUQsQ0FBSixJQUFXLENBQVosSUFBbUJBLElBQUksQ0FBQyxDQUFELENBQUosR0FBVSxJQUFqQyxFQUF3QztBQUMzQ1YsaUJBQU8sQ0FBQzlJLElBQVIsV0FBZ0I2SixLQUFoQixzQkFBaUNBLEtBQWpDLGlCQUE2Q0wsSUFBSSxDQUFDLENBQUQsQ0FBakQ7QUFDSDtBQUNKLE9BUkQ7QUFTQUYsZ0JBQVUsQ0FBQ1IsT0FBRCxDQUFWO0FBQ1QsS0FsQkQ7QUFtQkcsR0EzREo7O0FBMkRLRCxZQUFVOztBQUVmLE1BQU1TLFVBQVUsR0FBRyxTQUFiQSxVQUFhLENBQUNSLE9BQUQsRUFBMkI7QUFBQSxRQUFqQmdCLFNBQWlCLHVFQUFQLEVBQU87QUFDdkN2TSxXQUFPLENBQUNDLEdBQVIsQ0FBWXNMLE9BQVo7QUFDTixRQUFJOU4sSUFBSSxHQUFHOE4sT0FBTyxDQUFDaUIsTUFBUixDQUFlLFVBQUFDLENBQUM7QUFBQSxhQUFJQSxDQUFDLENBQUNDLFFBQUYsQ0FBVyxXQUFYLENBQUo7QUFBQSxLQUFoQixFQUE2Q1IsSUFBN0MsQ0FBa0QsTUFBbEQsQ0FBWDtBQUNNLFFBQUlTLFFBQVEsR0FBR3BCLE9BQU8sQ0FBQ2lCLE1BQVIsQ0FBZSxVQUFBQyxDQUFDO0FBQUEsYUFBSUEsQ0FBQyxDQUFDQyxRQUFGLENBQVcsT0FBWCxDQUFKO0FBQUEsS0FBaEIsRUFBeUNSLElBQXpDLENBQThDLE1BQTlDLENBQWY7QUFDQSxRQUFJVSxRQUFRLEdBQUdyQixPQUFPLENBQUNpQixNQUFSLENBQWUsVUFBQUMsQ0FBQztBQUFBLGFBQUlBLENBQUMsQ0FBQ0MsUUFBRixDQUFXLFFBQVgsQ0FBSjtBQUFBLEtBQWhCLEVBQTBDUixJQUExQyxDQUErQyxNQUEvQyxDQUFmO0FBRUFTLFlBQVEsQ0FBQzFRLE1BQVQsR0FBZ0IsQ0FBaEIsR0FBb0JzUSxTQUFTLENBQUM5SixJQUFWLFlBQW1Ca0ssUUFBbkIsT0FBcEIsR0FBc0QsS0FBdEQ7QUFDQWxQLFFBQUksQ0FBQ3hCLE1BQUwsR0FBWSxDQUFaLEdBQWdCc1EsU0FBUyxDQUFDOUosSUFBVixZQUFtQmhGLElBQW5CLE9BQWhCLEdBQThDLEtBQTlDO0FBQ0FtUCxZQUFRLENBQUMzUSxNQUFULEdBQWdCLENBQWhCLEdBQW9Cc1EsU0FBUyxDQUFDOUosSUFBVixZQUFtQm1LLFFBQW5CLE9BQXBCLEdBQXNELEtBQXREO0FBRU4sUUFBSUMsTUFBTSxHQUFHTixTQUFTLENBQUNMLElBQVYsQ0FBZSxPQUFmLENBQWIsQ0FWNkMsQ0FXN0M7O0FBQ0E1UCxVQUFNLENBQUM0RCxRQUFQLENBQWdCMk0sTUFBaEI7QUFDQWxOLHVGQUFzQixDQUFFbkcsR0FBRixFQUFPOEMsTUFBUCxDQUF0QjtBQUNBLEdBZEQ7QUFlRzs7QUFHSCxDQXpkRCxJIiwiZmlsZSI6ImNhbGVuZHJpZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gICAgYWRkUmVzdWx0c1RvU2lkZWJhcixcclxuICAgIHpvb21Ub01hcmtlcnNBZnRlckxvYWQsXHJcbiAgICBhZ29sRmVhdHVyZVNlcnZpY2VzLFxyXG4gICAgYXJjZ2lzT25saW5lUHJvdmlkZXIsXHJcbiAgICBpY29uT2luLFxyXG4gICAgc2V0RXNyaUdlb2NvZGVyLCBcclxuICAgIGJhY2tCdXR0b25OYXZpZ2F0aW9uLCBcclxuICAgIG5hdmlnYXRpb25IaXN0b3J5LFxyXG4gICAgc2lkZWJhclRvZ2dsZUNvbnRyb2wsXHJcbiAgICBjb2xzTGF5ZXIsXHJcbiAgICBsYXllckNvbnRyb2xUaXRsZXMsXHJcbiAgICBnZW9sb2NhdGlvbkJ1dHRvblxyXG59IGZyb20gXCIuLi9zZXJ2aWNlcy9tYXBDb25maWdcIjtcclxuXHJcbmltcG9ydCB7cG9wdXBUZW1wbGF0ZVBvaSwgcG9wdXBUZW1wbGF0ZUNpcmN1aXQsIHBvcHVwVGVtcGxhdGVNYW5pZnN9IGZyb20gXCIuLi9jb21wb25lbnRzL3BvcHVwXCI7XHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB7cmVuZGVyLCB1bm1vdW50Q29tcG9uZW50QXROb2RlfSBmcm9tICdyZWFjdC1kb20nO1xyXG5pbXBvcnQgSW5mb0ZlYXR1cmUgZnJvbSAnLi4vY29tcG9uZW50cy9JbmZvRmVhdHVyZSc7XHJcbmltcG9ydCBUYWdpZnkgZnJvbSAnQHlhaXJlby90YWdpZnknO1xyXG5pbXBvcnQgZmxhdHBpY2tyIGZyb20gXCJmbGF0cGlja3JcIjtcclxuaW1wb3J0IHsgRnJlbmNoIH0gZnJvbSAnZmxhdHBpY2tyL2Rpc3QvbDEwbi9mci5qcyc7XHJcbmltcG9ydCBub1VpU2xpZGVyIGZyb20gJ25vdWlzbGlkZXInO1xyXG5pbXBvcnQgd051bWIgZnJvbSBcIndudW1iXCI7XHJcblxyXG4oZnVuY3Rpb24gbWFwQ2FsZW5kcmllciAoKSB7XHJcbiAgICBcclxuICAgIC8vbWFwXHJcbiAgICBjb25zdCBtYXAgPSBMLm1hcCgnbWFwJywge1xyXG4gICAgICAgIGNlbnRlcjogWzQ3LCAwXSxcclxuICAgICAgICB6b29tOiA1LFxyXG4gICAgICAgIG1pblpvb206IDMsXHJcbiAgICAgICAgbWF4Wm9vbTogMTYsXHJcbiAgICAgICAgem9vbUNvbnRyb2w6IHRydWUsXHJcbiAgICAgICAgbGF5ZXJzOiBbTC5lc3JpLmJhc2VtYXBMYXllcignVG9wb2dyYXBoaWMnKV1cclxuICAgIH0pO1xyXG4gICAgTC5jb250cm9sLnNjYWxlKCkuYWRkVG8obWFwKTsvL2JhcnJlIGVjaGVsbGVcclxuXHRjb25zdCBkZWZhdWx0TGF5ZXIgPSBMLmVzcmkuYmFzZW1hcExheWVyKFwiVG9wb2dyYXBoaWNcIikuYWRkVG8obWFwKTtcclxuICAgIG1hcC5hdHRyaWJ1dGlvbkNvbnRyb2wuYWRkQXR0cmlidXRpb24oJzxhIHRhcmdldD1cIl9ibGFua1wiIGhyZWY9XCJodHRwczovL2ZmdmVsby5mclwiPkbDqWTDqXJhdGlvbiBmcmFuw6dhaXNlIGRlIGN5Y2xvdG91cmlzbWU8L2E+Jyk7XHJcblxyXG4gICAgLy92YXJpYWJsZXMgZ2xvYmFsZXNcclxuICAgIGNvbnN0IG1hcEVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1hcFwiKTtcclxuICAgIGNvbnN0IHNpZGViYXJFbGVtZW50ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJzaWRlYmFyXCIpO1xyXG4gICAgY29uc3QgbWFwV2lkdGggPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1hcFwiKS5vZmZzZXRXaWR0aDtcclxuXHJcbiAgICAvL2JvdXRvbiB0b2dnbGUgc2lkZWJhclxyXG4gICAgc2lkZWJhclRvZ2dsZUNvbnRyb2woTC5Db250cm9sLCBtYXAsIHNpZGViYXJFbGVtZW50LCBtYXBFbGVtZW50LCBtYXBXaWR0aCk7XHJcblxyXG4gICAgLy8gY29udHJvbGUgZGVzIGNvdWNoZXMgZXQgY2hhbmdlbWVudCBkZSBmb25kIGRlIGNhcnRlXHJcblx0Y29uc3QgYmFzZUxheWVycyA9IHtcclxuXHRcdCdUb3BvZ3JhcGhpcXVlIChFc3JpKSc6IGRlZmF1bHRMYXllcixcclxuXHRcdCdTYXRlbGxpdGUgKEVzcmkpJzogTC5lc3JpLmJhc2VtYXBMYXllcignSW1hZ2VyeScpLFxyXG5cdFx0J09wZW5TdHJlZXRNYXAnOiAgTC50aWxlTGF5ZXIoJ2h0dHBzOi8ve3N9LnRpbGUub3BlbnN0cmVldG1hcC5vcmcve3p9L3t4fS97eX0ucG5nJywge1xyXG5cdFx0XHRhdHRyaWJ1dGlvbjogJyZjb3B5OyA8YSBocmVmPVwiaHR0cHM6Ly93d3cub3BlbnN0cmVldG1hcC5vcmcvY29weXJpZ2h0XCI+T3BlblN0cmVldE1hcDwvYT4nLFxyXG5cdFx0fSksXHJcblx0fTtcclxuXHRjb25zdCBvdmVybGF5TWFwcyA9IHtcclxuXHRcdFwiQ29sc1wiOiBjb2xzTGF5ZXIoKVxyXG5cdH07XHJcblx0TC5jb250cm9sLmxheWVycyhiYXNlTGF5ZXJzLCBvdmVybGF5TWFwcywge1xyXG5cdFx0cG9zaXRpb246IFwiYm90dG9tbGVmdFwiXHJcblx0fSkuYWRkVG8obWFwKTtcclxuXHRsYXllckNvbnRyb2xUaXRsZXMoKTtcclxuXHRtYXAuYWRkQ29udHJvbChuZXcgZ2VvbG9jYXRpb25CdXR0b24obWFwKSk7XHJcblxyXG4gICAgLy9sYXllciBtYW5pZmVzdGF0aW9uc1xyXG4gICAgY29uc3QgbWFuaWZzTGF5ZXIgPSBMLm1hcmtlckNsdXN0ZXJHcm91cCh7IFxyXG4gICAgICAgIGNodW5rZWRMb2FkaW5nOiB0cnVlLFxyXG4gICAgICAgIGljb25DcmVhdGVGdW5jdGlvbjogZnVuY3Rpb24gKGNsdXN0ZXIpIHtcclxuICAgICAgICAgICAgbGV0IGNvdW50ID0gY2x1c3Rlci5nZXRDaGlsZENvdW50KCk7XHJcbiAgICAgICAgICAgIGxldCBkaWdpdHMgPSAoY291bnQgKyAnJykubGVuZ3RoO1xyXG4gICAgICAgICAgICByZXR1cm4gTC5kaXZJY29uKHtcclxuICAgICAgICAgICAgICBodG1sOiBjb3VudCxcclxuICAgICAgICAgICAgICBjbGFzc05hbWU6ICdjbHVzdGVyIGRpZ2l0cy0nICsgZGlnaXRzLFxyXG4gICAgICAgICAgICAgIGljb25TaXplOiBudWxsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG5cdGNvbnN0IG1hbmlmcyA9IEwuZXNyaS5DbHVzdGVyLmZlYXR1cmVMYXllcih7XHJcbiAgICAgICAgdXJsOiBhZ29sRmVhdHVyZVNlcnZpY2VzLm1hbmlmcyxcclxuICAgICAgICB0b2tlbjogdG9rZW4sXHJcblx0XHRwb2ludFRvTGF5ZXI6IChmZWF0dXJlLCBsYXRsbmcpID0+IHtcclxuXHRcdFx0cmV0dXJuIEwubWFya2VyKGxhdGxuZywge1xyXG5cdFx0XHRcdGljb246IGljb25PaW5bMV1cclxuXHRcdFx0fSk7XHJcblx0XHR9LFxyXG5cdFx0aWNvbkNyZWF0ZUZ1bmN0aW9uOiAoY2x1c3RlcikgPT4ge1xyXG5cdFx0XHRsZXQgY291bnQgPSBjbHVzdGVyLmdldENoaWxkQ291bnQoKTtcclxuXHRcdFx0bGV0IGRpZ2l0cyA9IChjb3VudCArICcnKS5sZW5ndGg7XHJcblx0XHRcdHJldHVybiBMLmRpdkljb24oe1xyXG5cdFx0XHQgIGh0bWw6IGNvdW50LFxyXG5cdFx0XHQgIGNsYXNzTmFtZTogJ2NsdXN0ZXIgZGlnaXRzLScgKyBkaWdpdHMsXHJcblx0XHRcdCAgaWNvblNpemU6IG51bGxcclxuXHRcdFx0fSk7XHJcblx0XHQgIH0sXHJcbiAgICB9KS5hZGRUbyhtYW5pZnNMYXllcikuYWRkVG8obWFwKTtcclxuXHJcblx0Ly9wb3B1cHMgbWFuaWZzXHJcblx0bWFuaWZzLmJpbmRQb3B1cCgobGF5ZXIpPT57XHJcbiAgICAgICAgbGV0IG5vbSA9IGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5Ob207XHJcbiAgICAgICAgbGV0IGlkID0gbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk1hbmlmSWQ7XHJcbiAgICAgICAgbGV0IHR5cGUgPSBsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuVHlwZTtcclxuICAgICAgICBsZXQgZGF0ZV9kZWJ1dCA9IGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5BY2N1ZWlsX0RhdGVEYnQ7XHJcbiAgICAgICAgbGV0IHByb3V0ZSA9IGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5QcmF0aXF1ZV9Sb3V0ZSA/IFwiUm91dGVcIiA6IGZhbHNlO1xyXG4gICAgICAgIGxldCBwdnR0ID0gbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLlByYXRpcXVlX1ZUVCA/IFwiVlRUXCIgOiBmYWxzZTtcclxuICAgICAgICBsZXQgcGdyYXZlbCA9IGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5QcmF0aXF1ZV9HcmF2ZWwgPyBcIkdyYXZlbFwiIDogZmFsc2U7XHJcbiAgICAgICAgbGV0IHBtYXJjaGUgPSBsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuUHJhdGlxdWVfTWFyY2hlID8gXCJNYXJjaGVcIiA6IGZhbHNlO1xyXG4gICAgICAgIGxldCBkdXJhYmxlID0gbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkRldkR1cmFibGUgPyBcIsOJY28tcmVzcG9uc2FibGVcIiA6IGZhbHNlO1xyXG4gICAgICAgIGxldCBwc2ggPSBsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuQWNjZXNQU0ggPyBcIkFjY3VlaWwgUFNIXCIgOiBmYWxzZTtcclxuICAgICAgICBsZXQgc3BlZmVtID0gbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLlNwZUZlbSA/IFwiU3DDqWNpZmlxdWUgZmVtbWVzXCIgOiBmYWxzZTtcclxuICAgICAgICBsZXQgZW1haWwgPSBsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuQWRyZXNzZU1haWw7XHJcbiAgICAgICAgbGV0IHNpdGV3ZWIgPSBsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuQWRyZXNzZVdlYjtcclxuICAgICAgICByZXR1cm4gTC5VdGlsLnRlbXBsYXRlKFxyXG4gICAgICAgICAgICBwb3B1cFRlbXBsYXRlTWFuaWZzKG5vbSwgaWQsIHR5cGUsIGRhdGVfZGVidXQsIHByb3V0ZSwgcHZ0dCwgcGdyYXZlbCwgcG1hcmNoZSwgZHVyYWJsZSwgcHNoLCBzcGVmZW0sIGVtYWlsLCBzaXRld2ViKVxyXG4gICAgICAgICk7XHJcbiAgICB9KTtcclxuICAgIFxyXG4gICAgLypldmVuZW1lbnRzIGNhcnRlIGV0IGxheWVyIHBvaW50IGRlcGFydCovXHJcblx0bWFuaWZzLm9uY2UoJ2xvYWQnLCAoKSA9PiB7XHJcblx0XHRpZihsb2NhdGlvbi5wYXRobmFtZSAhPSBtZW51VXJsKXtcclxuXHRcdFx0bGV0IHBhcnRzID0gbG9jYXRpb24uaHJlZi5zcGxpdChtZW51VXJsK1wiL1wiKTtcclxuXHRcdFx0bGV0IHVybGlkID0gcGFydHNbMV0uc3BsaXQoXCItXCIpO1xyXG5cdFx0XHRyZW5kZXJJbmZvRmVhdHVyZShtYW5pZnMsIHVybGlkWzBdKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHpvb21Ub01hcmtlcnNBZnRlckxvYWQobWFwLCBtYW5pZnMpO1xyXG5cdFx0fTtcclxuXHRcdC8vc3luY1NpZGViYXIoKTtcclxuXHR9KTtcclxuXHRtYW5pZnMub24oJ3BvcHVwb3BlbicsICgpID0+IHtcclxuXHRcdGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYnV0dG9uUG9wdXBcIikuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB7XHJcblx0XHRcdHJlbmRlckluZm9GZWF0dXJlKG1hbmlmcywgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJidXR0b25Qb3B1cFwiKS5nZXRBdHRyaWJ1dGUoJ3ZhbHVlJykpXHJcblx0XHR9LCB0cnVlKTtcclxuXHR9KTtcclxuXHRtYW5pZnMub24oJ2NsaWNrJywgKGUpID0+IHtcclxuXHRcdGNvbnNvbGUubG9nKGUpO1xyXG5cdH0pO1xyXG5cdG1hcC5vbihcIm1vdmVlbmRcIiwgKCkgPT4ge1xyXG5cdFx0bWFuaWZzLnNldFdoZXJlKG1hbmlmcy5nZXRXaGVyZSgpKTtcclxuXHRcdHN5bmNTaWRlYmFyKCk7XHJcblx0fSk7XHJcblx0bWFwLm9uKCdvdmVybGF5cmVtb3ZlJywgKCkgPT4ge1xyXG5cdFx0bWFuaWZzTGF5ZXIuY2xlYXJMYXllcnMoKTtcclxuICAgIH0pO1xyXG4gICAgd2luZG93Lm9ucG9wc3RhdGUgPSAoKSA9PiB7XHJcbiAgICAgICAgYmFja0J1dHRvbk5hdmlnYXRpb24obWVudVVybCwgbWFwLCBtYXBFbGVtZW50LCBtYXBXaWR0aCwgbWFuaWZzLCByZW5kZXJJbmZvRmVhdHVyZSk7XHJcbiAgICB9XHJcblx0LypmaW4gZXZlbmVtZW50cyBjYXJ0ZSBldCBsYXllciBwb2ludCBkZXBhcnQqL1xyXG5cclxuICAgIC8vYmFycmUgZGUgcmVjaGVyY2hlIGdlb2NvZGV1clxyXG4gICAgY29uc3Qgc2VhcmNoQ29udHJvbCA9IEwuZXNyaS5HZW9jb2RpbmcuZ2Vvc2VhcmNoKHtcclxuICAgICAgICB1c2VNYXBCb3VuZHM6ZmFsc2UsXHJcbiAgICAgICAgdGl0bGU6ICdSZWNoZXJjaGVyIHVuIGxpZXUgLyB1bmUgYWRyZXNzZScsXHJcbiAgICAgICAgcGxhY2Vob2xkZXI6ICdPw7kgPycsXHJcbiAgICAgICAgZXhwYW5kZWQ6dHJ1ZSxcclxuICAgICAgICBjb2xsYXBzZUFmdGVyUmVzdWx0OiBmYWxzZSxcclxuICAgICAgICBwb3NpdGlvbjogJ3RvcHJpZ2h0JyxcclxuICAgICAgICBwcm92aWRlcnM6IFtcclxuICAgICAgICAgICAgYXJjZ2lzT25saW5lUHJvdmlkZXIsXHJcbiAgICAgICAgICAgIEwuZXNyaS5HZW9jb2RpbmcuZmVhdHVyZUxheWVyUHJvdmlkZXIoeyAvL3JlY2hlcmNoZSBkZXMgY291Y2hlcyBkYW5zIGxhIGdkYiBvaW5cclxuXHRcdFx0XHR1cmw6IGFnb2xGZWF0dXJlU2VydmljZXMubWFuaWZzLFxyXG4gICAgICAgICAgICAgICAgd2hlcmU6IG1hbmlmcy5nZXRXaGVyZSgpLFxyXG4gICAgICAgICAgICAgICAgdG9rZW46IHRva2VuLFxyXG5cdFx0XHRcdHNlYXJjaEZpZWxkczogWydNYW5pZklkJywnTm9tJywnU3RydWN0dXJlJ10sXHJcblx0XHRcdFx0bGFiZWw6ICdNQU5JRkVTVEFUSU9OUyBGRlZFTE86JyxcclxuXHRcdFx0XHRtYXhSZXN1bHRzOiA3LFxyXG5cdFx0XHRcdGJ1ZmZlclJhZGl1czogMTAwMCxcclxuXHRcdFx0XHRmb3JtYXRTdWdnZXN0aW9uOiAoZmVhdHVyZSkgPT4ge1xyXG5cdFx0XHRcdFx0cmV0dXJuIGAke2ZlYXR1cmUucHJvcGVydGllcy5Ob219IC0gJHtmZWF0dXJlLnByb3BlcnRpZXMuVHlwZX1gO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSlcclxuICAgICAgICBdXHJcbiAgICB9KS5hZGRUbyhtYXApO1xyXG4gICAgLy9nZW9jb2RldXIgZW4gZGVob3JzIGRlIGxhIGNhcnRlXHJcbiAgICBjb25zdCBlc3JpR2VvY29kZXIgPSBzZWFyY2hDb250cm9sLmdldENvbnRhaW5lcigpO1xyXG4gICAgc2V0RXNyaUdlb2NvZGVyKGVzcmlHZW9jb2RlciwgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2dlb2NvZGVyJykpO1xyXG4gICAgLy9yZWN1cCBsYSByZXF1ZXRlIGVuIGNvdXJzIGV0IGFwcGxpcXVlIGF1eCByZXN1bHRhdHMgZHUgZ2VvY29kZXVyXHJcblx0c2VhcmNoQ29udHJvbC5vbihcInJlcXVlc3RlbmRcIiwgZnVuY3Rpb24gKCkge1xyXG5cdFx0c2VhcmNoQ29udHJvbC5vcHRpb25zLnByb3ZpZGVyc1sxXS5vcHRpb25zLndoZXJlID0gbWFuaWZzLmdldFdoZXJlKCk7XHJcbiAgICB9KTtcclxuICAgIFxyXG4gICAgIC8vZm9ybWF0IGRlIGxhIGRhdGUgZGVzIG1hbmlmc1xyXG4gICAgIGNvbnN0IGhhbmRsZURhdGUgPSAoc3RyKSA9PiB7XHJcbiAgICAgICAgbGV0IGRhdGUgPSBzdHIuc3BsaXQoJy0nKTtcclxuICAgICAgICByZXR1cm4gZGF0ZVsyXSArIFwiL1wiICsgZGF0ZVsxXSArIFwiL1wiICsgZGF0ZVswXTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgY29uc3Qgc3luY1NpZGViYXIgPSAoYXJyUmVzdWx0c1NpZGViYXI9W10pID0+IHtcclxuICAgICAgICAkKFwiI2ZlYXR1cmVzXCIpLmVtcHR5KCk7XHJcbiAgICAgICAgbWFuaWZzLmVhY2hBY3RpdmVGZWF0dXJlKGZ1bmN0aW9uIChsYXllcikgeyAvL2VhY2hBY3RpdmVGZWF0dXJlID0gZm9uY3Rpb24gYSBham91dGVyIGRhbnMgZXNyaS1sZWFmbGV0LWNsdXN0ZXIuanMgLSBhdHRlbnRpb24gc2kgbWFqIGRlIGxhIGxpYnJhaXJpZVxyXG4gICAgICAgICAgICBpZiAobWFwLmhhc0xheWVyKG1hbmlmcykpIHtcclxuICAgICAgICAgICAgICAgIGlmIChtYXAuZ2V0Qm91bmRzKCkuY29udGFpbnMobGF5ZXIuZ2V0TGF0TG5nKCkpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXJyUmVzdWx0c1NpZGViYXIucHVzaChcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2lkZWJhclRlbXBsYXRlTWFuaWZzKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk1hbmlmSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVEYXRlKGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5BY2N1ZWlsX0RhdGVEYnQpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk5vbSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5UeXBlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLlByYXRpcXVlX1JvdXRlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLlByYXRpcXVlX1ZUVCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5QcmF0aXF1ZV9HcmF2ZWwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuUHJhdGlxdWVfTWFyY2hlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkFjY3VlaWxfQ1AsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuQWNjdWVpbF9Db21tdW5lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkRldkR1cmFibGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuQWNjZXNQU0gsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuU3BlRmVtXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgYWRkUmVzdWx0c1RvU2lkZWJhcihhcnJSZXN1bHRzU2lkZWJhciwgbWFuaWZzLCByZW5kZXJJbmZvRmVhdHVyZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gc2lkZWJhciBjYXJkIGh0bWxcclxuICAgIGNvbnN0IHNpZGViYXJUZW1wbGF0ZU1hbmlmcyA9IChpZCwgZGF0ZSwgbm9tLCB0eXBlLCByb3V0ZSwgdnR0LCBncmF2ZWwsIG1hcmNoZSwgY3AsIHZpbGxlLCBkdXJhYmxlLCBwc2gsIHNwZWZlbSkgPT4ge1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIGA8ZGl2IGNsYXNzPVwiY2FyZCBmZWF0dXJlLWNhcmQgbXktMiBwLTJcIiBpZD1cIiR7aWR9XCIgZGF0YS1sYXQ9XCJcIiBkYXRhLWxvbj1cIlwiPiBcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wgYWxpZ24tc2VsZi1jZW50ZXIgcm91bmRlZC1yaWdodFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxoNSBjbGFzcz1cImQtYmxvY2sgdGV4dC11cHBlcmNhc2VcIj48aSBjbGFzcz1cImZhciBmYS1jYWxlbmRhci1jaGVja1wiPjwvaT4gJHtkYXRlfTwvaDU+XHJcbiAgICAgICAgICAgICAgICAgICAgPGg2IGNsYXNzPVwiZC1ibG9jayB0ZXh0LXVwcGVyY2FzZVwiPiR7bm9tfTwvaDY+XHJcbiAgICAgICAgICAgICAgICAgICAgJHtyb3V0ZSA/IGA8c3BhbiBjbGFzcz1cImJhZGdlIGJhZGdlLXBpbGwgYmFkZ2UtbGlnaHQgbXktMSBtci0xXCI+Um91dGU8L3NwYW4+YCA6IGBgfVxyXG4gICAgICAgICAgICAgICAgICAgICR7dnR0ID8gYDxzcGFuIGNsYXNzPVwiYmFkZ2UgYmFkZ2UtcGlsbCBiYWRnZS1saWdodCBteS0xIG1yLTFcIj5WVFQ8L3NwYW4+YCA6IGBgfVxyXG4gICAgICAgICAgICAgICAgICAgICR7Z3JhdmVsID8gYDxzcGFuIGNsYXNzPVwiYmFkZ2UgYmFkZ2UtcGlsbCBiYWRnZS1saWdodCBteS0xIG1yLTFcIj5HcmF2ZWw8L3NwYW4+YCA6IGBgfVxyXG4gICAgICAgICAgICAgICAgICAgICR7bWFyY2hlID8gYDxzcGFuIGNsYXNzPVwiYmFkZ2UgYmFkZ2UtcGlsbCBiYWRnZS1saWdodCBteS0xIG1yLTFcIj5NYXJjaGU8L3NwYW4+YCA6IGBgfVxyXG4gICAgICAgICAgICAgICAgICAgICR7ZHVyYWJsZSA/IGA8c3BhbiBjbGFzcz1cImJhZGdlIGJhZGdlLXBpbGwgYmFkZ2UtbGlnaHQgbXktMSBtci0xXCI+w4ljby1yZXNwb25zYWJsZTwvc3Bhbj5gIDogYGB9XHJcbiAgICAgICAgICAgICAgICAgICAgJHtwc2ggPyBgPHNwYW4gY2xhc3M9XCJiYWRnZSBiYWRnZS1waWxsIGJhZGdlLWxpZ2h0IG15LTEgbXItMVwiPkFjY8OocyBQU0g8L3NwYW4+YCA6IGBgfVxyXG4gICAgICAgICAgICAgICAgICAgICR7c3BlZmVtID8gYDxzcGFuIGNsYXNzPVwiYmFkZ2UgYmFkZ2UtcGlsbCBiYWRnZS1saWdodCBteS0xIG1yLTFcIj5TcMOpY2lmaXF1ZSBmZW1tZXM8L3NwYW4+YCA6IGBgfVxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiYmFkZ2UgYmFkZ2UtcGlsbCBiYWRnZS1saWdodCBteS0xIG1yLTFcIj4ke3R5cGV9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwibXktMSBtci0xXCI+PGJyLz48aSBjbGFzcz1cImZhcyBmYS1tYXAtbWFya2VkLWFsdFwiPjwvaT4gJHtjcH0sICR7dmlsbGV9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PmBcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8vY29tcG9zYW50IGZpY2hlIGRlc2NyaXB0aXZlXHJcbiAgICBjb25zdCByZW5kZXJJbmZvRmVhdHVyZSA9IChMYXllciwgaWQpID0+IHtcclxuICAgICAgICBMYXllci5lYWNoRmVhdHVyZShmdW5jdGlvbiAobGF5ZXIpIHtcclxuICAgICAgICAgICAgaWYobGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk1hbmlmSWQgPT0gaWQpe1xyXG4gICAgICAgICAgICAgICAgY29uc3QgaGFuZGxlR3B4TWFuaWZzID0gKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBncHhGaWxlcyA9IGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5GaWNoaWVyR3B4ID8gbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkZpY2hpZXJHcHguc3BsaXQoJywnKSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBncHhGaWxlcztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGxldCBpbmZvZmVhdHVyZUVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnaW5mb2ZlYXR1cmUnKTtcclxuICAgICAgICAgICAgICAgIHVubW91bnRDb21wb25lbnRBdE5vZGUoaW5mb2ZlYXR1cmVFbGVtZW50KTtcclxuICAgICAgICAgICAgICAgIHJlbmRlcihcdFxyXG4gICAgICAgICAgICAgICAgICAgIDxJbmZvRmVhdHVyZSBcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWRNYW5pZj17aWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5vbT17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk5vbX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgQWNjZXNQU0g9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5BY2Nlc1BTSH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgQWNjdWVpbF9DUD17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkFjY3VlaWxfQ1B9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEFjY3VlaWxfQ29tbXVuZT17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkFjY3VlaWxfQ29tbXVuZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgQWNjdWVpbF9EYXRlRGJ0PXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuQWNjdWVpbF9EYXRlRGJ0fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBBY2N1ZWlsX0xpZXU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5BY2N1ZWlsX0xpZXV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEFjY3VlaWxfQWRyZXNzZT17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkFjY3VlaWxfQWRyZXNzZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgQWRyZXNzZU1haWw9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5BZHJlc3NlTWFpbH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgQWRyZXNzZVdlYj17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkFkcmVzc2VXZWJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEFycml2ZWVfQ1A9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5BcnJpdmVlX0NQfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBBcnJpdmVlX0NvbW11bmU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5BcnJpdmVlX0NvbW11bmV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEFycml2ZWVfRGF0ZUZpbj17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkFycml2ZWVfRGF0ZUZpbn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgQXJyaXZlZV9MaWV1PXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuQXJyaXZlZV9MaWV1fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBDYXRlZ29yaWVMYWJlbD17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkNhdGVnb3JpZUxhYmVsfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBDb250YWN0X05vbT17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkNvbnRhY3RfTm9tfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBDb250YWN0X1ByZW5vbT17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkNvbnRhY3RfUHJlbm9tfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBEZXZEdXJhYmxlPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuRGV2RHVyYWJsZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgRmljaGllckZseWVyPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuRmljaGllckZseWVyfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBGaWNoaWVyR3B4PXtoYW5kbGVHcHhNYW5pZnMoKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgSW5zY3JpcHRpb25MaWduZT17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkluc2NyaXB0aW9uTGlnbmV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE1vZGVsZUlkPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuTW9kZWxlSWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE51bWVyb0xhYmVsPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuTnVtZXJvTGFiZWx9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pbk1hbmlmTGljVGFyaWZBZHVsdGU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5NYW5pZkxpY1RhcmlmQWR1bHRlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5NYW5pZkxpY1RhcmlmSmV1bmU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5NYW5pZkxpY1RhcmlmSmV1bmV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pbk1hbmlmTm9uTGljVGFyaWZBZHVsdGU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5NYW5pZk5vbkxpY1RhcmlmQWR1bHRlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5NYW5pZk5vbkxpY1RhcmlmSmV1bmU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5NYW5pZk5vbkxpY1RhcmlmSmV1bmV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pbk1hbmlmUmVzZXJ2ZUxpY2VuY2llPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luTWFuaWZSZXNlcnZlTGljZW5jaWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luTWFuaWZPYnNlcnZhdGlvbn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luTWFyY2hlRGlzdF8xPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luTWFyY2hlRGlzdF8xfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5NYXJjaGVEaXN0XzI9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5NYXJjaGVEaXN0XzJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pbk1hcmNoZURpc3RfMz17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk9pbk1hcmNoZURpc3RfM31cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luTWFyY2hlRGlzdF80PXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luTWFyY2hlRGlzdF80fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5NYXJjaGVEaXN0XzU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5NYXJjaGVEaXN0XzV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pbk1hcmNoZURpc3RfNj17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk9pbk1hcmNoZURpc3RfNn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luUm91dGVEZW5pdl8xPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luUm91dGVEZW5pdl8xfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5Sb3V0ZURlbml2XzI9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5Sb3V0ZURlbml2XzJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pblJvdXRlRGVuaXZfMz17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk9pblJvdXRlRGVuaXZfM31cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luUm91dGVEZW5pdl80PXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luUm91dGVEZW5pdl80fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5Sb3V0ZURlbml2XzU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5Sb3V0ZURlbml2XzV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pblJvdXRlRGVuaXZfNj17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk9pblJvdXRlRGVuaXZfNn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luUm91dGVEaWZmXzE9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5Sb3V0ZURpZmZfMX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luUm91dGVEaWZmXzI9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5Sb3V0ZURpZmZfMn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luUm91dGVEaWZmXzM9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5Sb3V0ZURpZmZfM31cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luUm91dGVEaWZmXzQ9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5Sb3V0ZURpZmZfNH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luUm91dGVEaWZmXzU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5Sb3V0ZURpZmZfNX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luUm91dGVEaWZmXzY9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5Sb3V0ZURpZmZfNn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luUm91dGVEaXN0XzE9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5Sb3V0ZURpc3RfMX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luUm91dGVEaXN0XzI9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5Sb3V0ZURpc3RfMn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luUm91dGVEaXN0XzM9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5Sb3V0ZURpc3RfM31cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luUm91dGVEaXN0XzQ9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5Sb3V0ZURpc3RfNH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luUm91dGVEaXN0XzU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5Sb3V0ZURpc3RfNX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luUm91dGVEaXN0XzY9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5Sb3V0ZURpc3RfNn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luVlRURGVuaXZfMT17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk9pblZUVERlbml2XzF9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pblZUVERlbml2XzI9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5WVFREZW5pdl8yfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5WVFREZW5pdl8zPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luVlRURGVuaXZfM31cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luVlRURGVuaXZfND17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk9pblZUVERlbml2XzR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pblZUVERlbml2XzU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5WVFREZW5pdl81fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5WVFREZW5pdl82PXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luVlRURGVuaXZfNn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luVlRURGlmZl8xPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luVlRURGlmZl8xfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5WVFREaWZmXzI9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5WVFREaWZmXzJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pblZUVERpZmZfMz17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk9pblZUVERpZmZfM31cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luVlRURGlmZl80PXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luVlRURGlmZl80fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5WVFREaWZmXzU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5WVFREaWZmXzV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pblZUVERpZmZfNj17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk9pblZUVERpZmZfNn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luVlRURGlzdF8xPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luVlRURGlzdF8xfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5WVFREaXN0XzI9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5WVFREaXN0XzJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pblZUVERpc3RfMz17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk9pblZUVERpc3RfM31cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luVlRURGlzdF80PXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luVlRURGlzdF80fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5WVFREaXN0XzU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5WVFREaXN0XzV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pblZUVERpc3RfNj17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk9pblZUVERpc3RfNn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luR3JhdmVsRGVuaXZfMT17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk9pbkdyYXZlbERlbml2XzF9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pbkdyYXZlbERlbml2XzI9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5HcmF2ZWxEZW5pdl8yfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5HcmF2ZWxEZW5pdl8zPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luR3JhdmVsRGVuaXZfM31cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luR3JhdmVsRGVuaXZfND17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk9pbkdyYXZlbERlbml2XzR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pbkdyYXZlbERlbml2XzU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5HcmF2ZWxEZW5pdl81fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5HcmF2ZWxEZW5pdl82PXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luR3JhdmVsRGVuaXZfNn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luR3JhdmVsRGlmZl8xPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luR3JhdmVsRGlmZl8xfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5HcmF2ZWxEaWZmXzI9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5HcmF2ZWxEaWZmXzJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pbkdyYXZlbERpZmZfMz17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk9pbkdyYXZlbERpZmZfM31cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luR3JhdmVsRGlmZl80PXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luR3JhdmVsRGlmZl80fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5HcmF2ZWxEaWZmXzU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5HcmF2ZWxEaWZmXzV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pbkdyYXZlbERpZmZfNj17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk9pbkdyYXZlbERpZmZfNn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luR3JhdmVsRGlzdF8xPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luR3JhdmVsRGlzdF8xfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5HcmF2ZWxEaXN0XzI9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5HcmF2ZWxEaXN0XzJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pbkdyYXZlbERpc3RfMz17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk9pbkdyYXZlbERpc3RfM31cclxuICAgICAgICAgICAgICAgICAgICAgICAgT2luR3JhdmVsRGlzdF80PXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuT2luR3JhdmVsRGlzdF80fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBPaW5HcmF2ZWxEaXN0XzU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5PaW5HcmF2ZWxEaXN0XzV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE9pbkdyYXZlbERpc3RfNj17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk9pbkdyYXZlbERpc3RfNn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgUHJhdGlxdWVfR3JhdmVsPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuUHJhdGlxdWVfR3JhdmVsfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBQcmF0aXF1ZV9NYXJjaGU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5QcmF0aXF1ZV9NYXJjaGV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFByYXRpcXVlX1JvdXRlPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuUHJhdGlxdWVfUm91dGV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFByYXRpcXVlX1ZUVD17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLlByYXRpcXVlX1ZUVH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgU3BlRmVtPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuU3BlRmVtfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBTdHJ1Y3R1cmU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5TdHJ1Y3R1cmV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFN0cnVjdHVyZURlcD17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLlN0cnVjdHVyZURlcH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgU3RydWN0dXJlUmVnPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuU3RydWN0dXJlUmVnfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBUeXBlPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuVHlwZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaGV1cmVEZUNsb3R1cmU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5oZXVyZURlQ2xvdHVyZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaGV1cmVzPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuaGV1cmVzfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICByb290Q29udGFpbmVyPXtpbmZvZmVhdHVyZUVsZW1lbnR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcD17bWFwfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXBXaWR0aCA9IHttYXBXaWR0aH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgTGF5ZXI9e0xheWVyfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYXllcj17bGF5ZXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lbnVVcmw9e21lbnVVcmx9XHJcbiAgICAgICAgICAgICAgICAgICAgLz4sIGluZm9mZWF0dXJlRWxlbWVudFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIG5hdmlnYXRpb25IaXN0b3J5KGlkLCBtZW51VXJsLGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5zbHVnKTtcclxuICAgICAgICAgICAgICAgIC8vZ2VzdGlvbiBkZSBsYSBjYXJ0ZVxyXG4gICAgICAgICAgICAgICAgbGV0IHJlc2l6ZSA9IG1hcFdpZHRoLWRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiaW5mby1mZWF0dXJlLWNvbnRlbnRcIikub2Zmc2V0V2lkdGg7XHJcbiAgICAgICAgICAgICAgICBtYXBFbGVtZW50LnN0eWxlLndpZHRoPXJlc2l6ZStcInB4XCI7XHJcbiAgICAgICAgICAgICAgICBtYXAuaW52YWxpZGF0ZVNpemUoKTtcclxuICAgICAgICAgICAgICAgIG1hcC5zZXRWaWV3KFtsYXllci5mZWF0dXJlLmdlb21ldHJ5LmNvb3JkaW5hdGVzWzFdLCBsYXllci5mZWF0dXJlLmdlb21ldHJ5LmNvb3JkaW5hdGVzWzBdXSwgMTIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLypmaWx0cmVzIGRlIHJlY2hlcmNoZXMgKi9cclxuICAgIGNvbnN0IGRhdGVGaWx0ZXIgPSAoKSA9PiB7XHJcbiAgICAgICAgZmxhdHBpY2tyKFwiI2ZsYXRwaWNrclwiLCB7XHJcbiAgICAgICAgICAgIGxvY2FsZToge1xyXG4gICAgICAgICAgICAgICAgZmlyc3REYXlPZldlZWs6IDFcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgXCJsb2NhbGVcIjogXCJmclwiLFxyXG4gICAgICAgICAgICBzaG93TW9udGhzOiAyLFxyXG4gICAgICAgICAgICBkYXRlRm9ybWF0OiBcImQtbS1ZXCIsXHJcbiAgICAgICAgICAgIC8vZGVmYXVsdERhdGU6IFwidG9kYXlcIixcclxuICAgICAgICAgICAgbWluRGF0ZTogXCJ0b2RheVwiLFxyXG4gICAgICAgICAgICBtb2RlOiBcInJhbmdlXCIsXHJcbiAgICAgICAgICAgIGFsdElucHV0OiB0cnVlLFxyXG4gICAgICAgICAgICBkaXNhYmxlTW9iaWxlOiBcInRydWVcIixcclxuICAgICAgICAgICAgYWx0Rm9ybWF0OiBcImQtbS1ZXCIsXHJcbiAgICAgICAgICAgIG9uQ2hhbmdlOiBmdW5jdGlvbiAoc2VsZWN0ZWREYXRlcywgZGF0ZVN0ciwgaW5zdGFuY2UpIHtcclxuICAgICAgICAgICAgICAgIGxldCBkYXRlc3RhcnQgPSBzZWxlY3RlZERhdGVzWzBdO1xyXG4gICAgICAgICAgICAgICAgbGV0IGRhdGVlbmQgPSBuZXcgRGF0ZShzZWxlY3RlZERhdGVzWzFdKTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGVzdGFydCwgZGF0ZWVuZCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH07ZGF0ZUZpbHRlcigpO1xyXG5cdGNvbnN0IGRpc3RTbGlkZXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZGlzdFNsaWRlcicpO1xyXG5cdG5vVWlTbGlkZXIuY3JlYXRlKGRpc3RTbGlkZXIsIHtcclxuXHRcdHN0YXJ0OiBbMCwgMjUwXSxcclxuXHRcdGNvbm5lY3Q6IHRydWUsXHJcblx0XHRyYW5nZToge1xyXG5cdFx0XHQnbWluJzogMCxcclxuXHRcdFx0J21heCc6IDI1MFxyXG5cdFx0fSxcclxuXHRcdGZvcm1hdDogd051bWIoe1xyXG5cdFx0XHRkZWNpbWFsczogMCwgLy8gZGVmYXVsdCBpcyAyXHJcblx0XHR9KSxcclxuXHRcdHBpcHM6IHttb2RlOiAnY291bnQnLCB2YWx1ZXM6IDV9LFxyXG5cdH0pO1xyXG5cdGNvbnN0IGRlbml2U2xpZGVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2Rlbml2U2xpZGVyJyk7XHJcblx0bm9VaVNsaWRlci5jcmVhdGUoZGVuaXZTbGlkZXIsIHtcclxuXHRcdHN0YXJ0OiBbMCwgMTAwMF0sXHJcblx0XHRjb25uZWN0OiB0cnVlLFxyXG5cdFx0cmFuZ2U6IHtcclxuXHRcdFx0J21pbic6IDAsXHJcblx0XHRcdCdtYXgnOiAxMDAwXHJcblx0XHR9LFxyXG5cdFx0Zm9ybWF0OiB3TnVtYih7XHJcblx0XHRcdGRlY2ltYWxzOiAwLCAvLyBkZWZhdWx0IGlzIDJcclxuXHRcdH0pLFxyXG5cdFx0cGlwczoge21vZGU6ICdjb3VudCcsIHZhbHVlczogM30sXHJcblx0fSk7XHJcblx0Y29uc3QgdGFnaWZ5Q2x1YnMgPSBuZXcgVGFnaWZ5KGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJ0ZXh0YXJlYVtuYW1lPXRhZ3NDbHVic11cIiksIHtcclxuICAgICAgICBlbmZvcmVXaGl0ZWxpc3Q6IHRydWUsXHJcbiAgICAgICAgd2hpdGVsaXN0IDogW10sXHJcbiAgICAgICAgZHVwbGljYXRlczogZmFsc2UsXHJcbiAgICB9KTtcclxuXHJcblx0Y29uc3QgZ2V0RmlsdGVycz0oZmlsdGVycz1bXSk9PntcclxuXHRcdGNvbnN0IGVsZW1lbnRzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi50eXBlVmVsb0ZpbHRlclwiKTtcclxuXHRcdFsuLi5lbGVtZW50c10ubWFwKGl0ZW0gPT4ge1xyXG5cdFx0XHRpdGVtLmFkZEV2ZW50TGlzdGVuZXIoJ2NoYW5nZScsIChlKSA9PiB7XHJcblx0XHRcdFx0aWYgKGl0ZW0uY2hlY2tlZCA9PT0gdHJ1ZSkge1xyXG5cdFx0XHRcdFx0ZmlsdGVycy5wdXNoKGl0ZW0udmFsdWUpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRmaWx0ZXJzLnNwbGljZShmaWx0ZXJzLmluZGV4T2YoaXRlbS52YWx1ZSksMSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHNldEZpbHRlcnMoZmlsdGVycyk7XHJcblx0XHRcdH0pO1xyXG5cdFx0fSk7XHJcblx0XHR0YWdpZnlDbHVic1xyXG4gICAgICAgICAgICAub24oJ2FkZCcsIChlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBmaWx0ZXJzLnB1c2goZS5kZXRhaWwuZGF0YS52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICBmaWx0ZXJzPSBmaWx0ZXJzLmpvaW4oXCJcIik7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5vbigncmVtb3ZlJywgKGUpID0+IHtcclxuICAgICAgICAgICAgICAgIGZpbHRlcnMuc3BsaWNlKGZpbHRlcnMuaW5kZXhPZihlLmRldGFpbC5kYXRhLnZhbHVlKSwxKTtcclxuICAgICAgICAgICAgICAgIGZpbHRlcnM9IGZpbHRlcnMuam9pbihcIlwiKTtcclxuICAgICAgICAgICAgfSk7XHJcblx0XHRkaXN0U2xpZGVyLm5vVWlTbGlkZXIub24oXCJjaGFuZ2VcIiwgKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgbGV0IGRpc3RMaXN0PVtcclxuICAgICAgICAgICAgICAgIFwiT2luUm91dGVEaXN0XzFcIiwgXCJPaW5Sb3V0ZURpc3RfMlwiLCBcIk9pblJvdXRlRGlzdF8zXCIsIFwiT2luUm91dGVEaXN0XzRcIiwgXCJPaW5Sb3V0ZURpc3RfNVwiLCBcIk9pblJvdXRlRGlzdF82XCIsXHJcbiAgICAgICAgICAgICAgICBcIk9pblZUVERpc3RfMVwiLCBcIk9pblZUVERpc3RfMlwiLCBcIk9pblZUVERpc3RfM1wiLCBcIk9pblZUVERpc3RfNFwiLCBcIk9pblZUVERpc3RfNVwiLCBcIk9pblZUVERpc3RfNlwiLFxyXG4gICAgICAgICAgICAgICAgXCJPaW5HcmF2ZWxEaXN0XzFcIiwgXCJPaW5HcmF2ZWxEaXN0XzJcIiwgXCJPaW5HcmF2ZWxEaXN0XzNcIiwgXCJPaW5HcmF2ZWxEaXN0XzRcIiwgXCJPaW5HcmF2ZWxEaXN0XzVcIiwgXCJPaW5HcmF2ZWxEaXN0XzZcIixcclxuICAgICAgICAgICAgICAgIFwiT2luTWFyY2hlRGlzdF8xXCIsIFwiT2luTWFyY2hlRGlzdF8yXCIsIFwiT2luTWFyY2hlRGlzdF8zXCIsIFwiT2luTWFyY2hlRGlzdF80XCIsIFwiT2luTWFyY2hlRGlzdF81XCIsIFwiT2luTWFyY2hlRGlzdF82XCIsXHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgZGlzdExpc3QubWFwKChkaXN0KT0+e2ZpbHRlcnMuc3BsaWNlKGZpbHRlcnMuaW5kZXhPZihkaXN0KSwxKX0pO1xyXG4gICAgICAgICAgICBkaXN0TGlzdC5tYXAoKGRpc3QpPT57XHJcbiAgICAgICAgICAgICAgICBpZiAoKGRhdGFbMF0gPiAwKSAmJiAoZGF0YVsxXSA8IDI1MCkpIHtcclxuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJzLnB1c2goYCR7ZGlzdH0gPj0gJHtkYXRhWzBdfSBBTkQgJHtkaXN0fSA8PSAke2RhdGFbMV19YCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKChkYXRhWzBdID4gMCkgJiYgKGRhdGFbMV0gPT0gMjUwKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcnMucHVzaChgJHtkaXN0fSA+PSAke2RhdGFbMF19YCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKChkYXRhWzBdID09IDApICYmIChkYXRhWzFdIDwgMjUwKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcnMucHVzaChgJHtkaXN0fSA+IDAgQU5EICR7ZGlzdH0gPD0gJHtkYXRhWzFdfWApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgc2V0RmlsdGVycyhmaWx0ZXJzKTtcclxuXHRcdH0pO1xyXG5cdFx0ZGVuaXZTbGlkZXIubm9VaVNsaWRlci5vbihcImNoYW5nZVwiLCAoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICBsZXQgZGVuaXZMaXN0PVtcclxuICAgICAgICAgICAgICAgIFwiT2luUm91dGVEZW5pdl8xXCIsIFwiT2luUm91dGVEZW5pdl8yXCIsIFwiT2luUm91dGVEZW5pdl8zXCIsIFwiT2luUm91dGVEZW5pdl80XCIsIFwiT2luUm91dGVEZW5pdl81XCIsIFwiT2luUm91dGVEZW5pdl82XCIsXHJcbiAgICAgICAgICAgICAgICBcIk9pblZUVERlbml2XzFcIiwgXCJPaW5WVFREZW5pdl8yXCIsIFwiT2luVlRURGVuaXZfM1wiLCBcIk9pblZUVERlbml2XzRcIiwgXCJPaW5WVFREZW5pdl81XCIsIFwiT2luVlRURGVuaXZfNlwiLFxyXG4gICAgICAgICAgICAgICAgXCJPaW5HcmF2ZWxEZW5pdl8xXCIsIFwiT2luR3JhdmVsRGVuaXZfMlwiLCBcIk9pbkdyYXZlbERlbml2XzNcIiwgXCJPaW5HcmF2ZWxEZW5pdl80XCIsIFwiT2luR3JhdmVsRGVuaXZfNVwiLCBcIk9pbkdyYXZlbERlbml2XzZcIixcclxuICAgICAgICAgICAgICAgIFwiT2luTWFyY2hlRGVuaXZfMVwiLCBcIk9pbk1hcmNoZURlbml2XzJcIiwgXCJPaW5NYXJjaGVEZW5pdl8zXCIsIFwiT2luTWFyY2hlRGVuaXZfNFwiLCBcIk9pbk1hcmNoZURlbml2XzVcIiwgXCJPaW5NYXJjaGVEZW5pdl82XCIsXHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgLy9kZW5pdkxpc3QubWFwKChkZW5pdik9PntmaWx0ZXJzLnNwbGljZShmaWx0ZXJzLmluZGV4T2YoZGVuaXYpLDEpfSk7XHJcbiAgICAgICAgICAgIGRlbml2TGlzdC5tYXAoKGRlbml2KT0+e1xyXG4gICAgICAgICAgICAgICAgaWYgKChkYXRhWzBdID4gMCkgJiYgKGRhdGFbMV0gPD0gMTAwMCkpIHtcclxuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJzLnB1c2goYCR7ZGVuaXZ9ID49ICR7ZGF0YVswXX0gQU5EICR7ZGVuaXZ9IDw9ICR7ZGF0YVsxXX1gKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoKGRhdGFbMF0gPiAwKSAmJiAoZGF0YVsxXSA9PSAxMDAwKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcnMucHVzaChgJHtkZW5pdn0gPj0gJHtkYXRhWzBdfWApO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICgoZGF0YVswXSA9PSAwKSAmJiAoZGF0YVsxXSA8IDEwMDApKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZmlsdGVycy5wdXNoKGAke2Rlbml2fSA+IDAgQU5EICR7ZGVuaXZ9IDw9ICR7ZGF0YVsxXX1gKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHNldEZpbHRlcnMoZmlsdGVycyk7XHJcblx0XHR9KTtcclxuICAgIH07Z2V0RmlsdGVycygpO1xyXG4gICAgXHJcblx0Y29uc3Qgc2V0RmlsdGVycyA9IChmaWx0ZXJzLCBhcnJSZXN1bHQ9W10pID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhmaWx0ZXJzKTtcclxuXHRcdGxldCB0eXBlID0gZmlsdGVycy5maWx0ZXIoZiA9PiBmLmluY2x1ZGVzKFwiUHJhdGlxdWVfXCIpKS5qb2luKFwiIE9SIFwiKTtcclxuICAgICAgICBsZXQgZGlzdGFuY2UgPSBmaWx0ZXJzLmZpbHRlcihmID0+IGYuaW5jbHVkZXMoXCJEaXN0X1wiKSkuam9pbihcIiBPUiBcIik7XHJcbiAgICAgICAgbGV0IGRlbml2ZWxlID0gZmlsdGVycy5maWx0ZXIoZiA9PiBmLmluY2x1ZGVzKFwiRGVuaXZfXCIpKS5qb2luKFwiIE9SIFwiKTtcclxuXHJcbiAgICAgICAgZGlzdGFuY2UubGVuZ3RoPjAgPyBhcnJSZXN1bHQucHVzaChgKCR7ZGlzdGFuY2V9KWApIDogZmFsc2U7XHJcbiAgICAgICAgdHlwZS5sZW5ndGg+MCA/IGFyclJlc3VsdC5wdXNoKGAoJHt0eXBlfSlgKSA6IGZhbHNlO1xyXG4gICAgICAgIGRlbml2ZWxlLmxlbmd0aD4wID8gYXJyUmVzdWx0LnB1c2goYCgke2Rlbml2ZWxlfSlgKSA6IGZhbHNlO1xyXG4gICAgICAgIFxyXG5cdFx0bGV0IHJlc3VsdCA9IGFyclJlc3VsdC5qb2luKFwiIEFORCBcIik7XHJcblx0XHQvL2NvbnNvbGUubG9nKHJlc3VsdCk7XHJcblx0XHRtYW5pZnMuc2V0V2hlcmUocmVzdWx0KTtcclxuXHRcdHpvb21Ub01hcmtlcnNBZnRlckxvYWQgKG1hcCwgbWFuaWZzKTtcclxuXHR9XHJcbiAgICAvKmZpbiBkZXMgZmlsdHJlcyBkZSByZWNoZXJjaGVzICovXHJcblxyXG4gICAgXHJcbn0pKCk7XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=