(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["clubs"],{

/***/ "./assets/js/pages/clubs.js":
/*!**********************************!*\
  !*** ./assets/js/pages/clubs.js ***!
  \**********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.concat.js */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_includes_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.includes.js */ "./node_modules/core-js/modules/es.array.includes.js");
/* harmony import */ var core_js_modules_es_array_includes_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_includes_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.regexp.exec.js */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_string_includes_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.string.includes.js */ "./node_modules/core-js/modules/es.string.includes.js");
/* harmony import */ var core_js_modules_es_string_includes_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_includes_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.string.split.js */ "./node_modules/core-js/modules/es.string.split.js");
/* harmony import */ var core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _services_mapConfig__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/mapConfig */ "./assets/js/services/mapConfig.js");
/* harmony import */ var _components_popup__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/popup */ "./assets/js/components/popup.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _components_InfoFeature__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/InfoFeature */ "./assets/js/components/InfoFeature.jsx");
/* harmony import */ var _yaireo_tagify__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @yaireo/tagify */ "./node_modules/@yaireo/tagify/dist/tagify.min.js");
/* harmony import */ var _yaireo_tagify__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_yaireo_tagify__WEBPACK_IMPORTED_MODULE_11__);













(function mapClubs() {
  var map = L.map('map', {
    center: [47, 0],
    zoom: 5,
    minZoom: 3,
    zoomControl: false,
    layers: [L.esri.basemapLayer('Topographic')]
  });
  map.attributionControl.addAttribution('<a target="_blank" href="https://ffvelo.fr">Fédération française de cyclotourisme</a>'); //variables globales

  var mapElement = document.getElementById("map");
  var mapWidth = document.getElementById("map").offsetWidth;
  var clubsLayer = L.markerClusterGroup({
    chunkedLoading: true,
    iconCreateFunction: function iconCreateFunction(cluster) {
      var count = cluster.getChildCount();
      var digits = (count + '').length;
      return L.divIcon({
        html: count,
        className: 'cluster digits-' + digits,
        iconSize: null
      });
    }
  }); //layer clubs

  var clubs = L.esri.Cluster.featureLayer({
    url: _services_mapConfig__WEBPACK_IMPORTED_MODULE_6__["agolFeatureServices"].clubs,
    token: token,
    pointToLayer: function pointToLayer(feature, latlng) {
      return L.marker(latlng, {
        icon: _services_mapConfig__WEBPACK_IMPORTED_MODULE_6__["iconOin"][2]
      });
    },
    iconCreateFunction: function iconCreateFunction(cluster) {
      var count = cluster.getChildCount();
      var digits = (count + '').length;
      return L.divIcon({
        html: count,
        className: 'cluster digits-' + digits,
        iconSize: null
      });
    }
  }).addTo(clubsLayer).addTo(map); //popups clubs

  clubs.bindPopup(function (layer) {
    var nom = layer.feature.properties.StructureNom;
    var ville = layer.feature.properties.AdrVille;
    var id = layer.feature.properties.StructureCode;
    var pratiqueRoute = layer.feature.properties.Pratique_Route;
    var pratiqueVttVtc = layer.feature.properties.Pratique_VTT_VTC;
    var tel = layer.feature.properties.AdrTel;
    var email = layer.feature.properties.AdrMail;
    var siteweb = layer.feature.properties.AdrWeb;
    var logo = layer.feature.properties.Logo;
    return L.Util.template(Object(_components_popup__WEBPACK_IMPORTED_MODULE_7__["popupTemplateClubs"])(nom, ville, id, pratiqueRoute, pratiqueVttVtc, tel, email, siteweb, logo));
  });
  /*evenements carte et layer point depart*/

  clubs.once('load', function () {
    if (location.pathname != menuUrl) {
      var parts = location.href.split(menuUrl + "/");
      var urlid = parts[1].split("-");
      renderInfoFeature(clubs, urlid[0]);
    } else {
      Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_6__["zoomToMarkersAfterLoad"])(map, clubs);
    }

    ; //syncSidebar();
  });
  clubs.on('popupopen', function () {
    document.getElementById("buttonPopup").addEventListener('click', function () {
      renderInfoFeature(clubs, document.getElementById("buttonPopup").getAttribute('value'));
    }, true);
  });
  clubs.on('click', function (e) {
    console.log(e);
  });
  map.on("moveend", function () {
    clubs.setWhere(clubs.getWhere());
    syncSidebar();
  });
  map.on('overlayremove', function () {
    clubsLayer.clearLayers();
  });

  window.onpopstate = function () {
    backButtonNavigation(menuUrl, map, mapElement, mapWidth, clubs, renderInfoFeature);
  };
  /*fin evenements carte et layer point depart*/
  //barre de recherche geocodage - saisir une adresse et creer un point	


  var searchControl = L.esri.Geocoding.geosearch({
    useMapBounds: false,
    title: 'Rechercher un lieu / une adresse',
    placeholder: 'Où ?',
    expanded: true,
    collapseAfterResult: false,
    position: 'topright',
    providers: [_services_mapConfig__WEBPACK_IMPORTED_MODULE_6__["arcgisOnlineProvider"], L.esri.Geocoding.featureLayerProvider({
      //recherche des couches dans la gdb oin
      url: _services_mapConfig__WEBPACK_IMPORTED_MODULE_6__["agolFeatureServices"].clubs,
      where: clubs.getWhere(),
      token: token,
      searchFields: ['StructureCode', 'StructureNom'],
      label: 'CLUBS FFVELO:',
      maxResults: 7,
      bufferRadius: 1000,
      formatSuggestion: function formatSuggestion(feature) {
        return "".concat(feature.properties.StructureCode, " - ").concat(feature.properties.StructureNom);
      }
    })]
  }).addTo(map); //geocodeur en dehors de la carte

  var esriGeocoder = searchControl.getContainer();
  Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_6__["setEsriGeocoder"])(esriGeocoder, document.getElementById('geocoder')); //recup la requete en cours et applique aux resultats du geocodeur

  searchControl.on("requestend", function () {
    searchControl.options.providers[1].options.where = clubs.getWhere();
  });

  var syncSidebar = function syncSidebar() {
    var arrResultsSidebar = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    $("#features").empty();
    clubs.eachActiveFeature(function (layer) {
      if (map.hasLayer(clubs)) {
        if (map.getBounds().contains(layer.getLatLng())) {
          arrResultsSidebar.push(sidebarTemplateClubs(layer.feature.properties.StructureCode, layer.feature.properties.StructureNom, layer.feature.properties.Pratique_Route, layer.feature.properties.Pratique_VTT_VTC, layer.feature.properties.VAE, layer.feature.properties.AccueilHandicap, layer.feature.properties.AccueilJeune, layer.feature.properties.EcoleCyclo, layer.feature.properties.VeloEcole, layer.feature.properties.AdrWeb, layer.feature.properties.AdrCP, layer.feature.properties.AdrVille));
        }
      }
    });
    Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_6__["addResultsToSidebar"])(arrResultsSidebar, clubs, renderInfoFeature);
  }; // bookmarks html


  var sidebarTemplateClubs = function sidebarTemplateClubs(id, nom, pratiqueRoute, pratiqueVttVtc, vae, accueilHandicap, accueilJeune, ecoleCyclo, veloEcole, siteweb, cp, ville) {
    siteweb = siteweb.includes("http") ? siteweb : "http://" + siteweb;
    return "<div class=\"card feature-card my-2 p-2\" id=\"".concat(id, "\" data-lat=\"\" data-lon=\"\"> \n                <div class=\"col align-self-center rounded-right\">\n                    <h6 class=\"d-block text-uppercase\">").concat(nom, "</h6>\n                    ").concat(pratiqueRoute ? "<span class=\"badge badge-pill badge-light my-1 mr-1\">Route</span>" : "", "\n                    ").concat(pratiqueVttVtc ? "<span class=\"badge badge-pill badge-light my-1 mr-1\">VTT/VTC</span>" : "", "\n                    ").concat(vae ? "<span class=\"badge badge-pill badge-light my-1 mr-1\">V\xE9lo \xE0 assistance \xE9lectrique</span>" : "", "\n                    ").concat(accueilHandicap ? "<span class=\"badge badge-pill badge-light my-1 mr-1\">Accueil handicap</span>" : "", "\n                    ").concat(accueilJeune ? "<span class=\"badge badge-pill badge-light my-1 mr-1\">Accueil jeune</span>" : "", "\n                    ").concat(ecoleCyclo ? "<span class=\"badge badge-pill badge-light my-1 mr-1\">\xC9cole cyclo</span>" : "", "\n                    ").concat(veloEcole ? "<span class=\"badge badge-pill badge-light my-1 mr-1\">V\xE9lo-\xE9cole</span>" : "", "\n                    ").concat(siteweb ? "<span class=\"my-1 mr-1\"><br/><i class=\"fas fa-globe\"></i> <a href='".concat(siteweb, "' target='_blank'>Site web</a></span>") : "", "\n                    <span class=\"my-1 mr-1\"><br/><i class=\"fas fa-map-marked-alt\"></i> ").concat(cp, ", ").concat(ville, "</span>\n                </div>\n            </div>");
  }; //composant fiche descriptive


  var renderInfoFeature = function renderInfoFeature(Layer, id) {
    Layer.eachFeature(function (layer) {
      if (layer.feature.properties.StructureCode == id) {
        var infofeatureElement = document.getElementById('infofeature');
        Object(react_dom__WEBPACK_IMPORTED_MODULE_9__["unmountComponentAtNode"])(infofeatureElement);
        var lfp = layer.feature.properties;
        Object(react_dom__WEBPACK_IMPORTED_MODULE_9__["render"])( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(_components_InfoFeature__WEBPACK_IMPORTED_MODULE_10__["default"], {
          idClub: id,
          StructureCodeDepartement: lfp.StructureCodeDepartement,
          StructureCodeRegion: lfp.StructureCodeRegion,
          nom: lfp.StructureNom,
          descr: "",
          AccueilHandicap: lfp.AccueilHandicap ? "Accueil Handicap" : "",
          AccueilJeune: lfp.AccueilJeune ? "Accueil Jeune" : "",
          EcoleCyclo: lfp.EcoleCyclo ? "École Cyclo" : "",
          vae: lfp.VAE ? "Vélo à assistance électrique" : "",
          VeloEcole: lfp.VeloEcole ? "Vélo-école" : "",
          Pratique_Route: lfp.Pratique_Route ? "Route" : "",
          Pratique_VTT_VTC: lfp.Pratique_VTT_VTC ? "VTT / VTC" : "",
          AdrEscalier: lfp.AdrEscalier,
          AdrNumVoie: lfp.AdrNumVoie,
          AdrNomVoie: lfp.AdrNomVoie,
          AdrCP: lfp.AdrCP,
          AdrVille: lfp.AdrVille,
          Nom_Correspondant: lfp.Nom_Correspondant,
          Prenom_Correspondant: lfp.Prenom_Correspondant,
          Mail_Correspondant: lfp.Mail_Correspondant,
          Tel_Correspondant: lfp.Tel_Correspondant,
          logo: lfp.Logo,
          rootContainer: infofeatureElement,
          map: map,
          mapWidth: mapWidth,
          Layer: Layer,
          layer: layer,
          menuUrl: menuUrl
        }), infofeatureElement);
        Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_6__["navigationHistory"])(id, menuUrl, lfp.slug); //gestion de la carte

        var resize = mapWidth - document.getElementById("info-feature-content").offsetWidth;
        mapElement.style.width = resize + "px";
        map.invalidateSize();
        map.setView([layer.feature.geometry.coordinates[1], layer.feature.geometry.coordinates[0]], 12);
      }
    });
  };

  var tagifyDeps = new _yaireo_tagify__WEBPACK_IMPORTED_MODULE_11___default.a(document.querySelector("textarea[name=tagsDeps]"), {
    enforeWhitelist: true,
    whitelist: [],
    duplicates: false
  });
  var tagifyRegs = new _yaireo_tagify__WEBPACK_IMPORTED_MODULE_11___default.a(document.querySelector("textarea[name=tagsRegs]"), {
    enforeWhitelist: true,
    whitelist: [],
    duplicates: false
  });
})();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ })

},[["./assets/js/pages/clubs.js","runtime","vendors~app~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_velor~10347b53","vendors~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~61364f2f","vendors~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~cotatio~d7e263c1","vendors~bonnes_adresses~calendrier~circuits~clubs~labels_velo~login_licencie","vendors~bonnes_adresses~calendrier~circuits~clubs~labels_velo","vendors~calendrier~circuits~clubs~labels_velo","bonnes_adresses~calendrier~circuits~clubs~labels_velo"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvcGFnZXMvY2x1YnMuanMiXSwibmFtZXMiOlsibWFwQ2x1YnMiLCJtYXAiLCJMIiwiY2VudGVyIiwiem9vbSIsIm1pblpvb20iLCJ6b29tQ29udHJvbCIsImxheWVycyIsImVzcmkiLCJiYXNlbWFwTGF5ZXIiLCJhdHRyaWJ1dGlvbkNvbnRyb2wiLCJhZGRBdHRyaWJ1dGlvbiIsIm1hcEVsZW1lbnQiLCJkb2N1bWVudCIsImdldEVsZW1lbnRCeUlkIiwibWFwV2lkdGgiLCJvZmZzZXRXaWR0aCIsImNsdWJzTGF5ZXIiLCJtYXJrZXJDbHVzdGVyR3JvdXAiLCJjaHVua2VkTG9hZGluZyIsImljb25DcmVhdGVGdW5jdGlvbiIsImNsdXN0ZXIiLCJjb3VudCIsImdldENoaWxkQ291bnQiLCJkaWdpdHMiLCJsZW5ndGgiLCJkaXZJY29uIiwiaHRtbCIsImNsYXNzTmFtZSIsImljb25TaXplIiwiY2x1YnMiLCJDbHVzdGVyIiwiZmVhdHVyZUxheWVyIiwidXJsIiwiYWdvbEZlYXR1cmVTZXJ2aWNlcyIsInRva2VuIiwicG9pbnRUb0xheWVyIiwiZmVhdHVyZSIsImxhdGxuZyIsIm1hcmtlciIsImljb24iLCJpY29uT2luIiwiYWRkVG8iLCJiaW5kUG9wdXAiLCJsYXllciIsIm5vbSIsInByb3BlcnRpZXMiLCJTdHJ1Y3R1cmVOb20iLCJ2aWxsZSIsIkFkclZpbGxlIiwiaWQiLCJTdHJ1Y3R1cmVDb2RlIiwicHJhdGlxdWVSb3V0ZSIsIlByYXRpcXVlX1JvdXRlIiwicHJhdGlxdWVWdHRWdGMiLCJQcmF0aXF1ZV9WVFRfVlRDIiwidGVsIiwiQWRyVGVsIiwiZW1haWwiLCJBZHJNYWlsIiwic2l0ZXdlYiIsIkFkcldlYiIsImxvZ28iLCJMb2dvIiwiVXRpbCIsInRlbXBsYXRlIiwicG9wdXBUZW1wbGF0ZUNsdWJzIiwib25jZSIsImxvY2F0aW9uIiwicGF0aG5hbWUiLCJtZW51VXJsIiwicGFydHMiLCJocmVmIiwic3BsaXQiLCJ1cmxpZCIsInJlbmRlckluZm9GZWF0dXJlIiwiem9vbVRvTWFya2Vyc0FmdGVyTG9hZCIsIm9uIiwiYWRkRXZlbnRMaXN0ZW5lciIsImdldEF0dHJpYnV0ZSIsImUiLCJjb25zb2xlIiwibG9nIiwic2V0V2hlcmUiLCJnZXRXaGVyZSIsInN5bmNTaWRlYmFyIiwiY2xlYXJMYXllcnMiLCJ3aW5kb3ciLCJvbnBvcHN0YXRlIiwiYmFja0J1dHRvbk5hdmlnYXRpb24iLCJzZWFyY2hDb250cm9sIiwiR2VvY29kaW5nIiwiZ2Vvc2VhcmNoIiwidXNlTWFwQm91bmRzIiwidGl0bGUiLCJwbGFjZWhvbGRlciIsImV4cGFuZGVkIiwiY29sbGFwc2VBZnRlclJlc3VsdCIsInBvc2l0aW9uIiwicHJvdmlkZXJzIiwiYXJjZ2lzT25saW5lUHJvdmlkZXIiLCJmZWF0dXJlTGF5ZXJQcm92aWRlciIsIndoZXJlIiwic2VhcmNoRmllbGRzIiwibGFiZWwiLCJtYXhSZXN1bHRzIiwiYnVmZmVyUmFkaXVzIiwiZm9ybWF0U3VnZ2VzdGlvbiIsImVzcmlHZW9jb2RlciIsImdldENvbnRhaW5lciIsInNldEVzcmlHZW9jb2RlciIsIm9wdGlvbnMiLCJhcnJSZXN1bHRzU2lkZWJhciIsIiQiLCJlbXB0eSIsImVhY2hBY3RpdmVGZWF0dXJlIiwiaGFzTGF5ZXIiLCJnZXRCb3VuZHMiLCJjb250YWlucyIsImdldExhdExuZyIsInB1c2giLCJzaWRlYmFyVGVtcGxhdGVDbHVicyIsIlZBRSIsIkFjY3VlaWxIYW5kaWNhcCIsIkFjY3VlaWxKZXVuZSIsIkVjb2xlQ3ljbG8iLCJWZWxvRWNvbGUiLCJBZHJDUCIsImFkZFJlc3VsdHNUb1NpZGViYXIiLCJ2YWUiLCJhY2N1ZWlsSGFuZGljYXAiLCJhY2N1ZWlsSmV1bmUiLCJlY29sZUN5Y2xvIiwidmVsb0Vjb2xlIiwiY3AiLCJpbmNsdWRlcyIsIkxheWVyIiwiZWFjaEZlYXR1cmUiLCJpbmZvZmVhdHVyZUVsZW1lbnQiLCJ1bm1vdW50Q29tcG9uZW50QXROb2RlIiwibGZwIiwicmVuZGVyIiwiU3RydWN0dXJlQ29kZURlcGFydGVtZW50IiwiU3RydWN0dXJlQ29kZVJlZ2lvbiIsIkFkckVzY2FsaWVyIiwiQWRyTnVtVm9pZSIsIkFkck5vbVZvaWUiLCJOb21fQ29ycmVzcG9uZGFudCIsIlByZW5vbV9Db3JyZXNwb25kYW50IiwiTWFpbF9Db3JyZXNwb25kYW50IiwiVGVsX0NvcnJlc3BvbmRhbnQiLCJuYXZpZ2F0aW9uSGlzdG9yeSIsInNsdWciLCJyZXNpemUiLCJzdHlsZSIsIndpZHRoIiwiaW52YWxpZGF0ZVNpemUiLCJzZXRWaWV3IiwiZ2VvbWV0cnkiLCJjb29yZGluYXRlcyIsInRhZ2lmeURlcHMiLCJUYWdpZnkiLCJxdWVyeVNlbGVjdG9yIiwiZW5mb3JlV2hpdGVsaXN0Iiwid2hpdGVsaXN0IiwiZHVwbGljYXRlcyIsInRhZ2lmeVJlZ3MiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLENBQUMsU0FBU0EsUUFBVCxHQUFxQjtBQUVsQixNQUFNQyxHQUFHLEdBQUdDLENBQUMsQ0FBQ0QsR0FBRixDQUFNLEtBQU4sRUFBYTtBQUNyQkUsVUFBTSxFQUFFLENBQUMsRUFBRCxFQUFLLENBQUwsQ0FEYTtBQUVyQkMsUUFBSSxFQUFFLENBRmU7QUFHckJDLFdBQU8sRUFBRSxDQUhZO0FBSXJCQyxlQUFXLEVBQUUsS0FKUTtBQUtyQkMsVUFBTSxFQUFFLENBQUNMLENBQUMsQ0FBQ00sSUFBRixDQUFPQyxZQUFQLENBQW9CLGFBQXBCLENBQUQ7QUFMYSxHQUFiLENBQVo7QUFPQVIsS0FBRyxDQUFDUyxrQkFBSixDQUF1QkMsY0FBdkIsQ0FBc0MsdUZBQXRDLEVBVGtCLENBV2xCOztBQUNBLE1BQU1DLFVBQVUsR0FBR0MsUUFBUSxDQUFDQyxjQUFULENBQXdCLEtBQXhCLENBQW5CO0FBQ0EsTUFBTUMsUUFBUSxHQUFHRixRQUFRLENBQUNDLGNBQVQsQ0FBd0IsS0FBeEIsRUFBK0JFLFdBQWhEO0FBQ0EsTUFBTUMsVUFBVSxHQUFHZixDQUFDLENBQUNnQixrQkFBRixDQUFxQjtBQUNwQ0Msa0JBQWMsRUFBRSxJQURvQjtBQUVwQ0Msc0JBQWtCLEVBQUUsNEJBQVVDLE9BQVYsRUFBbUI7QUFDbkMsVUFBSUMsS0FBSyxHQUFHRCxPQUFPLENBQUNFLGFBQVIsRUFBWjtBQUNBLFVBQUlDLE1BQU0sR0FBRyxDQUFDRixLQUFLLEdBQUcsRUFBVCxFQUFhRyxNQUExQjtBQUNBLGFBQU92QixDQUFDLENBQUN3QixPQUFGLENBQVU7QUFDakJDLFlBQUksRUFBRUwsS0FEVztBQUVqQk0saUJBQVMsRUFBRSxvQkFBb0JKLE1BRmQ7QUFHakJLLGdCQUFRLEVBQUU7QUFITyxPQUFWLENBQVA7QUFLSDtBQVZtQyxHQUFyQixDQUFuQixDQWRrQixDQTJCakI7O0FBQ0osTUFBTUMsS0FBSyxHQUFHNUIsQ0FBQyxDQUFDTSxJQUFGLENBQU91QixPQUFQLENBQWVDLFlBQWYsQ0FBNEI7QUFDbkNDLE9BQUcsRUFBRUMsdUVBQW1CLENBQUNKLEtBRFU7QUFFbkNLLFNBQUssRUFBRUEsS0FGNEI7QUFHekNDLGdCQUFZLEVBQUUsc0JBQUNDLE9BQUQsRUFBVUMsTUFBVixFQUFxQjtBQUNsQyxhQUFPcEMsQ0FBQyxDQUFDcUMsTUFBRixDQUFTRCxNQUFULEVBQWlCO0FBQ3ZCRSxZQUFJLEVBQUVDLDJEQUFPLENBQUMsQ0FBRDtBQURVLE9BQWpCLENBQVA7QUFHQSxLQVB3QztBQVF6Q3JCLHNCQUFrQixFQUFFLDRCQUFDQyxPQUFELEVBQWE7QUFDaEMsVUFBSUMsS0FBSyxHQUFHRCxPQUFPLENBQUNFLGFBQVIsRUFBWjtBQUNBLFVBQUlDLE1BQU0sR0FBRyxDQUFDRixLQUFLLEdBQUcsRUFBVCxFQUFhRyxNQUExQjtBQUNBLGFBQU92QixDQUFDLENBQUN3QixPQUFGLENBQVU7QUFDZkMsWUFBSSxFQUFFTCxLQURTO0FBRWZNLGlCQUFTLEVBQUUsb0JBQW9CSixNQUZoQjtBQUdmSyxnQkFBUSxFQUFFO0FBSEssT0FBVixDQUFQO0FBS0U7QUFoQnNDLEdBQTVCLEVBaUJSYSxLQWpCUSxDQWlCRnpCLFVBakJFLEVBaUJVeUIsS0FqQlYsQ0FpQmdCekMsR0FqQmhCLENBQWQsQ0E1QnFCLENBK0NyQjs7QUFDQTZCLE9BQUssQ0FBQ2EsU0FBTixDQUFnQixVQUFDQyxLQUFELEVBQVM7QUFDbEIsUUFBSUMsR0FBRyxHQUFHRCxLQUFLLENBQUNQLE9BQU4sQ0FBY1MsVUFBZCxDQUF5QkMsWUFBbkM7QUFDQSxRQUFJQyxLQUFLLEdBQUdKLEtBQUssQ0FBQ1AsT0FBTixDQUFjUyxVQUFkLENBQXlCRyxRQUFyQztBQUNBLFFBQUlDLEVBQUUsR0FBR04sS0FBSyxDQUFDUCxPQUFOLENBQWNTLFVBQWQsQ0FBeUJLLGFBQWxDO0FBQ0EsUUFBSUMsYUFBYSxHQUFDUixLQUFLLENBQUNQLE9BQU4sQ0FBY1MsVUFBZCxDQUF5Qk8sY0FBM0M7QUFDQSxRQUFJQyxjQUFjLEdBQUNWLEtBQUssQ0FBQ1AsT0FBTixDQUFjUyxVQUFkLENBQXlCUyxnQkFBNUM7QUFDQSxRQUFJQyxHQUFHLEdBQUdaLEtBQUssQ0FBQ1AsT0FBTixDQUFjUyxVQUFkLENBQXlCVyxNQUFuQztBQUNBLFFBQUlDLEtBQUssR0FBR2QsS0FBSyxDQUFDUCxPQUFOLENBQWNTLFVBQWQsQ0FBeUJhLE9BQXJDO0FBQ0EsUUFBSUMsT0FBTyxHQUFHaEIsS0FBSyxDQUFDUCxPQUFOLENBQWNTLFVBQWQsQ0FBeUJlLE1BQXZDO0FBQ0EsUUFBSUMsSUFBSSxHQUFHbEIsS0FBSyxDQUFDUCxPQUFOLENBQWNTLFVBQWQsQ0FBeUJpQixJQUFwQztBQUNBLFdBQU83RCxDQUFDLENBQUM4RCxJQUFGLENBQU9DLFFBQVAsQ0FDSEMsNEVBQWtCLENBQUNyQixHQUFELEVBQU1HLEtBQU4sRUFBYUUsRUFBYixFQUFpQkUsYUFBakIsRUFBZ0NFLGNBQWhDLEVBQWdERSxHQUFoRCxFQUFxREUsS0FBckQsRUFBNERFLE9BQTVELEVBQXFFRSxJQUFyRSxDQURmLENBQVA7QUFHSCxHQWJKO0FBZUc7O0FBQ0hoQyxPQUFLLENBQUNxQyxJQUFOLENBQVcsTUFBWCxFQUFtQixZQUFNO0FBQ3hCLFFBQUdDLFFBQVEsQ0FBQ0MsUUFBVCxJQUFxQkMsT0FBeEIsRUFBZ0M7QUFDL0IsVUFBSUMsS0FBSyxHQUFHSCxRQUFRLENBQUNJLElBQVQsQ0FBY0MsS0FBZCxDQUFvQkgsT0FBTyxHQUFDLEdBQTVCLENBQVo7QUFDQSxVQUFJSSxLQUFLLEdBQUdILEtBQUssQ0FBQyxDQUFELENBQUwsQ0FBU0UsS0FBVCxDQUFlLEdBQWYsQ0FBWjtBQUNBRSx1QkFBaUIsQ0FBQzdDLEtBQUQsRUFBUTRDLEtBQUssQ0FBQyxDQUFELENBQWIsQ0FBakI7QUFDQSxLQUpELE1BSU87QUFDTkUsd0ZBQXNCLENBQUMzRSxHQUFELEVBQU02QixLQUFOLENBQXRCO0FBQ0E7O0FBQUEsS0FQdUIsQ0FReEI7QUFDQSxHQVREO0FBVUFBLE9BQUssQ0FBQytDLEVBQU4sQ0FBUyxXQUFULEVBQXNCLFlBQU07QUFDM0JoRSxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsYUFBeEIsRUFBdUNnRSxnQkFBdkMsQ0FBd0QsT0FBeEQsRUFBaUUsWUFBTTtBQUN0RUgsdUJBQWlCLENBQUM3QyxLQUFELEVBQVFqQixRQUFRLENBQUNDLGNBQVQsQ0FBd0IsYUFBeEIsRUFBdUNpRSxZQUF2QyxDQUFvRCxPQUFwRCxDQUFSLENBQWpCO0FBQ0EsS0FGRCxFQUVHLElBRkg7QUFHQSxHQUpEO0FBS0FqRCxPQUFLLENBQUMrQyxFQUFOLENBQVMsT0FBVCxFQUFrQixVQUFDRyxDQUFELEVBQU87QUFDeEJDLFdBQU8sQ0FBQ0MsR0FBUixDQUFZRixDQUFaO0FBQ0EsR0FGRDtBQUdBL0UsS0FBRyxDQUFDNEUsRUFBSixDQUFPLFNBQVAsRUFBa0IsWUFBTTtBQUN2Qi9DLFNBQUssQ0FBQ3FELFFBQU4sQ0FBZXJELEtBQUssQ0FBQ3NELFFBQU4sRUFBZjtBQUNBQyxlQUFXO0FBQ1gsR0FIRDtBQUlBcEYsS0FBRyxDQUFDNEUsRUFBSixDQUFPLGVBQVAsRUFBd0IsWUFBTTtBQUM3QjVELGNBQVUsQ0FBQ3FFLFdBQVg7QUFDRyxHQUZKOztBQUdHQyxRQUFNLENBQUNDLFVBQVAsR0FBb0IsWUFBTTtBQUN0QkMsd0JBQW9CLENBQUNuQixPQUFELEVBQVVyRSxHQUFWLEVBQWVXLFVBQWYsRUFBMkJHLFFBQTNCLEVBQXFDZSxLQUFyQyxFQUE0QzZDLGlCQUE1QyxDQUFwQjtBQUNILEdBRkQ7QUFHQTtBQUVBOzs7QUFDQSxNQUFNZSxhQUFhLEdBQUd4RixDQUFDLENBQUNNLElBQUYsQ0FBT21GLFNBQVAsQ0FBaUJDLFNBQWpCLENBQTJCO0FBQzdDQyxnQkFBWSxFQUFDLEtBRGdDO0FBRTdDQyxTQUFLLEVBQUUsa0NBRnNDO0FBRzdDQyxlQUFXLEVBQUUsTUFIZ0M7QUFJN0NDLFlBQVEsRUFBQyxJQUpvQztBQUs3Q0MsdUJBQW1CLEVBQUUsS0FMd0I7QUFNN0NDLFlBQVEsRUFBRSxVQU5tQztBQU83Q0MsYUFBUyxFQUFFLENBQ1BDLHdFQURPLEVBRVBsRyxDQUFDLENBQUNNLElBQUYsQ0FBT21GLFNBQVAsQ0FBaUJVLG9CQUFqQixDQUFzQztBQUFFO0FBQ2hEcEUsU0FBRyxFQUFFQyx1RUFBbUIsQ0FBQ0osS0FEcUI7QUFFbEN3RSxXQUFLLEVBQUV4RSxLQUFLLENBQUNzRCxRQUFOLEVBRjJCO0FBR2xDakQsV0FBSyxFQUFFQSxLQUgyQjtBQUk5Q29FLGtCQUFZLEVBQUUsQ0FBQyxlQUFELEVBQWlCLGNBQWpCLENBSmdDO0FBSzlDQyxXQUFLLEVBQUUsZUFMdUM7QUFNOUNDLGdCQUFVLEVBQUUsQ0FOa0M7QUFPOUNDLGtCQUFZLEVBQUUsSUFQZ0M7QUFROUNDLHNCQUFnQixFQUFFLDBCQUFDdEUsT0FBRCxFQUFhO0FBQzlCLHlCQUFVQSxPQUFPLENBQUNTLFVBQVIsQ0FBbUJLLGFBQTdCLGdCQUFnRGQsT0FBTyxDQUFDUyxVQUFSLENBQW1CQyxZQUFuRTtBQUNBO0FBVjZDLEtBQXRDLENBRk87QUFQa0MsR0FBM0IsRUFzQm5CTCxLQXRCbUIsQ0FzQmJ6QyxHQXRCYSxDQUF0QixDQS9Ga0IsQ0FzSGxCOztBQUNBLE1BQU0yRyxZQUFZLEdBQUdsQixhQUFhLENBQUNtQixZQUFkLEVBQXJCO0FBQ0FDLDZFQUFlLENBQUNGLFlBQUQsRUFBZS9GLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixVQUF4QixDQUFmLENBQWYsQ0F4SGtCLENBeUhsQjs7QUFDSDRFLGVBQWEsQ0FBQ2IsRUFBZCxDQUFpQixZQUFqQixFQUErQixZQUFZO0FBQzFDYSxpQkFBYSxDQUFDcUIsT0FBZCxDQUFzQlosU0FBdEIsQ0FBZ0MsQ0FBaEMsRUFBbUNZLE9BQW5DLENBQTJDVCxLQUEzQyxHQUFtRHhFLEtBQUssQ0FBQ3NELFFBQU4sRUFBbkQ7QUFDQSxHQUZEOztBQUlHLE1BQU1DLFdBQVcsR0FBRyxTQUFkQSxXQUFjLEdBQTBCO0FBQUEsUUFBekIyQixpQkFBeUIsdUVBQVAsRUFBTztBQUMxQ0MsS0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlQyxLQUFmO0FBQ0FwRixTQUFLLENBQUNxRixpQkFBTixDQUF3QixVQUFVdkUsS0FBVixFQUFpQjtBQUNyQyxVQUFJM0MsR0FBRyxDQUFDbUgsUUFBSixDQUFhdEYsS0FBYixDQUFKLEVBQXlCO0FBQ3JCLFlBQUk3QixHQUFHLENBQUNvSCxTQUFKLEdBQWdCQyxRQUFoQixDQUF5QjFFLEtBQUssQ0FBQzJFLFNBQU4sRUFBekIsQ0FBSixFQUFpRDtBQUM3Q1AsMkJBQWlCLENBQUNRLElBQWxCLENBQ0lDLG9CQUFvQixDQUNoQjdFLEtBQUssQ0FBQ1AsT0FBTixDQUFjUyxVQUFkLENBQXlCSyxhQURULEVBRWhCUCxLQUFLLENBQUNQLE9BQU4sQ0FBY1MsVUFBZCxDQUF5QkMsWUFGVCxFQUdoQkgsS0FBSyxDQUFDUCxPQUFOLENBQWNTLFVBQWQsQ0FBeUJPLGNBSFQsRUFJaEJULEtBQUssQ0FBQ1AsT0FBTixDQUFjUyxVQUFkLENBQXlCUyxnQkFKVCxFQUtoQlgsS0FBSyxDQUFDUCxPQUFOLENBQWNTLFVBQWQsQ0FBeUI0RSxHQUxULEVBTWhCOUUsS0FBSyxDQUFDUCxPQUFOLENBQWNTLFVBQWQsQ0FBeUI2RSxlQU5ULEVBT2hCL0UsS0FBSyxDQUFDUCxPQUFOLENBQWNTLFVBQWQsQ0FBeUI4RSxZQVBULEVBUWhCaEYsS0FBSyxDQUFDUCxPQUFOLENBQWNTLFVBQWQsQ0FBeUIrRSxVQVJULEVBU2hCakYsS0FBSyxDQUFDUCxPQUFOLENBQWNTLFVBQWQsQ0FBeUJnRixTQVRULEVBVWhCbEYsS0FBSyxDQUFDUCxPQUFOLENBQWNTLFVBQWQsQ0FBeUJlLE1BVlQsRUFXaEJqQixLQUFLLENBQUNQLE9BQU4sQ0FBY1MsVUFBZCxDQUF5QmlGLEtBWFQsRUFZaEJuRixLQUFLLENBQUNQLE9BQU4sQ0FBY1MsVUFBZCxDQUF5QkcsUUFaVCxDQUR4QjtBQWdCSDtBQUNKO0FBQ0osS0FyQkQ7QUFzQkErRSxtRkFBbUIsQ0FBQ2hCLGlCQUFELEVBQW9CbEYsS0FBcEIsRUFBMkI2QyxpQkFBM0IsQ0FBbkI7QUFDSCxHQXpCRCxDQTlIa0IsQ0F5SmxCOzs7QUFDQSxNQUFNOEMsb0JBQW9CLEdBQUcsU0FBdkJBLG9CQUF1QixDQUFDdkUsRUFBRCxFQUFJTCxHQUFKLEVBQVFPLGFBQVIsRUFBc0JFLGNBQXRCLEVBQXFDMkUsR0FBckMsRUFBeUNDLGVBQXpDLEVBQXlEQyxZQUF6RCxFQUFzRUMsVUFBdEUsRUFBaUZDLFNBQWpGLEVBQTJGekUsT0FBM0YsRUFBbUcwRSxFQUFuRyxFQUFzR3RGLEtBQXRHLEVBQWdIO0FBQ3pJWSxXQUFPLEdBQUdBLE9BQU8sQ0FBQzJFLFFBQVIsQ0FBaUIsTUFBakIsSUFBMkIzRSxPQUEzQixHQUFxQyxZQUFZQSxPQUEzRDtBQUNBLG9FQUNtRFYsRUFEbkQsNktBR2lETCxHQUhqRCx3Q0FJY08sYUFBYSwyRUFBd0UsRUFKbkcsbUNBS2NFLGNBQWMsNkVBQTJFLEVBTHZHLG1DQU1jMkUsR0FBRywyR0FBK0YsRUFOaEgsbUNBT2NDLGVBQWUsc0ZBQW1GLEVBUGhILG1DQVFjQyxZQUFZLG1GQUFpRixFQVIzRyxtQ0FTY0MsVUFBVSxvRkFBK0UsRUFUdkcsbUNBVWNDLFNBQVMsc0ZBQThFLEVBVnJHLG1DQVdjekUsT0FBTyxvRkFBeUVBLE9BQXpFLDZDQUEwSCxFQVgvSSwwR0FZaUYwRSxFQVpqRixlQVl3RnRGLEtBWnhGO0FBZ0JILEdBbEJELENBMUprQixDQThLbEI7OztBQUNBLE1BQU0yQixpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLENBQUM2RCxLQUFELEVBQVF0RixFQUFSLEVBQWU7QUFDckNzRixTQUFLLENBQUNDLFdBQU4sQ0FBa0IsVUFBVTdGLEtBQVYsRUFBaUI7QUFDL0IsVUFBR0EsS0FBSyxDQUFDUCxPQUFOLENBQWNTLFVBQWQsQ0FBeUJLLGFBQXpCLElBQTBDRCxFQUE3QyxFQUFnRDtBQUM1QyxZQUFJd0Ysa0JBQWtCLEdBQUc3SCxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsYUFBeEIsQ0FBekI7QUFDQTZILGdGQUFzQixDQUFDRCxrQkFBRCxDQUF0QjtBQUNBLFlBQUlFLEdBQUcsR0FBQ2hHLEtBQUssQ0FBQ1AsT0FBTixDQUFjUyxVQUF0QjtBQUNBK0YsZ0VBQU0sZUFDRiwyREFBQyxnRUFBRDtBQUNJLGdCQUFNLEVBQUUzRixFQURaO0FBRUksa0NBQXdCLEVBQUkwRixHQUFHLENBQUNFLHdCQUZwQztBQUdJLDZCQUFtQixFQUFJRixHQUFHLENBQUNHLG1CQUgvQjtBQUlJLGFBQUcsRUFBRUgsR0FBRyxDQUFDN0YsWUFKYjtBQUtJLGVBQUssRUFBRSxFQUxYO0FBTUkseUJBQWUsRUFBRTZGLEdBQUcsQ0FBQ2pCLGVBQUosR0FBc0Isa0JBQXRCLEdBQTJDLEVBTmhFO0FBT0ksc0JBQVksRUFBSWlCLEdBQUcsQ0FBQ2hCLFlBQUosR0FBbUIsZUFBbkIsR0FBcUMsRUFQekQ7QUFRSSxvQkFBVSxFQUFJZ0IsR0FBRyxDQUFDZixVQUFKLEdBQWlCLGFBQWpCLEdBQWlDLEVBUm5EO0FBU0ksYUFBRyxFQUFJZSxHQUFHLENBQUNsQixHQUFKLEdBQVUsOEJBQVYsR0FBMkMsRUFUdEQ7QUFVSSxtQkFBUyxFQUFJa0IsR0FBRyxDQUFDZCxTQUFKLEdBQWdCLFlBQWhCLEdBQStCLEVBVmhEO0FBV0ksd0JBQWMsRUFBRWMsR0FBRyxDQUFDdkYsY0FBSixHQUFxQixPQUFyQixHQUErQixFQVhuRDtBQVlJLDBCQUFnQixFQUFJdUYsR0FBRyxDQUFDckYsZ0JBQUosR0FBdUIsV0FBdkIsR0FBcUMsRUFaN0Q7QUFhSSxxQkFBVyxFQUFJcUYsR0FBRyxDQUFDSSxXQWJ2QjtBQWNJLG9CQUFVLEVBQUVKLEdBQUcsQ0FBQ0ssVUFkcEI7QUFlSSxvQkFBVSxFQUFFTCxHQUFHLENBQUNNLFVBZnBCO0FBZ0JJLGVBQUssRUFBRU4sR0FBRyxDQUFDYixLQWhCZjtBQWlCSSxrQkFBUSxFQUFFYSxHQUFHLENBQUMzRixRQWpCbEI7QUFrQkksMkJBQWlCLEVBQUUyRixHQUFHLENBQUNPLGlCQWxCM0I7QUFtQkksOEJBQW9CLEVBQUVQLEdBQUcsQ0FBQ1Esb0JBbkI5QjtBQW9CSSw0QkFBa0IsRUFBRVIsR0FBRyxDQUFDUyxrQkFwQjVCO0FBcUJJLDJCQUFpQixFQUFFVCxHQUFHLENBQUNVLGlCQXJCM0I7QUFzQkksY0FBSSxFQUFFVixHQUFHLENBQUM3RSxJQXRCZDtBQXVCSSx1QkFBYSxFQUFFMkUsa0JBdkJuQjtBQXdCSSxhQUFHLEVBQUV6SSxHQXhCVDtBQXlCSSxrQkFBUSxFQUFJYyxRQXpCaEI7QUEwQkksZUFBSyxFQUFFeUgsS0ExQlg7QUEyQkksZUFBSyxFQUFFNUYsS0EzQlg7QUE0QkksaUJBQU8sRUFBRTBCO0FBNUJiLFVBREUsRUE4QkVvRSxrQkE5QkYsQ0FBTjtBQWdDQWEscUZBQWlCLENBQUNyRyxFQUFELEVBQUtvQixPQUFMLEVBQWFzRSxHQUFHLENBQUNZLElBQWpCLENBQWpCLENBcEM0QyxDQXFDNUM7O0FBQ0EsWUFBSUMsTUFBTSxHQUFHMUksUUFBUSxHQUFDRixRQUFRLENBQUNDLGNBQVQsQ0FBd0Isc0JBQXhCLEVBQWdERSxXQUF0RTtBQUNBSixrQkFBVSxDQUFDOEksS0FBWCxDQUFpQkMsS0FBakIsR0FBdUJGLE1BQU0sR0FBQyxJQUE5QjtBQUNBeEosV0FBRyxDQUFDMkosY0FBSjtBQUNBM0osV0FBRyxDQUFDNEosT0FBSixDQUFZLENBQUNqSCxLQUFLLENBQUNQLE9BQU4sQ0FBY3lILFFBQWQsQ0FBdUJDLFdBQXZCLENBQW1DLENBQW5DLENBQUQsRUFBd0NuSCxLQUFLLENBQUNQLE9BQU4sQ0FBY3lILFFBQWQsQ0FBdUJDLFdBQXZCLENBQW1DLENBQW5DLENBQXhDLENBQVosRUFBNEYsRUFBNUY7QUFDSDtBQUNKLEtBNUNEO0FBNkNILEdBOUNEOztBQWlEQSxNQUFNQyxVQUFVLEdBQUcsSUFBSUMsc0RBQUosQ0FBV3BKLFFBQVEsQ0FBQ3FKLGFBQVQsQ0FBdUIseUJBQXZCLENBQVgsRUFBOEQ7QUFDN0VDLG1CQUFlLEVBQUUsSUFENEQ7QUFFN0VDLGFBQVMsRUFBRyxFQUZpRTtBQUc3RUMsY0FBVSxFQUFFO0FBSGlFLEdBQTlELENBQW5CO0FBTUEsTUFBTUMsVUFBVSxHQUFHLElBQUlMLHNEQUFKLENBQVdwSixRQUFRLENBQUNxSixhQUFULENBQXVCLHlCQUF2QixDQUFYLEVBQThEO0FBQzdFQyxtQkFBZSxFQUFFLElBRDREO0FBRTdFQyxhQUFTLEVBQUcsRUFGaUU7QUFHN0VDLGNBQVUsRUFBRTtBQUhpRSxHQUE5RCxDQUFuQjtBQU9ILENBN09ELEkiLCJmaWxlIjoiY2x1YnMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gICAgYWdvbEZlYXR1cmVTZXJ2aWNlcyxcclxuICAgIGFyY2dpc09ubGluZVByb3ZpZGVyLFxyXG4gICAgc2V0RXNyaUdlb2NvZGVyLFxyXG4gICAgYWRkUmVzdWx0c1RvU2lkZWJhcixcclxuICAgIG5hdmlnYXRpb25IaXN0b3J5LFxyXG4gICAgem9vbVRvTWFya2Vyc0FmdGVyTG9hZCxcclxuICAgIGljb25PaW4sXHJcbiAgICBzdHJ1Y3R1cmVUeXBlXHJcbn0gZnJvbSBcIi4uL3NlcnZpY2VzL21hcENvbmZpZ1wiO1xyXG5cclxuaW1wb3J0IHtwb3B1cFRlbXBsYXRlQ2x1YnN9IGZyb20gXCIuLi9jb21wb25lbnRzL3BvcHVwXCI7XHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB7cmVuZGVyLCB1bm1vdW50Q29tcG9uZW50QXROb2RlfSBmcm9tICdyZWFjdC1kb20nO1xyXG5pbXBvcnQgSW5mb0ZlYXR1cmUgZnJvbSAnLi4vY29tcG9uZW50cy9JbmZvRmVhdHVyZSc7XHJcbmltcG9ydCBUYWdpZnkgZnJvbSAnQHlhaXJlby90YWdpZnknO1xyXG5cclxuKGZ1bmN0aW9uIG1hcENsdWJzICgpIHtcclxuXHJcbiAgICBjb25zdCBtYXAgPSBMLm1hcCgnbWFwJywge1xyXG4gICAgICAgIGNlbnRlcjogWzQ3LCAwXSxcclxuICAgICAgICB6b29tOiA1LFxyXG4gICAgICAgIG1pblpvb206IDMsXHJcbiAgICAgICAgem9vbUNvbnRyb2w6IGZhbHNlLFxyXG4gICAgICAgIGxheWVyczogW0wuZXNyaS5iYXNlbWFwTGF5ZXIoJ1RvcG9ncmFwaGljJyldXHJcbiAgICB9KTtcclxuICAgIG1hcC5hdHRyaWJ1dGlvbkNvbnRyb2wuYWRkQXR0cmlidXRpb24oJzxhIHRhcmdldD1cIl9ibGFua1wiIGhyZWY9XCJodHRwczovL2ZmdmVsby5mclwiPkbDqWTDqXJhdGlvbiBmcmFuw6dhaXNlIGRlIGN5Y2xvdG91cmlzbWU8L2E+Jyk7XHJcblxyXG4gICAgLy92YXJpYWJsZXMgZ2xvYmFsZXNcclxuICAgIGNvbnN0IG1hcEVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1hcFwiKTtcclxuICAgIGNvbnN0IG1hcFdpZHRoID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJtYXBcIikub2Zmc2V0V2lkdGg7XHJcbiAgICBjb25zdCBjbHVic0xheWVyID0gTC5tYXJrZXJDbHVzdGVyR3JvdXAoeyBcclxuICAgICAgICBjaHVua2VkTG9hZGluZzogdHJ1ZSxcclxuICAgICAgICBpY29uQ3JlYXRlRnVuY3Rpb246IGZ1bmN0aW9uIChjbHVzdGVyKSB7XHJcbiAgICAgICAgICAgIGxldCBjb3VudCA9IGNsdXN0ZXIuZ2V0Q2hpbGRDb3VudCgpO1xyXG4gICAgICAgICAgICBsZXQgZGlnaXRzID0gKGNvdW50ICsgJycpLmxlbmd0aDtcclxuICAgICAgICAgICAgcmV0dXJuIEwuZGl2SWNvbih7XHJcbiAgICAgICAgICAgIGh0bWw6IGNvdW50LFxyXG4gICAgICAgICAgICBjbGFzc05hbWU6ICdjbHVzdGVyIGRpZ2l0cy0nICsgZGlnaXRzLFxyXG4gICAgICAgICAgICBpY29uU2l6ZTogbnVsbFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICAgLy9sYXllciBjbHVic1xyXG5cdGNvbnN0IGNsdWJzID0gTC5lc3JpLkNsdXN0ZXIuZmVhdHVyZUxheWVyKHtcclxuICAgICAgICB1cmw6IGFnb2xGZWF0dXJlU2VydmljZXMuY2x1YnMsXHJcbiAgICAgICAgdG9rZW46IHRva2VuLFxyXG5cdFx0cG9pbnRUb0xheWVyOiAoZmVhdHVyZSwgbGF0bG5nKSA9PiB7XHJcblx0XHRcdHJldHVybiBMLm1hcmtlcihsYXRsbmcsIHtcclxuXHRcdFx0XHRpY29uOiBpY29uT2luWzJdXHJcblx0XHRcdH0pO1xyXG5cdFx0fSxcclxuXHRcdGljb25DcmVhdGVGdW5jdGlvbjogKGNsdXN0ZXIpID0+IHtcclxuXHRcdFx0bGV0IGNvdW50ID0gY2x1c3Rlci5nZXRDaGlsZENvdW50KCk7XHJcblx0XHRcdGxldCBkaWdpdHMgPSAoY291bnQgKyAnJykubGVuZ3RoO1xyXG5cdFx0XHRyZXR1cm4gTC5kaXZJY29uKHtcclxuXHRcdFx0ICBodG1sOiBjb3VudCxcclxuXHRcdFx0ICBjbGFzc05hbWU6ICdjbHVzdGVyIGRpZ2l0cy0nICsgZGlnaXRzLFxyXG5cdFx0XHQgIGljb25TaXplOiBudWxsXHJcblx0XHRcdH0pO1xyXG5cdFx0ICB9LFxyXG4gICAgfSkuYWRkVG8oY2x1YnNMYXllcikuYWRkVG8obWFwKTtcclxuXHJcblx0Ly9wb3B1cHMgY2x1YnNcclxuXHRjbHVicy5iaW5kUG9wdXAoKGxheWVyKT0+e1xyXG4gICAgICAgIGxldCBub20gPSBsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuU3RydWN0dXJlTm9tO1xyXG4gICAgICAgIGxldCB2aWxsZSA9IGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5BZHJWaWxsZTtcclxuICAgICAgICBsZXQgaWQgPSBsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuU3RydWN0dXJlQ29kZTtcclxuICAgICAgICBsZXQgcHJhdGlxdWVSb3V0ZT1sYXllci5mZWF0dXJlLnByb3BlcnRpZXMuUHJhdGlxdWVfUm91dGU7XHJcbiAgICAgICAgbGV0IHByYXRpcXVlVnR0VnRjPWxheWVyLmZlYXR1cmUucHJvcGVydGllcy5QcmF0aXF1ZV9WVFRfVlRDO1xyXG4gICAgICAgIGxldCB0ZWwgPSBsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuQWRyVGVsO1xyXG4gICAgICAgIGxldCBlbWFpbCA9IGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5BZHJNYWlsO1xyXG4gICAgICAgIGxldCBzaXRld2ViID0gbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkFkcldlYjtcclxuICAgICAgICBsZXQgbG9nbyA9IGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5Mb2dvO1xyXG4gICAgICAgIHJldHVybiBMLlV0aWwudGVtcGxhdGUoXHJcbiAgICAgICAgICAgIHBvcHVwVGVtcGxhdGVDbHVicyhub20sIHZpbGxlLCBpZCwgcHJhdGlxdWVSb3V0ZSwgcHJhdGlxdWVWdHRWdGMsIHRlbCwgZW1haWwsIHNpdGV3ZWIsIGxvZ28pXHJcbiAgICAgICAgKTtcclxuICAgIH0pO1xyXG5cclxuICAgIC8qZXZlbmVtZW50cyBjYXJ0ZSBldCBsYXllciBwb2ludCBkZXBhcnQqL1xyXG5cdGNsdWJzLm9uY2UoJ2xvYWQnLCAoKSA9PiB7XHJcblx0XHRpZihsb2NhdGlvbi5wYXRobmFtZSAhPSBtZW51VXJsKXtcclxuXHRcdFx0bGV0IHBhcnRzID0gbG9jYXRpb24uaHJlZi5zcGxpdChtZW51VXJsK1wiL1wiKTtcclxuXHRcdFx0bGV0IHVybGlkID0gcGFydHNbMV0uc3BsaXQoXCItXCIpO1xyXG5cdFx0XHRyZW5kZXJJbmZvRmVhdHVyZShjbHVicywgdXJsaWRbMF0pO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0em9vbVRvTWFya2Vyc0FmdGVyTG9hZChtYXAsIGNsdWJzKTtcclxuXHRcdH07XHJcblx0XHQvL3N5bmNTaWRlYmFyKCk7XHJcblx0fSk7XHJcblx0Y2x1YnMub24oJ3BvcHVwb3BlbicsICgpID0+IHtcclxuXHRcdGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYnV0dG9uUG9wdXBcIikuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB7XHJcblx0XHRcdHJlbmRlckluZm9GZWF0dXJlKGNsdWJzLCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImJ1dHRvblBvcHVwXCIpLmdldEF0dHJpYnV0ZSgndmFsdWUnKSlcclxuXHRcdH0sIHRydWUpO1xyXG5cdH0pO1xyXG5cdGNsdWJzLm9uKCdjbGljaycsIChlKSA9PiB7XHJcblx0XHRjb25zb2xlLmxvZyhlKTtcclxuXHR9KTtcclxuXHRtYXAub24oXCJtb3ZlZW5kXCIsICgpID0+IHtcclxuXHRcdGNsdWJzLnNldFdoZXJlKGNsdWJzLmdldFdoZXJlKCkpO1xyXG5cdFx0c3luY1NpZGViYXIoKTtcclxuXHR9KTtcclxuXHRtYXAub24oJ292ZXJsYXlyZW1vdmUnLCAoKSA9PiB7XHJcblx0XHRjbHVic0xheWVyLmNsZWFyTGF5ZXJzKCk7XHJcbiAgICB9KTsgXHJcbiAgICB3aW5kb3cub25wb3BzdGF0ZSA9ICgpID0+IHtcclxuICAgICAgICBiYWNrQnV0dG9uTmF2aWdhdGlvbihtZW51VXJsLCBtYXAsIG1hcEVsZW1lbnQsIG1hcFdpZHRoLCBjbHVicywgcmVuZGVySW5mb0ZlYXR1cmUpO1xyXG4gICAgfVxyXG4gICAgLypmaW4gZXZlbmVtZW50cyBjYXJ0ZSBldCBsYXllciBwb2ludCBkZXBhcnQqL1xyXG4gICAgXHJcbiAgICAvL2JhcnJlIGRlIHJlY2hlcmNoZSBnZW9jb2RhZ2UgLSBzYWlzaXIgdW5lIGFkcmVzc2UgZXQgY3JlZXIgdW4gcG9pbnRcdFxyXG4gICAgY29uc3Qgc2VhcmNoQ29udHJvbCA9IEwuZXNyaS5HZW9jb2RpbmcuZ2Vvc2VhcmNoKHtcclxuICAgICAgICB1c2VNYXBCb3VuZHM6ZmFsc2UsXHJcbiAgICAgICAgdGl0bGU6ICdSZWNoZXJjaGVyIHVuIGxpZXUgLyB1bmUgYWRyZXNzZScsXHJcbiAgICAgICAgcGxhY2Vob2xkZXI6ICdPw7kgPycsXHJcbiAgICAgICAgZXhwYW5kZWQ6dHJ1ZSxcclxuICAgICAgICBjb2xsYXBzZUFmdGVyUmVzdWx0OiBmYWxzZSxcclxuICAgICAgICBwb3NpdGlvbjogJ3RvcHJpZ2h0JyxcclxuICAgICAgICBwcm92aWRlcnM6IFtcclxuICAgICAgICAgICAgYXJjZ2lzT25saW5lUHJvdmlkZXIsXHJcbiAgICAgICAgICAgIEwuZXNyaS5HZW9jb2RpbmcuZmVhdHVyZUxheWVyUHJvdmlkZXIoeyAvL3JlY2hlcmNoZSBkZXMgY291Y2hlcyBkYW5zIGxhIGdkYiBvaW5cclxuXHRcdFx0XHR1cmw6IGFnb2xGZWF0dXJlU2VydmljZXMuY2x1YnMsXHJcbiAgICAgICAgICAgICAgICB3aGVyZTogY2x1YnMuZ2V0V2hlcmUoKSxcclxuICAgICAgICAgICAgICAgIHRva2VuOiB0b2tlbixcclxuXHRcdFx0XHRzZWFyY2hGaWVsZHM6IFsnU3RydWN0dXJlQ29kZScsJ1N0cnVjdHVyZU5vbSddLFxyXG5cdFx0XHRcdGxhYmVsOiAnQ0xVQlMgRkZWRUxPOicsXHJcblx0XHRcdFx0bWF4UmVzdWx0czogNyxcclxuXHRcdFx0XHRidWZmZXJSYWRpdXM6IDEwMDAsXHJcblx0XHRcdFx0Zm9ybWF0U3VnZ2VzdGlvbjogKGZlYXR1cmUpID0+IHtcclxuXHRcdFx0XHRcdHJldHVybiBgJHtmZWF0dXJlLnByb3BlcnRpZXMuU3RydWN0dXJlQ29kZX0gLSAke2ZlYXR1cmUucHJvcGVydGllcy5TdHJ1Y3R1cmVOb219YDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pXHJcbiAgICAgICAgXVxyXG4gICAgfSkuYWRkVG8obWFwKTtcclxuICAgIC8vZ2VvY29kZXVyIGVuIGRlaG9ycyBkZSBsYSBjYXJ0ZVxyXG4gICAgY29uc3QgZXNyaUdlb2NvZGVyID0gc2VhcmNoQ29udHJvbC5nZXRDb250YWluZXIoKTtcclxuICAgIHNldEVzcmlHZW9jb2Rlcihlc3JpR2VvY29kZXIsIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdnZW9jb2RlcicpKTtcclxuICAgIC8vcmVjdXAgbGEgcmVxdWV0ZSBlbiBjb3VycyBldCBhcHBsaXF1ZSBhdXggcmVzdWx0YXRzIGR1IGdlb2NvZGV1clxyXG5cdHNlYXJjaENvbnRyb2wub24oXCJyZXF1ZXN0ZW5kXCIsIGZ1bmN0aW9uICgpIHtcclxuXHRcdHNlYXJjaENvbnRyb2wub3B0aW9ucy5wcm92aWRlcnNbMV0ub3B0aW9ucy53aGVyZSA9IGNsdWJzLmdldFdoZXJlKCk7XHJcblx0fSk7XHJcblxyXG4gICAgY29uc3Qgc3luY1NpZGViYXIgPSAoYXJyUmVzdWx0c1NpZGViYXI9W10pID0+IHtcclxuICAgICAgICAkKFwiI2ZlYXR1cmVzXCIpLmVtcHR5KCk7XHJcbiAgICAgICAgY2x1YnMuZWFjaEFjdGl2ZUZlYXR1cmUoZnVuY3Rpb24gKGxheWVyKSB7XHJcbiAgICAgICAgICAgIGlmIChtYXAuaGFzTGF5ZXIoY2x1YnMpKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAobWFwLmdldEJvdW5kcygpLmNvbnRhaW5zKGxheWVyLmdldExhdExuZygpKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGFyclJlc3VsdHNTaWRlYmFyLnB1c2goXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpZGViYXJUZW1wbGF0ZUNsdWJzKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLlN0cnVjdHVyZUNvZGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuU3RydWN0dXJlTm9tLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLlByYXRpcXVlX1JvdXRlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLlByYXRpcXVlX1ZUVF9WVEMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuVkFFLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkFjY3VlaWxIYW5kaWNhcCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5BY2N1ZWlsSmV1bmUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuRWNvbGVDeWNsbyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5WZWxvRWNvbGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuQWRyV2ViLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkFkckNQLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkFkclZpbGxlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgYWRkUmVzdWx0c1RvU2lkZWJhcihhcnJSZXN1bHRzU2lkZWJhciwgY2x1YnMsIHJlbmRlckluZm9GZWF0dXJlKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBib29rbWFya3MgaHRtbFxyXG4gICAgY29uc3Qgc2lkZWJhclRlbXBsYXRlQ2x1YnMgPSAoaWQsbm9tLHByYXRpcXVlUm91dGUscHJhdGlxdWVWdHRWdGMsdmFlLGFjY3VlaWxIYW5kaWNhcCxhY2N1ZWlsSmV1bmUsZWNvbGVDeWNsbyx2ZWxvRWNvbGUsc2l0ZXdlYixjcCx2aWxsZSkgPT4ge1xyXG4gICAgICAgIHNpdGV3ZWIgPSBzaXRld2ViLmluY2x1ZGVzKFwiaHR0cFwiKSA/IHNpdGV3ZWIgOiBcImh0dHA6Ly9cIiArIHNpdGV3ZWI7ICBcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICBgPGRpdiBjbGFzcz1cImNhcmQgZmVhdHVyZS1jYXJkIG15LTIgcC0yXCIgaWQ9XCIke2lkfVwiIGRhdGEtbGF0PVwiXCIgZGF0YS1sb249XCJcIj4gXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sIGFsaWduLXNlbGYtY2VudGVyIHJvdW5kZWQtcmlnaHRcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aDYgY2xhc3M9XCJkLWJsb2NrIHRleHQtdXBwZXJjYXNlXCI+JHtub219PC9oNj5cclxuICAgICAgICAgICAgICAgICAgICAke3ByYXRpcXVlUm91dGUgPyBgPHNwYW4gY2xhc3M9XCJiYWRnZSBiYWRnZS1waWxsIGJhZGdlLWxpZ2h0IG15LTEgbXItMVwiPlJvdXRlPC9zcGFuPmA6IFwiXCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgJHtwcmF0aXF1ZVZ0dFZ0YyA/IGA8c3BhbiBjbGFzcz1cImJhZGdlIGJhZGdlLXBpbGwgYmFkZ2UtbGlnaHQgbXktMSBtci0xXCI+VlRUL1ZUQzwvc3Bhbj5gIDogXCJcIn1cclxuICAgICAgICAgICAgICAgICAgICAke3ZhZSA/IGA8c3BhbiBjbGFzcz1cImJhZGdlIGJhZGdlLXBpbGwgYmFkZ2UtbGlnaHQgbXktMSBtci0xXCI+VsOpbG8gw6AgYXNzaXN0YW5jZSDDqWxlY3RyaXF1ZTwvc3Bhbj5gOiBcIlwifVxyXG4gICAgICAgICAgICAgICAgICAgICR7YWNjdWVpbEhhbmRpY2FwID8gYDxzcGFuIGNsYXNzPVwiYmFkZ2UgYmFkZ2UtcGlsbCBiYWRnZS1saWdodCBteS0xIG1yLTFcIj5BY2N1ZWlsIGhhbmRpY2FwPC9zcGFuPmA6IFwiXCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgJHthY2N1ZWlsSmV1bmUgPyBgPHNwYW4gY2xhc3M9XCJiYWRnZSBiYWRnZS1waWxsIGJhZGdlLWxpZ2h0IG15LTEgbXItMVwiPkFjY3VlaWwgamV1bmU8L3NwYW4+YCA6IFwiXCIgfVxyXG4gICAgICAgICAgICAgICAgICAgICR7ZWNvbGVDeWNsbyA/IGA8c3BhbiBjbGFzcz1cImJhZGdlIGJhZGdlLXBpbGwgYmFkZ2UtbGlnaHQgbXktMSBtci0xXCI+w4ljb2xlIGN5Y2xvPC9zcGFuPmAgOiBcIlwifVxyXG4gICAgICAgICAgICAgICAgICAgICR7dmVsb0Vjb2xlID8gYDxzcGFuIGNsYXNzPVwiYmFkZ2UgYmFkZ2UtcGlsbCBiYWRnZS1saWdodCBteS0xIG1yLTFcIj5Ww6lsby3DqWNvbGU8L3NwYW4+YCA6IFwiXCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgJHtzaXRld2ViID8gYDxzcGFuIGNsYXNzPVwibXktMSBtci0xXCI+PGJyLz48aSBjbGFzcz1cImZhcyBmYS1nbG9iZVwiPjwvaT4gPGEgaHJlZj0nJHtzaXRld2VifScgdGFyZ2V0PSdfYmxhbmsnPlNpdGUgd2ViPC9hPjwvc3Bhbj5gIDogXCJcIn1cclxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cIm15LTEgbXItMVwiPjxici8+PGkgY2xhc3M9XCJmYXMgZmEtbWFwLW1hcmtlZC1hbHRcIj48L2k+ICR7Y3B9LCAke3ZpbGxlfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5gXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvL2NvbXBvc2FudCBmaWNoZSBkZXNjcmlwdGl2ZVxyXG4gICAgY29uc3QgcmVuZGVySW5mb0ZlYXR1cmUgPSAoTGF5ZXIsIGlkKSA9PiB7XHJcbiAgICAgICAgTGF5ZXIuZWFjaEZlYXR1cmUoZnVuY3Rpb24gKGxheWVyKSB7XHJcbiAgICAgICAgICAgIGlmKGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5TdHJ1Y3R1cmVDb2RlID09IGlkKXtcclxuICAgICAgICAgICAgICAgIGxldCBpbmZvZmVhdHVyZUVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnaW5mb2ZlYXR1cmUnKTtcclxuICAgICAgICAgICAgICAgIHVubW91bnRDb21wb25lbnRBdE5vZGUoaW5mb2ZlYXR1cmVFbGVtZW50KTtcclxuICAgICAgICAgICAgICAgIGxldCBsZnA9bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzO1xyXG4gICAgICAgICAgICAgICAgcmVuZGVyKFx0XHJcbiAgICAgICAgICAgICAgICAgICAgPEluZm9GZWF0dXJlIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZENsdWI9e2lkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBTdHJ1Y3R1cmVDb2RlRGVwYXJ0ZW1lbnQgPSB7bGZwLlN0cnVjdHVyZUNvZGVEZXBhcnRlbWVudH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgU3RydWN0dXJlQ29kZVJlZ2lvbiA9IHtsZnAuU3RydWN0dXJlQ29kZVJlZ2lvbn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgbm9tPXtsZnAuU3RydWN0dXJlTm9tfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcj17XCJcIn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgQWNjdWVpbEhhbmRpY2FwPXtsZnAuQWNjdWVpbEhhbmRpY2FwID8gXCJBY2N1ZWlsIEhhbmRpY2FwXCIgOiBcIlwifVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBBY2N1ZWlsSmV1bmUgPSB7bGZwLkFjY3VlaWxKZXVuZSA/IFwiQWNjdWVpbCBKZXVuZVwiIDogXCJcIn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgRWNvbGVDeWNsbyA9IHtsZnAuRWNvbGVDeWNsbyA/IFwiw4ljb2xlIEN5Y2xvXCIgOiBcIlwifVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWUgPSB7bGZwLlZBRSA/IFwiVsOpbG8gw6AgYXNzaXN0YW5jZSDDqWxlY3RyaXF1ZVwiIDogXCJcIn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgVmVsb0Vjb2xlID0ge2xmcC5WZWxvRWNvbGUgPyBcIlbDqWxvLcOpY29sZVwiIDogXCJcIn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgUHJhdGlxdWVfUm91dGU9e2xmcC5QcmF0aXF1ZV9Sb3V0ZSA/IFwiUm91dGVcIiA6IFwiXCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFByYXRpcXVlX1ZUVF9WVEMgPSB7bGZwLlByYXRpcXVlX1ZUVF9WVEMgPyBcIlZUVCAvIFZUQ1wiIDogXCJcIn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgQWRyRXNjYWxpZXIgPSB7bGZwLkFkckVzY2FsaWVyfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBBZHJOdW1Wb2llPXtsZnAuQWRyTnVtVm9pZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgQWRyTm9tVm9pZT17bGZwLkFkck5vbVZvaWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEFkckNQPXtsZnAuQWRyQ1B9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEFkclZpbGxlPXtsZnAuQWRyVmlsbGV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE5vbV9Db3JyZXNwb25kYW50PXtsZnAuTm9tX0NvcnJlc3BvbmRhbnR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFByZW5vbV9Db3JyZXNwb25kYW50PXtsZnAuUHJlbm9tX0NvcnJlc3BvbmRhbnR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIE1haWxfQ29ycmVzcG9uZGFudD17bGZwLk1haWxfQ29ycmVzcG9uZGFudH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgVGVsX0NvcnJlc3BvbmRhbnQ9e2xmcC5UZWxfQ29ycmVzcG9uZGFudH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgbG9nbz17bGZwLkxvZ299XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvb3RDb250YWluZXI9e2luZm9mZWF0dXJlRWxlbWVudH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgbWFwPXttYXB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcFdpZHRoID0ge21hcFdpZHRofVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBMYXllcj17TGF5ZXJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxheWVyPXtsYXllcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgbWVudVVybD17bWVudVVybH1cclxuICAgICAgICAgICAgICAgICAgICAvPiwgaW5mb2ZlYXR1cmVFbGVtZW50XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgbmF2aWdhdGlvbkhpc3RvcnkoaWQsIG1lbnVVcmwsbGZwLnNsdWcpO1xyXG4gICAgICAgICAgICAgICAgLy9nZXN0aW9uIGRlIGxhIGNhcnRlXHJcbiAgICAgICAgICAgICAgICBsZXQgcmVzaXplID0gbWFwV2lkdGgtZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJpbmZvLWZlYXR1cmUtY29udGVudFwiKS5vZmZzZXRXaWR0aDtcclxuICAgICAgICAgICAgICAgIG1hcEVsZW1lbnQuc3R5bGUud2lkdGg9cmVzaXplK1wicHhcIjtcclxuICAgICAgICAgICAgICAgIG1hcC5pbnZhbGlkYXRlU2l6ZSgpO1xyXG4gICAgICAgICAgICAgICAgbWFwLnNldFZpZXcoW2xheWVyLmZlYXR1cmUuZ2VvbWV0cnkuY29vcmRpbmF0ZXNbMV0sIGxheWVyLmZlYXR1cmUuZ2VvbWV0cnkuY29vcmRpbmF0ZXNbMF1dLCAxMik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgY29uc3QgdGFnaWZ5RGVwcyA9IG5ldyBUYWdpZnkoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInRleHRhcmVhW25hbWU9dGFnc0RlcHNdXCIpLCB7XHJcbiAgICAgICAgZW5mb3JlV2hpdGVsaXN0OiB0cnVlLFxyXG4gICAgICAgIHdoaXRlbGlzdCA6IFtdLFxyXG4gICAgICAgIGR1cGxpY2F0ZXM6IGZhbHNlLFxyXG4gICAgfSk7XHJcblxyXG4gICAgY29uc3QgdGFnaWZ5UmVncyA9IG5ldyBUYWdpZnkoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInRleHRhcmVhW25hbWU9dGFnc1JlZ3NdXCIpLCB7XHJcbiAgICAgICAgZW5mb3JlV2hpdGVsaXN0OiB0cnVlLFxyXG4gICAgICAgIHdoaXRlbGlzdCA6IFtdLFxyXG4gICAgICAgIGR1cGxpY2F0ZXM6IGZhbHNlLFxyXG4gICAgfSk7XHJcbiAgICBcclxuXHJcbn0pKCk7Il0sInNvdXJjZVJvb3QiOiIifQ==