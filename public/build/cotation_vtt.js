(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cotation_vtt"],{

/***/ "./assets/js/pages/cotation/cotation.js":
/*!**********************************************!*\
  !*** ./assets/js/pages/cotation/cotation.js ***!
  \**********************************************/
/*! exports provided: convertMinsToHrsMins, addValueToInput */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "convertMinsToHrsMins", function() { return convertMinsToHrsMins; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addValueToInput", function() { return addValueToInput; });
/* harmony import */ var core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.parse-float.js */ "./node_modules/core-js/modules/es.parse-float.js");
/* harmony import */ var core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.regexp.exec.js */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.string.replace.js */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2__);



//conversion minutes en heures
var convertMinsToHrsMins = function convertMinsToHrsMins(minutes) {
  var h = Math.floor(minutes / 60);
  var m = minutes % 60;
  h = h < 10 ? '0' + h : h;
  m = m < 10 ? '0' + m : m;
  return h + ':' + m;
}; //remplissage inputs

function addValueToInput(inputVal, id) {
  //document.getElementById(id).value = inputVal;
  $(id).val(inputVal);
} //recup coordonnees points departs

/*const getCoordsDepart = (xml) => {
    parser = new DOMParser();
    xmlDoc = parser.parseFromString(xml, "text/xml");
    var lat = xmlDoc.getElementsByTagName("trkpt")[0].getAttribute('lat');
    var lon = xmlDoc.getElementsByTagName("trkpt")[0].getAttribute('lon');
    return [lon, lat];
}*/
//calcul de la difficulte avec radios buttons

$(":radio").change(function () {
  var names = {};
  $(':radio').each(function () {
    names[$(this).attr('name')] = true;
  });
  var count = 0;
  $.each(names, function () {
    count++;
  });

  if ($(':radio:checked').length === count) {
    var total = 0;
    $("input[type=radio]:checked").each(function () {
      total += parseFloat($(this).val());
    });
    outputDiff(total);
  }
}); //input difficulte

function outputDiff(total) {
  var diff, color;

  if (total >= 4 && total <= 5) {
    diff = total + " - Très facile";
    color = "green";
  } else if (total >= 6 && total <= 8) {
    diff = total + " - Facile";
    color = "blue";
  } else if (total >= 9 && total <= 12) {
    diff = total + " - Difficile";
    color = "red";
  } else {
    diff = total + " - Très Difficile";
    color = "black";
  }

  $("#cotation_form_difficulte").val(diff);
  $("#cotation_form_difficulte").css({
    "color": color,
    "font-size": "25px"
  });
} //compte le nombre de caracteres dans le textarea


var textInit = 0;
$('#count_text').html(textInit + ' caractère /500');
$('#cotation_form_textDescr').keyup(function () {
  var text_length = $('#cotation_form_textDescr').val().length;
  $('#count_text').html(text_length + ' caractères /500');

  if (text_length <= 1) {
    $('#count_text').html(text_length + ' caractère /500');
  }

  if (text_length > 500) {
    document.getElementById("count_text").style.color = 'red';
  } else {
    document.getElementById("count_text").style.color = 'black';
  }
}); //empecher les retours charriots du textarea

$('#cotation_form_textDescr').on('keyup', function () {
  $(this).val($(this).val().replace(/[\r\n\v]+/g, ''));
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/pages/cotation/cotationVtt.js":
/*!*************************************************!*\
  !*** ./assets/js/pages/cotation/cotationVtt.js ***!
  \*************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var esri_leaflet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! esri-leaflet */ "./node_modules/esri-leaflet/src/EsriLeaflet.js");
/* harmony import */ var leaflet_gpx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! leaflet-gpx */ "./node_modules/leaflet-gpx/gpx.js");
/* harmony import */ var leaflet_gpx__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(leaflet_gpx__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _cotation__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cotation */ "./assets/js/pages/cotation/cotation.js");





var toGeoJSON = __webpack_require__(/*! @tmcw/togeojson */ "./node_modules/@tmcw/togeojson/dist/togeojson.umd.js");

__webpack_require__(/*! leaflet.heightgraph */ "./node_modules/leaflet.heightgraph/dist/L.Control.Heightgraph.min.js");

__webpack_require__(/*! leaflet-polylinedecorator */ "./node_modules/leaflet-polylinedecorator/dist/leaflet.polylineDecorator.js"); //coche bouton radio distance au chargement du gpx


function radioDistCheck(dist) {
  if (dist >= 0 && dist <= 10) {
    document.getElementById("cotation_form_distChoice_0").checked = true;
  }

  if (dist >= 11 && dist <= 20) {
    document.getElementById("cotation_form_distChoice_1").checked = true;
  }

  if (dist >= 21 && dist <= 40) {
    document.getElementById("cotation_form_distChoice_2").checked = true;
  }

  if (dist >= 41) {
    document.getElementById("cotation_form_distChoice_3").checked = true;
  }
} //coche bouton radio denivele au chargement du gpx


function radioDenivCheck(deniv) {
  if (deniv >= 0 && deniv <= 100) {
    document.getElementById("cotation_form_denivChoice_0").checked = true;
  }

  if (deniv >= 101 && deniv <= 250) {
    document.getElementById("cotation_form_denivChoice_1").checked = true;
  }

  if (deniv >= 251 && deniv <= 600) {
    document.getElementById("cotation_form_denivChoice_2").checked = true;
  }

  if (deniv >= 601) {
    document.getElementById("cotation_form_denivChoice_3").checked = true;
  }
} // fonds de carte


var map = L.map('cotation-map', {
  center: [47, 2],
  zoom: 5,
  minZoom: 3,
  zoomControl: true,
  layers: [Object(esri_leaflet__WEBPACK_IMPORTED_MODULE_1__["basemapLayer"])('Topographic')]
});
L.control.scale().addTo(map); //ouverture et affichage du gpx

var cpt = 0;

function loadGpxFile(g) {
  var files = g.target.files,
      reader = new FileReader();
  cpt += 1;

  if (cpt > 1) {
    map.remove();
    map = L.map('cotation-map', {
      center: [47, 2],
      zoom: 5,
      minZoom: 3,
      zoomControl: true,
      layers: [Object(esri_leaflet__WEBPACK_IMPORTED_MODULE_1__["basemapLayer"])('Topographic')]
    });
  }

  reader.onload = function (e) {
    var gpxString = reader.result;
    new leaflet_gpx__WEBPACK_IMPORTED_MODULE_2__["GPX"](gpxString, {
      async: true,
      marker_options: {
        startIconUrl: '../build/images/icon/pin-icon-start.png',
        endIconUrl: '../build/images/icon/pin-icon-end.png',
        shadowUrl: '',
        wptIconUrls: ''
      }
    }).on('loaded', function (e) {
      map.fitBounds(e.target.getBounds());
      var nom = e.target.get_name();
      var dist = Math.round(e.target.get_distance() / 1000);
      var temps = Math.round(dist / 15 * 60);
      var deniv = Math.round(e.target.get_elevation_gain());
      _cotation__WEBPACK_IMPORTED_MODULE_3__["addValueToInput"](nom, "#cotation_form_nom_circuit");
      _cotation__WEBPACK_IMPORTED_MODULE_3__["addValueToInput"](dist, "#cotation_form_distance");
      _cotation__WEBPACK_IMPORTED_MODULE_3__["addValueToInput"](_cotation__WEBPACK_IMPORTED_MODULE_3__["convertMinsToHrsMins"](temps), "#cotation_form_temps");
      _cotation__WEBPACK_IMPORTED_MODULE_3__["addValueToInput"](deniv, "#cotation_form_denivele");
      radioDistCheck(dist);
      radioDenivCheck(deniv);
      /*radioTypeVoieCheck(dist);
      radioPenteCheck(dist);*/
    }).addTo(map);
    var gpxDoc = new DOMParser().parseFromString(gpxString, 'text/xml');
    var geoJson = [];
    geoJson.push(toGeoJSON.gpx(gpxDoc));
    geoJson[0]['properties'] = {
      "Creator": "FFVélo",
      "summary": ""
    };
    /*var hg = L.control.heightgraph({
        width: 800,
        height: 280,
        margins: {
            top: 10,
            right: 30,
            bottom: 55,
            left: 50
        },
        position: "bottomright",
        mappings:  undefined
    });
    hg.addTo(map);
    hg.addData(geoJson);*/
    //Geojson pour fleches directionnelles sur le tracé du circuit

    var arrowLayer = L.geoJson(geoJson, {
      onEachFeature: function onEachFeature(feature, layer) {
        var coords = layer.getLatLngs();
        L.polylineDecorator(coords, {
          patterns: [{
            offset: 25,
            repeat: 100,
            symbol: L.Symbol.arrowHead({
              pixelSize: 15,
              pathOptions: {
                fillOpacity: 1,
                weight: 0,
                color: '#0000FF'
              }
            })
          }]
        }).addTo(map);
      }
    }).addTo(map);
  };

  reader.readAsText(files[0]);
}

opengpx.addEventListener("change", loadGpxFile, false);

/***/ })

},[["./assets/js/pages/cotation/cotationVtt.js","runtime","vendors~app~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_velor~10347b53","vendors~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~61364f2f","vendors~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~cotatio~d7e263c1","vendors~cotation_route~cotation_veloroute~cotation_vtt~cotation_vttae"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvcGFnZXMvY290YXRpb24vY290YXRpb24uanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL3BhZ2VzL2NvdGF0aW9uL2NvdGF0aW9uVnR0LmpzIl0sIm5hbWVzIjpbImNvbnZlcnRNaW5zVG9IcnNNaW5zIiwibWludXRlcyIsImgiLCJNYXRoIiwiZmxvb3IiLCJtIiwiYWRkVmFsdWVUb0lucHV0IiwiaW5wdXRWYWwiLCJpZCIsIiQiLCJ2YWwiLCJjaGFuZ2UiLCJuYW1lcyIsImVhY2giLCJhdHRyIiwiY291bnQiLCJsZW5ndGgiLCJ0b3RhbCIsInBhcnNlRmxvYXQiLCJvdXRwdXREaWZmIiwiZGlmZiIsImNvbG9yIiwiY3NzIiwidGV4dEluaXQiLCJodG1sIiwia2V5dXAiLCJ0ZXh0X2xlbmd0aCIsImRvY3VtZW50IiwiZ2V0RWxlbWVudEJ5SWQiLCJzdHlsZSIsIm9uIiwicmVwbGFjZSIsInRvR2VvSlNPTiIsInJlcXVpcmUiLCJyYWRpb0Rpc3RDaGVjayIsImRpc3QiLCJjaGVja2VkIiwicmFkaW9EZW5pdkNoZWNrIiwiZGVuaXYiLCJtYXAiLCJMIiwiY2VudGVyIiwiem9vbSIsIm1pblpvb20iLCJ6b29tQ29udHJvbCIsImxheWVycyIsImJhc2VtYXBMYXllciIsImNvbnRyb2wiLCJzY2FsZSIsImFkZFRvIiwiY3B0IiwibG9hZEdweEZpbGUiLCJnIiwiZmlsZXMiLCJ0YXJnZXQiLCJyZWFkZXIiLCJGaWxlUmVhZGVyIiwicmVtb3ZlIiwib25sb2FkIiwiZSIsImdweFN0cmluZyIsInJlc3VsdCIsIkdQWCIsImFzeW5jIiwibWFya2VyX29wdGlvbnMiLCJzdGFydEljb25VcmwiLCJlbmRJY29uVXJsIiwic2hhZG93VXJsIiwid3B0SWNvblVybHMiLCJmaXRCb3VuZHMiLCJnZXRCb3VuZHMiLCJub20iLCJnZXRfbmFtZSIsInJvdW5kIiwiZ2V0X2Rpc3RhbmNlIiwidGVtcHMiLCJnZXRfZWxldmF0aW9uX2dhaW4iLCJDb3RhdGlvbiIsImdweERvYyIsIkRPTVBhcnNlciIsInBhcnNlRnJvbVN0cmluZyIsImdlb0pzb24iLCJwdXNoIiwiZ3B4IiwiYXJyb3dMYXllciIsIm9uRWFjaEZlYXR1cmUiLCJmZWF0dXJlIiwibGF5ZXIiLCJjb29yZHMiLCJnZXRMYXRMbmdzIiwicG9seWxpbmVEZWNvcmF0b3IiLCJwYXR0ZXJucyIsIm9mZnNldCIsInJlcGVhdCIsInN5bWJvbCIsIlN5bWJvbCIsImFycm93SGVhZCIsInBpeGVsU2l6ZSIsInBhdGhPcHRpb25zIiwiZmlsbE9wYWNpdHkiLCJ3ZWlnaHQiLCJyZWFkQXNUZXh0Iiwib3BlbmdweCIsImFkZEV2ZW50TGlzdGVuZXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNPLElBQU1BLG9CQUFvQixHQUFHLFNBQXZCQSxvQkFBdUIsQ0FBQ0MsT0FBRCxFQUFhO0FBQzdDLE1BQUlDLENBQUMsR0FBR0MsSUFBSSxDQUFDQyxLQUFMLENBQVdILE9BQU8sR0FBRyxFQUFyQixDQUFSO0FBQ0EsTUFBSUksQ0FBQyxHQUFHSixPQUFPLEdBQUcsRUFBbEI7QUFDQUMsR0FBQyxHQUFHQSxDQUFDLEdBQUcsRUFBSixHQUFTLE1BQU1BLENBQWYsR0FBbUJBLENBQXZCO0FBQ0FHLEdBQUMsR0FBR0EsQ0FBQyxHQUFHLEVBQUosR0FBUyxNQUFNQSxDQUFmLEdBQW1CQSxDQUF2QjtBQUNBLFNBQU9ILENBQUMsR0FBRyxHQUFKLEdBQVVHLENBQWpCO0FBQ0gsQ0FOTSxDLENBUVA7O0FBQ08sU0FBU0MsZUFBVCxDQUF5QkMsUUFBekIsRUFBbUNDLEVBQW5DLEVBQXNDO0FBQ3pDO0FBQ0FDLEdBQUMsQ0FBQ0QsRUFBRCxDQUFELENBQU1FLEdBQU4sQ0FBVUgsUUFBVjtBQUNILEMsQ0FFRDs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOztBQUNBRSxDQUFDLENBQUMsUUFBRCxDQUFELENBQVlFLE1BQVosQ0FBbUIsWUFBWTtBQUMzQixNQUFJQyxLQUFLLEdBQUcsRUFBWjtBQUNBSCxHQUFDLENBQUMsUUFBRCxDQUFELENBQVlJLElBQVosQ0FBaUIsWUFBWTtBQUN6QkQsU0FBSyxDQUFDSCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFLLElBQVIsQ0FBYSxNQUFiLENBQUQsQ0FBTCxHQUE4QixJQUE5QjtBQUNILEdBRkQ7QUFHQSxNQUFJQyxLQUFLLEdBQUcsQ0FBWjtBQUNBTixHQUFDLENBQUNJLElBQUYsQ0FBT0QsS0FBUCxFQUFjLFlBQVk7QUFDdEJHLFNBQUs7QUFDUixHQUZEOztBQUdBLE1BQUlOLENBQUMsQ0FBQyxnQkFBRCxDQUFELENBQW9CTyxNQUFwQixLQUErQkQsS0FBbkMsRUFBMEM7QUFDdEMsUUFBSUUsS0FBSyxHQUFHLENBQVo7QUFDQVIsS0FBQyxDQUFDLDJCQUFELENBQUQsQ0FBK0JJLElBQS9CLENBQW9DLFlBQVk7QUFDNUNJLFdBQUssSUFBSUMsVUFBVSxDQUFDVCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFDLEdBQVIsRUFBRCxDQUFuQjtBQUNILEtBRkQ7QUFHQVMsY0FBVSxDQUFDRixLQUFELENBQVY7QUFDSDtBQUNKLENBaEJELEUsQ0FtQkE7O0FBQ0EsU0FBU0UsVUFBVCxDQUFvQkYsS0FBcEIsRUFBMEI7QUFDdEIsTUFBSUcsSUFBSixFQUFVQyxLQUFWOztBQUNBLE1BQUlKLEtBQUssSUFBSSxDQUFULElBQWNBLEtBQUssSUFBSSxDQUEzQixFQUE4QjtBQUMxQkcsUUFBSSxHQUFHSCxLQUFLLEdBQUcsZ0JBQWY7QUFDQUksU0FBSyxHQUFHLE9BQVI7QUFDSCxHQUhELE1BR08sSUFBSUosS0FBSyxJQUFJLENBQVQsSUFBY0EsS0FBSyxJQUFJLENBQTNCLEVBQThCO0FBQ2pDRyxRQUFJLEdBQUdILEtBQUssR0FBRyxXQUFmO0FBQ0FJLFNBQUssR0FBRyxNQUFSO0FBQ0gsR0FITSxNQUdBLElBQUlKLEtBQUssSUFBSSxDQUFULElBQWNBLEtBQUssSUFBSSxFQUEzQixFQUErQjtBQUNsQ0csUUFBSSxHQUFHSCxLQUFLLEdBQUcsY0FBZjtBQUNBSSxTQUFLLEdBQUcsS0FBUjtBQUNILEdBSE0sTUFHQTtBQUNIRCxRQUFJLEdBQUdILEtBQUssR0FBRyxtQkFBZjtBQUNBSSxTQUFLLEdBQUcsT0FBUjtBQUNIOztBQUNEWixHQUFDLENBQUMsMkJBQUQsQ0FBRCxDQUErQkMsR0FBL0IsQ0FBbUNVLElBQW5DO0FBQ0FYLEdBQUMsQ0FBQywyQkFBRCxDQUFELENBQStCYSxHQUEvQixDQUFtQztBQUFDLGFBQVNELEtBQVY7QUFBaUIsaUJBQWE7QUFBOUIsR0FBbkM7QUFDSCxDLENBRUQ7OztBQUNBLElBQUlFLFFBQVEsR0FBRyxDQUFmO0FBQ0FkLENBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJlLElBQWpCLENBQXNCRCxRQUFRLEdBQUcsaUJBQWpDO0FBQ0FkLENBQUMsQ0FBQywwQkFBRCxDQUFELENBQThCZ0IsS0FBOUIsQ0FBb0MsWUFBWTtBQUM1QyxNQUFJQyxXQUFXLEdBQUdqQixDQUFDLENBQUMsMEJBQUQsQ0FBRCxDQUE4QkMsR0FBOUIsR0FBb0NNLE1BQXREO0FBQ0FQLEdBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJlLElBQWpCLENBQXNCRSxXQUFXLEdBQUcsa0JBQXBDOztBQUNBLE1BQUlBLFdBQVcsSUFBSSxDQUFuQixFQUFzQjtBQUNsQmpCLEtBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJlLElBQWpCLENBQXNCRSxXQUFXLEdBQUcsaUJBQXBDO0FBQ0g7O0FBQ0QsTUFBSUEsV0FBVyxHQUFHLEdBQWxCLEVBQXVCO0FBQ25CQyxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0NDLEtBQXRDLENBQTRDUixLQUE1QyxHQUFvRCxLQUFwRDtBQUNILEdBRkQsTUFFTztBQUNITSxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0NDLEtBQXRDLENBQTRDUixLQUE1QyxHQUFvRCxPQUFwRDtBQUNIO0FBQ0osQ0FYRCxFLENBWUE7O0FBQ0FaLENBQUMsQ0FBQywwQkFBRCxDQUFELENBQThCcUIsRUFBOUIsQ0FBaUMsT0FBakMsRUFBMEMsWUFBWTtBQUNsRHJCLEdBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUUMsR0FBUixDQUFZRCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFDLEdBQVIsR0FBY3FCLE9BQWQsQ0FBc0IsWUFBdEIsRUFBb0MsRUFBcEMsQ0FBWjtBQUNILENBRkQsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaEZBO0FBQ0E7QUFDQTs7QUFDQSxJQUFNQyxTQUFTLEdBQUdDLG1CQUFPLENBQUMsNkVBQUQsQ0FBekI7O0FBQ0FBLG1CQUFPLENBQUMsaUdBQUQsQ0FBUDs7QUFDQUEsbUJBQU8sQ0FBQyw2R0FBRCxDQUFQLEMsQ0FFQzs7O0FBQ0EsU0FBU0MsY0FBVCxDQUF3QkMsSUFBeEIsRUFBNkI7QUFDMUIsTUFBS0EsSUFBSSxJQUFJLENBQVQsSUFBZ0JBLElBQUksSUFBSSxFQUE1QixFQUFpQztBQUFDUixZQUFRLENBQUNDLGNBQVQsQ0FBd0IsNEJBQXhCLEVBQXNEUSxPQUF0RCxHQUFnRSxJQUFoRTtBQUFxRTs7QUFDdkcsTUFBS0QsSUFBSSxJQUFJLEVBQVQsSUFBaUJBLElBQUksSUFBSSxFQUE3QixFQUFrQztBQUFDUixZQUFRLENBQUNDLGNBQVQsQ0FBd0IsNEJBQXhCLEVBQXNEUSxPQUF0RCxHQUFnRSxJQUFoRTtBQUFxRTs7QUFDeEcsTUFBS0QsSUFBSSxJQUFJLEVBQVQsSUFBaUJBLElBQUksSUFBSSxFQUE3QixFQUFrQztBQUFDUixZQUFRLENBQUNDLGNBQVQsQ0FBd0IsNEJBQXhCLEVBQXNEUSxPQUF0RCxHQUFnRSxJQUFoRTtBQUFxRTs7QUFDeEcsTUFBSUQsSUFBSSxJQUFJLEVBQVosRUFBZTtBQUFDUixZQUFRLENBQUNDLGNBQVQsQ0FBd0IsNEJBQXhCLEVBQXNEUSxPQUF0RCxHQUFnRSxJQUFoRTtBQUFxRTtBQUN4RixDLENBQ0Q7OztBQUNBLFNBQVNDLGVBQVQsQ0FBeUJDLEtBQXpCLEVBQStCO0FBQzNCLE1BQUtBLEtBQUssSUFBSSxDQUFWLElBQWlCQSxLQUFLLElBQUksR0FBOUIsRUFBb0M7QUFBQ1gsWUFBUSxDQUFDQyxjQUFULENBQXdCLDZCQUF4QixFQUF1RFEsT0FBdkQsR0FBaUUsSUFBakU7QUFBc0U7O0FBQzNHLE1BQUtFLEtBQUssSUFBSSxHQUFWLElBQW1CQSxLQUFLLElBQUksR0FBaEMsRUFBc0M7QUFBQ1gsWUFBUSxDQUFDQyxjQUFULENBQXdCLDZCQUF4QixFQUF1RFEsT0FBdkQsR0FBaUUsSUFBakU7QUFBc0U7O0FBQzdHLE1BQUtFLEtBQUssSUFBSSxHQUFWLElBQW1CQSxLQUFLLElBQUksR0FBaEMsRUFBc0M7QUFBQ1gsWUFBUSxDQUFDQyxjQUFULENBQXdCLDZCQUF4QixFQUF1RFEsT0FBdkQsR0FBaUUsSUFBakU7QUFBc0U7O0FBQzdHLE1BQUlFLEtBQUssSUFBSSxHQUFiLEVBQWlCO0FBQUNYLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3Qiw2QkFBeEIsRUFBdURRLE9BQXZELEdBQWlFLElBQWpFO0FBQXNFO0FBQzNGLEMsQ0FDRDs7O0FBQ0EsSUFBSUcsR0FBRyxHQUFHQyxDQUFDLENBQUNELEdBQUYsQ0FBTSxjQUFOLEVBQXNCO0FBQzVCRSxRQUFNLEVBQUUsQ0FBQyxFQUFELEVBQUssQ0FBTCxDQURvQjtBQUU1QkMsTUFBSSxFQUFFLENBRnNCO0FBRzVCQyxTQUFPLEVBQUUsQ0FIbUI7QUFJNUJDLGFBQVcsRUFBRSxJQUplO0FBSzVCQyxRQUFNLEVBQUUsQ0FBQ0MsaUVBQVksQ0FBQyxhQUFELENBQWI7QUFMb0IsQ0FBdEIsQ0FBVjtBQU9BTixDQUFDLENBQUNPLE9BQUYsQ0FBVUMsS0FBVixHQUFrQkMsS0FBbEIsQ0FBd0JWLEdBQXhCLEUsQ0FFQTs7QUFDQSxJQUFJVyxHQUFHLEdBQUcsQ0FBVjs7QUFDQSxTQUFTQyxXQUFULENBQXFCQyxDQUFyQixFQUF3QjtBQUNwQixNQUFJQyxLQUFLLEdBQUdELENBQUMsQ0FBQ0UsTUFBRixDQUFTRCxLQUFyQjtBQUFBLE1BQ0FFLE1BQU0sR0FBRyxJQUFJQyxVQUFKLEVBRFQ7QUFFQU4sS0FBRyxJQUFFLENBQUw7O0FBQ0EsTUFBSUEsR0FBRyxHQUFDLENBQVIsRUFBVTtBQUNOWCxPQUFHLENBQUNrQixNQUFKO0FBQ0FsQixPQUFHLEdBQUdDLENBQUMsQ0FBQ0QsR0FBRixDQUFNLGNBQU4sRUFBc0I7QUFDeEJFLFlBQU0sRUFBRSxDQUFDLEVBQUQsRUFBSyxDQUFMLENBRGdCO0FBRXhCQyxVQUFJLEVBQUUsQ0FGa0I7QUFHeEJDLGFBQU8sRUFBRSxDQUhlO0FBSXhCQyxpQkFBVyxFQUFFLElBSlc7QUFLeEJDLFlBQU0sRUFBRSxDQUFDQyxpRUFBWSxDQUFDLGFBQUQsQ0FBYjtBQUxnQixLQUF0QixDQUFOO0FBT0g7O0FBQ0RTLFFBQU0sQ0FBQ0csTUFBUCxHQUFnQixVQUFTQyxDQUFULEVBQVk7QUFDeEIsUUFBSUMsU0FBUyxHQUFHTCxNQUFNLENBQUNNLE1BQXZCO0FBQ0EsUUFBSUMsK0NBQUosQ0FBUUYsU0FBUixFQUFtQjtBQUFDRyxXQUFLLEVBQUUsSUFBUjtBQUNmQyxvQkFBYyxFQUFFO0FBQ1pDLG9CQUFZLEVBQUUseUNBREY7QUFFWkMsa0JBQVUsRUFBRSx1Q0FGQTtBQUdaQyxpQkFBUyxFQUFFLEVBSEM7QUFJWkMsbUJBQVcsRUFBRTtBQUpEO0FBREQsS0FBbkIsRUFPR3RDLEVBUEgsQ0FPTSxRQVBOLEVBT2dCLFVBQVM2QixDQUFULEVBQVk7QUFDeEJwQixTQUFHLENBQUM4QixTQUFKLENBQWNWLENBQUMsQ0FBQ0wsTUFBRixDQUFTZ0IsU0FBVCxFQUFkO0FBQ0EsVUFBSUMsR0FBRyxHQUFDWixDQUFDLENBQUNMLE1BQUYsQ0FBU2tCLFFBQVQsRUFBUjtBQUNBLFVBQUlyQyxJQUFJLEdBQUNoQyxJQUFJLENBQUNzRSxLQUFMLENBQVdkLENBQUMsQ0FBQ0wsTUFBRixDQUFTb0IsWUFBVCxLQUF3QixJQUFuQyxDQUFUO0FBQ0EsVUFBSUMsS0FBSyxHQUFDeEUsSUFBSSxDQUFDc0UsS0FBTCxDQUFZdEMsSUFBSSxHQUFDLEVBQU4sR0FBVSxFQUFyQixDQUFWO0FBQ0EsVUFBSUcsS0FBSyxHQUFHbkMsSUFBSSxDQUFDc0UsS0FBTCxDQUFXZCxDQUFDLENBQUNMLE1BQUYsQ0FBU3NCLGtCQUFULEVBQVgsQ0FBWjtBQUNBQywrREFBQSxDQUF5Qk4sR0FBekIsRUFBOEIsNEJBQTlCO0FBQ0FNLCtEQUFBLENBQXlCMUMsSUFBekIsRUFBK0IseUJBQS9CO0FBQ0EwQywrREFBQSxDQUF5QkEsOERBQUEsQ0FBOEJGLEtBQTlCLENBQXpCLEVBQStELHNCQUEvRDtBQUNBRSwrREFBQSxDQUF5QnZDLEtBQXpCLEVBQWdDLHlCQUFoQztBQUNBSixvQkFBYyxDQUFDQyxJQUFELENBQWQ7QUFDQUUscUJBQWUsQ0FBQ0MsS0FBRCxDQUFmO0FBQ0E7QUFDWjtBQUNTLEtBckJELEVBcUJHVyxLQXJCSCxDQXFCU1YsR0FyQlQ7QUF1QkEsUUFBSXVDLE1BQU0sR0FBRyxJQUFJQyxTQUFKLEdBQWdCQyxlQUFoQixDQUFnQ3BCLFNBQWhDLEVBQTJDLFVBQTNDLENBQWI7QUFDQSxRQUFJcUIsT0FBTyxHQUFDLEVBQVo7QUFDQUEsV0FBTyxDQUFDQyxJQUFSLENBQWFsRCxTQUFTLENBQUNtRCxHQUFWLENBQWNMLE1BQWQsQ0FBYjtBQUVBRyxXQUFPLENBQUMsQ0FBRCxDQUFQLENBQVcsWUFBWCxJQUNBO0FBQ0ksaUJBQVcsUUFEZjtBQUVJLGlCQUFXO0FBRmYsS0FEQTtBQUtBO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDUTs7QUFDQSxRQUFJRyxVQUFVLEdBQUc1QyxDQUFDLENBQUN5QyxPQUFGLENBQVVBLE9BQVYsRUFBbUI7QUFDaENJLG1CQUFhLEVBQUUsdUJBQVVDLE9BQVYsRUFBbUJDLEtBQW5CLEVBQTBCO0FBQ3JDLFlBQUlDLE1BQU0sR0FBR0QsS0FBSyxDQUFDRSxVQUFOLEVBQWI7QUFDQWpELFNBQUMsQ0FBQ2tELGlCQUFGLENBQW9CRixNQUFwQixFQUE0QjtBQUN4Qkcsa0JBQVEsRUFBRSxDQUFDO0FBQ1BDLGtCQUFNLEVBQUUsRUFERDtBQUVQQyxrQkFBTSxFQUFFLEdBRkQ7QUFHUEMsa0JBQU0sRUFBRXRELENBQUMsQ0FBQ3VELE1BQUYsQ0FBU0MsU0FBVCxDQUFtQjtBQUN2QkMsdUJBQVMsRUFBRSxFQURZO0FBRXZCQyx5QkFBVyxFQUFFO0FBQ1RDLDJCQUFXLEVBQUUsQ0FESjtBQUVUQyxzQkFBTSxFQUFFLENBRkM7QUFHVC9FLHFCQUFLLEVBQUM7QUFIRztBQUZVLGFBQW5CO0FBSEQsV0FBRDtBQURjLFNBQTVCLEVBYUc0QixLQWJILENBYVNWLEdBYlQ7QUFjSDtBQWpCK0IsS0FBbkIsRUFrQmRVLEtBbEJjLENBa0JSVixHQWxCUSxDQUFqQjtBQW1CSCxHQXBFRDs7QUFxRUFnQixRQUFNLENBQUM4QyxVQUFQLENBQWtCaEQsS0FBSyxDQUFDLENBQUQsQ0FBdkI7QUFDSDs7QUFDRGlELE9BQU8sQ0FBQ0MsZ0JBQVIsQ0FBeUIsUUFBekIsRUFBa0NwRCxXQUFsQyxFQUErQyxLQUEvQyxFIiwiZmlsZSI6ImNvdGF0aW9uX3Z0dC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vY29udmVyc2lvbiBtaW51dGVzIGVuIGhldXJlc1xyXG5leHBvcnQgY29uc3QgY29udmVydE1pbnNUb0hyc01pbnMgPSAobWludXRlcykgPT4ge1xyXG4gICAgdmFyIGggPSBNYXRoLmZsb29yKG1pbnV0ZXMgLyA2MCk7XHJcbiAgICB2YXIgbSA9IG1pbnV0ZXMgJSA2MDtcclxuICAgIGggPSBoIDwgMTAgPyAnMCcgKyBoIDogaDtcclxuICAgIG0gPSBtIDwgMTAgPyAnMCcgKyBtIDogbTtcclxuICAgIHJldHVybiBoICsgJzonICsgbTtcclxufVxyXG5cclxuLy9yZW1wbGlzc2FnZSBpbnB1dHNcclxuZXhwb3J0IGZ1bmN0aW9uIGFkZFZhbHVlVG9JbnB1dChpbnB1dFZhbCwgaWQpe1xyXG4gICAgLy9kb2N1bWVudC5nZXRFbGVtZW50QnlJZChpZCkudmFsdWUgPSBpbnB1dFZhbDtcclxuICAgICQoaWQpLnZhbChpbnB1dFZhbCk7XHJcbn1cclxuXHJcbi8vcmVjdXAgY29vcmRvbm5lZXMgcG9pbnRzIGRlcGFydHNcclxuLypjb25zdCBnZXRDb29yZHNEZXBhcnQgPSAoeG1sKSA9PiB7XHJcbiAgICBwYXJzZXIgPSBuZXcgRE9NUGFyc2VyKCk7XHJcbiAgICB4bWxEb2MgPSBwYXJzZXIucGFyc2VGcm9tU3RyaW5nKHhtbCwgXCJ0ZXh0L3htbFwiKTtcclxuICAgIHZhciBsYXQgPSB4bWxEb2MuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJ0cmtwdFwiKVswXS5nZXRBdHRyaWJ1dGUoJ2xhdCcpO1xyXG4gICAgdmFyIGxvbiA9IHhtbERvYy5nZXRFbGVtZW50c0J5VGFnTmFtZShcInRya3B0XCIpWzBdLmdldEF0dHJpYnV0ZSgnbG9uJyk7XHJcbiAgICByZXR1cm4gW2xvbiwgbGF0XTtcclxufSovXHJcblxyXG4vL2NhbGN1bCBkZSBsYSBkaWZmaWN1bHRlIGF2ZWMgcmFkaW9zIGJ1dHRvbnNcclxuJChcIjpyYWRpb1wiKS5jaGFuZ2UoZnVuY3Rpb24gKCkge1xyXG4gICAgbGV0IG5hbWVzID0ge307XHJcbiAgICAkKCc6cmFkaW8nKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBuYW1lc1skKHRoaXMpLmF0dHIoJ25hbWUnKV0gPSB0cnVlO1xyXG4gICAgfSk7XHJcbiAgICBsZXQgY291bnQgPSAwO1xyXG4gICAgJC5lYWNoKG5hbWVzLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgY291bnQrKztcclxuICAgIH0pO1xyXG4gICAgaWYgKCQoJzpyYWRpbzpjaGVja2VkJykubGVuZ3RoID09PSBjb3VudCkge1xyXG4gICAgICAgIGxldCB0b3RhbCA9IDA7XHJcbiAgICAgICAgJChcImlucHV0W3R5cGU9cmFkaW9dOmNoZWNrZWRcIikuZWFjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHRvdGFsICs9IHBhcnNlRmxvYXQoJCh0aGlzKS52YWwoKSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgb3V0cHV0RGlmZih0b3RhbCk7XHJcbiAgICB9XHJcbn0pO1xyXG5cclxuXHJcbi8vaW5wdXQgZGlmZmljdWx0ZVxyXG5mdW5jdGlvbiBvdXRwdXREaWZmKHRvdGFsKXtcclxuICAgIHZhciBkaWZmLCBjb2xvcjtcclxuICAgIGlmICh0b3RhbCA+PSA0ICYmIHRvdGFsIDw9IDUpIHtcclxuICAgICAgICBkaWZmID0gdG90YWwgKyBcIiAtIFRyw6hzIGZhY2lsZVwiO1xyXG4gICAgICAgIGNvbG9yID0gXCJncmVlblwiO1xyXG4gICAgfSBlbHNlIGlmICh0b3RhbCA+PSA2ICYmIHRvdGFsIDw9IDgpIHtcclxuICAgICAgICBkaWZmID0gdG90YWwgKyBcIiAtIEZhY2lsZVwiO1xyXG4gICAgICAgIGNvbG9yID0gXCJibHVlXCI7XHJcbiAgICB9IGVsc2UgaWYgKHRvdGFsID49IDkgJiYgdG90YWwgPD0gMTIpIHtcclxuICAgICAgICBkaWZmID0gdG90YWwgKyBcIiAtIERpZmZpY2lsZVwiO1xyXG4gICAgICAgIGNvbG9yID0gXCJyZWRcIjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgZGlmZiA9IHRvdGFsICsgXCIgLSBUcsOocyBEaWZmaWNpbGVcIjtcclxuICAgICAgICBjb2xvciA9IFwiYmxhY2tcIjtcclxuICAgIH1cclxuICAgICQoXCIjY290YXRpb25fZm9ybV9kaWZmaWN1bHRlXCIpLnZhbChkaWZmKTtcclxuICAgICQoXCIjY290YXRpb25fZm9ybV9kaWZmaWN1bHRlXCIpLmNzcyh7XCJjb2xvclwiOiBjb2xvciwgXCJmb250LXNpemVcIjogXCIyNXB4XCJ9KTtcclxufVxyXG5cclxuLy9jb21wdGUgbGUgbm9tYnJlIGRlIGNhcmFjdGVyZXMgZGFucyBsZSB0ZXh0YXJlYVxyXG52YXIgdGV4dEluaXQgPSAwO1xyXG4kKCcjY291bnRfdGV4dCcpLmh0bWwodGV4dEluaXQgKyAnIGNhcmFjdMOocmUgLzUwMCcpO1xyXG4kKCcjY290YXRpb25fZm9ybV90ZXh0RGVzY3InKS5rZXl1cChmdW5jdGlvbiAoKSB7XHJcbiAgICBsZXQgdGV4dF9sZW5ndGggPSAkKCcjY290YXRpb25fZm9ybV90ZXh0RGVzY3InKS52YWwoKS5sZW5ndGg7XHJcbiAgICAkKCcjY291bnRfdGV4dCcpLmh0bWwodGV4dF9sZW5ndGggKyAnIGNhcmFjdMOocmVzIC81MDAnKTtcclxuICAgIGlmICh0ZXh0X2xlbmd0aCA8PSAxKSB7XHJcbiAgICAgICAgJCgnI2NvdW50X3RleHQnKS5odG1sKHRleHRfbGVuZ3RoICsgJyBjYXJhY3TDqHJlIC81MDAnKTtcclxuICAgIH1cclxuICAgIGlmICh0ZXh0X2xlbmd0aCA+IDUwMCkge1xyXG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY291bnRfdGV4dFwiKS5zdHlsZS5jb2xvciA9ICdyZWQnO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdW50X3RleHRcIikuc3R5bGUuY29sb3IgPSAnYmxhY2snO1xyXG4gICAgfVxyXG59KTtcclxuLy9lbXBlY2hlciBsZXMgcmV0b3VycyBjaGFycmlvdHMgZHUgdGV4dGFyZWFcclxuJCgnI2NvdGF0aW9uX2Zvcm1fdGV4dERlc2NyJykub24oJ2tleXVwJywgZnVuY3Rpb24gKCkge1xyXG4gICAgJCh0aGlzKS52YWwoJCh0aGlzKS52YWwoKS5yZXBsYWNlKC9bXFxyXFxuXFx2XSsvZywgJycpKTtcclxufSk7XHJcbiIsImltcG9ydCB7IGJhc2VtYXBMYXllciB9IGZyb20gJ2VzcmktbGVhZmxldCc7XHJcbmltcG9ydCB7IEdQWCB9IGZyb20gJ2xlYWZsZXQtZ3B4JztcclxuaW1wb3J0ICogYXMgQ290YXRpb24gZnJvbSAnLi9jb3RhdGlvbic7XHJcbmNvbnN0IHRvR2VvSlNPTiA9IHJlcXVpcmUoXCJAdG1jdy90b2dlb2pzb25cIik7XHJcbnJlcXVpcmUoJ2xlYWZsZXQuaGVpZ2h0Z3JhcGgnKTtcclxucmVxdWlyZSgnbGVhZmxldC1wb2x5bGluZWRlY29yYXRvcicpO1xyXG5cclxuIC8vY29jaGUgYm91dG9uIHJhZGlvIGRpc3RhbmNlIGF1IGNoYXJnZW1lbnQgZHUgZ3B4XHJcbiBmdW5jdGlvbiByYWRpb0Rpc3RDaGVjayhkaXN0KXtcclxuICAgIGlmICgoZGlzdCA+PSAwKSAmJiAoZGlzdCA8PSAxMCkpIHtkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdGF0aW9uX2Zvcm1fZGlzdENob2ljZV8wXCIpLmNoZWNrZWQgPSB0cnVlfVxyXG4gICAgaWYgKChkaXN0ID49IDExKSAmJiAoZGlzdCA8PSAyMCkpIHtkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdGF0aW9uX2Zvcm1fZGlzdENob2ljZV8xXCIpLmNoZWNrZWQgPSB0cnVlfVxyXG4gICAgaWYgKChkaXN0ID49IDIxKSAmJiAoZGlzdCA8PSA0MCkpIHtkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdGF0aW9uX2Zvcm1fZGlzdENob2ljZV8yXCIpLmNoZWNrZWQgPSB0cnVlfVxyXG4gICAgaWYgKGRpc3QgPj0gNDEpe2RvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY290YXRpb25fZm9ybV9kaXN0Q2hvaWNlXzNcIikuY2hlY2tlZCA9IHRydWV9XHJcbn1cclxuLy9jb2NoZSBib3V0b24gcmFkaW8gZGVuaXZlbGUgYXUgY2hhcmdlbWVudCBkdSBncHhcclxuZnVuY3Rpb24gcmFkaW9EZW5pdkNoZWNrKGRlbml2KXtcclxuICAgIGlmICgoZGVuaXYgPj0gMCkgJiYgKGRlbml2IDw9IDEwMCkpIHtkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdGF0aW9uX2Zvcm1fZGVuaXZDaG9pY2VfMFwiKS5jaGVja2VkID0gdHJ1ZX1cclxuICAgIGlmICgoZGVuaXYgPj0gMTAxKSAmJiAoZGVuaXYgPD0gMjUwKSkge2RvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY290YXRpb25fZm9ybV9kZW5pdkNob2ljZV8xXCIpLmNoZWNrZWQgPSB0cnVlfVxyXG4gICAgaWYgKChkZW5pdiA+PSAyNTEpICYmIChkZW5pdiA8PSA2MDApKSB7ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjb3RhdGlvbl9mb3JtX2Rlbml2Q2hvaWNlXzJcIikuY2hlY2tlZCA9IHRydWV9XHJcbiAgICBpZiAoZGVuaXYgPj0gNjAxKXtkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdGF0aW9uX2Zvcm1fZGVuaXZDaG9pY2VfM1wiKS5jaGVja2VkID0gdHJ1ZX1cclxufVxyXG4vLyBmb25kcyBkZSBjYXJ0ZVxyXG52YXIgbWFwID0gTC5tYXAoJ2NvdGF0aW9uLW1hcCcsIHtcclxuICAgIGNlbnRlcjogWzQ3LCAyXSxcclxuICAgIHpvb206IDUsXHJcbiAgICBtaW5ab29tOiAzLFxyXG4gICAgem9vbUNvbnRyb2w6IHRydWUsXHJcbiAgICBsYXllcnM6IFtiYXNlbWFwTGF5ZXIoJ1RvcG9ncmFwaGljJyldXHJcbn0pO1xyXG5MLmNvbnRyb2wuc2NhbGUoKS5hZGRUbyhtYXApO1xyXG5cclxuLy9vdXZlcnR1cmUgZXQgYWZmaWNoYWdlIGR1IGdweFxyXG52YXIgY3B0ID0gMDtcclxuZnVuY3Rpb24gbG9hZEdweEZpbGUoZykge1xyXG4gICAgdmFyIGZpbGVzID0gZy50YXJnZXQuZmlsZXMsXHJcbiAgICByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xyXG4gICAgY3B0Kz0xO1xyXG4gICAgaWYgKGNwdD4xKXtcclxuICAgICAgICBtYXAucmVtb3ZlKCk7XHJcbiAgICAgICAgbWFwID0gTC5tYXAoJ2NvdGF0aW9uLW1hcCcsIHtcclxuICAgICAgICAgICAgY2VudGVyOiBbNDcsIDJdLFxyXG4gICAgICAgICAgICB6b29tOiA1LFxyXG4gICAgICAgICAgICBtaW5ab29tOiAzLFxyXG4gICAgICAgICAgICB6b29tQ29udHJvbDogdHJ1ZSxcclxuICAgICAgICAgICAgbGF5ZXJzOiBbYmFzZW1hcExheWVyKCdUb3BvZ3JhcGhpYycpXVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgcmVhZGVyLm9ubG9hZCA9IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICB2YXIgZ3B4U3RyaW5nID0gcmVhZGVyLnJlc3VsdDtcclxuICAgICAgICBuZXcgR1BYKGdweFN0cmluZywge2FzeW5jOiB0cnVlLFxyXG4gICAgICAgICAgICBtYXJrZXJfb3B0aW9uczoge1xyXG4gICAgICAgICAgICAgICAgc3RhcnRJY29uVXJsOiAnLi4vYnVpbGQvaW1hZ2VzL2ljb24vcGluLWljb24tc3RhcnQucG5nJyxcclxuICAgICAgICAgICAgICAgIGVuZEljb25Vcmw6ICcuLi9idWlsZC9pbWFnZXMvaWNvbi9waW4taWNvbi1lbmQucG5nJyxcclxuICAgICAgICAgICAgICAgIHNoYWRvd1VybDogJycsXHJcbiAgICAgICAgICAgICAgICB3cHRJY29uVXJsczogJydcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLm9uKCdsb2FkZWQnLCBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgICAgIG1hcC5maXRCb3VuZHMoZS50YXJnZXQuZ2V0Qm91bmRzKCkpO1xyXG4gICAgICAgICAgICBsZXQgbm9tPWUudGFyZ2V0LmdldF9uYW1lKCk7XHJcbiAgICAgICAgICAgIGxldCBkaXN0PU1hdGgucm91bmQoZS50YXJnZXQuZ2V0X2Rpc3RhbmNlKCkvMTAwMCk7XHJcbiAgICAgICAgICAgIGxldCB0ZW1wcz1NYXRoLnJvdW5kKChkaXN0LzE1KSo2MCk7XHJcbiAgICAgICAgICAgIGxldCBkZW5pdiA9IE1hdGgucm91bmQoZS50YXJnZXQuZ2V0X2VsZXZhdGlvbl9nYWluKCkpO1xyXG4gICAgICAgICAgICBDb3RhdGlvbi5hZGRWYWx1ZVRvSW5wdXQobm9tLCBcIiNjb3RhdGlvbl9mb3JtX25vbV9jaXJjdWl0XCIpO1xyXG4gICAgICAgICAgICBDb3RhdGlvbi5hZGRWYWx1ZVRvSW5wdXQoZGlzdCwgXCIjY290YXRpb25fZm9ybV9kaXN0YW5jZVwiKTtcclxuICAgICAgICAgICAgQ290YXRpb24uYWRkVmFsdWVUb0lucHV0KENvdGF0aW9uLmNvbnZlcnRNaW5zVG9IcnNNaW5zKHRlbXBzKSwgXCIjY290YXRpb25fZm9ybV90ZW1wc1wiKTtcclxuICAgICAgICAgICAgQ290YXRpb24uYWRkVmFsdWVUb0lucHV0KGRlbml2LCBcIiNjb3RhdGlvbl9mb3JtX2Rlbml2ZWxlXCIpO1xyXG4gICAgICAgICAgICByYWRpb0Rpc3RDaGVjayhkaXN0KTtcclxuICAgICAgICAgICAgcmFkaW9EZW5pdkNoZWNrKGRlbml2KTtcclxuICAgICAgICAgICAgLypyYWRpb1R5cGVWb2llQ2hlY2soZGlzdCk7XHJcbiAgICAgICAgICAgIHJhZGlvUGVudGVDaGVjayhkaXN0KTsqL1xyXG4gICAgICAgIH0pLmFkZFRvKG1hcCk7XHJcblxyXG4gICAgICAgIHZhciBncHhEb2MgPSBuZXcgRE9NUGFyc2VyKCkucGFyc2VGcm9tU3RyaW5nKGdweFN0cmluZywgJ3RleHQveG1sJyk7XHJcbiAgICAgICAgdmFyIGdlb0pzb249W107XHJcbiAgICAgICAgZ2VvSnNvbi5wdXNoKHRvR2VvSlNPTi5ncHgoZ3B4RG9jKSk7XHJcblxyXG4gICAgICAgIGdlb0pzb25bMF1bJ3Byb3BlcnRpZXMnXT1cclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIFwiQ3JlYXRvclwiOiBcIkZGVsOpbG9cIixcclxuICAgICAgICAgICAgXCJzdW1tYXJ5XCI6IFwiXCJcclxuICAgICAgICB9XHJcbiAgICAgICAgLyp2YXIgaGcgPSBMLmNvbnRyb2wuaGVpZ2h0Z3JhcGgoe1xyXG4gICAgICAgICAgICB3aWR0aDogODAwLFxyXG4gICAgICAgICAgICBoZWlnaHQ6IDI4MCxcclxuICAgICAgICAgICAgbWFyZ2luczoge1xyXG4gICAgICAgICAgICAgICAgdG9wOiAxMCxcclxuICAgICAgICAgICAgICAgIHJpZ2h0OiAzMCxcclxuICAgICAgICAgICAgICAgIGJvdHRvbTogNTUsXHJcbiAgICAgICAgICAgICAgICBsZWZ0OiA1MFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBwb3NpdGlvbjogXCJib3R0b21yaWdodFwiLFxyXG4gICAgICAgICAgICBtYXBwaW5nczogIHVuZGVmaW5lZFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGhnLmFkZFRvKG1hcCk7XHJcbiAgICAgICAgaGcuYWRkRGF0YShnZW9Kc29uKTsqL1xyXG4gICAgICAgIC8vR2VvanNvbiBwb3VyIGZsZWNoZXMgZGlyZWN0aW9ubmVsbGVzIHN1ciBsZSB0cmFjw6kgZHUgY2lyY3VpdFxyXG4gICAgICAgIHZhciBhcnJvd0xheWVyID0gTC5nZW9Kc29uKGdlb0pzb24sIHtcclxuICAgICAgICAgICAgb25FYWNoRmVhdHVyZTogZnVuY3Rpb24gKGZlYXR1cmUsIGxheWVyKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgY29vcmRzID0gbGF5ZXIuZ2V0TGF0TG5ncygpO1xyXG4gICAgICAgICAgICAgICAgTC5wb2x5bGluZURlY29yYXRvcihjb29yZHMsIHtcclxuICAgICAgICAgICAgICAgICAgICBwYXR0ZXJuczogW3tcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2Zmc2V0OiAyNSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVwZWF0OiAxMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN5bWJvbDogTC5TeW1ib2wuYXJyb3dIZWFkKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBpeGVsU2l6ZTogMTUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXRoT3B0aW9uczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGxPcGFjaXR5OiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdlaWdodDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjonIzAwMDBGRidcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICB9XVxyXG4gICAgICAgICAgICAgICAgfSkuYWRkVG8obWFwKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLmFkZFRvKG1hcCk7XHJcbiAgICB9XHJcbiAgICByZWFkZXIucmVhZEFzVGV4dChmaWxlc1swXSk7XHJcbn1cclxub3BlbmdweC5hZGRFdmVudExpc3RlbmVyKFwiY2hhbmdlXCIsbG9hZEdweEZpbGUsIGZhbHNlKTsiXSwic291cmNlUm9vdCI6IiJ9