(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cotation_route"],{

/***/ "./assets/js/pages/cotation/cotation.js":
/*!**********************************************!*\
  !*** ./assets/js/pages/cotation/cotation.js ***!
  \**********************************************/
/*! exports provided: convertMinsToHrsMins, addValueToInput */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "convertMinsToHrsMins", function() { return convertMinsToHrsMins; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addValueToInput", function() { return addValueToInput; });
/* harmony import */ var core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.parse-float.js */ "./node_modules/core-js/modules/es.parse-float.js");
/* harmony import */ var core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.regexp.exec.js */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.string.replace.js */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2__);



//conversion minutes en heures
var convertMinsToHrsMins = function convertMinsToHrsMins(minutes) {
  var h = Math.floor(minutes / 60);
  var m = minutes % 60;
  h = h < 10 ? '0' + h : h;
  m = m < 10 ? '0' + m : m;
  return h + ':' + m;
}; //remplissage inputs

function addValueToInput(inputVal, id) {
  //document.getElementById(id).value = inputVal;
  $(id).val(inputVal);
} //recup coordonnees points departs

/*const getCoordsDepart = (xml) => {
    parser = new DOMParser();
    xmlDoc = parser.parseFromString(xml, "text/xml");
    var lat = xmlDoc.getElementsByTagName("trkpt")[0].getAttribute('lat');
    var lon = xmlDoc.getElementsByTagName("trkpt")[0].getAttribute('lon');
    return [lon, lat];
}*/
//calcul de la difficulte avec radios buttons

$(":radio").change(function () {
  var names = {};
  $(':radio').each(function () {
    names[$(this).attr('name')] = true;
  });
  var count = 0;
  $.each(names, function () {
    count++;
  });

  if ($(':radio:checked').length === count) {
    var total = 0;
    $("input[type=radio]:checked").each(function () {
      total += parseFloat($(this).val());
    });
    outputDiff(total);
  }
}); //input difficulte

function outputDiff(total) {
  var diff, color;

  if (total >= 4 && total <= 5) {
    diff = total + " - Très facile";
    color = "green";
  } else if (total >= 6 && total <= 8) {
    diff = total + " - Facile";
    color = "blue";
  } else if (total >= 9 && total <= 12) {
    diff = total + " - Difficile";
    color = "red";
  } else {
    diff = total + " - Très Difficile";
    color = "black";
  }

  $("#cotation_form_difficulte").val(diff);
  $("#cotation_form_difficulte").css({
    "color": color,
    "font-size": "25px"
  });
} //compte le nombre de caracteres dans le textarea


var textInit = 0;
$('#count_text').html(textInit + ' caractère /500');
$('#cotation_form_textDescr').keyup(function () {
  var text_length = $('#cotation_form_textDescr').val().length;
  $('#count_text').html(text_length + ' caractères /500');

  if (text_length <= 1) {
    $('#count_text').html(text_length + ' caractère /500');
  }

  if (text_length > 500) {
    document.getElementById("count_text").style.color = 'red';
  } else {
    document.getElementById("count_text").style.color = 'black';
  }
}); //empecher les retours charriots du textarea

$('#cotation_form_textDescr').on('keyup', function () {
  $(this).val($(this).val().replace(/[\r\n\v]+/g, ''));
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/pages/cotation/cotationRoute.js":
/*!***************************************************!*\
  !*** ./assets/js/pages/cotation/cotationRoute.js ***!
  \***************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var esri_leaflet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! esri-leaflet */ "./node_modules/esri-leaflet/src/EsriLeaflet.js");
/* harmony import */ var leaflet_gpx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! leaflet-gpx */ "./node_modules/leaflet-gpx/gpx.js");
/* harmony import */ var leaflet_gpx__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(leaflet_gpx__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _cotation__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cotation */ "./assets/js/pages/cotation/cotation.js");





var toGeoJSON = __webpack_require__(/*! @tmcw/togeojson */ "./node_modules/@tmcw/togeojson/dist/togeojson.umd.js");

__webpack_require__(/*! leaflet.heightgraph */ "./node_modules/leaflet.heightgraph/dist/L.Control.Heightgraph.min.js");

__webpack_require__(/*! leaflet-polylinedecorator */ "./node_modules/leaflet-polylinedecorator/dist/leaflet.polylineDecorator.js"); //coche bouton radio distance au chargement du gpx


function radioDistCheck(dist) {
  if (dist >= 0 && dist <= 30) {
    document.getElementById("cotation_form_distChoice_0").checked = true;
  }

  if (dist >= 31 && dist <= 50) {
    document.getElementById("cotation_form_distChoice_1").checked = true;
  }

  if (dist >= 51 && dist <= 70) {
    document.getElementById("cotation_form_distChoice_2").checked = true;
  }

  if (dist >= 71) {
    document.getElementById("cotation_form_distChoice_3").checked = true;
  }
} //coche bouton radio denivele au chargement du gpx


function radioDenivCheck(deniv) {
  if (deniv >= 0 && deniv <= 100) {
    document.getElementById("cotation_form_denivChoice_0").checked = true;
  }

  if (deniv >= 101 && deniv <= 300) {
    document.getElementById("cotation_form_denivChoice_1").checked = true;
  }

  if (deniv >= 301 && deniv <= 1000) {
    document.getElementById("cotation_form_denivChoice_2").checked = true;
  }

  if (deniv >= 1001) {
    document.getElementById("cotation_form_denivChoice_3").checked = true;
  }
} // fonds de carte


var map = L.map('cotation-map', {
  center: [47, 2],
  zoom: 5,
  minZoom: 3,
  zoomControl: true,
  layers: [Object(esri_leaflet__WEBPACK_IMPORTED_MODULE_1__["basemapLayer"])('Topographic')]
});
L.control.scale().addTo(map); //ouverture et affichage du gpx

var cpt = 0;

function loadGpxFile(g) {
  var files = g.target.files,
      reader = new FileReader();
  cpt += 1;

  if (cpt > 1) {
    map.remove();
    map = L.map('cotation-map', {
      center: [47, 2],
      zoom: 5,
      minZoom: 3,
      zoomControl: true,
      layers: [Object(esri_leaflet__WEBPACK_IMPORTED_MODULE_1__["basemapLayer"])('Topographic')]
    });
  }

  reader.onload = function (e) {
    var gpxString = reader.result;
    new leaflet_gpx__WEBPACK_IMPORTED_MODULE_2__["GPX"](gpxString, {
      async: true,
      marker_options: {
        startIconUrl: '../build/images/icon/pin-icon-start.png',
        endIconUrl: '../build/images/icon/pin-icon-end.png',
        shadowUrl: '',
        wptIconUrls: ''
      }
    }).on('loaded', function (e) {
      map.fitBounds(e.target.getBounds());
      var nom = e.target.get_name();
      var dist = Math.round(e.target.get_distance() / 1000);
      var temps = Math.round(dist / 15 * 60);
      var deniv = Math.round(e.target.get_elevation_gain()); //console.log(nom,dist,temps,deniv);

      _cotation__WEBPACK_IMPORTED_MODULE_3__["addValueToInput"](nom, "#cotation_form_nom_circuit");
      _cotation__WEBPACK_IMPORTED_MODULE_3__["addValueToInput"](dist, "#cotation_form_distance");
      _cotation__WEBPACK_IMPORTED_MODULE_3__["addValueToInput"](_cotation__WEBPACK_IMPORTED_MODULE_3__["convertMinsToHrsMins"](temps), "#cotation_form_temps");
      _cotation__WEBPACK_IMPORTED_MODULE_3__["addValueToInput"](deniv, "#cotation_form_denivele");
      radioDistCheck(dist);
      radioDenivCheck(deniv);
    }).addTo(map);
    var gpxDoc = new DOMParser().parseFromString(gpxString, 'text/xml');
    var geoJson = [];
    geoJson.push(toGeoJSON.gpx(gpxDoc));
    geoJson[0]['properties'] = {
      "Creator": "Federation francaise de cyclotourisme",
      "summary": ""
    };
    /*var hg = L.control.heightgraph({
        width: 800,
        height: 280,
        margins: {
            top: 10,
            right: 30,
            bottom: 55,
            left: 50
        },
        position: "bottomright",
        mappings:  undefined
    });
    hg.addTo(map);
    hg.addData(geoJson);*/
    //Geojson pour fleches directionnelles sur le trace du circuit

    L.geoJson(geoJson, {
      onEachFeature: function onEachFeature(feature, layer) {
        var coords = layer.getLatLngs();
        L.polylineDecorator(coords, {
          patterns: [{
            offset: 25,
            repeat: 100,
            symbol: L.Symbol.arrowHead({
              pixelSize: 15,
              pathOptions: {
                fillOpacity: 1,
                weight: 0,
                color: '#0000FF'
              }
            })
          }]
        }).addTo(map);
      }
    }).addTo(map);
  };

  reader.readAsText(files[0]);
}

opengpx.addEventListener("change", loadGpxFile, false);

/***/ })

},[["./assets/js/pages/cotation/cotationRoute.js","runtime","vendors~app~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_velor~10347b53","vendors~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~61364f2f","vendors~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~cotatio~d7e263c1","vendors~cotation_route~cotation_veloroute~cotation_vtt~cotation_vttae"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvcGFnZXMvY290YXRpb24vY290YXRpb24uanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL3BhZ2VzL2NvdGF0aW9uL2NvdGF0aW9uUm91dGUuanMiXSwibmFtZXMiOlsiY29udmVydE1pbnNUb0hyc01pbnMiLCJtaW51dGVzIiwiaCIsIk1hdGgiLCJmbG9vciIsIm0iLCJhZGRWYWx1ZVRvSW5wdXQiLCJpbnB1dFZhbCIsImlkIiwiJCIsInZhbCIsImNoYW5nZSIsIm5hbWVzIiwiZWFjaCIsImF0dHIiLCJjb3VudCIsImxlbmd0aCIsInRvdGFsIiwicGFyc2VGbG9hdCIsIm91dHB1dERpZmYiLCJkaWZmIiwiY29sb3IiLCJjc3MiLCJ0ZXh0SW5pdCIsImh0bWwiLCJrZXl1cCIsInRleHRfbGVuZ3RoIiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50QnlJZCIsInN0eWxlIiwib24iLCJyZXBsYWNlIiwidG9HZW9KU09OIiwicmVxdWlyZSIsInJhZGlvRGlzdENoZWNrIiwiZGlzdCIsImNoZWNrZWQiLCJyYWRpb0Rlbml2Q2hlY2siLCJkZW5pdiIsIm1hcCIsIkwiLCJjZW50ZXIiLCJ6b29tIiwibWluWm9vbSIsInpvb21Db250cm9sIiwibGF5ZXJzIiwiYmFzZW1hcExheWVyIiwiY29udHJvbCIsInNjYWxlIiwiYWRkVG8iLCJjcHQiLCJsb2FkR3B4RmlsZSIsImciLCJmaWxlcyIsInRhcmdldCIsInJlYWRlciIsIkZpbGVSZWFkZXIiLCJyZW1vdmUiLCJvbmxvYWQiLCJlIiwiZ3B4U3RyaW5nIiwicmVzdWx0IiwiR1BYIiwiYXN5bmMiLCJtYXJrZXJfb3B0aW9ucyIsInN0YXJ0SWNvblVybCIsImVuZEljb25VcmwiLCJzaGFkb3dVcmwiLCJ3cHRJY29uVXJscyIsImZpdEJvdW5kcyIsImdldEJvdW5kcyIsIm5vbSIsImdldF9uYW1lIiwicm91bmQiLCJnZXRfZGlzdGFuY2UiLCJ0ZW1wcyIsImdldF9lbGV2YXRpb25fZ2FpbiIsIkNvdGF0aW9uIiwiZ3B4RG9jIiwiRE9NUGFyc2VyIiwicGFyc2VGcm9tU3RyaW5nIiwiZ2VvSnNvbiIsInB1c2giLCJncHgiLCJvbkVhY2hGZWF0dXJlIiwiZmVhdHVyZSIsImxheWVyIiwiY29vcmRzIiwiZ2V0TGF0TG5ncyIsInBvbHlsaW5lRGVjb3JhdG9yIiwicGF0dGVybnMiLCJvZmZzZXQiLCJyZXBlYXQiLCJzeW1ib2wiLCJTeW1ib2wiLCJhcnJvd0hlYWQiLCJwaXhlbFNpemUiLCJwYXRoT3B0aW9ucyIsImZpbGxPcGFjaXR5Iiwid2VpZ2h0IiwicmVhZEFzVGV4dCIsIm9wZW5ncHgiLCJhZGRFdmVudExpc3RlbmVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDTyxJQUFNQSxvQkFBb0IsR0FBRyxTQUF2QkEsb0JBQXVCLENBQUNDLE9BQUQsRUFBYTtBQUM3QyxNQUFJQyxDQUFDLEdBQUdDLElBQUksQ0FBQ0MsS0FBTCxDQUFXSCxPQUFPLEdBQUcsRUFBckIsQ0FBUjtBQUNBLE1BQUlJLENBQUMsR0FBR0osT0FBTyxHQUFHLEVBQWxCO0FBQ0FDLEdBQUMsR0FBR0EsQ0FBQyxHQUFHLEVBQUosR0FBUyxNQUFNQSxDQUFmLEdBQW1CQSxDQUF2QjtBQUNBRyxHQUFDLEdBQUdBLENBQUMsR0FBRyxFQUFKLEdBQVMsTUFBTUEsQ0FBZixHQUFtQkEsQ0FBdkI7QUFDQSxTQUFPSCxDQUFDLEdBQUcsR0FBSixHQUFVRyxDQUFqQjtBQUNILENBTk0sQyxDQVFQOztBQUNPLFNBQVNDLGVBQVQsQ0FBeUJDLFFBQXpCLEVBQW1DQyxFQUFuQyxFQUFzQztBQUN6QztBQUNBQyxHQUFDLENBQUNELEVBQUQsQ0FBRCxDQUFNRSxHQUFOLENBQVVILFFBQVY7QUFDSCxDLENBRUQ7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7QUFDQUUsQ0FBQyxDQUFDLFFBQUQsQ0FBRCxDQUFZRSxNQUFaLENBQW1CLFlBQVk7QUFDM0IsTUFBSUMsS0FBSyxHQUFHLEVBQVo7QUFDQUgsR0FBQyxDQUFDLFFBQUQsQ0FBRCxDQUFZSSxJQUFaLENBQWlCLFlBQVk7QUFDekJELFNBQUssQ0FBQ0gsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRSyxJQUFSLENBQWEsTUFBYixDQUFELENBQUwsR0FBOEIsSUFBOUI7QUFDSCxHQUZEO0FBR0EsTUFBSUMsS0FBSyxHQUFHLENBQVo7QUFDQU4sR0FBQyxDQUFDSSxJQUFGLENBQU9ELEtBQVAsRUFBYyxZQUFZO0FBQ3RCRyxTQUFLO0FBQ1IsR0FGRDs7QUFHQSxNQUFJTixDQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQk8sTUFBcEIsS0FBK0JELEtBQW5DLEVBQTBDO0FBQ3RDLFFBQUlFLEtBQUssR0FBRyxDQUFaO0FBQ0FSLEtBQUMsQ0FBQywyQkFBRCxDQUFELENBQStCSSxJQUEvQixDQUFvQyxZQUFZO0FBQzVDSSxXQUFLLElBQUlDLFVBQVUsQ0FBQ1QsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRQyxHQUFSLEVBQUQsQ0FBbkI7QUFDSCxLQUZEO0FBR0FTLGNBQVUsQ0FBQ0YsS0FBRCxDQUFWO0FBQ0g7QUFDSixDQWhCRCxFLENBbUJBOztBQUNBLFNBQVNFLFVBQVQsQ0FBb0JGLEtBQXBCLEVBQTBCO0FBQ3RCLE1BQUlHLElBQUosRUFBVUMsS0FBVjs7QUFDQSxNQUFJSixLQUFLLElBQUksQ0FBVCxJQUFjQSxLQUFLLElBQUksQ0FBM0IsRUFBOEI7QUFDMUJHLFFBQUksR0FBR0gsS0FBSyxHQUFHLGdCQUFmO0FBQ0FJLFNBQUssR0FBRyxPQUFSO0FBQ0gsR0FIRCxNQUdPLElBQUlKLEtBQUssSUFBSSxDQUFULElBQWNBLEtBQUssSUFBSSxDQUEzQixFQUE4QjtBQUNqQ0csUUFBSSxHQUFHSCxLQUFLLEdBQUcsV0FBZjtBQUNBSSxTQUFLLEdBQUcsTUFBUjtBQUNILEdBSE0sTUFHQSxJQUFJSixLQUFLLElBQUksQ0FBVCxJQUFjQSxLQUFLLElBQUksRUFBM0IsRUFBK0I7QUFDbENHLFFBQUksR0FBR0gsS0FBSyxHQUFHLGNBQWY7QUFDQUksU0FBSyxHQUFHLEtBQVI7QUFDSCxHQUhNLE1BR0E7QUFDSEQsUUFBSSxHQUFHSCxLQUFLLEdBQUcsbUJBQWY7QUFDQUksU0FBSyxHQUFHLE9BQVI7QUFDSDs7QUFDRFosR0FBQyxDQUFDLDJCQUFELENBQUQsQ0FBK0JDLEdBQS9CLENBQW1DVSxJQUFuQztBQUNBWCxHQUFDLENBQUMsMkJBQUQsQ0FBRCxDQUErQmEsR0FBL0IsQ0FBbUM7QUFBQyxhQUFTRCxLQUFWO0FBQWlCLGlCQUFhO0FBQTlCLEdBQW5DO0FBQ0gsQyxDQUVEOzs7QUFDQSxJQUFJRSxRQUFRLEdBQUcsQ0FBZjtBQUNBZCxDQUFDLENBQUMsYUFBRCxDQUFELENBQWlCZSxJQUFqQixDQUFzQkQsUUFBUSxHQUFHLGlCQUFqQztBQUNBZCxDQUFDLENBQUMsMEJBQUQsQ0FBRCxDQUE4QmdCLEtBQTlCLENBQW9DLFlBQVk7QUFDNUMsTUFBSUMsV0FBVyxHQUFHakIsQ0FBQyxDQUFDLDBCQUFELENBQUQsQ0FBOEJDLEdBQTlCLEdBQW9DTSxNQUF0RDtBQUNBUCxHQUFDLENBQUMsYUFBRCxDQUFELENBQWlCZSxJQUFqQixDQUFzQkUsV0FBVyxHQUFHLGtCQUFwQzs7QUFDQSxNQUFJQSxXQUFXLElBQUksQ0FBbkIsRUFBc0I7QUFDbEJqQixLQUFDLENBQUMsYUFBRCxDQUFELENBQWlCZSxJQUFqQixDQUFzQkUsV0FBVyxHQUFHLGlCQUFwQztBQUNIOztBQUNELE1BQUlBLFdBQVcsR0FBRyxHQUFsQixFQUF1QjtBQUNuQkMsWUFBUSxDQUFDQyxjQUFULENBQXdCLFlBQXhCLEVBQXNDQyxLQUF0QyxDQUE0Q1IsS0FBNUMsR0FBb0QsS0FBcEQ7QUFDSCxHQUZELE1BRU87QUFDSE0sWUFBUSxDQUFDQyxjQUFULENBQXdCLFlBQXhCLEVBQXNDQyxLQUF0QyxDQUE0Q1IsS0FBNUMsR0FBb0QsT0FBcEQ7QUFDSDtBQUNKLENBWEQsRSxDQVlBOztBQUNBWixDQUFDLENBQUMsMEJBQUQsQ0FBRCxDQUE4QnFCLEVBQTlCLENBQWlDLE9BQWpDLEVBQTBDLFlBQVk7QUFDbERyQixHQUFDLENBQUMsSUFBRCxDQUFELENBQVFDLEdBQVIsQ0FBWUQsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRQyxHQUFSLEdBQWNxQixPQUFkLENBQXNCLFlBQXRCLEVBQW9DLEVBQXBDLENBQVo7QUFDSCxDQUZELEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2hGQTtBQUNBO0FBQ0E7O0FBQ0EsSUFBTUMsU0FBUyxHQUFHQyxtQkFBTyxDQUFDLDZFQUFELENBQXpCOztBQUNBQSxtQkFBTyxDQUFDLGlHQUFELENBQVA7O0FBQ0FBLG1CQUFPLENBQUMsNkdBQUQsQ0FBUCxDLENBRUM7OztBQUNBLFNBQVNDLGNBQVQsQ0FBd0JDLElBQXhCLEVBQTZCO0FBQzFCLE1BQUtBLElBQUksSUFBSSxDQUFULElBQWdCQSxJQUFJLElBQUksRUFBNUIsRUFBaUM7QUFBQ1IsWUFBUSxDQUFDQyxjQUFULENBQXdCLDRCQUF4QixFQUFzRFEsT0FBdEQsR0FBZ0UsSUFBaEU7QUFBcUU7O0FBQ3ZHLE1BQUtELElBQUksSUFBSSxFQUFULElBQWlCQSxJQUFJLElBQUksRUFBN0IsRUFBa0M7QUFBQ1IsWUFBUSxDQUFDQyxjQUFULENBQXdCLDRCQUF4QixFQUFzRFEsT0FBdEQsR0FBZ0UsSUFBaEU7QUFBcUU7O0FBQ3hHLE1BQUtELElBQUksSUFBSSxFQUFULElBQWlCQSxJQUFJLElBQUksRUFBN0IsRUFBa0M7QUFBQ1IsWUFBUSxDQUFDQyxjQUFULENBQXdCLDRCQUF4QixFQUFzRFEsT0FBdEQsR0FBZ0UsSUFBaEU7QUFBcUU7O0FBQ3hHLE1BQUlELElBQUksSUFBSSxFQUFaLEVBQWU7QUFBQ1IsWUFBUSxDQUFDQyxjQUFULENBQXdCLDRCQUF4QixFQUFzRFEsT0FBdEQsR0FBZ0UsSUFBaEU7QUFBcUU7QUFDeEYsQyxDQUNEOzs7QUFDQSxTQUFTQyxlQUFULENBQXlCQyxLQUF6QixFQUErQjtBQUMzQixNQUFLQSxLQUFLLElBQUksQ0FBVixJQUFpQkEsS0FBSyxJQUFJLEdBQTlCLEVBQW9DO0FBQUNYLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3Qiw2QkFBeEIsRUFBdURRLE9BQXZELEdBQWlFLElBQWpFO0FBQXNFOztBQUMzRyxNQUFLRSxLQUFLLElBQUksR0FBVixJQUFtQkEsS0FBSyxJQUFJLEdBQWhDLEVBQXNDO0FBQUNYLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3Qiw2QkFBeEIsRUFBdURRLE9BQXZELEdBQWlFLElBQWpFO0FBQXNFOztBQUM3RyxNQUFLRSxLQUFLLElBQUksR0FBVixJQUFtQkEsS0FBSyxJQUFJLElBQWhDLEVBQXVDO0FBQUNYLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3Qiw2QkFBeEIsRUFBdURRLE9BQXZELEdBQWlFLElBQWpFO0FBQXNFOztBQUM5RyxNQUFJRSxLQUFLLElBQUksSUFBYixFQUFrQjtBQUFDWCxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsNkJBQXhCLEVBQXVEUSxPQUF2RCxHQUFpRSxJQUFqRTtBQUFzRTtBQUM1RixDLENBRUQ7OztBQUNBLElBQUlHLEdBQUcsR0FBR0MsQ0FBQyxDQUFDRCxHQUFGLENBQU0sY0FBTixFQUFzQjtBQUM1QkUsUUFBTSxFQUFFLENBQUMsRUFBRCxFQUFLLENBQUwsQ0FEb0I7QUFFNUJDLE1BQUksRUFBRSxDQUZzQjtBQUc1QkMsU0FBTyxFQUFFLENBSG1CO0FBSTVCQyxhQUFXLEVBQUUsSUFKZTtBQUs1QkMsUUFBTSxFQUFFLENBQUNDLGlFQUFZLENBQUMsYUFBRCxDQUFiO0FBTG9CLENBQXRCLENBQVY7QUFPQU4sQ0FBQyxDQUFDTyxPQUFGLENBQVVDLEtBQVYsR0FBa0JDLEtBQWxCLENBQXdCVixHQUF4QixFLENBRUE7O0FBQ0EsSUFBSVcsR0FBRyxHQUFHLENBQVY7O0FBQ0EsU0FBU0MsV0FBVCxDQUFxQkMsQ0FBckIsRUFBd0I7QUFDcEIsTUFBSUMsS0FBSyxHQUFHRCxDQUFDLENBQUNFLE1BQUYsQ0FBU0QsS0FBckI7QUFBQSxNQUNBRSxNQUFNLEdBQUcsSUFBSUMsVUFBSixFQURUO0FBRUFOLEtBQUcsSUFBRSxDQUFMOztBQUNBLE1BQUlBLEdBQUcsR0FBQyxDQUFSLEVBQVU7QUFDTlgsT0FBRyxDQUFDa0IsTUFBSjtBQUNBbEIsT0FBRyxHQUFHQyxDQUFDLENBQUNELEdBQUYsQ0FBTSxjQUFOLEVBQXNCO0FBQ3hCRSxZQUFNLEVBQUUsQ0FBQyxFQUFELEVBQUssQ0FBTCxDQURnQjtBQUV4QkMsVUFBSSxFQUFFLENBRmtCO0FBR3hCQyxhQUFPLEVBQUUsQ0FIZTtBQUl4QkMsaUJBQVcsRUFBRSxJQUpXO0FBS3hCQyxZQUFNLEVBQUUsQ0FBQ0MsaUVBQVksQ0FBQyxhQUFELENBQWI7QUFMZ0IsS0FBdEIsQ0FBTjtBQU9IOztBQUNEUyxRQUFNLENBQUNHLE1BQVAsR0FBZ0IsVUFBU0MsQ0FBVCxFQUFZO0FBQ3hCLFFBQUlDLFNBQVMsR0FBR0wsTUFBTSxDQUFDTSxNQUF2QjtBQUNBLFFBQUlDLCtDQUFKLENBQVFGLFNBQVIsRUFBbUI7QUFBQ0csV0FBSyxFQUFFLElBQVI7QUFDZkMsb0JBQWMsRUFBRTtBQUNaQyxvQkFBWSxFQUFFLHlDQURGO0FBRVpDLGtCQUFVLEVBQUUsdUNBRkE7QUFHWkMsaUJBQVMsRUFBRSxFQUhDO0FBSVpDLG1CQUFXLEVBQUU7QUFKRDtBQURELEtBQW5CLEVBT0d0QyxFQVBILENBT00sUUFQTixFQU9nQixVQUFTNkIsQ0FBVCxFQUFZO0FBQ3hCcEIsU0FBRyxDQUFDOEIsU0FBSixDQUFjVixDQUFDLENBQUNMLE1BQUYsQ0FBU2dCLFNBQVQsRUFBZDtBQUNBLFVBQUlDLEdBQUcsR0FBQ1osQ0FBQyxDQUFDTCxNQUFGLENBQVNrQixRQUFULEVBQVI7QUFDQSxVQUFJckMsSUFBSSxHQUFDaEMsSUFBSSxDQUFDc0UsS0FBTCxDQUFXZCxDQUFDLENBQUNMLE1BQUYsQ0FBU29CLFlBQVQsS0FBd0IsSUFBbkMsQ0FBVDtBQUNBLFVBQUlDLEtBQUssR0FBQ3hFLElBQUksQ0FBQ3NFLEtBQUwsQ0FBWXRDLElBQUksR0FBQyxFQUFOLEdBQVUsRUFBckIsQ0FBVjtBQUNBLFVBQUlHLEtBQUssR0FBR25DLElBQUksQ0FBQ3NFLEtBQUwsQ0FBV2QsQ0FBQyxDQUFDTCxNQUFGLENBQVNzQixrQkFBVCxFQUFYLENBQVosQ0FMd0IsQ0FNeEI7O0FBQ0FDLCtEQUFBLENBQXlCTixHQUF6QixFQUE4Qiw0QkFBOUI7QUFDQU0sK0RBQUEsQ0FBeUIxQyxJQUF6QixFQUErQix5QkFBL0I7QUFDQTBDLCtEQUFBLENBQXlCQSw4REFBQSxDQUE4QkYsS0FBOUIsQ0FBekIsRUFBK0Qsc0JBQS9EO0FBQ0FFLCtEQUFBLENBQXlCdkMsS0FBekIsRUFBZ0MseUJBQWhDO0FBQ0FKLG9CQUFjLENBQUNDLElBQUQsQ0FBZDtBQUNBRSxxQkFBZSxDQUFDQyxLQUFELENBQWY7QUFDSCxLQXBCRCxFQW9CR1csS0FwQkgsQ0FvQlNWLEdBcEJUO0FBcUJBLFFBQUl1QyxNQUFNLEdBQUcsSUFBSUMsU0FBSixHQUFnQkMsZUFBaEIsQ0FBZ0NwQixTQUFoQyxFQUEyQyxVQUEzQyxDQUFiO0FBQ0EsUUFBSXFCLE9BQU8sR0FBQyxFQUFaO0FBQ0FBLFdBQU8sQ0FBQ0MsSUFBUixDQUFhbEQsU0FBUyxDQUFDbUQsR0FBVixDQUFjTCxNQUFkLENBQWI7QUFDQUcsV0FBTyxDQUFDLENBQUQsQ0FBUCxDQUFXLFlBQVgsSUFDQTtBQUNJLGlCQUFXLHVDQURmO0FBRUksaUJBQVc7QUFGZixLQURBO0FBS0E7QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNROztBQUNBekMsS0FBQyxDQUFDeUMsT0FBRixDQUFVQSxPQUFWLEVBQW1CO0FBQ2ZHLG1CQUFhLEVBQUUsdUJBQVVDLE9BQVYsRUFBbUJDLEtBQW5CLEVBQTBCO0FBQ3JDLFlBQUlDLE1BQU0sR0FBR0QsS0FBSyxDQUFDRSxVQUFOLEVBQWI7QUFDQWhELFNBQUMsQ0FBQ2lELGlCQUFGLENBQW9CRixNQUFwQixFQUE0QjtBQUN4Qkcsa0JBQVEsRUFBRSxDQUFDO0FBQ1BDLGtCQUFNLEVBQUUsRUFERDtBQUVQQyxrQkFBTSxFQUFFLEdBRkQ7QUFHUEMsa0JBQU0sRUFBRXJELENBQUMsQ0FBQ3NELE1BQUYsQ0FBU0MsU0FBVCxDQUFtQjtBQUN2QkMsdUJBQVMsRUFBRSxFQURZO0FBRXZCQyx5QkFBVyxFQUFFO0FBQ1RDLDJCQUFXLEVBQUUsQ0FESjtBQUVUQyxzQkFBTSxFQUFFLENBRkM7QUFHVDlFLHFCQUFLLEVBQUM7QUFIRztBQUZVLGFBQW5CO0FBSEQsV0FBRDtBQURjLFNBQTVCLEVBYUc0QixLQWJILENBYVNWLEdBYlQ7QUFjSDtBQWpCYyxLQUFuQixFQWtCR1UsS0FsQkgsQ0FrQlNWLEdBbEJUO0FBbUJILEdBakVEOztBQWtFQWdCLFFBQU0sQ0FBQzZDLFVBQVAsQ0FBa0IvQyxLQUFLLENBQUMsQ0FBRCxDQUF2QjtBQUNIOztBQUNEZ0QsT0FBTyxDQUFDQyxnQkFBUixDQUF5QixRQUF6QixFQUFrQ25ELFdBQWxDLEVBQStDLEtBQS9DLEUiLCJmaWxlIjoiY290YXRpb25fcm91dGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvL2NvbnZlcnNpb24gbWludXRlcyBlbiBoZXVyZXNcclxuZXhwb3J0IGNvbnN0IGNvbnZlcnRNaW5zVG9IcnNNaW5zID0gKG1pbnV0ZXMpID0+IHtcclxuICAgIHZhciBoID0gTWF0aC5mbG9vcihtaW51dGVzIC8gNjApO1xyXG4gICAgdmFyIG0gPSBtaW51dGVzICUgNjA7XHJcbiAgICBoID0gaCA8IDEwID8gJzAnICsgaCA6IGg7XHJcbiAgICBtID0gbSA8IDEwID8gJzAnICsgbSA6IG07XHJcbiAgICByZXR1cm4gaCArICc6JyArIG07XHJcbn1cclxuXHJcbi8vcmVtcGxpc3NhZ2UgaW5wdXRzXHJcbmV4cG9ydCBmdW5jdGlvbiBhZGRWYWx1ZVRvSW5wdXQoaW5wdXRWYWwsIGlkKXtcclxuICAgIC8vZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoaWQpLnZhbHVlID0gaW5wdXRWYWw7XHJcbiAgICAkKGlkKS52YWwoaW5wdXRWYWwpO1xyXG59XHJcblxyXG4vL3JlY3VwIGNvb3Jkb25uZWVzIHBvaW50cyBkZXBhcnRzXHJcbi8qY29uc3QgZ2V0Q29vcmRzRGVwYXJ0ID0gKHhtbCkgPT4ge1xyXG4gICAgcGFyc2VyID0gbmV3IERPTVBhcnNlcigpO1xyXG4gICAgeG1sRG9jID0gcGFyc2VyLnBhcnNlRnJvbVN0cmluZyh4bWwsIFwidGV4dC94bWxcIik7XHJcbiAgICB2YXIgbGF0ID0geG1sRG9jLmdldEVsZW1lbnRzQnlUYWdOYW1lKFwidHJrcHRcIilbMF0uZ2V0QXR0cmlidXRlKCdsYXQnKTtcclxuICAgIHZhciBsb24gPSB4bWxEb2MuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJ0cmtwdFwiKVswXS5nZXRBdHRyaWJ1dGUoJ2xvbicpO1xyXG4gICAgcmV0dXJuIFtsb24sIGxhdF07XHJcbn0qL1xyXG5cclxuLy9jYWxjdWwgZGUgbGEgZGlmZmljdWx0ZSBhdmVjIHJhZGlvcyBidXR0b25zXHJcbiQoXCI6cmFkaW9cIikuY2hhbmdlKGZ1bmN0aW9uICgpIHtcclxuICAgIGxldCBuYW1lcyA9IHt9O1xyXG4gICAgJCgnOnJhZGlvJykuZWFjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgbmFtZXNbJCh0aGlzKS5hdHRyKCduYW1lJyldID0gdHJ1ZTtcclxuICAgIH0pO1xyXG4gICAgbGV0IGNvdW50ID0gMDtcclxuICAgICQuZWFjaChuYW1lcywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGNvdW50Kys7XHJcbiAgICB9KTtcclxuICAgIGlmICgkKCc6cmFkaW86Y2hlY2tlZCcpLmxlbmd0aCA9PT0gY291bnQpIHtcclxuICAgICAgICBsZXQgdG90YWwgPSAwO1xyXG4gICAgICAgICQoXCJpbnB1dFt0eXBlPXJhZGlvXTpjaGVja2VkXCIpLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB0b3RhbCArPSBwYXJzZUZsb2F0KCQodGhpcykudmFsKCkpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIG91dHB1dERpZmYodG90YWwpO1xyXG4gICAgfVxyXG59KTtcclxuXHJcblxyXG4vL2lucHV0IGRpZmZpY3VsdGVcclxuZnVuY3Rpb24gb3V0cHV0RGlmZih0b3RhbCl7XHJcbiAgICB2YXIgZGlmZiwgY29sb3I7XHJcbiAgICBpZiAodG90YWwgPj0gNCAmJiB0b3RhbCA8PSA1KSB7XHJcbiAgICAgICAgZGlmZiA9IHRvdGFsICsgXCIgLSBUcsOocyBmYWNpbGVcIjtcclxuICAgICAgICBjb2xvciA9IFwiZ3JlZW5cIjtcclxuICAgIH0gZWxzZSBpZiAodG90YWwgPj0gNiAmJiB0b3RhbCA8PSA4KSB7XHJcbiAgICAgICAgZGlmZiA9IHRvdGFsICsgXCIgLSBGYWNpbGVcIjtcclxuICAgICAgICBjb2xvciA9IFwiYmx1ZVwiO1xyXG4gICAgfSBlbHNlIGlmICh0b3RhbCA+PSA5ICYmIHRvdGFsIDw9IDEyKSB7XHJcbiAgICAgICAgZGlmZiA9IHRvdGFsICsgXCIgLSBEaWZmaWNpbGVcIjtcclxuICAgICAgICBjb2xvciA9IFwicmVkXCI7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIGRpZmYgPSB0b3RhbCArIFwiIC0gVHLDqHMgRGlmZmljaWxlXCI7XHJcbiAgICAgICAgY29sb3IgPSBcImJsYWNrXCI7XHJcbiAgICB9XHJcbiAgICAkKFwiI2NvdGF0aW9uX2Zvcm1fZGlmZmljdWx0ZVwiKS52YWwoZGlmZik7XHJcbiAgICAkKFwiI2NvdGF0aW9uX2Zvcm1fZGlmZmljdWx0ZVwiKS5jc3Moe1wiY29sb3JcIjogY29sb3IsIFwiZm9udC1zaXplXCI6IFwiMjVweFwifSk7XHJcbn1cclxuXHJcbi8vY29tcHRlIGxlIG5vbWJyZSBkZSBjYXJhY3RlcmVzIGRhbnMgbGUgdGV4dGFyZWFcclxudmFyIHRleHRJbml0ID0gMDtcclxuJCgnI2NvdW50X3RleHQnKS5odG1sKHRleHRJbml0ICsgJyBjYXJhY3TDqHJlIC81MDAnKTtcclxuJCgnI2NvdGF0aW9uX2Zvcm1fdGV4dERlc2NyJykua2V5dXAoZnVuY3Rpb24gKCkge1xyXG4gICAgbGV0IHRleHRfbGVuZ3RoID0gJCgnI2NvdGF0aW9uX2Zvcm1fdGV4dERlc2NyJykudmFsKCkubGVuZ3RoO1xyXG4gICAgJCgnI2NvdW50X3RleHQnKS5odG1sKHRleHRfbGVuZ3RoICsgJyBjYXJhY3TDqHJlcyAvNTAwJyk7XHJcbiAgICBpZiAodGV4dF9sZW5ndGggPD0gMSkge1xyXG4gICAgICAgICQoJyNjb3VudF90ZXh0JykuaHRtbCh0ZXh0X2xlbmd0aCArICcgY2FyYWN0w6hyZSAvNTAwJyk7XHJcbiAgICB9XHJcbiAgICBpZiAodGV4dF9sZW5ndGggPiA1MDApIHtcclxuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdW50X3RleHRcIikuc3R5bGUuY29sb3IgPSAncmVkJztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjb3VudF90ZXh0XCIpLnN0eWxlLmNvbG9yID0gJ2JsYWNrJztcclxuICAgIH1cclxufSk7XHJcbi8vZW1wZWNoZXIgbGVzIHJldG91cnMgY2hhcnJpb3RzIGR1IHRleHRhcmVhXHJcbiQoJyNjb3RhdGlvbl9mb3JtX3RleHREZXNjcicpLm9uKCdrZXl1cCcsIGZ1bmN0aW9uICgpIHtcclxuICAgICQodGhpcykudmFsKCQodGhpcykudmFsKCkucmVwbGFjZSgvW1xcclxcblxcdl0rL2csICcnKSk7XHJcbn0pO1xyXG4iLCJpbXBvcnQgeyBiYXNlbWFwTGF5ZXIgfSBmcm9tICdlc3JpLWxlYWZsZXQnO1xyXG5pbXBvcnQgeyBHUFggfSBmcm9tICdsZWFmbGV0LWdweCc7XHJcbmltcG9ydCAqIGFzIENvdGF0aW9uIGZyb20gJy4vY290YXRpb24nO1xyXG5jb25zdCB0b0dlb0pTT04gPSByZXF1aXJlKFwiQHRtY3cvdG9nZW9qc29uXCIpO1xyXG5yZXF1aXJlKCdsZWFmbGV0LmhlaWdodGdyYXBoJyk7XHJcbnJlcXVpcmUoJ2xlYWZsZXQtcG9seWxpbmVkZWNvcmF0b3InKTtcclxuXHJcbiAvL2NvY2hlIGJvdXRvbiByYWRpbyBkaXN0YW5jZSBhdSBjaGFyZ2VtZW50IGR1IGdweFxyXG4gZnVuY3Rpb24gcmFkaW9EaXN0Q2hlY2soZGlzdCl7XHJcbiAgICBpZiAoKGRpc3QgPj0gMCkgJiYgKGRpc3QgPD0gMzApKSB7ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjb3RhdGlvbl9mb3JtX2Rpc3RDaG9pY2VfMFwiKS5jaGVja2VkID0gdHJ1ZX1cclxuICAgIGlmICgoZGlzdCA+PSAzMSkgJiYgKGRpc3QgPD0gNTApKSB7ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjb3RhdGlvbl9mb3JtX2Rpc3RDaG9pY2VfMVwiKS5jaGVja2VkID0gdHJ1ZX1cclxuICAgIGlmICgoZGlzdCA+PSA1MSkgJiYgKGRpc3QgPD0gNzApKSB7ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjb3RhdGlvbl9mb3JtX2Rpc3RDaG9pY2VfMlwiKS5jaGVja2VkID0gdHJ1ZX1cclxuICAgIGlmIChkaXN0ID49IDcxKXtkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdGF0aW9uX2Zvcm1fZGlzdENob2ljZV8zXCIpLmNoZWNrZWQgPSB0cnVlfVxyXG59XHJcbi8vY29jaGUgYm91dG9uIHJhZGlvIGRlbml2ZWxlIGF1IGNoYXJnZW1lbnQgZHUgZ3B4XHJcbmZ1bmN0aW9uIHJhZGlvRGVuaXZDaGVjayhkZW5pdil7XHJcbiAgICBpZiAoKGRlbml2ID49IDApICYmIChkZW5pdiA8PSAxMDApKSB7ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjb3RhdGlvbl9mb3JtX2Rlbml2Q2hvaWNlXzBcIikuY2hlY2tlZCA9IHRydWV9XHJcbiAgICBpZiAoKGRlbml2ID49IDEwMSkgJiYgKGRlbml2IDw9IDMwMCkpIHtkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdGF0aW9uX2Zvcm1fZGVuaXZDaG9pY2VfMVwiKS5jaGVja2VkID0gdHJ1ZX1cclxuICAgIGlmICgoZGVuaXYgPj0gMzAxKSAmJiAoZGVuaXYgPD0gMTAwMCkpIHtkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdGF0aW9uX2Zvcm1fZGVuaXZDaG9pY2VfMlwiKS5jaGVja2VkID0gdHJ1ZX1cclxuICAgIGlmIChkZW5pdiA+PSAxMDAxKXtkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdGF0aW9uX2Zvcm1fZGVuaXZDaG9pY2VfM1wiKS5jaGVja2VkID0gdHJ1ZX1cclxufVxyXG5cclxuLy8gZm9uZHMgZGUgY2FydGVcclxudmFyIG1hcCA9IEwubWFwKCdjb3RhdGlvbi1tYXAnLCB7XHJcbiAgICBjZW50ZXI6IFs0NywgMl0sXHJcbiAgICB6b29tOiA1LFxyXG4gICAgbWluWm9vbTogMyxcclxuICAgIHpvb21Db250cm9sOiB0cnVlLFxyXG4gICAgbGF5ZXJzOiBbYmFzZW1hcExheWVyKCdUb3BvZ3JhcGhpYycpXVxyXG59KTtcclxuTC5jb250cm9sLnNjYWxlKCkuYWRkVG8obWFwKTtcclxuXHJcbi8vb3V2ZXJ0dXJlIGV0IGFmZmljaGFnZSBkdSBncHhcclxudmFyIGNwdCA9IDA7XHJcbmZ1bmN0aW9uIGxvYWRHcHhGaWxlKGcpIHtcclxuICAgIHZhciBmaWxlcyA9IGcudGFyZ2V0LmZpbGVzLFxyXG4gICAgcmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcclxuICAgIGNwdCs9MTtcclxuICAgIGlmIChjcHQ+MSl7XHJcbiAgICAgICAgbWFwLnJlbW92ZSgpO1xyXG4gICAgICAgIG1hcCA9IEwubWFwKCdjb3RhdGlvbi1tYXAnLCB7XHJcbiAgICAgICAgICAgIGNlbnRlcjogWzQ3LCAyXSxcclxuICAgICAgICAgICAgem9vbTogNSxcclxuICAgICAgICAgICAgbWluWm9vbTogMyxcclxuICAgICAgICAgICAgem9vbUNvbnRyb2w6IHRydWUsXHJcbiAgICAgICAgICAgIGxheWVyczogW2Jhc2VtYXBMYXllcignVG9wb2dyYXBoaWMnKV1cclxuICAgICAgICB9KTtcdFxyXG4gICAgfVxyXG4gICAgcmVhZGVyLm9ubG9hZCA9IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICB2YXIgZ3B4U3RyaW5nID0gcmVhZGVyLnJlc3VsdDtcclxuICAgICAgICBuZXcgR1BYKGdweFN0cmluZywge2FzeW5jOiB0cnVlLFxyXG4gICAgICAgICAgICBtYXJrZXJfb3B0aW9uczoge1xyXG4gICAgICAgICAgICAgICAgc3RhcnRJY29uVXJsOiAnLi4vYnVpbGQvaW1hZ2VzL2ljb24vcGluLWljb24tc3RhcnQucG5nJyxcclxuICAgICAgICAgICAgICAgIGVuZEljb25Vcmw6ICcuLi9idWlsZC9pbWFnZXMvaWNvbi9waW4taWNvbi1lbmQucG5nJyxcclxuICAgICAgICAgICAgICAgIHNoYWRvd1VybDogJycsXHJcbiAgICAgICAgICAgICAgICB3cHRJY29uVXJsczogJydcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLm9uKCdsb2FkZWQnLCBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgICAgIG1hcC5maXRCb3VuZHMoZS50YXJnZXQuZ2V0Qm91bmRzKCkpO1xyXG4gICAgICAgICAgICBsZXQgbm9tPWUudGFyZ2V0LmdldF9uYW1lKCk7XHJcbiAgICAgICAgICAgIGxldCBkaXN0PU1hdGgucm91bmQoZS50YXJnZXQuZ2V0X2Rpc3RhbmNlKCkvMTAwMCk7XHJcbiAgICAgICAgICAgIGxldCB0ZW1wcz1NYXRoLnJvdW5kKChkaXN0LzE1KSo2MCk7XHJcbiAgICAgICAgICAgIGxldCBkZW5pdiA9IE1hdGgucm91bmQoZS50YXJnZXQuZ2V0X2VsZXZhdGlvbl9nYWluKCkpO1xyXG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKG5vbSxkaXN0LHRlbXBzLGRlbml2KTtcclxuICAgICAgICAgICAgQ290YXRpb24uYWRkVmFsdWVUb0lucHV0KG5vbSwgXCIjY290YXRpb25fZm9ybV9ub21fY2lyY3VpdFwiKTtcclxuICAgICAgICAgICAgQ290YXRpb24uYWRkVmFsdWVUb0lucHV0KGRpc3QsIFwiI2NvdGF0aW9uX2Zvcm1fZGlzdGFuY2VcIik7XHJcbiAgICAgICAgICAgIENvdGF0aW9uLmFkZFZhbHVlVG9JbnB1dChDb3RhdGlvbi5jb252ZXJ0TWluc1RvSHJzTWlucyh0ZW1wcyksIFwiI2NvdGF0aW9uX2Zvcm1fdGVtcHNcIik7XHJcbiAgICAgICAgICAgIENvdGF0aW9uLmFkZFZhbHVlVG9JbnB1dChkZW5pdiwgXCIjY290YXRpb25fZm9ybV9kZW5pdmVsZVwiKTtcclxuICAgICAgICAgICAgcmFkaW9EaXN0Q2hlY2soZGlzdCk7XHJcbiAgICAgICAgICAgIHJhZGlvRGVuaXZDaGVjayhkZW5pdik7XHJcbiAgICAgICAgfSkuYWRkVG8obWFwKTtcclxuICAgICAgICB2YXIgZ3B4RG9jID0gbmV3IERPTVBhcnNlcigpLnBhcnNlRnJvbVN0cmluZyhncHhTdHJpbmcsICd0ZXh0L3htbCcpO1xyXG4gICAgICAgIHZhciBnZW9Kc29uPVtdO1xyXG4gICAgICAgIGdlb0pzb24ucHVzaCh0b0dlb0pTT04uZ3B4KGdweERvYykpO1xyXG4gICAgICAgIGdlb0pzb25bMF1bJ3Byb3BlcnRpZXMnXT1cclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIFwiQ3JlYXRvclwiOiBcIkZlZGVyYXRpb24gZnJhbmNhaXNlIGRlIGN5Y2xvdG91cmlzbWVcIixcclxuICAgICAgICAgICAgXCJzdW1tYXJ5XCI6IFwiXCJcclxuICAgICAgICB9XHJcbiAgICAgICAgLyp2YXIgaGcgPSBMLmNvbnRyb2wuaGVpZ2h0Z3JhcGgoe1xyXG4gICAgICAgICAgICB3aWR0aDogODAwLFxyXG4gICAgICAgICAgICBoZWlnaHQ6IDI4MCxcclxuICAgICAgICAgICAgbWFyZ2luczoge1xyXG4gICAgICAgICAgICAgICAgdG9wOiAxMCxcclxuICAgICAgICAgICAgICAgIHJpZ2h0OiAzMCxcclxuICAgICAgICAgICAgICAgIGJvdHRvbTogNTUsXHJcbiAgICAgICAgICAgICAgICBsZWZ0OiA1MFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBwb3NpdGlvbjogXCJib3R0b21yaWdodFwiLFxyXG4gICAgICAgICAgICBtYXBwaW5nczogIHVuZGVmaW5lZFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGhnLmFkZFRvKG1hcCk7XHJcbiAgICAgICAgaGcuYWRkRGF0YShnZW9Kc29uKTsqL1xyXG4gICAgICAgIC8vR2VvanNvbiBwb3VyIGZsZWNoZXMgZGlyZWN0aW9ubmVsbGVzIHN1ciBsZSB0cmFjZSBkdSBjaXJjdWl0XHJcbiAgICAgICAgTC5nZW9Kc29uKGdlb0pzb24sIHtcclxuICAgICAgICAgICAgb25FYWNoRmVhdHVyZTogZnVuY3Rpb24gKGZlYXR1cmUsIGxheWVyKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgY29vcmRzID0gbGF5ZXIuZ2V0TGF0TG5ncygpO1xyXG4gICAgICAgICAgICAgICAgTC5wb2x5bGluZURlY29yYXRvcihjb29yZHMsIHtcclxuICAgICAgICAgICAgICAgICAgICBwYXR0ZXJuczogW3tcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2Zmc2V0OiAyNSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVwZWF0OiAxMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN5bWJvbDogTC5TeW1ib2wuYXJyb3dIZWFkKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBpeGVsU2l6ZTogMTUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXRoT3B0aW9uczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGxPcGFjaXR5OiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdlaWdodDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjonIzAwMDBGRidcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICB9XVxyXG4gICAgICAgICAgICAgICAgfSkuYWRkVG8obWFwKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLmFkZFRvKG1hcCk7XHJcbiAgICB9XHJcbiAgICByZWFkZXIucmVhZEFzVGV4dChmaWxlc1swXSk7XHJcbn1cclxub3BlbmdweC5hZGRFdmVudExpc3RlbmVyKFwiY2hhbmdlXCIsbG9hZEdweEZpbGUsIGZhbHNlKTsiXSwic291cmNlUm9vdCI6IiJ9