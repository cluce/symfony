(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["labels_velo"],{

/***/ "./assets/js/pages/labelsVelo.js":
/*!***************************************!*\
  !*** ./assets/js/pages/labelsVelo.js ***!
  \***************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.concat.js */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.filter.js */ "./node_modules/core-js/modules/es.array.filter.js");
/* harmony import */ var core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.array.for-each.js */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_includes_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.includes.js */ "./node_modules/core-js/modules/es.array.includes.js");
/* harmony import */ var core_js_modules_es_array_includes_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_includes_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_index_of_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.index-of.js */ "./node_modules/core-js/modules/es.array.index-of.js");
/* harmony import */ var core_js_modules_es_array_index_of_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_index_of_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_join_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.join.js */ "./node_modules/core-js/modules/es.array.join.js");
/* harmony import */ var core_js_modules_es_array_join_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_join_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_array_reverse_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.array.reverse.js */ "./node_modules/core-js/modules/es.array.reverse.js");
/* harmony import */ var core_js_modules_es_array_reverse_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_reverse_js__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_array_slice_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.array.slice.js */ "./node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_array_splice_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.array.splice.js */ "./node_modules/core-js/modules/es.array.splice.js");
/* harmony import */ var core_js_modules_es_array_splice_js__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_splice_js__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.regexp.exec.js */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_string_includes_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.string.includes.js */ "./node_modules/core-js/modules/es.string.includes.js");
/* harmony import */ var core_js_modules_es_string_includes_js__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_includes_js__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_es_string_search_js__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/es.string.search.js */ "./node_modules/core-js/modules/es.string.search.js");
/* harmony import */ var core_js_modules_es_string_search_js__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_search_js__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! core-js/modules/es.string.split.js */ "./node_modules/core-js/modules/es.string.split.js");
/* harmony import */ var core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each.js */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _services_mapConfig__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../services/mapConfig */ "./assets/js/services/mapConfig.js");
/* harmony import */ var _components_popup__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../components/popup */ "./assets/js/components/popup.js");
/* harmony import */ var _services_whiteList__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../services/whiteList */ "./assets/js/services/whiteList.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var _components_InfoFeature__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../components/InfoFeature */ "./assets/js/components/InfoFeature.jsx");
/* harmony import */ var _yaireo_tagify__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @yaireo/tagify */ "./node_modules/@yaireo/tagify/dist/tagify.min.js");
/* harmony import */ var _yaireo_tagify__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(_yaireo_tagify__WEBPACK_IMPORTED_MODULE_21__);























(function mapLabelsVelo() {
  //map
  var map = L.map('map', {
    center: [47, 0],
    zoom: 5,
    minZoom: 3,
    zoomControl: true,
    layers: [L.esri.basemapLayer('Topographic')]
  });
  map.addControl(new _services_mapConfig__WEBPACK_IMPORTED_MODULE_15__["geolocationButton"](map)); //variables globales

  var mapElement = document.getElementById("map");
  var mapWidth = document.getElementById("map").offsetWidth;
  var defaultWhereClause = location.search ? Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_15__["queryString"])(location.search) : "(REFERENCE_SOUSTYPEPOI IN (101,204,205)) AND VISIBILITE=1"; //layer points departs circuits

  var poiLayer = L.esri.Cluster.featureLayer({
    url: _services_mapConfig__WEBPACK_IMPORTED_MODULE_15__["agolFeatureServices"].poi,
    where: defaultWhereClause,
    pointToLayer: function pointToLayer(feature, latlng) {
      var ssTypePoi = feature.properties.REFERENCE_SOUSTYPEPOI;
      return L.marker(latlng, {
        icon: _services_mapConfig__WEBPACK_IMPORTED_MODULE_15__["iconPoi"][ssTypePoi]
      });
    },
    iconCreateFunction: function iconCreateFunction(cluster) {
      var count = cluster.getChildCount();
      var digits = (count + '').length;
      return L.divIcon({
        html: count,
        className: 'cluster digits-' + digits,
        iconSize: null
      });
    }
  }).addTo(_services_mapConfig__WEBPACK_IMPORTED_MODULE_15__["poiLayerGroup"]).addTo(map); //popup bonnes adresses

  poiLayer.bindPopup(function (layer) {
    var nom = layer.feature.properties.NOM;
    var idPoi = layer.feature.properties.IDENTIFIANT_POI;
    var type = layer.feature.properties.REFERENCE_TYPEPOI;
    var refSousType = layer.feature.properties.REFERENCE_SOUSTYPEPOI;
    return L.Util.template(Object(_components_popup__WEBPACK_IMPORTED_MODULE_16__["popupTemplatePoi"])(nom, idPoi, type, refSousType));
  });
  /*evenements carte et layer bonnes adresses*/

  poiLayer.on('load', function () {
    syncSidebar();
  });
  poiLayer.once('load', function () {
    if (location.pathname != menuUrl) {
      var parts = location.href.split(menuUrl + "/");
      var urlid = parts[1].split("-");
      renderInfoFeature(poiLayer, urlid[0]);
    }

    ;
    Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_15__["zoomToMarkersAfterLoad"])(map, poiLayer);
  });
  poiLayer.on('popupopen', function () {
    document.getElementById("buttonPopup").addEventListener('click', function () {
      renderInfoFeature(poiLayer, document.getElementById("buttonPopup").getAttribute('value'));
    }, false);
  });
  map.on("moveend", function () {
    poiLayer.setWhere(poiLayer.getWhere());
    syncSidebar();
  });
  map.on('overlayremove', function (e) {
    _services_mapConfig__WEBPACK_IMPORTED_MODULE_15__["poiLayerGroup"].clearLayers();
  });
  /*fin evenements carte et layer point depart*/

  /*filtres de recherches */

  new _yaireo_tagify__WEBPACK_IMPORTED_MODULE_21___default.a(document.querySelector("textarea[name=tagsDepts]"), {
    enforeWhitelist: true,
    whitelist: _services_whiteList__WEBPACK_IMPORTED_MODULE_17__["deptList"]
  });

  var filters = function filters() {
    var filters = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var elements = document.querySelectorAll(".typePoiFilter, textarea[name=tagsDepts]");
    elements.forEach(function (item) {
      item.addEventListener('change', function (e) {
        if (document.querySelectorAll("input:checked, textarea[name=tagsDepts]").length === 0) {
          filters = [];
          globalFilter(filters);
        } else {
          if (item.checked === true && e.target.id != "deptFilter") {
            filters.push(item.value);
          } else if (e.target.id == "deptFilter" && e.target.value) {
            var jsonDepts = JSON.parse(e.target.value);
            filters = filters.filter(function (f) {
              return f.indexOf("Code_Dept") !== 0;
            });
            jsonDepts.forEach(function (itemDept) {
              filters.push(itemDept.queryPoi);
            });
          } else {
            filters.splice(filters.indexOf(item.value), 1);
          }

          globalFilter(filters);
        }
      });
    });
  };

  filters();

  var globalFilter = function globalFilter(filters) {
    var dept = filters.filter(function (f) {
      return f.includes("Code_Dept");
    }).join(" OR ");
    var typePoi = filters.filter(function (f) {
      return f.includes("REFERENCE_SOUSTYPEPOI");
    }).join(" OR ");
    var sousTypePoi = filters.filter(function (f) {
      return f.includes("REFERENCE_TYPEPOI");
    }).join(" OR ");
    var result = "".concat(defaultWhereClause =  false ? undefined : "").concat(typePoi ? "(" + typePoi + ") AND " : "").concat(sousTypePoi ? "(" + sousTypePoi + ") AND " : "").concat(dept ? "(" + dept + ") AND " : "").slice(0, -4); //console.log(result);

    poiLayer.setWhere(result);
  };
  /*fin des filtres de recherches */

  /* barre de recherche adresse et poi (geocodeur en haut a gauche de la carte) */


  var searchControl = L.esri.Geocoding.geosearch({
    useMapBounds: false,
    title: 'Rechercher un lieu / un label vélo',
    placeholder: 'Trouver un lieu / un label vélo',
    expanded: true,
    collapseAfterResult: false,
    position: 'topleft',
    providers: [_services_mapConfig__WEBPACK_IMPORTED_MODULE_15__["arcgisOnlineProvider"], L.esri.Geocoding.featureLayerProvider({
      //recherche des couches dans la gdb vef
      url: poiLayer.options.url,
      where: poiLayer.getWhere(),
      searchFields: ['NOM'],
      label: 'LABELS VELO:',
      maxResults: 7,
      bufferRadius: 1000,
      formatSuggestion: function formatSuggestion(feature) {
        return "".concat(feature.properties.NOM, " - ").concat(_services_mapConfig__WEBPACK_IMPORTED_MODULE_15__["baName"][feature.properties.REFERENCE_SOUSTYPEPOI]);
      }
    })]
  }).addTo(map); //barre de recherche en dehors de la carte

  var esriGeocoder = searchControl.getContainer();
  Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_15__["setEsriGeocoder"])(esriGeocoder, document.getElementById('geocoder')); //recup la requete en cours et applique aux resultats du geocodeur

  searchControl.on("requestend", function () {
    searchControl.options.providers[1].options.where = poiLayer.getWhere();
  });
  /* fin barre de recherche adresse et poi (geocodeur en haut a gauche de la carte) */

  /*cards dans la sidebar*/

  var syncSidebar = function syncSidebar() {
    var arrResultsSidebar = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    $("#features").empty();
    poiLayer.eachActiveFeature(function (layer) {
      //eachActiveFeature = fonction a ajouter dans esri-leaflet-cluster.js - attention si maj de la librairie
      if (map.hasLayer(poiLayer)) {
        if (map.getBounds().contains(layer.getLatLng())) {
          arrResultsSidebar.push(sidebarTemplatePoi(layer.feature.properties.NOM, _services_mapConfig__WEBPACK_IMPORTED_MODULE_15__["poiName"][layer.feature.properties.REFERENCE_SOUSTYPEPOI], layer.feature.properties.COMMUNE, layer.feature.properties.Nom_Dept, layer.feature.properties.IDENTIFIANT_POI, layer.feature.geometry.coordinates[1], layer.feature.geometry.coordinates[0]));
        }
      }
    });
    arrResultsSidebar.reverse();
    Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_15__["addResultsToSidebar"])(arrResultsSidebar, poiLayer, renderInfoFeature);
  }; //template cards


  var sidebarTemplatePoi = function sidebarTemplatePoi(nom, soustype, ville, dept, id, lat, lon) {
    return "<div class=\"card feature-card my-2 p-2\" id=\"".concat(id, "\" data-lat=\"").concat(lat, "\" data-lon=\"").concat(lon, "\"> \n\t\t\t\t<div class=\"col align-self-center rounded-right\">\n\t\t\t\t\t<h6 class=\"d-block text-uppercase\">").concat(nom, "</h6>\n\t\t\t\t\t<span class=\"badge badge-pill badge-light my-1 mr-1\">").concat(soustype, "</span>\n\t\t\t\t\t<span class=\"my-1 mr-1\"><br/><i class=\"fas fa-map-marked-alt\"></i> ").concat(ville, ", ").concat(dept, "</span>\n\t\t\t\t</div>\n\t\t\t</div>");
  };
  /* fin des cards dans la sidebar*/
  //composant fiche descriptive


  var renderInfoFeature = function renderInfoFeature(Layer, id) {
    var queryContact = L.esri.query({
      url: _services_mapConfig__WEBPACK_IMPORTED_MODULE_15__["agolFeatureServices"].fournisseur
    });
    mapElement.style.width = mapWidth + "px";
    var params = location.search;
    var infofeatureElement = document.getElementById('infofeature');
    infofeatureElement.value = Layer.getWhere();
    Layer.eachFeature(function (layer) {
      if (layer.feature.properties.IDENTIFIANT_POI == id) {
        Layer.setWhere("IDENTIFIANT_POI=" + id);
        Object(react_dom__WEBPACK_IMPORTED_MODULE_19__["unmountComponentAtNode"])(infofeatureElement);
        Object(react_dom__WEBPACK_IMPORTED_MODULE_19__["render"])( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_18___default.a.createElement(_components_InfoFeature__WEBPACK_IMPORTED_MODULE_20__["default"], {
          idPoi: id,
          nom: layer.feature.properties.NOM,
          queryContactLabels: queryContact,
          descr: layer.feature.properties.DESCRIPTION,
          soustype: _services_mapConfig__WEBPACK_IMPORTED_MODULE_15__["poiName"][layer.feature.properties.REFERENCE_SOUSTYPEPOI],
          rootContainer: infofeatureElement,
          map: map,
          mapWidth: mapWidth,
          Layer: poiLayer,
          whereClause: infofeatureElement.value,
          params: params
        }), infofeatureElement);
        Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_15__["navigationHistory"])(id, menuUrl, layer.feature.properties.SLUG); //gestion de la carte

        var resize = mapWidth - document.getElementById("info-feature-content").offsetWidth;
        mapElement.style.width = resize + "px";
        map.invalidateSize();
        Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_15__["zoomToMarkersAfterLoad"])(map, Layer); //map.setView([layer.feature.geometry.coordinates[1],layer.feature.geometry.coordinates[0]], 10);
      }
    });
  }; //gestion de la navigation avec boutons precedent/suivant


  window.onpopstate = function () {
    Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_15__["backButtonNavigation"])(menuUrl, map, mapElement, mapWidth, poiLayer, renderInfoFeature);
  };
})();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./node_modules/core-js/modules/es.array.filter.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/modules/es.array.filter.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $ = __webpack_require__(/*! ../internals/export */ "./node_modules/core-js/internals/export.js");
var $filter = __webpack_require__(/*! ../internals/array-iteration */ "./node_modules/core-js/internals/array-iteration.js").filter;
var arrayMethodHasSpeciesSupport = __webpack_require__(/*! ../internals/array-method-has-species-support */ "./node_modules/core-js/internals/array-method-has-species-support.js");
var arrayMethodUsesToLength = __webpack_require__(/*! ../internals/array-method-uses-to-length */ "./node_modules/core-js/internals/array-method-uses-to-length.js");

var HAS_SPECIES_SUPPORT = arrayMethodHasSpeciesSupport('filter');
// Edge 14- issue
var USES_TO_LENGTH = arrayMethodUsesToLength('filter');

// `Array.prototype.filter` method
// https://tc39.es/ecma262/#sec-array.prototype.filter
// with adding support of @@species
$({ target: 'Array', proto: true, forced: !HAS_SPECIES_SUPPORT || !USES_TO_LENGTH }, {
  filter: function filter(callbackfn /* , thisArg */) {
    return $filter(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
  }
});


/***/ }),

/***/ "./node_modules/core-js/modules/es.array.index-of.js":
/*!***********************************************************!*\
  !*** ./node_modules/core-js/modules/es.array.index-of.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $ = __webpack_require__(/*! ../internals/export */ "./node_modules/core-js/internals/export.js");
var $indexOf = __webpack_require__(/*! ../internals/array-includes */ "./node_modules/core-js/internals/array-includes.js").indexOf;
var arrayMethodIsStrict = __webpack_require__(/*! ../internals/array-method-is-strict */ "./node_modules/core-js/internals/array-method-is-strict.js");
var arrayMethodUsesToLength = __webpack_require__(/*! ../internals/array-method-uses-to-length */ "./node_modules/core-js/internals/array-method-uses-to-length.js");

var nativeIndexOf = [].indexOf;

var NEGATIVE_ZERO = !!nativeIndexOf && 1 / [1].indexOf(1, -0) < 0;
var STRICT_METHOD = arrayMethodIsStrict('indexOf');
var USES_TO_LENGTH = arrayMethodUsesToLength('indexOf', { ACCESSORS: true, 1: 0 });

// `Array.prototype.indexOf` method
// https://tc39.es/ecma262/#sec-array.prototype.indexof
$({ target: 'Array', proto: true, forced: NEGATIVE_ZERO || !STRICT_METHOD || !USES_TO_LENGTH }, {
  indexOf: function indexOf(searchElement /* , fromIndex = 0 */) {
    return NEGATIVE_ZERO
      // convert -0 to +0
      ? nativeIndexOf.apply(this, arguments) || 0
      : $indexOf(this, searchElement, arguments.length > 1 ? arguments[1] : undefined);
  }
});


/***/ }),

/***/ "./node_modules/core-js/modules/es.array.slice.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/modules/es.array.slice.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $ = __webpack_require__(/*! ../internals/export */ "./node_modules/core-js/internals/export.js");
var isObject = __webpack_require__(/*! ../internals/is-object */ "./node_modules/core-js/internals/is-object.js");
var isArray = __webpack_require__(/*! ../internals/is-array */ "./node_modules/core-js/internals/is-array.js");
var toAbsoluteIndex = __webpack_require__(/*! ../internals/to-absolute-index */ "./node_modules/core-js/internals/to-absolute-index.js");
var toLength = __webpack_require__(/*! ../internals/to-length */ "./node_modules/core-js/internals/to-length.js");
var toIndexedObject = __webpack_require__(/*! ../internals/to-indexed-object */ "./node_modules/core-js/internals/to-indexed-object.js");
var createProperty = __webpack_require__(/*! ../internals/create-property */ "./node_modules/core-js/internals/create-property.js");
var wellKnownSymbol = __webpack_require__(/*! ../internals/well-known-symbol */ "./node_modules/core-js/internals/well-known-symbol.js");
var arrayMethodHasSpeciesSupport = __webpack_require__(/*! ../internals/array-method-has-species-support */ "./node_modules/core-js/internals/array-method-has-species-support.js");
var arrayMethodUsesToLength = __webpack_require__(/*! ../internals/array-method-uses-to-length */ "./node_modules/core-js/internals/array-method-uses-to-length.js");

var HAS_SPECIES_SUPPORT = arrayMethodHasSpeciesSupport('slice');
var USES_TO_LENGTH = arrayMethodUsesToLength('slice', { ACCESSORS: true, 0: 0, 1: 2 });

var SPECIES = wellKnownSymbol('species');
var nativeSlice = [].slice;
var max = Math.max;

// `Array.prototype.slice` method
// https://tc39.es/ecma262/#sec-array.prototype.slice
// fallback for not array-like ES3 strings and DOM objects
$({ target: 'Array', proto: true, forced: !HAS_SPECIES_SUPPORT || !USES_TO_LENGTH }, {
  slice: function slice(start, end) {
    var O = toIndexedObject(this);
    var length = toLength(O.length);
    var k = toAbsoluteIndex(start, length);
    var fin = toAbsoluteIndex(end === undefined ? length : end, length);
    // inline `ArraySpeciesCreate` for usage native `Array#slice` where it's possible
    var Constructor, result, n;
    if (isArray(O)) {
      Constructor = O.constructor;
      // cross-realm fallback
      if (typeof Constructor == 'function' && (Constructor === Array || isArray(Constructor.prototype))) {
        Constructor = undefined;
      } else if (isObject(Constructor)) {
        Constructor = Constructor[SPECIES];
        if (Constructor === null) Constructor = undefined;
      }
      if (Constructor === Array || Constructor === undefined) {
        return nativeSlice.call(O, k, fin);
      }
    }
    result = new (Constructor === undefined ? Array : Constructor)(max(fin - k, 0));
    for (n = 0; k < fin; k++, n++) if (k in O) createProperty(result, n, O[k]);
    result.length = n;
    return result;
  }
});


/***/ })

},[["./assets/js/pages/labelsVelo.js","runtime","vendors~app~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_velor~10347b53","vendors~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~61364f2f","vendors~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~cotatio~d7e263c1","vendors~bonnes_adresses~calendrier~circuits~clubs~labels_velo~login_licencie","vendors~bonnes_adresses~calendrier~circuits~clubs~labels_velo","vendors~calendrier~circuits~clubs~labels_velo","bonnes_adresses~calendrier~circuits~clubs~labels_velo","circuits~labels_velo"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvcGFnZXMvbGFiZWxzVmVsby5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9tb2R1bGVzL2VzLmFycmF5LmZpbHRlci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9tb2R1bGVzL2VzLmFycmF5LmluZGV4LW9mLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL21vZHVsZXMvZXMuYXJyYXkuc2xpY2UuanMiXSwibmFtZXMiOlsibWFwTGFiZWxzVmVsbyIsIm1hcCIsIkwiLCJjZW50ZXIiLCJ6b29tIiwibWluWm9vbSIsInpvb21Db250cm9sIiwibGF5ZXJzIiwiZXNyaSIsImJhc2VtYXBMYXllciIsImFkZENvbnRyb2wiLCJnZW9sb2NhdGlvbkJ1dHRvbiIsIm1hcEVsZW1lbnQiLCJkb2N1bWVudCIsImdldEVsZW1lbnRCeUlkIiwibWFwV2lkdGgiLCJvZmZzZXRXaWR0aCIsImRlZmF1bHRXaGVyZUNsYXVzZSIsImxvY2F0aW9uIiwic2VhcmNoIiwicXVlcnlTdHJpbmciLCJwb2lMYXllciIsIkNsdXN0ZXIiLCJmZWF0dXJlTGF5ZXIiLCJ1cmwiLCJhZ29sRmVhdHVyZVNlcnZpY2VzIiwicG9pIiwid2hlcmUiLCJwb2ludFRvTGF5ZXIiLCJmZWF0dXJlIiwibGF0bG5nIiwic3NUeXBlUG9pIiwicHJvcGVydGllcyIsIlJFRkVSRU5DRV9TT1VTVFlQRVBPSSIsIm1hcmtlciIsImljb24iLCJpY29uUG9pIiwiaWNvbkNyZWF0ZUZ1bmN0aW9uIiwiY2x1c3RlciIsImNvdW50IiwiZ2V0Q2hpbGRDb3VudCIsImRpZ2l0cyIsImxlbmd0aCIsImRpdkljb24iLCJodG1sIiwiY2xhc3NOYW1lIiwiaWNvblNpemUiLCJhZGRUbyIsInBvaUxheWVyR3JvdXAiLCJiaW5kUG9wdXAiLCJsYXllciIsIm5vbSIsIk5PTSIsImlkUG9pIiwiSURFTlRJRklBTlRfUE9JIiwidHlwZSIsIlJFRkVSRU5DRV9UWVBFUE9JIiwicmVmU291c1R5cGUiLCJVdGlsIiwidGVtcGxhdGUiLCJwb3B1cFRlbXBsYXRlUG9pIiwib24iLCJzeW5jU2lkZWJhciIsIm9uY2UiLCJwYXRobmFtZSIsIm1lbnVVcmwiLCJwYXJ0cyIsImhyZWYiLCJzcGxpdCIsInVybGlkIiwicmVuZGVySW5mb0ZlYXR1cmUiLCJ6b29tVG9NYXJrZXJzQWZ0ZXJMb2FkIiwiYWRkRXZlbnRMaXN0ZW5lciIsImdldEF0dHJpYnV0ZSIsInNldFdoZXJlIiwiZ2V0V2hlcmUiLCJlIiwiY2xlYXJMYXllcnMiLCJUYWdpZnkiLCJxdWVyeVNlbGVjdG9yIiwiZW5mb3JlV2hpdGVsaXN0Iiwid2hpdGVsaXN0IiwiZGVwdExpc3QiLCJmaWx0ZXJzIiwiZWxlbWVudHMiLCJxdWVyeVNlbGVjdG9yQWxsIiwiZm9yRWFjaCIsIml0ZW0iLCJnbG9iYWxGaWx0ZXIiLCJjaGVja2VkIiwidGFyZ2V0IiwiaWQiLCJwdXNoIiwidmFsdWUiLCJqc29uRGVwdHMiLCJKU09OIiwicGFyc2UiLCJmaWx0ZXIiLCJmIiwiaW5kZXhPZiIsIml0ZW1EZXB0IiwicXVlcnlQb2kiLCJzcGxpY2UiLCJkZXB0IiwiaW5jbHVkZXMiLCJqb2luIiwidHlwZVBvaSIsInNvdXNUeXBlUG9pIiwicmVzdWx0Iiwic2xpY2UiLCJzZWFyY2hDb250cm9sIiwiR2VvY29kaW5nIiwiZ2Vvc2VhcmNoIiwidXNlTWFwQm91bmRzIiwidGl0bGUiLCJwbGFjZWhvbGRlciIsImV4cGFuZGVkIiwiY29sbGFwc2VBZnRlclJlc3VsdCIsInBvc2l0aW9uIiwicHJvdmlkZXJzIiwiYXJjZ2lzT25saW5lUHJvdmlkZXIiLCJmZWF0dXJlTGF5ZXJQcm92aWRlciIsIm9wdGlvbnMiLCJzZWFyY2hGaWVsZHMiLCJsYWJlbCIsIm1heFJlc3VsdHMiLCJidWZmZXJSYWRpdXMiLCJmb3JtYXRTdWdnZXN0aW9uIiwiYmFOYW1lIiwiZXNyaUdlb2NvZGVyIiwiZ2V0Q29udGFpbmVyIiwic2V0RXNyaUdlb2NvZGVyIiwiYXJyUmVzdWx0c1NpZGViYXIiLCIkIiwiZW1wdHkiLCJlYWNoQWN0aXZlRmVhdHVyZSIsImhhc0xheWVyIiwiZ2V0Qm91bmRzIiwiY29udGFpbnMiLCJnZXRMYXRMbmciLCJzaWRlYmFyVGVtcGxhdGVQb2kiLCJwb2lOYW1lIiwiQ09NTVVORSIsIk5vbV9EZXB0IiwiZ2VvbWV0cnkiLCJjb29yZGluYXRlcyIsInJldmVyc2UiLCJhZGRSZXN1bHRzVG9TaWRlYmFyIiwic291c3R5cGUiLCJ2aWxsZSIsImxhdCIsImxvbiIsIkxheWVyIiwicXVlcnlDb250YWN0IiwicXVlcnkiLCJmb3Vybmlzc2V1ciIsInN0eWxlIiwid2lkdGgiLCJwYXJhbXMiLCJpbmZvZmVhdHVyZUVsZW1lbnQiLCJlYWNoRmVhdHVyZSIsInVubW91bnRDb21wb25lbnRBdE5vZGUiLCJyZW5kZXIiLCJERVNDUklQVElPTiIsIm5hdmlnYXRpb25IaXN0b3J5IiwiU0xVRyIsInJlc2l6ZSIsImludmFsaWRhdGVTaXplIiwid2luZG93Iiwib25wb3BzdGF0ZSIsImJhY2tCdXR0b25OYXZpZ2F0aW9uIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQW9CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsQ0FBQyxTQUFTQSxhQUFULEdBQTBCO0FBQzFCO0FBQ0EsTUFBTUMsR0FBRyxHQUFHQyxDQUFDLENBQUNELEdBQUYsQ0FBTSxLQUFOLEVBQWE7QUFDeEJFLFVBQU0sRUFBRSxDQUFDLEVBQUQsRUFBSyxDQUFMLENBRGdCO0FBRXhCQyxRQUFJLEVBQUUsQ0FGa0I7QUFHeEJDLFdBQU8sRUFBRSxDQUhlO0FBSXhCQyxlQUFXLEVBQUUsSUFKVztBQUt4QkMsVUFBTSxFQUFFLENBQUNMLENBQUMsQ0FBQ00sSUFBRixDQUFPQyxZQUFQLENBQW9CLGFBQXBCLENBQUQ7QUFMZ0IsR0FBYixDQUFaO0FBT0FSLEtBQUcsQ0FBQ1MsVUFBSixDQUFlLElBQUlDLHNFQUFKLENBQXNCVixHQUF0QixDQUFmLEVBVDBCLENBVzFCOztBQUNBLE1BQU1XLFVBQVUsR0FBR0MsUUFBUSxDQUFDQyxjQUFULENBQXdCLEtBQXhCLENBQW5CO0FBQ0EsTUFBTUMsUUFBUSxHQUFHRixRQUFRLENBQUNDLGNBQVQsQ0FBd0IsS0FBeEIsRUFBK0JFLFdBQWhEO0FBQ0EsTUFBSUMsa0JBQWtCLEdBQUdDLFFBQVEsQ0FBQ0MsTUFBVCxHQUFrQkMsd0VBQVcsQ0FBQ0YsUUFBUSxDQUFDQyxNQUFWLENBQTdCLEdBQWlELDJEQUExRSxDQWQwQixDQWdCMUI7O0FBQ0EsTUFBTUUsUUFBUSxHQUFHbkIsQ0FBQyxDQUFDTSxJQUFGLENBQU9jLE9BQVAsQ0FBZUMsWUFBZixDQUE0QjtBQUM1Q0MsT0FBRyxFQUFFQyx3RUFBbUIsQ0FBQ0MsR0FEbUI7QUFFNUNDLFNBQUssRUFBRVYsa0JBRnFDO0FBRzVDVyxnQkFBWSxFQUFFLHNCQUFVQyxPQUFWLEVBQW1CQyxNQUFuQixFQUEyQjtBQUN4QyxVQUFJQyxTQUFTLEdBQUdGLE9BQU8sQ0FBQ0csVUFBUixDQUFtQkMscUJBQW5DO0FBQ0EsYUFBTy9CLENBQUMsQ0FBQ2dDLE1BQUYsQ0FBU0osTUFBVCxFQUFpQjtBQUN2QkssWUFBSSxFQUFFQyw0REFBTyxDQUFDTCxTQUFEO0FBRFUsT0FBakIsQ0FBUDtBQUdBLEtBUjJDO0FBUzVDTSxzQkFBa0IsRUFBRSw0QkFBVUMsT0FBVixFQUFtQjtBQUM3QixVQUFJQyxLQUFLLEdBQUdELE9BQU8sQ0FBQ0UsYUFBUixFQUFaO0FBQ0EsVUFBSUMsTUFBTSxHQUFHLENBQUNGLEtBQUssR0FBRyxFQUFULEVBQWFHLE1BQTFCO0FBQ0EsYUFBT3hDLENBQUMsQ0FBQ3lDLE9BQUYsQ0FBVTtBQUNmQyxZQUFJLEVBQUVMLEtBRFM7QUFFZk0saUJBQVMsRUFBRSxvQkFBb0JKLE1BRmhCO0FBR2ZLLGdCQUFRLEVBQUU7QUFISyxPQUFWLENBQVA7QUFLSDtBQWpCcUMsR0FBNUIsRUFrQmRDLEtBbEJjLENBa0JSQyxrRUFsQlEsRUFrQk9ELEtBbEJQLENBa0JhOUMsR0FsQmIsQ0FBakIsQ0FqQjBCLENBcUMxQjs7QUFDQW9CLFVBQVEsQ0FBQzRCLFNBQVQsQ0FBbUIsVUFBVUMsS0FBVixFQUFpQjtBQUNuQyxRQUFJQyxHQUFHLEdBQUdELEtBQUssQ0FBQ3JCLE9BQU4sQ0FBY0csVUFBZCxDQUF5Qm9CLEdBQW5DO0FBQ0EsUUFBSUMsS0FBSyxHQUFHSCxLQUFLLENBQUNyQixPQUFOLENBQWNHLFVBQWQsQ0FBeUJzQixlQUFyQztBQUNBLFFBQUlDLElBQUksR0FBR0wsS0FBSyxDQUFDckIsT0FBTixDQUFjRyxVQUFkLENBQXlCd0IsaUJBQXBDO0FBQ0EsUUFBSUMsV0FBVyxHQUFHUCxLQUFLLENBQUNyQixPQUFOLENBQWNHLFVBQWQsQ0FBeUJDLHFCQUEzQztBQUNBLFdBQU8vQixDQUFDLENBQUN3RCxJQUFGLENBQU9DLFFBQVAsQ0FBZ0JDLDJFQUFnQixDQUFDVCxHQUFELEVBQU1FLEtBQU4sRUFBYUUsSUFBYixFQUFtQkUsV0FBbkIsQ0FBaEMsQ0FBUDtBQUNBLEdBTkQ7QUFRQTs7QUFDQXBDLFVBQVEsQ0FBQ3dDLEVBQVQsQ0FBWSxNQUFaLEVBQW9CLFlBQU07QUFDekJDLGVBQVc7QUFDWCxHQUZEO0FBR0F6QyxVQUFRLENBQUMwQyxJQUFULENBQWMsTUFBZCxFQUFzQixZQUFNO0FBQzNCLFFBQUc3QyxRQUFRLENBQUM4QyxRQUFULElBQXFCQyxPQUF4QixFQUFnQztBQUMvQixVQUFJQyxLQUFLLEdBQUdoRCxRQUFRLENBQUNpRCxJQUFULENBQWNDLEtBQWQsQ0FBb0JILE9BQU8sR0FBQyxHQUE1QixDQUFaO0FBQ0EsVUFBSUksS0FBSyxHQUFHSCxLQUFLLENBQUMsQ0FBRCxDQUFMLENBQVNFLEtBQVQsQ0FBZSxHQUFmLENBQVo7QUFDQUUsdUJBQWlCLENBQUNqRCxRQUFELEVBQVdnRCxLQUFLLENBQUMsQ0FBRCxDQUFoQixDQUFqQjtBQUNBOztBQUFBO0FBQ0RFLHVGQUFzQixDQUFDdEUsR0FBRCxFQUFNb0IsUUFBTixDQUF0QjtBQUNBLEdBUEQ7QUFRQUEsVUFBUSxDQUFDd0MsRUFBVCxDQUFZLFdBQVosRUFBeUIsWUFBTTtBQUM5QmhELFlBQVEsQ0FBQ0MsY0FBVCxDQUF3QixhQUF4QixFQUF1QzBELGdCQUF2QyxDQUF3RCxPQUF4RCxFQUFpRSxZQUFNO0FBQ3RFRix1QkFBaUIsQ0FBQ2pELFFBQUQsRUFBV1IsUUFBUSxDQUFDQyxjQUFULENBQXdCLGFBQXhCLEVBQXVDMkQsWUFBdkMsQ0FBb0QsT0FBcEQsQ0FBWCxDQUFqQjtBQUNBLEtBRkQsRUFFRyxLQUZIO0FBR0EsR0FKRDtBQUtBeEUsS0FBRyxDQUFDNEQsRUFBSixDQUFPLFNBQVAsRUFBa0IsWUFBWTtBQUM3QnhDLFlBQVEsQ0FBQ3FELFFBQVQsQ0FBa0JyRCxRQUFRLENBQUNzRCxRQUFULEVBQWxCO0FBQ0FiLGVBQVc7QUFDWCxHQUhEO0FBSUE3RCxLQUFHLENBQUM0RCxFQUFKLENBQU8sZUFBUCxFQUF3QixVQUFTZSxDQUFULEVBQVk7QUFDbkM1QixzRUFBYSxDQUFDNkIsV0FBZDtBQUNBLEdBRkQ7QUFHQTs7QUFFQTs7QUFDQSxNQUFJQyxzREFBSixDQUFXakUsUUFBUSxDQUFDa0UsYUFBVCxDQUF1QiwwQkFBdkIsQ0FBWCxFQUErRDtBQUM5REMsbUJBQWUsRUFBRSxJQUQ2QztBQUU5REMsYUFBUyxFQUFFQyw2REFBUUE7QUFGMkMsR0FBL0Q7O0FBSUEsTUFBTUMsT0FBTyxHQUFDLG1CQUFjO0FBQUEsUUFBYkEsT0FBYSx1RUFBTCxFQUFLO0FBQzNCLFFBQU1DLFFBQVEsR0FBR3ZFLFFBQVEsQ0FBQ3dFLGdCQUFULENBQTBCLDBDQUExQixDQUFqQjtBQUNBRCxZQUFRLENBQUNFLE9BQVQsQ0FBaUIsVUFBQUMsSUFBSSxFQUFJO0FBQ3hCQSxVQUFJLENBQUNmLGdCQUFMLENBQXNCLFFBQXRCLEVBQWdDLFVBQUNJLENBQUQsRUFBTztBQUN0QyxZQUFJL0QsUUFBUSxDQUFDd0UsZ0JBQVQsQ0FBMEIseUNBQTFCLEVBQXFFM0MsTUFBckUsS0FBZ0YsQ0FBcEYsRUFBc0Y7QUFDckZ5QyxpQkFBTyxHQUFDLEVBQVI7QUFDQUssc0JBQVksQ0FBQ0wsT0FBRCxDQUFaO0FBQ0EsU0FIRCxNQUdPO0FBQ04sY0FBSUksSUFBSSxDQUFDRSxPQUFMLEtBQWlCLElBQWpCLElBQXlCYixDQUFDLENBQUNjLE1BQUYsQ0FBU0MsRUFBVCxJQUFjLFlBQTNDLEVBQXdEO0FBQ3ZEUixtQkFBTyxDQUFDUyxJQUFSLENBQWFMLElBQUksQ0FBQ00sS0FBbEI7QUFDQSxXQUZELE1BRU8sSUFBSWpCLENBQUMsQ0FBQ2MsTUFBRixDQUFTQyxFQUFULElBQWEsWUFBYixJQUE2QmYsQ0FBQyxDQUFDYyxNQUFGLENBQVNHLEtBQTFDLEVBQWdEO0FBQ3RELGdCQUFJQyxTQUFTLEdBQUdDLElBQUksQ0FBQ0MsS0FBTCxDQUFXcEIsQ0FBQyxDQUFDYyxNQUFGLENBQVNHLEtBQXBCLENBQWhCO0FBQ0FWLG1CQUFPLEdBQUNBLE9BQU8sQ0FBQ2MsTUFBUixDQUFlLFVBQUFDLENBQUM7QUFBQSxxQkFBSUEsQ0FBQyxDQUFDQyxPQUFGLENBQVUsV0FBVixNQUEyQixDQUEvQjtBQUFBLGFBQWhCLENBQVI7QUFDQUwscUJBQVMsQ0FBQ1IsT0FBVixDQUFrQixVQUFTYyxRQUFULEVBQWtCO0FBQ25DakIscUJBQU8sQ0FBQ1MsSUFBUixDQUFhUSxRQUFRLENBQUNDLFFBQXRCO0FBQ0EsYUFGRDtBQUdBLFdBTk0sTUFNQTtBQUNObEIsbUJBQU8sQ0FBQ21CLE1BQVIsQ0FBZW5CLE9BQU8sQ0FBQ2dCLE9BQVIsQ0FBZ0JaLElBQUksQ0FBQ00sS0FBckIsQ0FBZixFQUEyQyxDQUEzQztBQUNBOztBQUNETCxzQkFBWSxDQUFDTCxPQUFELENBQVo7QUFDQTtBQUNELE9BbEJEO0FBbUJBLEtBcEJEO0FBcUJBLEdBdkJEOztBQXdCQUEsU0FBTzs7QUFDUCxNQUFNSyxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFDTCxPQUFELEVBQWE7QUFDakMsUUFBSW9CLElBQUksR0FBR3BCLE9BQU8sQ0FBQ2MsTUFBUixDQUFlLFVBQUFDLENBQUM7QUFBQSxhQUFJQSxDQUFDLENBQUNNLFFBQUYsQ0FBVyxXQUFYLENBQUo7QUFBQSxLQUFoQixFQUE2Q0MsSUFBN0MsQ0FBa0QsTUFBbEQsQ0FBWDtBQUNBLFFBQUlDLE9BQU8sR0FBR3ZCLE9BQU8sQ0FBQ2MsTUFBUixDQUFlLFVBQUFDLENBQUM7QUFBQSxhQUFJQSxDQUFDLENBQUNNLFFBQUYsQ0FBVyx1QkFBWCxDQUFKO0FBQUEsS0FBaEIsRUFBeURDLElBQXpELENBQThELE1BQTlELENBQWQ7QUFDQSxRQUFJRSxXQUFXLEdBQUV4QixPQUFPLENBQUNjLE1BQVIsQ0FBZSxVQUFBQyxDQUFDO0FBQUEsYUFBSUEsQ0FBQyxDQUFDTSxRQUFGLENBQVcsbUJBQVgsQ0FBSjtBQUFBLEtBQWhCLEVBQXFEQyxJQUFyRCxDQUEwRCxNQUExRCxDQUFqQjtBQUNBLFFBQUlHLE1BQU0sR0FBRyxVQUFHM0Ysa0JBQWtCLEdBQUUsU0FBZ0UsU0FBaEUsR0FBa0csRUFBekgsU0FBOEh5RixPQUFPLEdBQUcsTUFBSUEsT0FBSixHQUFZLFFBQWYsR0FBMEIsRUFBL0osU0FBb0tDLFdBQVcsR0FBRyxNQUFJQSxXQUFKLEdBQWdCLFFBQW5CLEdBQThCLEVBQTdNLFNBQWtOSixJQUFJLEdBQUcsTUFBSUEsSUFBSixHQUFTLFFBQVosR0FBdUIsRUFBN08sRUFBa1BNLEtBQWxQLENBQXdQLENBQXhQLEVBQTBQLENBQUMsQ0FBM1AsQ0FBYixDQUppQyxDQUtqQzs7QUFDQXhGLFlBQVEsQ0FBQ3FELFFBQVQsQ0FBa0JrQyxNQUFsQjtBQUNBLEdBUEQ7QUFRQTs7QUFFQTs7O0FBQ0EsTUFBTUUsYUFBYSxHQUFHNUcsQ0FBQyxDQUFDTSxJQUFGLENBQU91RyxTQUFQLENBQWlCQyxTQUFqQixDQUEyQjtBQUNoREMsZ0JBQVksRUFBRSxLQURrQztBQUVoREMsU0FBSyxFQUFFLG9DQUZ5QztBQUdoREMsZUFBVyxFQUFFLGlDQUhtQztBQUloREMsWUFBUSxFQUFFLElBSnNDO0FBS2hEQyx1QkFBbUIsRUFBRSxLQUwyQjtBQU1oREMsWUFBUSxFQUFFLFNBTnNDO0FBT2hEQyxhQUFTLEVBQUUsQ0FDVkMseUVBRFUsRUFFVnRILENBQUMsQ0FBQ00sSUFBRixDQUFPdUcsU0FBUCxDQUFpQlUsb0JBQWpCLENBQXNDO0FBQUU7QUFDdkNqRyxTQUFHLEVBQUVILFFBQVEsQ0FBQ3FHLE9BQVQsQ0FBaUJsRyxHQURlO0FBRXJDRyxXQUFLLEVBQUVOLFFBQVEsQ0FBQ3NELFFBQVQsRUFGOEI7QUFHckNnRCxrQkFBWSxFQUFFLENBQUMsS0FBRCxDQUh1QjtBQUlyQ0MsV0FBSyxFQUFFLGNBSjhCO0FBS3JDQyxnQkFBVSxFQUFFLENBTHlCO0FBTXJDQyxrQkFBWSxFQUFFLElBTnVCO0FBT3JDQyxzQkFBZ0IsRUFBRSwwQkFBVWxHLE9BQVYsRUFBbUI7QUFDcEMseUJBQVVBLE9BQU8sQ0FBQ0csVUFBUixDQUFtQm9CLEdBQTdCLGdCQUFzQzRFLDJEQUFNLENBQUNuRyxPQUFPLENBQUNHLFVBQVIsQ0FBbUJDLHFCQUFwQixDQUE1QztBQUNBO0FBVG9DLEtBQXRDLENBRlU7QUFQcUMsR0FBM0IsRUFxQm5CYyxLQXJCbUIsQ0FxQmI5QyxHQXJCYSxDQUF0QixDQWpIMEIsQ0F1STFCOztBQUNBLE1BQU1nSSxZQUFZLEdBQUduQixhQUFhLENBQUNvQixZQUFkLEVBQXJCO0FBQ0FDLDhFQUFlLENBQUNGLFlBQUQsRUFBZXBILFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixVQUF4QixDQUFmLENBQWYsQ0F6STBCLENBMEkxQjs7QUFDQWdHLGVBQWEsQ0FBQ2pELEVBQWQsQ0FBaUIsWUFBakIsRUFBK0IsWUFBWTtBQUMxQ2lELGlCQUFhLENBQUNZLE9BQWQsQ0FBc0JILFNBQXRCLENBQWdDLENBQWhDLEVBQW1DRyxPQUFuQyxDQUEyQy9GLEtBQTNDLEdBQW1ETixRQUFRLENBQUNzRCxRQUFULEVBQW5EO0FBQ0EsR0FGRDtBQUdBOztBQUVBOztBQUNBLE1BQU1iLFdBQVcsR0FBRyxTQUFkQSxXQUFjLEdBQTBCO0FBQUEsUUFBekJzRSxpQkFBeUIsdUVBQVAsRUFBTztBQUM3Q0MsS0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlQyxLQUFmO0FBQ0FqSCxZQUFRLENBQUNrSCxpQkFBVCxDQUEyQixVQUFVckYsS0FBVixFQUFpQjtBQUFFO0FBQzdDLFVBQUlqRCxHQUFHLENBQUN1SSxRQUFKLENBQWFuSCxRQUFiLENBQUosRUFBNEI7QUFDM0IsWUFBSXBCLEdBQUcsQ0FBQ3dJLFNBQUosR0FBZ0JDLFFBQWhCLENBQXlCeEYsS0FBSyxDQUFDeUYsU0FBTixFQUF6QixDQUFKLEVBQWlEO0FBQ2hEUCwyQkFBaUIsQ0FBQ3hDLElBQWxCLENBQ0NnRCxrQkFBa0IsQ0FDakIxRixLQUFLLENBQUNyQixPQUFOLENBQWNHLFVBQWQsQ0FBeUJvQixHQURSLEVBRWpCeUYsNERBQU8sQ0FBQzNGLEtBQUssQ0FBQ3JCLE9BQU4sQ0FBY0csVUFBZCxDQUF5QkMscUJBQTFCLENBRlUsRUFHakJpQixLQUFLLENBQUNyQixPQUFOLENBQWNHLFVBQWQsQ0FBeUI4RyxPQUhSLEVBSWpCNUYsS0FBSyxDQUFDckIsT0FBTixDQUFjRyxVQUFkLENBQXlCK0csUUFKUixFQUtqQjdGLEtBQUssQ0FBQ3JCLE9BQU4sQ0FBY0csVUFBZCxDQUF5QnNCLGVBTFIsRUFNakJKLEtBQUssQ0FBQ3JCLE9BQU4sQ0FBY21ILFFBQWQsQ0FBdUJDLFdBQXZCLENBQW1DLENBQW5DLENBTmlCLEVBT2pCL0YsS0FBSyxDQUFDckIsT0FBTixDQUFjbUgsUUFBZCxDQUF1QkMsV0FBdkIsQ0FBbUMsQ0FBbkMsQ0FQaUIsQ0FEbkI7QUFXQTtBQUNEO0FBQ0QsS0FoQkQ7QUFpQkFiLHFCQUFpQixDQUFDYyxPQUFsQjtBQUNBQyxvRkFBbUIsQ0FBQ2YsaUJBQUQsRUFBbUIvRyxRQUFuQixFQUE0QmlELGlCQUE1QixDQUFuQjtBQUNBLEdBckJELENBakowQixDQXdLMUI7OztBQUNBLE1BQU1zRSxrQkFBa0IsR0FBRyxTQUFyQkEsa0JBQXFCLENBQUN6RixHQUFELEVBQUtpRyxRQUFMLEVBQWNDLEtBQWQsRUFBb0I5QyxJQUFwQixFQUF5QlosRUFBekIsRUFBNEIyRCxHQUE1QixFQUFnQ0MsR0FBaEMsRUFBd0M7QUFDbEUsb0VBQ2dENUQsRUFEaEQsMkJBQ2lFMkQsR0FEakUsMkJBQ21GQyxHQURuRiwrSEFHd0NwRyxHQUh4QyxxRkFJMERpRyxRQUoxRCx1R0FLd0VDLEtBTHhFLGVBS2tGOUMsSUFMbEY7QUFTQSxHQVZEO0FBV0E7QUFFQTs7O0FBQ0EsTUFBTWpDLGlCQUFpQixHQUFHLFNBQXBCQSxpQkFBb0IsQ0FBQ2tGLEtBQUQsRUFBUTdELEVBQVIsRUFBZTtBQUN4QyxRQUFNOEQsWUFBWSxHQUFHdkosQ0FBQyxDQUFDTSxJQUFGLENBQU9rSixLQUFQLENBQWE7QUFDakNsSSxTQUFHLEVBQUVDLHdFQUFtQixDQUFDa0k7QUFEUSxLQUFiLENBQXJCO0FBR0EvSSxjQUFVLENBQUNnSixLQUFYLENBQWlCQyxLQUFqQixHQUF3QjlJLFFBQVEsR0FBQyxJQUFqQztBQUNBLFFBQUkrSSxNQUFNLEdBQUM1SSxRQUFRLENBQUNDLE1BQXBCO0FBQ0EsUUFBSTRJLGtCQUFrQixHQUFHbEosUUFBUSxDQUFDQyxjQUFULENBQXdCLGFBQXhCLENBQXpCO0FBQ0FpSixzQkFBa0IsQ0FBQ2xFLEtBQW5CLEdBQXlCMkQsS0FBSyxDQUFDN0UsUUFBTixFQUF6QjtBQUNBNkUsU0FBSyxDQUFDUSxXQUFOLENBQWtCLFVBQVU5RyxLQUFWLEVBQWlCO0FBQ2xDLFVBQUdBLEtBQUssQ0FBQ3JCLE9BQU4sQ0FBY0csVUFBZCxDQUF5QnNCLGVBQXpCLElBQTRDcUMsRUFBL0MsRUFBa0Q7QUFDakQ2RCxhQUFLLENBQUM5RSxRQUFOLENBQWUscUJBQW1CaUIsRUFBbEM7QUFDQXNFLGlGQUFzQixDQUFDRixrQkFBRCxDQUF0QjtBQUNBRyxpRUFBTSxlQUNMLDREQUFDLGdFQUFEO0FBQ0MsZUFBSyxFQUFFdkUsRUFEUjtBQUVDLGFBQUcsRUFBRXpDLEtBQUssQ0FBQ3JCLE9BQU4sQ0FBY0csVUFBZCxDQUF5Qm9CLEdBRi9CO0FBR0MsNEJBQWtCLEVBQUVxRyxZQUhyQjtBQUlDLGVBQUssRUFBRXZHLEtBQUssQ0FBQ3JCLE9BQU4sQ0FBY0csVUFBZCxDQUF5Qm1JLFdBSmpDO0FBS0Msa0JBQVEsRUFBRXRCLDREQUFPLENBQUMzRixLQUFLLENBQUNyQixPQUFOLENBQWNHLFVBQWQsQ0FBeUJDLHFCQUExQixDQUxsQjtBQU1DLHVCQUFhLEVBQUU4SCxrQkFOaEI7QUFPQyxhQUFHLEVBQUU5SixHQVBOO0FBUUMsa0JBQVEsRUFBSWMsUUFSYjtBQVNDLGVBQUssRUFBRU0sUUFUUjtBQVVDLHFCQUFXLEVBQUUwSSxrQkFBa0IsQ0FBQ2xFLEtBVmpDO0FBV0MsZ0JBQU0sRUFBRWlFO0FBWFQsVUFESyxFQWFEQyxrQkFiQyxDQUFOO0FBZUFLLHNGQUFpQixDQUFDekUsRUFBRCxFQUFLMUIsT0FBTCxFQUFhZixLQUFLLENBQUNyQixPQUFOLENBQWNHLFVBQWQsQ0FBeUJxSSxJQUF0QyxDQUFqQixDQWxCaUQsQ0FtQmpEOztBQUNBLFlBQUlDLE1BQU0sR0FBR3ZKLFFBQVEsR0FBQ0YsUUFBUSxDQUFDQyxjQUFULENBQXdCLHNCQUF4QixFQUFnREUsV0FBdEU7QUFDQUosa0JBQVUsQ0FBQ2dKLEtBQVgsQ0FBaUJDLEtBQWpCLEdBQXVCUyxNQUFNLEdBQUMsSUFBOUI7QUFDQXJLLFdBQUcsQ0FBQ3NLLGNBQUo7QUFDQWhHLDJGQUFzQixDQUFFdEUsR0FBRixFQUFPdUosS0FBUCxDQUF0QixDQXZCaUQsQ0F3QmpEO0FBQ0E7QUFDRCxLQTNCRDtBQTRCQSxHQXBDRCxDQXZMMEIsQ0E2TjFCOzs7QUFDQWdCLFFBQU0sQ0FBQ0MsVUFBUCxHQUFvQixZQUFNO0FBQ3pCQyxxRkFBb0IsQ0FBQ3pHLE9BQUQsRUFBVWhFLEdBQVYsRUFBZVcsVUFBZixFQUEyQkcsUUFBM0IsRUFBcUNNLFFBQXJDLEVBQStDaUQsaUJBQS9DLENBQXBCO0FBQ0EsR0FGRDtBQUlBLENBbE9ELEk7Ozs7Ozs7Ozs7Ozs7QUMzQmE7QUFDYixRQUFRLG1CQUFPLENBQUMsdUVBQXFCO0FBQ3JDLGNBQWMsbUJBQU8sQ0FBQyx5RkFBOEI7QUFDcEQsbUNBQW1DLG1CQUFPLENBQUMsMkhBQStDO0FBQzFGLDhCQUE4QixtQkFBTyxDQUFDLGlIQUEwQzs7QUFFaEY7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUcsZ0ZBQWdGO0FBQ25GO0FBQ0E7QUFDQTtBQUNBLENBQUM7Ozs7Ozs7Ozs7Ozs7QUNqQlk7QUFDYixRQUFRLG1CQUFPLENBQUMsdUVBQXFCO0FBQ3JDLGVBQWUsbUJBQU8sQ0FBQyx1RkFBNkI7QUFDcEQsMEJBQTBCLG1CQUFPLENBQUMsdUdBQXFDO0FBQ3ZFLDhCQUE4QixtQkFBTyxDQUFDLGlIQUEwQzs7QUFFaEY7O0FBRUE7QUFDQTtBQUNBLHlEQUF5RCx3QkFBd0I7O0FBRWpGO0FBQ0E7QUFDQSxHQUFHLDJGQUEyRjtBQUM5RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOzs7Ozs7Ozs7Ozs7O0FDckJZO0FBQ2IsUUFBUSxtQkFBTyxDQUFDLHVFQUFxQjtBQUNyQyxlQUFlLG1CQUFPLENBQUMsNkVBQXdCO0FBQy9DLGNBQWMsbUJBQU8sQ0FBQywyRUFBdUI7QUFDN0Msc0JBQXNCLG1CQUFPLENBQUMsNkZBQWdDO0FBQzlELGVBQWUsbUJBQU8sQ0FBQyw2RUFBd0I7QUFDL0Msc0JBQXNCLG1CQUFPLENBQUMsNkZBQWdDO0FBQzlELHFCQUFxQixtQkFBTyxDQUFDLHlGQUE4QjtBQUMzRCxzQkFBc0IsbUJBQU8sQ0FBQyw2RkFBZ0M7QUFDOUQsbUNBQW1DLG1CQUFPLENBQUMsMkhBQStDO0FBQzFGLDhCQUE4QixtQkFBTyxDQUFDLGlIQUEwQzs7QUFFaEY7QUFDQSx1REFBdUQsOEJBQThCOztBQUVyRjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRyxnRkFBZ0Y7QUFDbkY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLFNBQVM7QUFDeEI7QUFDQTtBQUNBO0FBQ0EsQ0FBQyIsImZpbGUiOiJsYWJlbHNfdmVsby5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcblx0Y2lyY3VpdExheWVyR3JvdXAsIFxyXG5cdHBvaW50RGVwYXJ0TGF5ZXJHcm91cCwgXHJcblx0cG9pTGF5ZXJHcm91cCxcclxuXHRxdWVyeVN0cmluZyxcclxuXHRhZGRSZXN1bHRzVG9TaWRlYmFyLFxyXG5cdHpvb21Ub01hcmtlcnNBZnRlckxvYWQsXHJcblx0YWdvbEZlYXR1cmVTZXJ2aWNlcyxcclxuXHRhcmNnaXNPbmxpbmVQcm92aWRlcixcclxuXHRpY29uUG9pLFxyXG5cdHNzVHlwZVBvaSxcclxuXHRiYU5hbWUsXHJcblx0cG9pTmFtZSxcclxuXHRnZW9sb2NhdGlvbkJ1dHRvbiwgXHJcblx0c2V0RXNyaUdlb2NvZGVyLCBcclxuXHRzaG93Q2lyY3VpdCxcclxuXHRiYWNrQnV0dG9uTmF2aWdhdGlvbiwgXHJcblx0bmF2aWdhdGlvbkhpc3RvcnlcclxufSBmcm9tIFwiLi4vc2VydmljZXMvbWFwQ29uZmlnXCI7XHJcblxyXG5pbXBvcnQge3BvcHVwVGVtcGxhdGVQb2l9IGZyb20gXCIuLi9jb21wb25lbnRzL3BvcHVwXCI7XHJcbmltcG9ydCB7ZGVwdExpc3R9IGZyb20gJy4uL3NlcnZpY2VzL3doaXRlTGlzdCdcclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHtyZW5kZXIsIHVubW91bnRDb21wb25lbnRBdE5vZGV9IGZyb20gJ3JlYWN0LWRvbSc7XHJcbmltcG9ydCBJbmZvRmVhdHVyZSBmcm9tICcuLi9jb21wb25lbnRzL0luZm9GZWF0dXJlJztcclxuaW1wb3J0IFRhZ2lmeSBmcm9tICdAeWFpcmVvL3RhZ2lmeSc7XHJcblxyXG4oZnVuY3Rpb24gbWFwTGFiZWxzVmVsbyAoKSB7XHJcblx0Ly9tYXBcclxuXHRjb25zdCBtYXAgPSBMLm1hcCgnbWFwJywge1xyXG5cdFx0Y2VudGVyOiBbNDcsIDBdLFxyXG5cdFx0em9vbTogNSxcclxuXHRcdG1pblpvb206IDMsXHJcblx0XHR6b29tQ29udHJvbDogdHJ1ZSxcclxuXHRcdGxheWVyczogW0wuZXNyaS5iYXNlbWFwTGF5ZXIoJ1RvcG9ncmFwaGljJyldXHJcblx0fSk7XHJcblx0bWFwLmFkZENvbnRyb2wobmV3IGdlb2xvY2F0aW9uQnV0dG9uKG1hcCkpO1xyXG5cclxuXHQvL3ZhcmlhYmxlcyBnbG9iYWxlc1xyXG5cdGNvbnN0IG1hcEVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1hcFwiKTtcclxuXHRjb25zdCBtYXBXaWR0aCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibWFwXCIpLm9mZnNldFdpZHRoO1xyXG5cdGxldCBkZWZhdWx0V2hlcmVDbGF1c2UgPSBsb2NhdGlvbi5zZWFyY2ggPyBxdWVyeVN0cmluZyhsb2NhdGlvbi5zZWFyY2gpIDogXCIoUkVGRVJFTkNFX1NPVVNUWVBFUE9JIElOICgxMDEsMjA0LDIwNSkpIEFORCBWSVNJQklMSVRFPTFcIjtcclxuXHJcblx0Ly9sYXllciBwb2ludHMgZGVwYXJ0cyBjaXJjdWl0c1xyXG5cdGNvbnN0IHBvaUxheWVyID0gTC5lc3JpLkNsdXN0ZXIuZmVhdHVyZUxheWVyKHtcclxuXHRcdHVybDogYWdvbEZlYXR1cmVTZXJ2aWNlcy5wb2ksIFxyXG5cdFx0d2hlcmU6IGRlZmF1bHRXaGVyZUNsYXVzZSxcclxuXHRcdHBvaW50VG9MYXllcjogZnVuY3Rpb24gKGZlYXR1cmUsIGxhdGxuZykge1xyXG5cdFx0XHRsZXQgc3NUeXBlUG9pID0gZmVhdHVyZS5wcm9wZXJ0aWVzLlJFRkVSRU5DRV9TT1VTVFlQRVBPSTtcclxuXHRcdFx0cmV0dXJuIEwubWFya2VyKGxhdGxuZywge1xyXG5cdFx0XHRcdGljb246IGljb25Qb2lbc3NUeXBlUG9pXVxyXG5cdFx0XHR9KTtcclxuXHRcdH0sXHJcblx0XHRpY29uQ3JlYXRlRnVuY3Rpb246IGZ1bmN0aW9uIChjbHVzdGVyKSB7XHJcbiAgICAgICAgICAgIGxldCBjb3VudCA9IGNsdXN0ZXIuZ2V0Q2hpbGRDb3VudCgpO1xyXG4gICAgICAgICAgICBsZXQgZGlnaXRzID0gKGNvdW50ICsgJycpLmxlbmd0aDtcclxuICAgICAgICAgICAgcmV0dXJuIEwuZGl2SWNvbih7XHJcbiAgICAgICAgICAgICAgaHRtbDogY291bnQsXHJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnY2x1c3RlciBkaWdpdHMtJyArIGRpZ2l0cyxcclxuICAgICAgICAgICAgICBpY29uU2l6ZTogbnVsbFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblx0fSkuYWRkVG8ocG9pTGF5ZXJHcm91cCkuYWRkVG8obWFwKTtcclxuXHJcblx0Ly9wb3B1cCBib25uZXMgYWRyZXNzZXNcclxuXHRwb2lMYXllci5iaW5kUG9wdXAoZnVuY3Rpb24gKGxheWVyKSB7XHJcblx0XHR2YXIgbm9tID0gbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk5PTTtcclxuXHRcdHZhciBpZFBvaSA9IGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5JREVOVElGSUFOVF9QT0k7XHJcblx0XHR2YXIgdHlwZSA9IGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5SRUZFUkVOQ0VfVFlQRVBPSTtcclxuXHRcdHZhciByZWZTb3VzVHlwZSA9IGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5SRUZFUkVOQ0VfU09VU1RZUEVQT0k7XHJcblx0XHRyZXR1cm4gTC5VdGlsLnRlbXBsYXRlKHBvcHVwVGVtcGxhdGVQb2kobm9tLCBpZFBvaSwgdHlwZSwgcmVmU291c1R5cGUpKTtcclxuXHR9KTtcclxuXHJcblx0LypldmVuZW1lbnRzIGNhcnRlIGV0IGxheWVyIGJvbm5lcyBhZHJlc3NlcyovXHJcblx0cG9pTGF5ZXIub24oJ2xvYWQnLCAoKSA9PiB7XHJcblx0XHRzeW5jU2lkZWJhcigpO1xyXG5cdH0pO1xyXG5cdHBvaUxheWVyLm9uY2UoJ2xvYWQnLCAoKSA9PiB7XHJcblx0XHRpZihsb2NhdGlvbi5wYXRobmFtZSAhPSBtZW51VXJsKXtcclxuXHRcdFx0bGV0IHBhcnRzID0gbG9jYXRpb24uaHJlZi5zcGxpdChtZW51VXJsK1wiL1wiKTtcclxuXHRcdFx0bGV0IHVybGlkID0gcGFydHNbMV0uc3BsaXQoXCItXCIpO1xyXG5cdFx0XHRyZW5kZXJJbmZvRmVhdHVyZShwb2lMYXllciwgdXJsaWRbMF0pO1xyXG5cdFx0fTtcclxuXHRcdHpvb21Ub01hcmtlcnNBZnRlckxvYWQobWFwLCBwb2lMYXllcik7XHJcblx0fSk7XHJcblx0cG9pTGF5ZXIub24oJ3BvcHVwb3BlbicsICgpID0+IHtcclxuXHRcdGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYnV0dG9uUG9wdXBcIikuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB7XHJcblx0XHRcdHJlbmRlckluZm9GZWF0dXJlKHBvaUxheWVyLCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImJ1dHRvblBvcHVwXCIpLmdldEF0dHJpYnV0ZSgndmFsdWUnKSlcclxuXHRcdH0sIGZhbHNlKTtcclxuXHR9KTtcclxuXHRtYXAub24oXCJtb3ZlZW5kXCIsIGZ1bmN0aW9uICgpIHtcclxuXHRcdHBvaUxheWVyLnNldFdoZXJlKHBvaUxheWVyLmdldFdoZXJlKCkpO1xyXG5cdFx0c3luY1NpZGViYXIoKTtcclxuXHR9KTtcclxuXHRtYXAub24oJ292ZXJsYXlyZW1vdmUnLCBmdW5jdGlvbihlKSB7XHJcblx0XHRwb2lMYXllckdyb3VwLmNsZWFyTGF5ZXJzKCk7XHJcblx0fSk7XHJcblx0LypmaW4gZXZlbmVtZW50cyBjYXJ0ZSBldCBsYXllciBwb2ludCBkZXBhcnQqL1xyXG5cclxuXHQvKmZpbHRyZXMgZGUgcmVjaGVyY2hlcyAqL1xyXG5cdG5ldyBUYWdpZnkoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInRleHRhcmVhW25hbWU9dGFnc0RlcHRzXVwiKSwge1xyXG5cdFx0ZW5mb3JlV2hpdGVsaXN0OiB0cnVlLFxyXG5cdFx0d2hpdGVsaXN0IDpkZXB0TGlzdFxyXG5cdH0pO1xyXG5cdGNvbnN0IGZpbHRlcnM9KGZpbHRlcnM9W10pPT57XHJcblx0XHRjb25zdCBlbGVtZW50cyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIudHlwZVBvaUZpbHRlciwgdGV4dGFyZWFbbmFtZT10YWdzRGVwdHNdXCIpO1xyXG5cdFx0ZWxlbWVudHMuZm9yRWFjaChpdGVtID0+IHtcclxuXHRcdFx0aXRlbS5hZGRFdmVudExpc3RlbmVyKCdjaGFuZ2UnLCAoZSkgPT4ge1xyXG5cdFx0XHRcdGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiaW5wdXQ6Y2hlY2tlZCwgdGV4dGFyZWFbbmFtZT10YWdzRGVwdHNdXCIpLmxlbmd0aCA9PT0gMCl7XHJcblx0XHRcdFx0XHRmaWx0ZXJzPVtdO1xyXG5cdFx0XHRcdFx0Z2xvYmFsRmlsdGVyKGZpbHRlcnMpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRpZiAoaXRlbS5jaGVja2VkID09PSB0cnVlICYmIGUudGFyZ2V0LmlkICE9XCJkZXB0RmlsdGVyXCIpe1xyXG5cdFx0XHRcdFx0XHRmaWx0ZXJzLnB1c2goaXRlbS52YWx1ZSk7XHJcblx0XHRcdFx0XHR9IGVsc2UgaWYgKGUudGFyZ2V0LmlkPT1cImRlcHRGaWx0ZXJcIiAmJiBlLnRhcmdldC52YWx1ZSl7XHJcblx0XHRcdFx0XHRcdGxldCBqc29uRGVwdHMgPSBKU09OLnBhcnNlKGUudGFyZ2V0LnZhbHVlKTtcclxuXHRcdFx0XHRcdFx0ZmlsdGVycz1maWx0ZXJzLmZpbHRlcihmID0+IGYuaW5kZXhPZihcIkNvZGVfRGVwdFwiKSAhPT0gMCk7XHJcblx0XHRcdFx0XHRcdGpzb25EZXB0cy5mb3JFYWNoKGZ1bmN0aW9uKGl0ZW1EZXB0KXtcclxuXHRcdFx0XHRcdFx0XHRmaWx0ZXJzLnB1c2goaXRlbURlcHQucXVlcnlQb2kpO1xyXG5cdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdGZpbHRlcnMuc3BsaWNlKGZpbHRlcnMuaW5kZXhPZihpdGVtLnZhbHVlKSwxKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGdsb2JhbEZpbHRlcihmaWx0ZXJzKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cdFx0fSk7XHJcblx0fVxyXG5cdGZpbHRlcnMoKTtcclxuXHRjb25zdCBnbG9iYWxGaWx0ZXIgPSAoZmlsdGVycykgPT4ge1xyXG5cdFx0bGV0IGRlcHQgPSBmaWx0ZXJzLmZpbHRlcihmID0+IGYuaW5jbHVkZXMoXCJDb2RlX0RlcHRcIikpLmpvaW4oXCIgT1IgXCIpO1xyXG5cdFx0bGV0IHR5cGVQb2kgPSBmaWx0ZXJzLmZpbHRlcihmID0+IGYuaW5jbHVkZXMoXCJSRUZFUkVOQ0VfU09VU1RZUEVQT0lcIikpLmpvaW4oXCIgT1IgXCIpO1xyXG5cdFx0bGV0IHNvdXNUeXBlUG9pPSBmaWx0ZXJzLmZpbHRlcihmID0+IGYuaW5jbHVkZXMoXCJSRUZFUkVOQ0VfVFlQRVBPSVwiKSkuam9pbihcIiBPUiBcIik7XHJcblx0XHRsZXQgcmVzdWx0ID0gYCR7ZGVmYXVsdFdoZXJlQ2xhdXNlID0hIFwiKFJFRkVSRU5DRV9TT1VTVFlQRVBPSSBJTiAoMTAxLDIwNCwyMDUpKSBBTkQgVklTSUJJTElURT0xXCIgPyBcIihcIitkZWZhdWx0V2hlcmVDbGF1c2UrXCIpIEFORCBcIiA6IFwiXCJ9JHt0eXBlUG9pID8gXCIoXCIrdHlwZVBvaStcIikgQU5EIFwiIDogXCJcIn0ke3NvdXNUeXBlUG9pID8gXCIoXCIrc291c1R5cGVQb2krXCIpIEFORCBcIiA6IFwiXCJ9JHtkZXB0ID8gXCIoXCIrZGVwdCtcIikgQU5EIFwiIDogXCJcIn1gLnNsaWNlKDAsLTQpO1xyXG5cdFx0Ly9jb25zb2xlLmxvZyhyZXN1bHQpO1xyXG5cdFx0cG9pTGF5ZXIuc2V0V2hlcmUocmVzdWx0KTtcclxuXHR9XHJcblx0LypmaW4gZGVzIGZpbHRyZXMgZGUgcmVjaGVyY2hlcyAqL1xyXG5cclxuXHQvKiBiYXJyZSBkZSByZWNoZXJjaGUgYWRyZXNzZSBldCBwb2kgKGdlb2NvZGV1ciBlbiBoYXV0IGEgZ2F1Y2hlIGRlIGxhIGNhcnRlKSAqL1xyXG5cdGNvbnN0IHNlYXJjaENvbnRyb2wgPSBMLmVzcmkuR2VvY29kaW5nLmdlb3NlYXJjaCh7XHJcblx0XHR1c2VNYXBCb3VuZHM6IGZhbHNlLFxyXG5cdFx0dGl0bGU6ICdSZWNoZXJjaGVyIHVuIGxpZXUgLyB1biBsYWJlbCB2w6lsbycsXHJcblx0XHRwbGFjZWhvbGRlcjogJ1Ryb3V2ZXIgdW4gbGlldSAvIHVuIGxhYmVsIHbDqWxvJyxcclxuXHRcdGV4cGFuZGVkOiB0cnVlLFxyXG5cdFx0Y29sbGFwc2VBZnRlclJlc3VsdDogZmFsc2UsXHJcblx0XHRwb3NpdGlvbjogJ3RvcGxlZnQnLFxyXG5cdFx0cHJvdmlkZXJzOiBbXHJcblx0XHRcdGFyY2dpc09ubGluZVByb3ZpZGVyLFxyXG5cdFx0XHRMLmVzcmkuR2VvY29kaW5nLmZlYXR1cmVMYXllclByb3ZpZGVyKHsgLy9yZWNoZXJjaGUgZGVzIGNvdWNoZXMgZGFucyBsYSBnZGIgdmVmXHJcblx0XHRcdFx0dXJsOiBwb2lMYXllci5vcHRpb25zLnVybCxcclxuXHRcdFx0XHR3aGVyZTogcG9pTGF5ZXIuZ2V0V2hlcmUoKSxcclxuXHRcdFx0XHRzZWFyY2hGaWVsZHM6IFsnTk9NJ10sXHJcblx0XHRcdFx0bGFiZWw6ICdMQUJFTFMgVkVMTzonLFxyXG5cdFx0XHRcdG1heFJlc3VsdHM6IDcsXHJcblx0XHRcdFx0YnVmZmVyUmFkaXVzOiAxMDAwLFxyXG5cdFx0XHRcdGZvcm1hdFN1Z2dlc3Rpb246IGZ1bmN0aW9uIChmZWF0dXJlKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gYCR7ZmVhdHVyZS5wcm9wZXJ0aWVzLk5PTX0gLSAke2JhTmFtZVtmZWF0dXJlLnByb3BlcnRpZXMuUkVGRVJFTkNFX1NPVVNUWVBFUE9JXX1gO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSlcclxuXHRcdF1cclxuXHR9KS5hZGRUbyhtYXApO1xyXG5cdC8vYmFycmUgZGUgcmVjaGVyY2hlIGVuIGRlaG9ycyBkZSBsYSBjYXJ0ZVxyXG5cdGNvbnN0IGVzcmlHZW9jb2RlciA9IHNlYXJjaENvbnRyb2wuZ2V0Q29udGFpbmVyKCk7XHJcblx0c2V0RXNyaUdlb2NvZGVyKGVzcmlHZW9jb2RlciwgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2dlb2NvZGVyJykpO1xyXG5cdC8vcmVjdXAgbGEgcmVxdWV0ZSBlbiBjb3VycyBldCBhcHBsaXF1ZSBhdXggcmVzdWx0YXRzIGR1IGdlb2NvZGV1clxyXG5cdHNlYXJjaENvbnRyb2wub24oXCJyZXF1ZXN0ZW5kXCIsIGZ1bmN0aW9uICgpIHtcclxuXHRcdHNlYXJjaENvbnRyb2wub3B0aW9ucy5wcm92aWRlcnNbMV0ub3B0aW9ucy53aGVyZSA9IHBvaUxheWVyLmdldFdoZXJlKCk7XHJcblx0fSk7XHJcblx0LyogZmluIGJhcnJlIGRlIHJlY2hlcmNoZSBhZHJlc3NlIGV0IHBvaSAoZ2VvY29kZXVyIGVuIGhhdXQgYSBnYXVjaGUgZGUgbGEgY2FydGUpICovXHJcblxyXG5cdC8qY2FyZHMgZGFucyBsYSBzaWRlYmFyKi9cclxuXHRjb25zdCBzeW5jU2lkZWJhciA9IChhcnJSZXN1bHRzU2lkZWJhcj1bXSkgPT4ge1xyXG5cdFx0JChcIiNmZWF0dXJlc1wiKS5lbXB0eSgpO1xyXG5cdFx0cG9pTGF5ZXIuZWFjaEFjdGl2ZUZlYXR1cmUoZnVuY3Rpb24gKGxheWVyKSB7IC8vZWFjaEFjdGl2ZUZlYXR1cmUgPSBmb25jdGlvbiBhIGFqb3V0ZXIgZGFucyBlc3JpLWxlYWZsZXQtY2x1c3Rlci5qcyAtIGF0dGVudGlvbiBzaSBtYWogZGUgbGEgbGlicmFpcmllXHJcblx0XHRcdGlmIChtYXAuaGFzTGF5ZXIocG9pTGF5ZXIpKSB7XHJcblx0XHRcdFx0aWYgKG1hcC5nZXRCb3VuZHMoKS5jb250YWlucyhsYXllci5nZXRMYXRMbmcoKSkpIHtcclxuXHRcdFx0XHRcdGFyclJlc3VsdHNTaWRlYmFyLnB1c2goXHJcblx0XHRcdFx0XHRcdHNpZGViYXJUZW1wbGF0ZVBvaShcclxuXHRcdFx0XHRcdFx0XHRsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuTk9NLFxyXG5cdFx0XHRcdFx0XHRcdHBvaU5hbWVbbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLlJFRkVSRU5DRV9TT1VTVFlQRVBPSV0sXHJcblx0XHRcdFx0XHRcdFx0bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkNPTU1VTkUsXHJcblx0XHRcdFx0XHRcdFx0bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk5vbV9EZXB0LFxyXG5cdFx0XHRcdFx0XHRcdGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5JREVOVElGSUFOVF9QT0ksXHJcblx0XHRcdFx0XHRcdFx0bGF5ZXIuZmVhdHVyZS5nZW9tZXRyeS5jb29yZGluYXRlc1sxXSxcclxuXHRcdFx0XHRcdFx0XHRsYXllci5mZWF0dXJlLmdlb21ldHJ5LmNvb3JkaW5hdGVzWzBdXHJcblx0XHRcdFx0XHRcdClcclxuXHRcdFx0XHRcdCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHRcdGFyclJlc3VsdHNTaWRlYmFyLnJldmVyc2UoKTtcclxuXHRcdGFkZFJlc3VsdHNUb1NpZGViYXIoYXJyUmVzdWx0c1NpZGViYXIscG9pTGF5ZXIscmVuZGVySW5mb0ZlYXR1cmUpO1xyXG5cdH1cclxuXHJcblx0Ly90ZW1wbGF0ZSBjYXJkc1xyXG5cdGNvbnN0IHNpZGViYXJUZW1wbGF0ZVBvaSA9IChub20sc291c3R5cGUsdmlsbGUsZGVwdCxpZCxsYXQsbG9uKSA9PiB7XHJcblx0XHRyZXR1cm4gKFxyXG5cdFx0XHRgPGRpdiBjbGFzcz1cImNhcmQgZmVhdHVyZS1jYXJkIG15LTIgcC0yXCIgaWQ9XCIke2lkfVwiIGRhdGEtbGF0PVwiJHtsYXR9XCIgZGF0YS1sb249XCIke2xvbn1cIj4gXHJcblx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbCBhbGlnbi1zZWxmLWNlbnRlciByb3VuZGVkLXJpZ2h0XCI+XHJcblx0XHRcdFx0XHQ8aDYgY2xhc3M9XCJkLWJsb2NrIHRleHQtdXBwZXJjYXNlXCI+JHtub219PC9oNj5cclxuXHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwiYmFkZ2UgYmFkZ2UtcGlsbCBiYWRnZS1saWdodCBteS0xIG1yLTFcIj4ke3NvdXN0eXBlfTwvc3Bhbj5cclxuXHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwibXktMSBtci0xXCI+PGJyLz48aSBjbGFzcz1cImZhcyBmYS1tYXAtbWFya2VkLWFsdFwiPjwvaT4gJHt2aWxsZX0sICR7ZGVwdH08L3NwYW4+XHJcblx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdDwvZGl2PmBcclxuXHRcdCk7XHJcblx0fVxyXG5cdC8qIGZpbiBkZXMgY2FyZHMgZGFucyBsYSBzaWRlYmFyKi9cclxuXHJcblx0Ly9jb21wb3NhbnQgZmljaGUgZGVzY3JpcHRpdmVcclxuXHRjb25zdCByZW5kZXJJbmZvRmVhdHVyZSA9IChMYXllciwgaWQpID0+IHtcclxuXHRcdGNvbnN0IHF1ZXJ5Q29udGFjdCA9IEwuZXNyaS5xdWVyeSh7XHJcblx0XHRcdHVybDogYWdvbEZlYXR1cmVTZXJ2aWNlcy5mb3Vybmlzc2V1clxyXG5cdFx0fSk7XHJcblx0XHRtYXBFbGVtZW50LnN0eWxlLndpZHRoPSBtYXBXaWR0aCtcInB4XCI7XHJcblx0XHRsZXQgcGFyYW1zPWxvY2F0aW9uLnNlYXJjaDtcclxuXHRcdGxldCBpbmZvZmVhdHVyZUVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnaW5mb2ZlYXR1cmUnKTtcclxuXHRcdGluZm9mZWF0dXJlRWxlbWVudC52YWx1ZT1MYXllci5nZXRXaGVyZSgpO1xyXG5cdFx0TGF5ZXIuZWFjaEZlYXR1cmUoZnVuY3Rpb24gKGxheWVyKSB7XHJcblx0XHRcdGlmKGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5JREVOVElGSUFOVF9QT0kgPT0gaWQpe1xyXG5cdFx0XHRcdExheWVyLnNldFdoZXJlKFwiSURFTlRJRklBTlRfUE9JPVwiK2lkKTtcclxuXHRcdFx0XHR1bm1vdW50Q29tcG9uZW50QXROb2RlKGluZm9mZWF0dXJlRWxlbWVudCk7XHJcblx0XHRcdFx0cmVuZGVyKFx0XHJcblx0XHRcdFx0XHQ8SW5mb0ZlYXR1cmUgXHJcblx0XHRcdFx0XHRcdGlkUG9pPXtpZH1cclxuXHRcdFx0XHRcdFx0bm9tPXtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuTk9NfVxyXG5cdFx0XHRcdFx0XHRxdWVyeUNvbnRhY3RMYWJlbHM9e3F1ZXJ5Q29udGFjdH1cclxuXHRcdFx0XHRcdFx0ZGVzY3I9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5ERVNDUklQVElPTn1cclxuXHRcdFx0XHRcdFx0c291c3R5cGU9e3BvaU5hbWVbbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLlJFRkVSRU5DRV9TT1VTVFlQRVBPSV19XHJcblx0XHRcdFx0XHRcdHJvb3RDb250YWluZXI9e2luZm9mZWF0dXJlRWxlbWVudH1cclxuXHRcdFx0XHRcdFx0bWFwPXttYXB9XHJcblx0XHRcdFx0XHRcdG1hcFdpZHRoID0ge21hcFdpZHRofVxyXG5cdFx0XHRcdFx0XHRMYXllcj17cG9pTGF5ZXJ9XHJcblx0XHRcdFx0XHRcdHdoZXJlQ2xhdXNlPXtpbmZvZmVhdHVyZUVsZW1lbnQudmFsdWV9XHJcblx0XHRcdFx0XHRcdHBhcmFtcz17cGFyYW1zfVxyXG5cdFx0XHRcdFx0Lz4sIGluZm9mZWF0dXJlRWxlbWVudFxyXG5cdFx0XHRcdCk7XHJcblx0XHRcdFx0bmF2aWdhdGlvbkhpc3RvcnkoaWQsIG1lbnVVcmwsbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLlNMVUcpO1xyXG5cdFx0XHRcdC8vZ2VzdGlvbiBkZSBsYSBjYXJ0ZVxyXG5cdFx0XHRcdGxldCByZXNpemUgPSBtYXBXaWR0aC1kb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImluZm8tZmVhdHVyZS1jb250ZW50XCIpLm9mZnNldFdpZHRoO1xyXG5cdFx0XHRcdG1hcEVsZW1lbnQuc3R5bGUud2lkdGg9cmVzaXplK1wicHhcIjtcclxuXHRcdFx0XHRtYXAuaW52YWxpZGF0ZVNpemUoKTtcclxuXHRcdFx0XHR6b29tVG9NYXJrZXJzQWZ0ZXJMb2FkIChtYXAsIExheWVyKTtcclxuXHRcdFx0XHQvL21hcC5zZXRWaWV3KFtsYXllci5mZWF0dXJlLmdlb21ldHJ5LmNvb3JkaW5hdGVzWzFdLGxheWVyLmZlYXR1cmUuZ2VvbWV0cnkuY29vcmRpbmF0ZXNbMF1dLCAxMCk7XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblx0Ly9nZXN0aW9uIGRlIGxhIG5hdmlnYXRpb24gYXZlYyBib3V0b25zIHByZWNlZGVudC9zdWl2YW50XHJcblx0d2luZG93Lm9ucG9wc3RhdGUgPSAoKSA9PiB7XHJcblx0XHRiYWNrQnV0dG9uTmF2aWdhdGlvbihtZW51VXJsLCBtYXAsIG1hcEVsZW1lbnQsIG1hcFdpZHRoLCBwb2lMYXllciwgcmVuZGVySW5mb0ZlYXR1cmUpO1xyXG5cdH1cclxuXHJcbn0pKCk7XHJcbiIsIid1c2Ugc3RyaWN0JztcbnZhciAkID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2V4cG9ydCcpO1xudmFyICRmaWx0ZXIgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvYXJyYXktaXRlcmF0aW9uJykuZmlsdGVyO1xudmFyIGFycmF5TWV0aG9kSGFzU3BlY2llc1N1cHBvcnQgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvYXJyYXktbWV0aG9kLWhhcy1zcGVjaWVzLXN1cHBvcnQnKTtcbnZhciBhcnJheU1ldGhvZFVzZXNUb0xlbmd0aCA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9hcnJheS1tZXRob2QtdXNlcy10by1sZW5ndGgnKTtcblxudmFyIEhBU19TUEVDSUVTX1NVUFBPUlQgPSBhcnJheU1ldGhvZEhhc1NwZWNpZXNTdXBwb3J0KCdmaWx0ZXInKTtcbi8vIEVkZ2UgMTQtIGlzc3VlXG52YXIgVVNFU19UT19MRU5HVEggPSBhcnJheU1ldGhvZFVzZXNUb0xlbmd0aCgnZmlsdGVyJyk7XG5cbi8vIGBBcnJheS5wcm90b3R5cGUuZmlsdGVyYCBtZXRob2Rcbi8vIGh0dHBzOi8vdGMzOS5lcy9lY21hMjYyLyNzZWMtYXJyYXkucHJvdG90eXBlLmZpbHRlclxuLy8gd2l0aCBhZGRpbmcgc3VwcG9ydCBvZiBAQHNwZWNpZXNcbiQoeyB0YXJnZXQ6ICdBcnJheScsIHByb3RvOiB0cnVlLCBmb3JjZWQ6ICFIQVNfU1BFQ0lFU19TVVBQT1JUIHx8ICFVU0VTX1RPX0xFTkdUSCB9LCB7XG4gIGZpbHRlcjogZnVuY3Rpb24gZmlsdGVyKGNhbGxiYWNrZm4gLyogLCB0aGlzQXJnICovKSB7XG4gICAgcmV0dXJuICRmaWx0ZXIodGhpcywgY2FsbGJhY2tmbiwgYXJndW1lbnRzLmxlbmd0aCA+IDEgPyBhcmd1bWVudHNbMV0gOiB1bmRlZmluZWQpO1xuICB9XG59KTtcbiIsIid1c2Ugc3RyaWN0JztcbnZhciAkID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2V4cG9ydCcpO1xudmFyICRpbmRleE9mID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2FycmF5LWluY2x1ZGVzJykuaW5kZXhPZjtcbnZhciBhcnJheU1ldGhvZElzU3RyaWN0ID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2FycmF5LW1ldGhvZC1pcy1zdHJpY3QnKTtcbnZhciBhcnJheU1ldGhvZFVzZXNUb0xlbmd0aCA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9hcnJheS1tZXRob2QtdXNlcy10by1sZW5ndGgnKTtcblxudmFyIG5hdGl2ZUluZGV4T2YgPSBbXS5pbmRleE9mO1xuXG52YXIgTkVHQVRJVkVfWkVSTyA9ICEhbmF0aXZlSW5kZXhPZiAmJiAxIC8gWzFdLmluZGV4T2YoMSwgLTApIDwgMDtcbnZhciBTVFJJQ1RfTUVUSE9EID0gYXJyYXlNZXRob2RJc1N0cmljdCgnaW5kZXhPZicpO1xudmFyIFVTRVNfVE9fTEVOR1RIID0gYXJyYXlNZXRob2RVc2VzVG9MZW5ndGgoJ2luZGV4T2YnLCB7IEFDQ0VTU09SUzogdHJ1ZSwgMTogMCB9KTtcblxuLy8gYEFycmF5LnByb3RvdHlwZS5pbmRleE9mYCBtZXRob2Rcbi8vIGh0dHBzOi8vdGMzOS5lcy9lY21hMjYyLyNzZWMtYXJyYXkucHJvdG90eXBlLmluZGV4b2ZcbiQoeyB0YXJnZXQ6ICdBcnJheScsIHByb3RvOiB0cnVlLCBmb3JjZWQ6IE5FR0FUSVZFX1pFUk8gfHwgIVNUUklDVF9NRVRIT0QgfHwgIVVTRVNfVE9fTEVOR1RIIH0sIHtcbiAgaW5kZXhPZjogZnVuY3Rpb24gaW5kZXhPZihzZWFyY2hFbGVtZW50IC8qICwgZnJvbUluZGV4ID0gMCAqLykge1xuICAgIHJldHVybiBORUdBVElWRV9aRVJPXG4gICAgICAvLyBjb252ZXJ0IC0wIHRvICswXG4gICAgICA/IG5hdGl2ZUluZGV4T2YuYXBwbHkodGhpcywgYXJndW1lbnRzKSB8fCAwXG4gICAgICA6ICRpbmRleE9mKHRoaXMsIHNlYXJjaEVsZW1lbnQsIGFyZ3VtZW50cy5sZW5ndGggPiAxID8gYXJndW1lbnRzWzFdIDogdW5kZWZpbmVkKTtcbiAgfVxufSk7XG4iLCIndXNlIHN0cmljdCc7XG52YXIgJCA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9leHBvcnQnKTtcbnZhciBpc09iamVjdCA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9pcy1vYmplY3QnKTtcbnZhciBpc0FycmF5ID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2lzLWFycmF5Jyk7XG52YXIgdG9BYnNvbHV0ZUluZGV4ID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL3RvLWFic29sdXRlLWluZGV4Jyk7XG52YXIgdG9MZW5ndGggPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvdG8tbGVuZ3RoJyk7XG52YXIgdG9JbmRleGVkT2JqZWN0ID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL3RvLWluZGV4ZWQtb2JqZWN0Jyk7XG52YXIgY3JlYXRlUHJvcGVydHkgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvY3JlYXRlLXByb3BlcnR5Jyk7XG52YXIgd2VsbEtub3duU3ltYm9sID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL3dlbGwta25vd24tc3ltYm9sJyk7XG52YXIgYXJyYXlNZXRob2RIYXNTcGVjaWVzU3VwcG9ydCA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9hcnJheS1tZXRob2QtaGFzLXNwZWNpZXMtc3VwcG9ydCcpO1xudmFyIGFycmF5TWV0aG9kVXNlc1RvTGVuZ3RoID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2FycmF5LW1ldGhvZC11c2VzLXRvLWxlbmd0aCcpO1xuXG52YXIgSEFTX1NQRUNJRVNfU1VQUE9SVCA9IGFycmF5TWV0aG9kSGFzU3BlY2llc1N1cHBvcnQoJ3NsaWNlJyk7XG52YXIgVVNFU19UT19MRU5HVEggPSBhcnJheU1ldGhvZFVzZXNUb0xlbmd0aCgnc2xpY2UnLCB7IEFDQ0VTU09SUzogdHJ1ZSwgMDogMCwgMTogMiB9KTtcblxudmFyIFNQRUNJRVMgPSB3ZWxsS25vd25TeW1ib2woJ3NwZWNpZXMnKTtcbnZhciBuYXRpdmVTbGljZSA9IFtdLnNsaWNlO1xudmFyIG1heCA9IE1hdGgubWF4O1xuXG4vLyBgQXJyYXkucHJvdG90eXBlLnNsaWNlYCBtZXRob2Rcbi8vIGh0dHBzOi8vdGMzOS5lcy9lY21hMjYyLyNzZWMtYXJyYXkucHJvdG90eXBlLnNsaWNlXG4vLyBmYWxsYmFjayBmb3Igbm90IGFycmF5LWxpa2UgRVMzIHN0cmluZ3MgYW5kIERPTSBvYmplY3RzXG4kKHsgdGFyZ2V0OiAnQXJyYXknLCBwcm90bzogdHJ1ZSwgZm9yY2VkOiAhSEFTX1NQRUNJRVNfU1VQUE9SVCB8fCAhVVNFU19UT19MRU5HVEggfSwge1xuICBzbGljZTogZnVuY3Rpb24gc2xpY2Uoc3RhcnQsIGVuZCkge1xuICAgIHZhciBPID0gdG9JbmRleGVkT2JqZWN0KHRoaXMpO1xuICAgIHZhciBsZW5ndGggPSB0b0xlbmd0aChPLmxlbmd0aCk7XG4gICAgdmFyIGsgPSB0b0Fic29sdXRlSW5kZXgoc3RhcnQsIGxlbmd0aCk7XG4gICAgdmFyIGZpbiA9IHRvQWJzb2x1dGVJbmRleChlbmQgPT09IHVuZGVmaW5lZCA/IGxlbmd0aCA6IGVuZCwgbGVuZ3RoKTtcbiAgICAvLyBpbmxpbmUgYEFycmF5U3BlY2llc0NyZWF0ZWAgZm9yIHVzYWdlIG5hdGl2ZSBgQXJyYXkjc2xpY2VgIHdoZXJlIGl0J3MgcG9zc2libGVcbiAgICB2YXIgQ29uc3RydWN0b3IsIHJlc3VsdCwgbjtcbiAgICBpZiAoaXNBcnJheShPKSkge1xuICAgICAgQ29uc3RydWN0b3IgPSBPLmNvbnN0cnVjdG9yO1xuICAgICAgLy8gY3Jvc3MtcmVhbG0gZmFsbGJhY2tcbiAgICAgIGlmICh0eXBlb2YgQ29uc3RydWN0b3IgPT0gJ2Z1bmN0aW9uJyAmJiAoQ29uc3RydWN0b3IgPT09IEFycmF5IHx8IGlzQXJyYXkoQ29uc3RydWN0b3IucHJvdG90eXBlKSkpIHtcbiAgICAgICAgQ29uc3RydWN0b3IgPSB1bmRlZmluZWQ7XG4gICAgICB9IGVsc2UgaWYgKGlzT2JqZWN0KENvbnN0cnVjdG9yKSkge1xuICAgICAgICBDb25zdHJ1Y3RvciA9IENvbnN0cnVjdG9yW1NQRUNJRVNdO1xuICAgICAgICBpZiAoQ29uc3RydWN0b3IgPT09IG51bGwpIENvbnN0cnVjdG9yID0gdW5kZWZpbmVkO1xuICAgICAgfVxuICAgICAgaWYgKENvbnN0cnVjdG9yID09PSBBcnJheSB8fCBDb25zdHJ1Y3RvciA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIHJldHVybiBuYXRpdmVTbGljZS5jYWxsKE8sIGssIGZpbik7XG4gICAgICB9XG4gICAgfVxuICAgIHJlc3VsdCA9IG5ldyAoQ29uc3RydWN0b3IgPT09IHVuZGVmaW5lZCA/IEFycmF5IDogQ29uc3RydWN0b3IpKG1heChmaW4gLSBrLCAwKSk7XG4gICAgZm9yIChuID0gMDsgayA8IGZpbjsgaysrLCBuKyspIGlmIChrIGluIE8pIGNyZWF0ZVByb3BlcnR5KHJlc3VsdCwgbiwgT1trXSk7XG4gICAgcmVzdWx0Lmxlbmd0aCA9IG47XG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfVxufSk7XG4iXSwic291cmNlUm9vdCI6IiJ9