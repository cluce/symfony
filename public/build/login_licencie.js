(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login_licencie"],{

/***/ "./assets/js/components/LoginLicencie.jsx":
/*!************************************************!*\
  !*** ./assets/js/components/LoginLicencie.jsx ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_is_array_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.is-array.js */ "./node_modules/core-js/modules/es.array.is-array.js");
/* harmony import */ var core_js_modules_es_array_is_array_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_is_array_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.function.name.js */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_3__);



function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }




var LoginLicencie = function LoginLicencie(props) {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_2__["useState"])({
    username: "",
    password: ""
  }),
      _useState2 = _slicedToArray(_useState, 2),
      credentials = _useState2[0],
      setCredentials = _useState2[1];

  try {
    axios__WEBPACK_IMPORTED_MODULE_3___default.a.post("https://sso.ffcyclo.org/connect/token", {
      headers: {
        'Authorization': 'Basic ZGV2RkZDeWNsbzpjbGllbnQ4NzY1c2VjcmV0NDY1NDIxZGV2NDUzMjFka2xqZmZjeWNsb3NxZG1rbFlqc3Fzaw==',
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: {
        'grant_type': 'client_credentials'
      }
    }).then(function (response) {
      return console.log(response);
    });
  } catch (error) {
    console.log(error.response);
  }

  var handleChange = function handleChange(event) {
    var value = event.currentTarget.value;
    var name = event.currentTarget.name;
    setCredentials(_objectSpread(_objectSpread({}, credentials), {}, _defineProperty({}, name, value)));
  };

  var handleSubmit = function handleSubmit(event) {
    event.preventDefault();
    console.log(credentials);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_2___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("h1", null, "Connexion"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("form", {
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
    className: "form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
    htmlFor: "username"
  }, "Adresse email"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
    value: credentials.username,
    onChange: handleChange,
    type: "text",
    placeholder: "Adresse email de connexion",
    name: "username",
    id: "username",
    className: "form-control"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
    className: "form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
    htmlFor: "password"
  }, "Mot de passe"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
    value: credentials.password,
    onChange: handleChange,
    type: "password",
    placeholder: "Mot de passe",
    name: "password",
    id: "password",
    className: "form-control"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
    className: "form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("button", {
    type: "submit",
    className: "btn btn-success"
  }, "Je me connecte"))));
};

/* harmony default export */ __webpack_exports__["default"] = (LoginLicencie);

/***/ }),

/***/ "./assets/js/pages/loginLicencie.js":
/*!******************************************!*\
  !*** ./assets/js/pages/loginLicencie.js ***!
  \******************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_LoginLicencie__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/LoginLicencie */ "./assets/js/components/LoginLicencie.jsx");



react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_LoginLicencie__WEBPACK_IMPORTED_MODULE_2__["default"], null), document.getElementById("ssologin"));

/***/ })

},[["./assets/js/pages/loginLicencie.js","runtime","vendors~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~61364f2f","vendors~bonnes_adresses~calendrier~circuits~clubs~labels_velo~login_licencie","vendors~login_licencie"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29tcG9uZW50cy9Mb2dpbkxpY2VuY2llLmpzeCIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvcGFnZXMvbG9naW5MaWNlbmNpZS5qcyJdLCJuYW1lcyI6WyJMb2dpbkxpY2VuY2llIiwicHJvcHMiLCJ1c2VTdGF0ZSIsInVzZXJuYW1lIiwicGFzc3dvcmQiLCJjcmVkZW50aWFscyIsInNldENyZWRlbnRpYWxzIiwiYXhpb3MiLCJwb3N0IiwiaGVhZGVycyIsImRhdGEiLCJ0aGVuIiwicmVzcG9uc2UiLCJjb25zb2xlIiwibG9nIiwiZXJyb3IiLCJoYW5kbGVDaGFuZ2UiLCJldmVudCIsInZhbHVlIiwiY3VycmVudFRhcmdldCIsIm5hbWUiLCJoYW5kbGVTdWJtaXQiLCJwcmV2ZW50RGVmYXVsdCIsIlJlYWN0RE9NIiwicmVuZGVyIiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50QnlJZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7O0FBR0EsSUFBTUEsYUFBYSxHQUFHLFNBQWhCQSxhQUFnQixDQUFBQyxLQUFLLEVBQUk7QUFBQSxrQkFDV0Msc0RBQVEsQ0FBQztBQUMzQ0MsWUFBUSxFQUFFLEVBRGlDO0FBRTNDQyxZQUFRLEVBQUU7QUFGaUMsR0FBRCxDQURuQjtBQUFBO0FBQUEsTUFDcEJDLFdBRG9CO0FBQUEsTUFDUEMsY0FETzs7QUFNM0IsTUFBRztBQUNDQyxnREFBSyxDQUFDQyxJQUFOLENBQVcsdUNBQVgsRUFBbUQ7QUFDL0NDLGFBQU8sRUFBQztBQUNKLHlCQUFpQixnR0FEYjtBQUVKLHdCQUFnQjtBQUZaLE9BRHVDO0FBSy9DQyxVQUFJLEVBQUU7QUFBQyxzQkFBYztBQUFmO0FBTHlDLEtBQW5ELEVBTUdDLElBTkgsQ0FNUSxVQUFBQyxRQUFRO0FBQUEsYUFBSUMsT0FBTyxDQUFDQyxHQUFSLENBQVlGLFFBQVosQ0FBSjtBQUFBLEtBTmhCO0FBT0gsR0FSRCxDQVFFLE9BQU1HLEtBQU4sRUFBWTtBQUNWRixXQUFPLENBQUNDLEdBQVIsQ0FBWUMsS0FBSyxDQUFDSCxRQUFsQjtBQUNIOztBQUVELE1BQU1JLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUFDLEtBQUssRUFBSTtBQUMxQixRQUFNQyxLQUFLLEdBQUdELEtBQUssQ0FBQ0UsYUFBTixDQUFvQkQsS0FBbEM7QUFDQSxRQUFNRSxJQUFJLEdBQUdILEtBQUssQ0FBQ0UsYUFBTixDQUFvQkMsSUFBakM7QUFDQWQsa0JBQWMsaUNBQUtELFdBQUwsMkJBQW1CZSxJQUFuQixFQUEwQkYsS0FBMUIsR0FBZDtBQUNILEdBSkQ7O0FBTUEsTUFBTUcsWUFBWSxHQUFHLFNBQWZBLFlBQWUsQ0FBQUosS0FBSyxFQUFJO0FBQzFCQSxTQUFLLENBQUNLLGNBQU47QUFDQVQsV0FBTyxDQUFDQyxHQUFSLENBQVlULFdBQVo7QUFDSCxHQUhEOztBQUtBLHNCQUNJLHFJQUNJLG1GQURKLGVBRUk7QUFBTSxZQUFRLEVBQUVnQjtBQUFoQixrQkFDSTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNJO0FBQU8sV0FBTyxFQUFDO0FBQWYscUJBREosZUFFSTtBQUNJLFNBQUssRUFBRWhCLFdBQVcsQ0FBQ0YsUUFEdkI7QUFFSSxZQUFRLEVBQUVhLFlBRmQ7QUFHSSxRQUFJLEVBQUMsTUFIVDtBQUlJLGVBQVcsRUFBQyw0QkFKaEI7QUFLSSxRQUFJLEVBQUMsVUFMVDtBQU1JLE1BQUUsRUFBQyxVQU5QO0FBT0ksYUFBUyxFQUFDO0FBUGQsSUFGSixDQURKLGVBYUk7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDSTtBQUFPLFdBQU8sRUFBQztBQUFmLG9CQURKLGVBRUk7QUFDSSxTQUFLLEVBQUVYLFdBQVcsQ0FBQ0QsUUFEdkI7QUFFSSxZQUFRLEVBQUVZLFlBRmQ7QUFHSSxRQUFJLEVBQUMsVUFIVDtBQUlJLGVBQVcsRUFBQyxjQUpoQjtBQUtJLFFBQUksRUFBQyxVQUxUO0FBTUksTUFBRSxFQUFDLFVBTlA7QUFPSSxhQUFTLEVBQUM7QUFQZCxJQUZKLENBYkosZUF5Qkk7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDSTtBQUFRLFFBQUksRUFBQyxRQUFiO0FBQXNCLGFBQVMsRUFBQztBQUFoQyxzQkFESixDQXpCSixDQUZKLENBREo7QUFrQ0gsQ0EvREQ7O0FBa0VlaEIsNEVBQWYsRTs7Ozs7Ozs7Ozs7O0FDdEVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBdUIsZ0RBQVEsQ0FBQ0MsTUFBVCxlQUFnQiwyREFBQyxpRUFBRCxPQUFoQixFQUFrQ0MsUUFBUSxDQUFDQyxjQUFULENBQXdCLFVBQXhCLENBQWxDLEUiLCJmaWxlIjoibG9naW5fbGljZW5jaWUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHt1c2VTdGF0ZX0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnO1xyXG5cclxuXHJcbmNvbnN0IExvZ2luTGljZW5jaWUgPSBwcm9wcyA9PiB7XHJcbiAgICBjb25zdCBbY3JlZGVudGlhbHMsIHNldENyZWRlbnRpYWxzXSA9IHVzZVN0YXRlKHtcclxuICAgICAgICB1c2VybmFtZTogXCJcIixcclxuICAgICAgICBwYXNzd29yZDogXCJcIlxyXG4gICAgfSlcclxuXHJcbiAgICB0cnl7XHJcbiAgICAgICAgYXhpb3MucG9zdChcImh0dHBzOi8vc3NvLmZmY3ljbG8ub3JnL2Nvbm5lY3QvdG9rZW5cIix7XHJcbiAgICAgICAgICAgIGhlYWRlcnM6e1xyXG4gICAgICAgICAgICAgICAgJ0F1dGhvcml6YXRpb24nOiAnQmFzaWMgWkdWMlJrWkRlV05zYnpwamJHbGxiblE0TnpZMWMyVmpjbVYwTkRZMU5ESXhaR1YyTkRVek1qRmthMnhxWm1aamVXTnNiM054WkcxcmJGbHFjM0Z6YXc9PScsIFxyXG4gICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnLCBcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZGF0YTogeydncmFudF90eXBlJzogJ2NsaWVudF9jcmVkZW50aWFscyd9XHJcbiAgICAgICAgfSkudGhlbihyZXNwb25zZSA9PiBjb25zb2xlLmxvZyhyZXNwb25zZSkpXHJcbiAgICB9IGNhdGNoKGVycm9yKXtcclxuICAgICAgICBjb25zb2xlLmxvZyhlcnJvci5yZXNwb25zZSk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgaGFuZGxlQ2hhbmdlID0gZXZlbnQgPT4ge1xyXG4gICAgICAgIGNvbnN0IHZhbHVlID0gZXZlbnQuY3VycmVudFRhcmdldC52YWx1ZTtcclxuICAgICAgICBjb25zdCBuYW1lID0gZXZlbnQuY3VycmVudFRhcmdldC5uYW1lO1xyXG4gICAgICAgIHNldENyZWRlbnRpYWxzKHsuLi5jcmVkZW50aWFscywgW25hbWVdOiB2YWx1ZX0pO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IGhhbmRsZVN1Ym1pdCA9IGV2ZW50ID0+IHtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGNyZWRlbnRpYWxzKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDw+XHJcbiAgICAgICAgICAgIDxoMT5Db25uZXhpb248L2gxPlxyXG4gICAgICAgICAgICA8Zm9ybSBvblN1Ym1pdD17aGFuZGxlU3VibWl0fT5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBodG1sRm9yPVwidXNlcm5hbWVcIj5BZHJlc3NlIGVtYWlsPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICA8aW5wdXQgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXtjcmVkZW50aWFscy51c2VybmFtZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e2hhbmRsZUNoYW5nZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJBZHJlc3NlIGVtYWlsIGRlIGNvbm5leGlvblwiIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lPVwidXNlcm5hbWVcIiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ9XCJ1c2VybmFtZVwiIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJmb3JtLWNvbnRyb2xcIlxyXG4gICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBodG1sRm9yPVwicGFzc3dvcmRcIj5Nb3QgZGUgcGFzc2U8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2NyZWRlbnRpYWxzLnBhc3N3b3JkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17aGFuZGxlQ2hhbmdlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwicGFzc3dvcmRcIiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJNb3QgZGUgcGFzc2VcIiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZT1cInBhc3N3b3JkXCIgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkPVwicGFzc3dvcmRcIiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCJcclxuICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIiBjbGFzc05hbWU9XCJidG4gYnRuLXN1Y2Nlc3NcIj5KZSBtZSBjb25uZWN0ZTwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICA8Lz5cclxuICAgICk7XHJcbn1cclxuXHJcblxyXG5leHBvcnQgZGVmYXVsdCBMb2dpbkxpY2VuY2llO1xyXG4iLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUmVhY3RET00gZnJvbSAncmVhY3QtZG9tJztcclxuaW1wb3J0IExvZ2luTGljZW5jaWUgZnJvbSAnLi4vY29tcG9uZW50cy9Mb2dpbkxpY2VuY2llJztcclxuXHJcblJlYWN0RE9NLnJlbmRlcig8TG9naW5MaWNlbmNpZS8+LCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInNzb2xvZ2luXCIpKTsgIl0sInNvdXJjZVJvb3QiOiIifQ==