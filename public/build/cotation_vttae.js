(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cotation_vttae"],{

/***/ "./assets/js/pages/cotation/cotation.js":
/*!**********************************************!*\
  !*** ./assets/js/pages/cotation/cotation.js ***!
  \**********************************************/
/*! exports provided: convertMinsToHrsMins, addValueToInput */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "convertMinsToHrsMins", function() { return convertMinsToHrsMins; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addValueToInput", function() { return addValueToInput; });
/* harmony import */ var core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.parse-float.js */ "./node_modules/core-js/modules/es.parse-float.js");
/* harmony import */ var core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.regexp.exec.js */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.string.replace.js */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2__);



//conversion minutes en heures
var convertMinsToHrsMins = function convertMinsToHrsMins(minutes) {
  var h = Math.floor(minutes / 60);
  var m = minutes % 60;
  h = h < 10 ? '0' + h : h;
  m = m < 10 ? '0' + m : m;
  return h + ':' + m;
}; //remplissage inputs

function addValueToInput(inputVal, id) {
  //document.getElementById(id).value = inputVal;
  $(id).val(inputVal);
} //recup coordonnees points departs

/*const getCoordsDepart = (xml) => {
    parser = new DOMParser();
    xmlDoc = parser.parseFromString(xml, "text/xml");
    var lat = xmlDoc.getElementsByTagName("trkpt")[0].getAttribute('lat');
    var lon = xmlDoc.getElementsByTagName("trkpt")[0].getAttribute('lon');
    return [lon, lat];
}*/
//calcul de la difficulte avec radios buttons

$(":radio").change(function () {
  var names = {};
  $(':radio').each(function () {
    names[$(this).attr('name')] = true;
  });
  var count = 0;
  $.each(names, function () {
    count++;
  });

  if ($(':radio:checked').length === count) {
    var total = 0;
    $("input[type=radio]:checked").each(function () {
      total += parseFloat($(this).val());
    });
    outputDiff(total);
  }
}); //input difficulte

function outputDiff(total) {
  var diff, color;

  if (total >= 4 && total <= 5) {
    diff = total + " - Très facile";
    color = "green";
  } else if (total >= 6 && total <= 8) {
    diff = total + " - Facile";
    color = "blue";
  } else if (total >= 9 && total <= 12) {
    diff = total + " - Difficile";
    color = "red";
  } else {
    diff = total + " - Très Difficile";
    color = "black";
  }

  $("#cotation_form_difficulte").val(diff);
  $("#cotation_form_difficulte").css({
    "color": color,
    "font-size": "25px"
  });
} //compte le nombre de caracteres dans le textarea


var textInit = 0;
$('#count_text').html(textInit + ' caractère /500');
$('#cotation_form_textDescr').keyup(function () {
  var text_length = $('#cotation_form_textDescr').val().length;
  $('#count_text').html(text_length + ' caractères /500');

  if (text_length <= 1) {
    $('#count_text').html(text_length + ' caractère /500');
  }

  if (text_length > 500) {
    document.getElementById("count_text").style.color = 'red';
  } else {
    document.getElementById("count_text").style.color = 'black';
  }
}); //empecher les retours charriots du textarea

$('#cotation_form_textDescr').on('keyup', function () {
  $(this).val($(this).val().replace(/[\r\n\v]+/g, ''));
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/pages/cotation/cotationVttae.js":
/*!***************************************************!*\
  !*** ./assets/js/pages/cotation/cotationVttae.js ***!
  \***************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var esri_leaflet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! esri-leaflet */ "./node_modules/esri-leaflet/src/EsriLeaflet.js");
/* harmony import */ var leaflet_gpx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! leaflet-gpx */ "./node_modules/leaflet-gpx/gpx.js");
/* harmony import */ var leaflet_gpx__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(leaflet_gpx__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _cotation__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cotation */ "./assets/js/pages/cotation/cotation.js");


function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }





var toGeoJSON = __webpack_require__(/*! @tmcw/togeojson */ "./node_modules/@tmcw/togeojson/dist/togeojson.umd.js");

__webpack_require__(/*! leaflet.heightgraph */ "./node_modules/leaflet.heightgraph/dist/L.Control.Heightgraph.min.js");

__webpack_require__(/*! leaflet-polylinedecorator */ "./node_modules/leaflet-polylinedecorator/dist/leaflet.polylineDecorator.js"); //coche bouton radio distance au chargement du gpx


function radioDistCheck(dist) {
  if (dist >= 0 && dist <= 30) {
    document.getElementById("cotation_form_distChoice_0").checked = true;
  }

  if (dist >= 31 && dist <= 50) {
    document.getElementById("cotation_form_distChoice_1").checked = true;
  }

  if (dist >= 51 && dist <= 70) {
    document.getElementById("cotation_form_distChoice_2").checked = true;
  }

  if (dist >= 71) {
    document.getElementById("cotation_form_distChoice_3").checked = true;
  }
} //coche bouton radio denivele au chargement du gpx


function radioDenivCheck(deniv) {
  if (deniv >= 0 && deniv <= 100) {
    document.getElementById("cotation_form_denivChoice_0").checked = true;
  }

  if (deniv >= 101 && deniv <= 300) {
    document.getElementById("cotation_form_denivChoice_1").checked = true;
  }

  if (deniv >= 301 && deniv <= 1000) {
    document.getElementById("cotation_form_denivChoice_2").checked = true;
  }

  if (deniv >= 1001) {
    document.getElementById("cotation_form_denivChoice_3").checked = true;
  }
}

var inputElement = document.querySelectorAll('#cotation_form_distance, #cotation_form_denivele');

_toConsumableArray(inputElement).map(function (item) {
  item.addEventListener('change', function (e) {
    if (item.id == "cotation_form_distance") {
      var dist = document.getElementById("cotation_form_distance").value;
      radioDistCheck(dist);
    }

    if (item.id == "cotation_form_denivele") {
      var deniv = document.getElementById("cotation_form_denivele").value;
      radioDenivCheck(deniv);
    }
  });
}); // fonds de carte


var map = L.map('cotation-map', {
  center: [47, 2],
  zoom: 5,
  minZoom: 3,
  zoomControl: true,
  layers: [Object(esri_leaflet__WEBPACK_IMPORTED_MODULE_1__["basemapLayer"])('Topographic')]
});
L.control.scale().addTo(map); //ouverture et affichage du gpx

var nbfiles = 0;

function loadGpxFile(g) {
  var files = g.target.files,
      reader = new FileReader();
  nbfiles += 1;

  if (nbfiles > 1) {
    map.remove();
    map = L.map('cotation-map', {
      center: [47, 2],
      zoom: 5,
      minZoom: 3,
      zoomControl: true,
      layers: [Object(esri_leaflet__WEBPACK_IMPORTED_MODULE_1__["basemapLayer"])('Topographic')]
    });
  }

  reader.onload = function (e) {
    var gpxString = reader.result;
    new leaflet_gpx__WEBPACK_IMPORTED_MODULE_2__["GPX"](gpxString, {
      async: true,
      marker_options: {
        startIconUrl: '../build/images/icon/pin-icon-start.png',
        endIconUrl: '../build/images/icon/pin-icon-end.png',
        shadowUrl: '',
        wptIconUrls: ''
      }
    }).on('loaded', function (e) {
      map.fitBounds(e.target.getBounds());
      var nom = e.target.get_name();
      var dist = Math.round(e.target.get_distance() / 1000);
      var temps = Math.round(dist / 15 * 60);
      var deniv = Math.round(e.target.get_elevation_gain());
      _cotation__WEBPACK_IMPORTED_MODULE_3__["addValueToInput"](nom, "#cotation_form_nom_circuit");
      _cotation__WEBPACK_IMPORTED_MODULE_3__["addValueToInput"](dist, "#cotation_form_distance");
      _cotation__WEBPACK_IMPORTED_MODULE_3__["addValueToInput"](_cotation__WEBPACK_IMPORTED_MODULE_3__["convertMinsToHrsMins"](temps), "#cotation_form_temps");
      _cotation__WEBPACK_IMPORTED_MODULE_3__["addValueToInput"](deniv, "#cotation_form_denivele");
      radioDistCheck(dist);
      radioDenivCheck(deniv);
      /*radioTypeVoieCheck(dist);
      radioPenteCheck(dist);*/
    }).addTo(map);
    var gpxDoc = new DOMParser().parseFromString(gpxString, 'text/xml');
    var geoJson = [];
    geoJson.push(toGeoJSON.gpx(gpxDoc));
    geoJson[0]['properties'] = {
      "Creator": "FFVélo",
      "summary": ""
    };
    /*var hg = L.control.heightgraph({
        width: 800,
        height: 280,
        margins: {
            top: 10,
            right: 30,
            bottom: 55,
            left: 50
        },
        position: "bottomright",
        mappings:  undefined
    });
    hg.addTo(map);
    hg.addData(geoJson);*/
    //Geojson pour fleches directionnelles sur le tracé du circuit

    var arrowLayer = L.geoJson(geoJson, {
      onEachFeature: function onEachFeature(feature, layer) {
        var coords = layer.getLatLngs();
        L.polylineDecorator(coords, {
          patterns: [{
            offset: 25,
            repeat: 100,
            symbol: L.Symbol.arrowHead({
              pixelSize: 15,
              pathOptions: {
                fillOpacity: 1,
                weight: 0,
                color: '#0000FF'
              }
            })
          }]
        }).addTo(map);
      }
    }).addTo(map);
  };

  reader.readAsText(files[0]);
}

opengpx.addEventListener("change", loadGpxFile, false);

/***/ })

},[["./assets/js/pages/cotation/cotationVttae.js","runtime","vendors~app~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_velor~10347b53","vendors~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~61364f2f","vendors~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~cotatio~d7e263c1","vendors~cotation_route~cotation_veloroute~cotation_vtt~cotation_vttae"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvcGFnZXMvY290YXRpb24vY290YXRpb24uanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL3BhZ2VzL2NvdGF0aW9uL2NvdGF0aW9uVnR0YWUuanMiXSwibmFtZXMiOlsiY29udmVydE1pbnNUb0hyc01pbnMiLCJtaW51dGVzIiwiaCIsIk1hdGgiLCJmbG9vciIsIm0iLCJhZGRWYWx1ZVRvSW5wdXQiLCJpbnB1dFZhbCIsImlkIiwiJCIsInZhbCIsImNoYW5nZSIsIm5hbWVzIiwiZWFjaCIsImF0dHIiLCJjb3VudCIsImxlbmd0aCIsInRvdGFsIiwicGFyc2VGbG9hdCIsIm91dHB1dERpZmYiLCJkaWZmIiwiY29sb3IiLCJjc3MiLCJ0ZXh0SW5pdCIsImh0bWwiLCJrZXl1cCIsInRleHRfbGVuZ3RoIiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50QnlJZCIsInN0eWxlIiwib24iLCJyZXBsYWNlIiwidG9HZW9KU09OIiwicmVxdWlyZSIsInJhZGlvRGlzdENoZWNrIiwiZGlzdCIsImNoZWNrZWQiLCJyYWRpb0Rlbml2Q2hlY2siLCJkZW5pdiIsImlucHV0RWxlbWVudCIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJtYXAiLCJpdGVtIiwiYWRkRXZlbnRMaXN0ZW5lciIsImUiLCJ2YWx1ZSIsIkwiLCJjZW50ZXIiLCJ6b29tIiwibWluWm9vbSIsInpvb21Db250cm9sIiwibGF5ZXJzIiwiYmFzZW1hcExheWVyIiwiY29udHJvbCIsInNjYWxlIiwiYWRkVG8iLCJuYmZpbGVzIiwibG9hZEdweEZpbGUiLCJnIiwiZmlsZXMiLCJ0YXJnZXQiLCJyZWFkZXIiLCJGaWxlUmVhZGVyIiwicmVtb3ZlIiwib25sb2FkIiwiZ3B4U3RyaW5nIiwicmVzdWx0IiwiR1BYIiwiYXN5bmMiLCJtYXJrZXJfb3B0aW9ucyIsInN0YXJ0SWNvblVybCIsImVuZEljb25VcmwiLCJzaGFkb3dVcmwiLCJ3cHRJY29uVXJscyIsImZpdEJvdW5kcyIsImdldEJvdW5kcyIsIm5vbSIsImdldF9uYW1lIiwicm91bmQiLCJnZXRfZGlzdGFuY2UiLCJ0ZW1wcyIsImdldF9lbGV2YXRpb25fZ2FpbiIsIkNvdGF0aW9uIiwiZ3B4RG9jIiwiRE9NUGFyc2VyIiwicGFyc2VGcm9tU3RyaW5nIiwiZ2VvSnNvbiIsInB1c2giLCJncHgiLCJhcnJvd0xheWVyIiwib25FYWNoRmVhdHVyZSIsImZlYXR1cmUiLCJsYXllciIsImNvb3JkcyIsImdldExhdExuZ3MiLCJwb2x5bGluZURlY29yYXRvciIsInBhdHRlcm5zIiwib2Zmc2V0IiwicmVwZWF0Iiwic3ltYm9sIiwiU3ltYm9sIiwiYXJyb3dIZWFkIiwicGl4ZWxTaXplIiwicGF0aE9wdGlvbnMiLCJmaWxsT3BhY2l0eSIsIndlaWdodCIsInJlYWRBc1RleHQiLCJvcGVuZ3B4Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDTyxJQUFNQSxvQkFBb0IsR0FBRyxTQUF2QkEsb0JBQXVCLENBQUNDLE9BQUQsRUFBYTtBQUM3QyxNQUFJQyxDQUFDLEdBQUdDLElBQUksQ0FBQ0MsS0FBTCxDQUFXSCxPQUFPLEdBQUcsRUFBckIsQ0FBUjtBQUNBLE1BQUlJLENBQUMsR0FBR0osT0FBTyxHQUFHLEVBQWxCO0FBQ0FDLEdBQUMsR0FBR0EsQ0FBQyxHQUFHLEVBQUosR0FBUyxNQUFNQSxDQUFmLEdBQW1CQSxDQUF2QjtBQUNBRyxHQUFDLEdBQUdBLENBQUMsR0FBRyxFQUFKLEdBQVMsTUFBTUEsQ0FBZixHQUFtQkEsQ0FBdkI7QUFDQSxTQUFPSCxDQUFDLEdBQUcsR0FBSixHQUFVRyxDQUFqQjtBQUNILENBTk0sQyxDQVFQOztBQUNPLFNBQVNDLGVBQVQsQ0FBeUJDLFFBQXpCLEVBQW1DQyxFQUFuQyxFQUFzQztBQUN6QztBQUNBQyxHQUFDLENBQUNELEVBQUQsQ0FBRCxDQUFNRSxHQUFOLENBQVVILFFBQVY7QUFDSCxDLENBRUQ7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7QUFDQUUsQ0FBQyxDQUFDLFFBQUQsQ0FBRCxDQUFZRSxNQUFaLENBQW1CLFlBQVk7QUFDM0IsTUFBSUMsS0FBSyxHQUFHLEVBQVo7QUFDQUgsR0FBQyxDQUFDLFFBQUQsQ0FBRCxDQUFZSSxJQUFaLENBQWlCLFlBQVk7QUFDekJELFNBQUssQ0FBQ0gsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRSyxJQUFSLENBQWEsTUFBYixDQUFELENBQUwsR0FBOEIsSUFBOUI7QUFDSCxHQUZEO0FBR0EsTUFBSUMsS0FBSyxHQUFHLENBQVo7QUFDQU4sR0FBQyxDQUFDSSxJQUFGLENBQU9ELEtBQVAsRUFBYyxZQUFZO0FBQ3RCRyxTQUFLO0FBQ1IsR0FGRDs7QUFHQSxNQUFJTixDQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQk8sTUFBcEIsS0FBK0JELEtBQW5DLEVBQTBDO0FBQ3RDLFFBQUlFLEtBQUssR0FBRyxDQUFaO0FBQ0FSLEtBQUMsQ0FBQywyQkFBRCxDQUFELENBQStCSSxJQUEvQixDQUFvQyxZQUFZO0FBQzVDSSxXQUFLLElBQUlDLFVBQVUsQ0FBQ1QsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRQyxHQUFSLEVBQUQsQ0FBbkI7QUFDSCxLQUZEO0FBR0FTLGNBQVUsQ0FBQ0YsS0FBRCxDQUFWO0FBQ0g7QUFDSixDQWhCRCxFLENBbUJBOztBQUNBLFNBQVNFLFVBQVQsQ0FBb0JGLEtBQXBCLEVBQTBCO0FBQ3RCLE1BQUlHLElBQUosRUFBVUMsS0FBVjs7QUFDQSxNQUFJSixLQUFLLElBQUksQ0FBVCxJQUFjQSxLQUFLLElBQUksQ0FBM0IsRUFBOEI7QUFDMUJHLFFBQUksR0FBR0gsS0FBSyxHQUFHLGdCQUFmO0FBQ0FJLFNBQUssR0FBRyxPQUFSO0FBQ0gsR0FIRCxNQUdPLElBQUlKLEtBQUssSUFBSSxDQUFULElBQWNBLEtBQUssSUFBSSxDQUEzQixFQUE4QjtBQUNqQ0csUUFBSSxHQUFHSCxLQUFLLEdBQUcsV0FBZjtBQUNBSSxTQUFLLEdBQUcsTUFBUjtBQUNILEdBSE0sTUFHQSxJQUFJSixLQUFLLElBQUksQ0FBVCxJQUFjQSxLQUFLLElBQUksRUFBM0IsRUFBK0I7QUFDbENHLFFBQUksR0FBR0gsS0FBSyxHQUFHLGNBQWY7QUFDQUksU0FBSyxHQUFHLEtBQVI7QUFDSCxHQUhNLE1BR0E7QUFDSEQsUUFBSSxHQUFHSCxLQUFLLEdBQUcsbUJBQWY7QUFDQUksU0FBSyxHQUFHLE9BQVI7QUFDSDs7QUFDRFosR0FBQyxDQUFDLDJCQUFELENBQUQsQ0FBK0JDLEdBQS9CLENBQW1DVSxJQUFuQztBQUNBWCxHQUFDLENBQUMsMkJBQUQsQ0FBRCxDQUErQmEsR0FBL0IsQ0FBbUM7QUFBQyxhQUFTRCxLQUFWO0FBQWlCLGlCQUFhO0FBQTlCLEdBQW5DO0FBQ0gsQyxDQUVEOzs7QUFDQSxJQUFJRSxRQUFRLEdBQUcsQ0FBZjtBQUNBZCxDQUFDLENBQUMsYUFBRCxDQUFELENBQWlCZSxJQUFqQixDQUFzQkQsUUFBUSxHQUFHLGlCQUFqQztBQUNBZCxDQUFDLENBQUMsMEJBQUQsQ0FBRCxDQUE4QmdCLEtBQTlCLENBQW9DLFlBQVk7QUFDNUMsTUFBSUMsV0FBVyxHQUFHakIsQ0FBQyxDQUFDLDBCQUFELENBQUQsQ0FBOEJDLEdBQTlCLEdBQW9DTSxNQUF0RDtBQUNBUCxHQUFDLENBQUMsYUFBRCxDQUFELENBQWlCZSxJQUFqQixDQUFzQkUsV0FBVyxHQUFHLGtCQUFwQzs7QUFDQSxNQUFJQSxXQUFXLElBQUksQ0FBbkIsRUFBc0I7QUFDbEJqQixLQUFDLENBQUMsYUFBRCxDQUFELENBQWlCZSxJQUFqQixDQUFzQkUsV0FBVyxHQUFHLGlCQUFwQztBQUNIOztBQUNELE1BQUlBLFdBQVcsR0FBRyxHQUFsQixFQUF1QjtBQUNuQkMsWUFBUSxDQUFDQyxjQUFULENBQXdCLFlBQXhCLEVBQXNDQyxLQUF0QyxDQUE0Q1IsS0FBNUMsR0FBb0QsS0FBcEQ7QUFDSCxHQUZELE1BRU87QUFDSE0sWUFBUSxDQUFDQyxjQUFULENBQXdCLFlBQXhCLEVBQXNDQyxLQUF0QyxDQUE0Q1IsS0FBNUMsR0FBb0QsT0FBcEQ7QUFDSDtBQUNKLENBWEQsRSxDQVlBOztBQUNBWixDQUFDLENBQUMsMEJBQUQsQ0FBRCxDQUE4QnFCLEVBQTlCLENBQWlDLE9BQWpDLEVBQTBDLFlBQVk7QUFDbERyQixHQUFDLENBQUMsSUFBRCxDQUFELENBQVFDLEdBQVIsQ0FBWUQsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRQyxHQUFSLEdBQWNxQixPQUFkLENBQXNCLFlBQXRCLEVBQW9DLEVBQXBDLENBQVo7QUFDSCxDQUZELEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoRkE7QUFDQTtBQUNBOztBQUNBLElBQU1DLFNBQVMsR0FBR0MsbUJBQU8sQ0FBQyw2RUFBRCxDQUF6Qjs7QUFDQUEsbUJBQU8sQ0FBQyxpR0FBRCxDQUFQOztBQUNBQSxtQkFBTyxDQUFDLDZHQUFELENBQVAsQyxDQUVBOzs7QUFDQSxTQUFTQyxjQUFULENBQXdCQyxJQUF4QixFQUE2QjtBQUN6QixNQUFLQSxJQUFJLElBQUksQ0FBVCxJQUFnQkEsSUFBSSxJQUFJLEVBQTVCLEVBQWlDO0FBQUNSLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3Qiw0QkFBeEIsRUFBc0RRLE9BQXRELEdBQWdFLElBQWhFO0FBQXFFOztBQUN2RyxNQUFLRCxJQUFJLElBQUksRUFBVCxJQUFpQkEsSUFBSSxJQUFJLEVBQTdCLEVBQWtDO0FBQUNSLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3Qiw0QkFBeEIsRUFBc0RRLE9BQXRELEdBQWdFLElBQWhFO0FBQXFFOztBQUN4RyxNQUFLRCxJQUFJLElBQUksRUFBVCxJQUFpQkEsSUFBSSxJQUFJLEVBQTdCLEVBQWtDO0FBQUNSLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3Qiw0QkFBeEIsRUFBc0RRLE9BQXRELEdBQWdFLElBQWhFO0FBQXFFOztBQUN4RyxNQUFJRCxJQUFJLElBQUksRUFBWixFQUFlO0FBQUNSLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3Qiw0QkFBeEIsRUFBc0RRLE9BQXRELEdBQWdFLElBQWhFO0FBQXFFO0FBQ3hGLEMsQ0FDRDs7O0FBQ0EsU0FBU0MsZUFBVCxDQUF5QkMsS0FBekIsRUFBK0I7QUFDM0IsTUFBS0EsS0FBSyxJQUFJLENBQVYsSUFBaUJBLEtBQUssSUFBSSxHQUE5QixFQUFvQztBQUFDWCxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsNkJBQXhCLEVBQXVEUSxPQUF2RCxHQUFpRSxJQUFqRTtBQUFzRTs7QUFDM0csTUFBS0UsS0FBSyxJQUFJLEdBQVYsSUFBbUJBLEtBQUssSUFBSSxHQUFoQyxFQUFzQztBQUFDWCxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsNkJBQXhCLEVBQXVEUSxPQUF2RCxHQUFpRSxJQUFqRTtBQUFzRTs7QUFDN0csTUFBS0UsS0FBSyxJQUFJLEdBQVYsSUFBbUJBLEtBQUssSUFBSSxJQUFoQyxFQUF1QztBQUFDWCxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsNkJBQXhCLEVBQXVEUSxPQUF2RCxHQUFpRSxJQUFqRTtBQUFzRTs7QUFDOUcsTUFBSUUsS0FBSyxJQUFJLElBQWIsRUFBa0I7QUFBQ1gsWUFBUSxDQUFDQyxjQUFULENBQXdCLDZCQUF4QixFQUF1RFEsT0FBdkQsR0FBaUUsSUFBakU7QUFBc0U7QUFDNUY7O0FBRUQsSUFBTUcsWUFBWSxHQUFHWixRQUFRLENBQUNhLGdCQUFULENBQTBCLGtEQUExQixDQUFyQjs7QUFDQSxtQkFBSUQsWUFBSixFQUFrQkUsR0FBbEIsQ0FBc0IsVUFBQUMsSUFBSSxFQUFJO0FBQzFCQSxNQUFJLENBQUNDLGdCQUFMLENBQXNCLFFBQXRCLEVBQWdDLFVBQUNDLENBQUQsRUFBTztBQUNuQyxRQUFHRixJQUFJLENBQUNsQyxFQUFMLElBQVUsd0JBQWIsRUFBc0M7QUFDbEMsVUFBSTJCLElBQUksR0FBR1IsUUFBUSxDQUFDQyxjQUFULENBQXdCLHdCQUF4QixFQUFrRGlCLEtBQTdEO0FBQ0FYLG9CQUFjLENBQUNDLElBQUQsQ0FBZDtBQUNIOztBQUNELFFBQUdPLElBQUksQ0FBQ2xDLEVBQUwsSUFBVSx3QkFBYixFQUFzQztBQUNsQyxVQUFJOEIsS0FBSyxHQUFHWCxRQUFRLENBQUNDLGNBQVQsQ0FBd0Isd0JBQXhCLEVBQWtEaUIsS0FBOUQ7QUFDQVIscUJBQWUsQ0FBQ0MsS0FBRCxDQUFmO0FBQ0g7QUFDSixHQVREO0FBVUgsQ0FYRCxFLENBYUE7OztBQUNBLElBQUlHLEdBQUcsR0FBR0ssQ0FBQyxDQUFDTCxHQUFGLENBQU0sY0FBTixFQUFzQjtBQUM1Qk0sUUFBTSxFQUFFLENBQUMsRUFBRCxFQUFLLENBQUwsQ0FEb0I7QUFFNUJDLE1BQUksRUFBRSxDQUZzQjtBQUc1QkMsU0FBTyxFQUFFLENBSG1CO0FBSTVCQyxhQUFXLEVBQUUsSUFKZTtBQUs1QkMsUUFBTSxFQUFFLENBQUNDLGlFQUFZLENBQUMsYUFBRCxDQUFiO0FBTG9CLENBQXRCLENBQVY7QUFPQU4sQ0FBQyxDQUFDTyxPQUFGLENBQVVDLEtBQVYsR0FBa0JDLEtBQWxCLENBQXdCZCxHQUF4QixFLENBRUE7O0FBQ0EsSUFBSWUsT0FBTyxHQUFHLENBQWQ7O0FBQ0EsU0FBU0MsV0FBVCxDQUFxQkMsQ0FBckIsRUFBd0I7QUFDcEIsTUFBSUMsS0FBSyxHQUFHRCxDQUFDLENBQUNFLE1BQUYsQ0FBU0QsS0FBckI7QUFBQSxNQUNBRSxNQUFNLEdBQUcsSUFBSUMsVUFBSixFQURUO0FBRUFOLFNBQU8sSUFBRSxDQUFUOztBQUNBLE1BQUlBLE9BQU8sR0FBQyxDQUFaLEVBQWM7QUFDVmYsT0FBRyxDQUFDc0IsTUFBSjtBQUNBdEIsT0FBRyxHQUFHSyxDQUFDLENBQUNMLEdBQUYsQ0FBTSxjQUFOLEVBQXNCO0FBQ3hCTSxZQUFNLEVBQUUsQ0FBQyxFQUFELEVBQUssQ0FBTCxDQURnQjtBQUV4QkMsVUFBSSxFQUFFLENBRmtCO0FBR3hCQyxhQUFPLEVBQUUsQ0FIZTtBQUl4QkMsaUJBQVcsRUFBRSxJQUpXO0FBS3hCQyxZQUFNLEVBQUUsQ0FBQ0MsaUVBQVksQ0FBQyxhQUFELENBQWI7QUFMZ0IsS0FBdEIsQ0FBTjtBQU9IOztBQUNEUyxRQUFNLENBQUNHLE1BQVAsR0FBZ0IsVUFBU3BCLENBQVQsRUFBWTtBQUN4QixRQUFJcUIsU0FBUyxHQUFHSixNQUFNLENBQUNLLE1BQXZCO0FBQ0EsUUFBSUMsK0NBQUosQ0FBUUYsU0FBUixFQUFtQjtBQUFDRyxXQUFLLEVBQUUsSUFBUjtBQUNmQyxvQkFBYyxFQUFFO0FBQ1pDLG9CQUFZLEVBQUUseUNBREY7QUFFWkMsa0JBQVUsRUFBRSx1Q0FGQTtBQUdaQyxpQkFBUyxFQUFFLEVBSEM7QUFJWkMsbUJBQVcsRUFBRTtBQUpEO0FBREQsS0FBbkIsRUFPRzNDLEVBUEgsQ0FPTSxRQVBOLEVBT2dCLFVBQVNjLENBQVQsRUFBWTtBQUN4QkgsU0FBRyxDQUFDaUMsU0FBSixDQUFjOUIsQ0FBQyxDQUFDZ0IsTUFBRixDQUFTZSxTQUFULEVBQWQ7QUFDQSxVQUFJQyxHQUFHLEdBQUNoQyxDQUFDLENBQUNnQixNQUFGLENBQVNpQixRQUFULEVBQVI7QUFDQSxVQUFJMUMsSUFBSSxHQUFDaEMsSUFBSSxDQUFDMkUsS0FBTCxDQUFXbEMsQ0FBQyxDQUFDZ0IsTUFBRixDQUFTbUIsWUFBVCxLQUF3QixJQUFuQyxDQUFUO0FBQ0EsVUFBSUMsS0FBSyxHQUFDN0UsSUFBSSxDQUFDMkUsS0FBTCxDQUFZM0MsSUFBSSxHQUFDLEVBQU4sR0FBVSxFQUFyQixDQUFWO0FBQ0EsVUFBSUcsS0FBSyxHQUFHbkMsSUFBSSxDQUFDMkUsS0FBTCxDQUFXbEMsQ0FBQyxDQUFDZ0IsTUFBRixDQUFTcUIsa0JBQVQsRUFBWCxDQUFaO0FBQ0FDLCtEQUFBLENBQXlCTixHQUF6QixFQUE4Qiw0QkFBOUI7QUFDQU0sK0RBQUEsQ0FBeUIvQyxJQUF6QixFQUErQix5QkFBL0I7QUFDQStDLCtEQUFBLENBQXlCQSw4REFBQSxDQUE4QkYsS0FBOUIsQ0FBekIsRUFBK0Qsc0JBQS9EO0FBQ0FFLCtEQUFBLENBQXlCNUMsS0FBekIsRUFBZ0MseUJBQWhDO0FBQ0FKLG9CQUFjLENBQUNDLElBQUQsQ0FBZDtBQUNBRSxxQkFBZSxDQUFDQyxLQUFELENBQWY7QUFDQTtBQUNaO0FBQ1MsS0FyQkQsRUFxQkdpQixLQXJCSCxDQXFCU2QsR0FyQlQ7QUF1QkEsUUFBSTBDLE1BQU0sR0FBRyxJQUFJQyxTQUFKLEdBQWdCQyxlQUFoQixDQUFnQ3BCLFNBQWhDLEVBQTJDLFVBQTNDLENBQWI7QUFDQSxRQUFJcUIsT0FBTyxHQUFDLEVBQVo7QUFDQUEsV0FBTyxDQUFDQyxJQUFSLENBQWF2RCxTQUFTLENBQUN3RCxHQUFWLENBQWNMLE1BQWQsQ0FBYjtBQUNBRyxXQUFPLENBQUMsQ0FBRCxDQUFQLENBQVcsWUFBWCxJQUNBO0FBQ0ksaUJBQVcsUUFEZjtBQUVJLGlCQUFXO0FBRmYsS0FEQTtBQUtBO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDUTs7QUFDQSxRQUFJRyxVQUFVLEdBQUczQyxDQUFDLENBQUN3QyxPQUFGLENBQVVBLE9BQVYsRUFBbUI7QUFDaENJLG1CQUFhLEVBQUUsdUJBQVVDLE9BQVYsRUFBbUJDLEtBQW5CLEVBQTBCO0FBQ3JDLFlBQUlDLE1BQU0sR0FBR0QsS0FBSyxDQUFDRSxVQUFOLEVBQWI7QUFDQWhELFNBQUMsQ0FBQ2lELGlCQUFGLENBQW9CRixNQUFwQixFQUE0QjtBQUN4Qkcsa0JBQVEsRUFBRSxDQUFDO0FBQ1BDLGtCQUFNLEVBQUUsRUFERDtBQUVQQyxrQkFBTSxFQUFFLEdBRkQ7QUFHUEMsa0JBQU0sRUFBRXJELENBQUMsQ0FBQ3NELE1BQUYsQ0FBU0MsU0FBVCxDQUFtQjtBQUN2QkMsdUJBQVMsRUFBRSxFQURZO0FBRXZCQyx5QkFBVyxFQUFFO0FBQ1RDLDJCQUFXLEVBQUUsQ0FESjtBQUVUQyxzQkFBTSxFQUFFLENBRkM7QUFHVHBGLHFCQUFLLEVBQUM7QUFIRztBQUZVLGFBQW5CO0FBSEQsV0FBRDtBQURjLFNBQTVCLEVBYUdrQyxLQWJILENBYVNkLEdBYlQ7QUFjSDtBQWpCK0IsS0FBbkIsRUFrQmRjLEtBbEJjLENBa0JSZCxHQWxCUSxDQUFqQjtBQW1CSCxHQW5FRDs7QUFvRUFvQixRQUFNLENBQUM2QyxVQUFQLENBQWtCL0MsS0FBSyxDQUFDLENBQUQsQ0FBdkI7QUFDSDs7QUFDRGdELE9BQU8sQ0FBQ2hFLGdCQUFSLENBQXlCLFFBQXpCLEVBQWtDYyxXQUFsQyxFQUErQyxLQUEvQyxFIiwiZmlsZSI6ImNvdGF0aW9uX3Z0dGFlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy9jb252ZXJzaW9uIG1pbnV0ZXMgZW4gaGV1cmVzXHJcbmV4cG9ydCBjb25zdCBjb252ZXJ0TWluc1RvSHJzTWlucyA9IChtaW51dGVzKSA9PiB7XHJcbiAgICB2YXIgaCA9IE1hdGguZmxvb3IobWludXRlcyAvIDYwKTtcclxuICAgIHZhciBtID0gbWludXRlcyAlIDYwO1xyXG4gICAgaCA9IGggPCAxMCA/ICcwJyArIGggOiBoO1xyXG4gICAgbSA9IG0gPCAxMCA/ICcwJyArIG0gOiBtO1xyXG4gICAgcmV0dXJuIGggKyAnOicgKyBtO1xyXG59XHJcblxyXG4vL3JlbXBsaXNzYWdlIGlucHV0c1xyXG5leHBvcnQgZnVuY3Rpb24gYWRkVmFsdWVUb0lucHV0KGlucHV0VmFsLCBpZCl7XHJcbiAgICAvL2RvY3VtZW50LmdldEVsZW1lbnRCeUlkKGlkKS52YWx1ZSA9IGlucHV0VmFsO1xyXG4gICAgJChpZCkudmFsKGlucHV0VmFsKTtcclxufVxyXG5cclxuLy9yZWN1cCBjb29yZG9ubmVlcyBwb2ludHMgZGVwYXJ0c1xyXG4vKmNvbnN0IGdldENvb3Jkc0RlcGFydCA9ICh4bWwpID0+IHtcclxuICAgIHBhcnNlciA9IG5ldyBET01QYXJzZXIoKTtcclxuICAgIHhtbERvYyA9IHBhcnNlci5wYXJzZUZyb21TdHJpbmcoeG1sLCBcInRleHQveG1sXCIpO1xyXG4gICAgdmFyIGxhdCA9IHhtbERvYy5nZXRFbGVtZW50c0J5VGFnTmFtZShcInRya3B0XCIpWzBdLmdldEF0dHJpYnV0ZSgnbGF0Jyk7XHJcbiAgICB2YXIgbG9uID0geG1sRG9jLmdldEVsZW1lbnRzQnlUYWdOYW1lKFwidHJrcHRcIilbMF0uZ2V0QXR0cmlidXRlKCdsb24nKTtcclxuICAgIHJldHVybiBbbG9uLCBsYXRdO1xyXG59Ki9cclxuXHJcbi8vY2FsY3VsIGRlIGxhIGRpZmZpY3VsdGUgYXZlYyByYWRpb3MgYnV0dG9uc1xyXG4kKFwiOnJhZGlvXCIpLmNoYW5nZShmdW5jdGlvbiAoKSB7XHJcbiAgICBsZXQgbmFtZXMgPSB7fTtcclxuICAgICQoJzpyYWRpbycpLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIG5hbWVzWyQodGhpcykuYXR0cignbmFtZScpXSA9IHRydWU7XHJcbiAgICB9KTtcclxuICAgIGxldCBjb3VudCA9IDA7XHJcbiAgICAkLmVhY2gobmFtZXMsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBjb3VudCsrO1xyXG4gICAgfSk7XHJcbiAgICBpZiAoJCgnOnJhZGlvOmNoZWNrZWQnKS5sZW5ndGggPT09IGNvdW50KSB7XHJcbiAgICAgICAgbGV0IHRvdGFsID0gMDtcclxuICAgICAgICAkKFwiaW5wdXRbdHlwZT1yYWRpb106Y2hlY2tlZFwiKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdG90YWwgKz0gcGFyc2VGbG9hdCgkKHRoaXMpLnZhbCgpKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICBvdXRwdXREaWZmKHRvdGFsKTtcclxuICAgIH1cclxufSk7XHJcblxyXG5cclxuLy9pbnB1dCBkaWZmaWN1bHRlXHJcbmZ1bmN0aW9uIG91dHB1dERpZmYodG90YWwpe1xyXG4gICAgdmFyIGRpZmYsIGNvbG9yO1xyXG4gICAgaWYgKHRvdGFsID49IDQgJiYgdG90YWwgPD0gNSkge1xyXG4gICAgICAgIGRpZmYgPSB0b3RhbCArIFwiIC0gVHLDqHMgZmFjaWxlXCI7XHJcbiAgICAgICAgY29sb3IgPSBcImdyZWVuXCI7XHJcbiAgICB9IGVsc2UgaWYgKHRvdGFsID49IDYgJiYgdG90YWwgPD0gOCkge1xyXG4gICAgICAgIGRpZmYgPSB0b3RhbCArIFwiIC0gRmFjaWxlXCI7XHJcbiAgICAgICAgY29sb3IgPSBcImJsdWVcIjtcclxuICAgIH0gZWxzZSBpZiAodG90YWwgPj0gOSAmJiB0b3RhbCA8PSAxMikge1xyXG4gICAgICAgIGRpZmYgPSB0b3RhbCArIFwiIC0gRGlmZmljaWxlXCI7XHJcbiAgICAgICAgY29sb3IgPSBcInJlZFwiO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBkaWZmID0gdG90YWwgKyBcIiAtIFRyw6hzIERpZmZpY2lsZVwiO1xyXG4gICAgICAgIGNvbG9yID0gXCJibGFja1wiO1xyXG4gICAgfVxyXG4gICAgJChcIiNjb3RhdGlvbl9mb3JtX2RpZmZpY3VsdGVcIikudmFsKGRpZmYpO1xyXG4gICAgJChcIiNjb3RhdGlvbl9mb3JtX2RpZmZpY3VsdGVcIikuY3NzKHtcImNvbG9yXCI6IGNvbG9yLCBcImZvbnQtc2l6ZVwiOiBcIjI1cHhcIn0pO1xyXG59XHJcblxyXG4vL2NvbXB0ZSBsZSBub21icmUgZGUgY2FyYWN0ZXJlcyBkYW5zIGxlIHRleHRhcmVhXHJcbnZhciB0ZXh0SW5pdCA9IDA7XHJcbiQoJyNjb3VudF90ZXh0JykuaHRtbCh0ZXh0SW5pdCArICcgY2FyYWN0w6hyZSAvNTAwJyk7XHJcbiQoJyNjb3RhdGlvbl9mb3JtX3RleHREZXNjcicpLmtleXVwKGZ1bmN0aW9uICgpIHtcclxuICAgIGxldCB0ZXh0X2xlbmd0aCA9ICQoJyNjb3RhdGlvbl9mb3JtX3RleHREZXNjcicpLnZhbCgpLmxlbmd0aDtcclxuICAgICQoJyNjb3VudF90ZXh0JykuaHRtbCh0ZXh0X2xlbmd0aCArICcgY2FyYWN0w6hyZXMgLzUwMCcpO1xyXG4gICAgaWYgKHRleHRfbGVuZ3RoIDw9IDEpIHtcclxuICAgICAgICAkKCcjY291bnRfdGV4dCcpLmh0bWwodGV4dF9sZW5ndGggKyAnIGNhcmFjdMOocmUgLzUwMCcpO1xyXG4gICAgfVxyXG4gICAgaWYgKHRleHRfbGVuZ3RoID4gNTAwKSB7XHJcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjb3VudF90ZXh0XCIpLnN0eWxlLmNvbG9yID0gJ3JlZCc7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY291bnRfdGV4dFwiKS5zdHlsZS5jb2xvciA9ICdibGFjayc7XHJcbiAgICB9XHJcbn0pO1xyXG4vL2VtcGVjaGVyIGxlcyByZXRvdXJzIGNoYXJyaW90cyBkdSB0ZXh0YXJlYVxyXG4kKCcjY290YXRpb25fZm9ybV90ZXh0RGVzY3InKS5vbigna2V5dXAnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAkKHRoaXMpLnZhbCgkKHRoaXMpLnZhbCgpLnJlcGxhY2UoL1tcXHJcXG5cXHZdKy9nLCAnJykpO1xyXG59KTtcclxuIiwiaW1wb3J0IHsgYmFzZW1hcExheWVyIH0gZnJvbSAnZXNyaS1sZWFmbGV0JztcclxuaW1wb3J0IHsgR1BYIH0gZnJvbSAnbGVhZmxldC1ncHgnO1xyXG5pbXBvcnQgKiBhcyBDb3RhdGlvbiBmcm9tICcuL2NvdGF0aW9uJztcclxuY29uc3QgdG9HZW9KU09OID0gcmVxdWlyZShcIkB0bWN3L3RvZ2VvanNvblwiKTtcclxucmVxdWlyZSgnbGVhZmxldC5oZWlnaHRncmFwaCcpO1xyXG5yZXF1aXJlKCdsZWFmbGV0LXBvbHlsaW5lZGVjb3JhdG9yJyk7XHJcblxyXG4vL2NvY2hlIGJvdXRvbiByYWRpbyBkaXN0YW5jZSBhdSBjaGFyZ2VtZW50IGR1IGdweFxyXG5mdW5jdGlvbiByYWRpb0Rpc3RDaGVjayhkaXN0KXtcclxuICAgIGlmICgoZGlzdCA+PSAwKSAmJiAoZGlzdCA8PSAzMCkpIHtkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdGF0aW9uX2Zvcm1fZGlzdENob2ljZV8wXCIpLmNoZWNrZWQgPSB0cnVlfVxyXG4gICAgaWYgKChkaXN0ID49IDMxKSAmJiAoZGlzdCA8PSA1MCkpIHtkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdGF0aW9uX2Zvcm1fZGlzdENob2ljZV8xXCIpLmNoZWNrZWQgPSB0cnVlfVxyXG4gICAgaWYgKChkaXN0ID49IDUxKSAmJiAoZGlzdCA8PSA3MCkpIHtkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdGF0aW9uX2Zvcm1fZGlzdENob2ljZV8yXCIpLmNoZWNrZWQgPSB0cnVlfVxyXG4gICAgaWYgKGRpc3QgPj0gNzEpe2RvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY290YXRpb25fZm9ybV9kaXN0Q2hvaWNlXzNcIikuY2hlY2tlZCA9IHRydWV9XHJcbn1cclxuLy9jb2NoZSBib3V0b24gcmFkaW8gZGVuaXZlbGUgYXUgY2hhcmdlbWVudCBkdSBncHhcclxuZnVuY3Rpb24gcmFkaW9EZW5pdkNoZWNrKGRlbml2KXtcclxuICAgIGlmICgoZGVuaXYgPj0gMCkgJiYgKGRlbml2IDw9IDEwMCkpIHtkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdGF0aW9uX2Zvcm1fZGVuaXZDaG9pY2VfMFwiKS5jaGVja2VkID0gdHJ1ZX1cclxuICAgIGlmICgoZGVuaXYgPj0gMTAxKSAmJiAoZGVuaXYgPD0gMzAwKSkge2RvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY290YXRpb25fZm9ybV9kZW5pdkNob2ljZV8xXCIpLmNoZWNrZWQgPSB0cnVlfVxyXG4gICAgaWYgKChkZW5pdiA+PSAzMDEpICYmIChkZW5pdiA8PSAxMDAwKSkge2RvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY290YXRpb25fZm9ybV9kZW5pdkNob2ljZV8yXCIpLmNoZWNrZWQgPSB0cnVlfVxyXG4gICAgaWYgKGRlbml2ID49IDEwMDEpe2RvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY290YXRpb25fZm9ybV9kZW5pdkNob2ljZV8zXCIpLmNoZWNrZWQgPSB0cnVlfVxyXG59XHJcblxyXG5jb25zdCBpbnB1dEVsZW1lbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcjY290YXRpb25fZm9ybV9kaXN0YW5jZSwgI2NvdGF0aW9uX2Zvcm1fZGVuaXZlbGUnKTtcclxuWy4uLmlucHV0RWxlbWVudF0ubWFwKGl0ZW0gPT4ge1xyXG4gICAgaXRlbS5hZGRFdmVudExpc3RlbmVyKCdjaGFuZ2UnLCAoZSkgPT4ge1xyXG4gICAgICAgIGlmKGl0ZW0uaWQgPT1cImNvdGF0aW9uX2Zvcm1fZGlzdGFuY2VcIil7XHJcbiAgICAgICAgICAgIGxldCBkaXN0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjb3RhdGlvbl9mb3JtX2Rpc3RhbmNlXCIpLnZhbHVlO1xyXG4gICAgICAgICAgICByYWRpb0Rpc3RDaGVjayhkaXN0KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYoaXRlbS5pZCA9PVwiY290YXRpb25fZm9ybV9kZW5pdmVsZVwiKXtcclxuICAgICAgICAgICAgbGV0IGRlbml2ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjb3RhdGlvbl9mb3JtX2Rlbml2ZWxlXCIpLnZhbHVlO1xyXG4gICAgICAgICAgICByYWRpb0Rlbml2Q2hlY2soZGVuaXYpO1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG59KTtcclxuXHJcbi8vIGZvbmRzIGRlIGNhcnRlXHJcbnZhciBtYXAgPSBMLm1hcCgnY290YXRpb24tbWFwJywge1xyXG4gICAgY2VudGVyOiBbNDcsIDJdLFxyXG4gICAgem9vbTogNSxcclxuICAgIG1pblpvb206IDMsXHJcbiAgICB6b29tQ29udHJvbDogdHJ1ZSxcclxuICAgIGxheWVyczogW2Jhc2VtYXBMYXllcignVG9wb2dyYXBoaWMnKV1cclxufSk7XHJcbkwuY29udHJvbC5zY2FsZSgpLmFkZFRvKG1hcCk7XHJcblxyXG4vL291dmVydHVyZSBldCBhZmZpY2hhZ2UgZHUgZ3B4XHJcbnZhciBuYmZpbGVzID0gMDtcclxuZnVuY3Rpb24gbG9hZEdweEZpbGUoZykge1xyXG4gICAgdmFyIGZpbGVzID0gZy50YXJnZXQuZmlsZXMsXHJcbiAgICByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xyXG4gICAgbmJmaWxlcys9MTtcclxuICAgIGlmIChuYmZpbGVzPjEpe1xyXG4gICAgICAgIG1hcC5yZW1vdmUoKTtcclxuICAgICAgICBtYXAgPSBMLm1hcCgnY290YXRpb24tbWFwJywge1xyXG4gICAgICAgICAgICBjZW50ZXI6IFs0NywgMl0sXHJcbiAgICAgICAgICAgIHpvb206IDUsXHJcbiAgICAgICAgICAgIG1pblpvb206IDMsXHJcbiAgICAgICAgICAgIHpvb21Db250cm9sOiB0cnVlLFxyXG4gICAgICAgICAgICBsYXllcnM6IFtiYXNlbWFwTGF5ZXIoJ1RvcG9ncmFwaGljJyldXHJcbiAgICAgICAgfSk7XHRcclxuICAgIH1cclxuICAgIHJlYWRlci5vbmxvYWQgPSBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgdmFyIGdweFN0cmluZyA9IHJlYWRlci5yZXN1bHQ7XHJcbiAgICAgICAgbmV3IEdQWChncHhTdHJpbmcsIHthc3luYzogdHJ1ZSxcclxuICAgICAgICAgICAgbWFya2VyX29wdGlvbnM6IHtcclxuICAgICAgICAgICAgICAgIHN0YXJ0SWNvblVybDogJy4uL2J1aWxkL2ltYWdlcy9pY29uL3Bpbi1pY29uLXN0YXJ0LnBuZycsXHJcbiAgICAgICAgICAgICAgICBlbmRJY29uVXJsOiAnLi4vYnVpbGQvaW1hZ2VzL2ljb24vcGluLWljb24tZW5kLnBuZycsXHJcbiAgICAgICAgICAgICAgICBzaGFkb3dVcmw6ICcnLFxyXG4gICAgICAgICAgICAgICAgd3B0SWNvblVybHM6ICcnXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KS5vbignbG9hZGVkJywgZnVuY3Rpb24oZSkge1xyXG4gICAgICAgICAgICBtYXAuZml0Qm91bmRzKGUudGFyZ2V0LmdldEJvdW5kcygpKTtcclxuICAgICAgICAgICAgbGV0IG5vbT1lLnRhcmdldC5nZXRfbmFtZSgpO1xyXG4gICAgICAgICAgICBsZXQgZGlzdD1NYXRoLnJvdW5kKGUudGFyZ2V0LmdldF9kaXN0YW5jZSgpLzEwMDApO1xyXG4gICAgICAgICAgICBsZXQgdGVtcHM9TWF0aC5yb3VuZCgoZGlzdC8xNSkqNjApO1xyXG4gICAgICAgICAgICBsZXQgZGVuaXYgPSBNYXRoLnJvdW5kKGUudGFyZ2V0LmdldF9lbGV2YXRpb25fZ2FpbigpKTtcclxuICAgICAgICAgICAgQ290YXRpb24uYWRkVmFsdWVUb0lucHV0KG5vbSwgXCIjY290YXRpb25fZm9ybV9ub21fY2lyY3VpdFwiKTtcclxuICAgICAgICAgICAgQ290YXRpb24uYWRkVmFsdWVUb0lucHV0KGRpc3QsIFwiI2NvdGF0aW9uX2Zvcm1fZGlzdGFuY2VcIik7XHJcbiAgICAgICAgICAgIENvdGF0aW9uLmFkZFZhbHVlVG9JbnB1dChDb3RhdGlvbi5jb252ZXJ0TWluc1RvSHJzTWlucyh0ZW1wcyksIFwiI2NvdGF0aW9uX2Zvcm1fdGVtcHNcIik7XHJcbiAgICAgICAgICAgIENvdGF0aW9uLmFkZFZhbHVlVG9JbnB1dChkZW5pdiwgXCIjY290YXRpb25fZm9ybV9kZW5pdmVsZVwiKTtcclxuICAgICAgICAgICAgcmFkaW9EaXN0Q2hlY2soZGlzdCk7XHJcbiAgICAgICAgICAgIHJhZGlvRGVuaXZDaGVjayhkZW5pdik7XHJcbiAgICAgICAgICAgIC8qcmFkaW9UeXBlVm9pZUNoZWNrKGRpc3QpO1xyXG4gICAgICAgICAgICByYWRpb1BlbnRlQ2hlY2soZGlzdCk7Ki9cclxuICAgICAgICB9KS5hZGRUbyhtYXApO1xyXG5cclxuICAgICAgICB2YXIgZ3B4RG9jID0gbmV3IERPTVBhcnNlcigpLnBhcnNlRnJvbVN0cmluZyhncHhTdHJpbmcsICd0ZXh0L3htbCcpO1xyXG4gICAgICAgIHZhciBnZW9Kc29uPVtdO1xyXG4gICAgICAgIGdlb0pzb24ucHVzaCh0b0dlb0pTT04uZ3B4KGdweERvYykpO1xyXG4gICAgICAgIGdlb0pzb25bMF1bJ3Byb3BlcnRpZXMnXT1cclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIFwiQ3JlYXRvclwiOiBcIkZGVsOpbG9cIixcclxuICAgICAgICAgICAgXCJzdW1tYXJ5XCI6IFwiXCJcclxuICAgICAgICB9XHJcbiAgICAgICAgLyp2YXIgaGcgPSBMLmNvbnRyb2wuaGVpZ2h0Z3JhcGgoe1xyXG4gICAgICAgICAgICB3aWR0aDogODAwLFxyXG4gICAgICAgICAgICBoZWlnaHQ6IDI4MCxcclxuICAgICAgICAgICAgbWFyZ2luczoge1xyXG4gICAgICAgICAgICAgICAgdG9wOiAxMCxcclxuICAgICAgICAgICAgICAgIHJpZ2h0OiAzMCxcclxuICAgICAgICAgICAgICAgIGJvdHRvbTogNTUsXHJcbiAgICAgICAgICAgICAgICBsZWZ0OiA1MFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBwb3NpdGlvbjogXCJib3R0b21yaWdodFwiLFxyXG4gICAgICAgICAgICBtYXBwaW5nczogIHVuZGVmaW5lZFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGhnLmFkZFRvKG1hcCk7XHJcbiAgICAgICAgaGcuYWRkRGF0YShnZW9Kc29uKTsqL1xyXG4gICAgICAgIC8vR2VvanNvbiBwb3VyIGZsZWNoZXMgZGlyZWN0aW9ubmVsbGVzIHN1ciBsZSB0cmFjw6kgZHUgY2lyY3VpdFxyXG4gICAgICAgIHZhciBhcnJvd0xheWVyID0gTC5nZW9Kc29uKGdlb0pzb24sIHtcclxuICAgICAgICAgICAgb25FYWNoRmVhdHVyZTogZnVuY3Rpb24gKGZlYXR1cmUsIGxheWVyKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgY29vcmRzID0gbGF5ZXIuZ2V0TGF0TG5ncygpO1xyXG4gICAgICAgICAgICAgICAgTC5wb2x5bGluZURlY29yYXRvcihjb29yZHMsIHtcclxuICAgICAgICAgICAgICAgICAgICBwYXR0ZXJuczogW3tcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2Zmc2V0OiAyNSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVwZWF0OiAxMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN5bWJvbDogTC5TeW1ib2wuYXJyb3dIZWFkKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBpeGVsU2l6ZTogMTUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXRoT3B0aW9uczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGxPcGFjaXR5OiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdlaWdodDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjonIzAwMDBGRidcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICB9XVxyXG4gICAgICAgICAgICAgICAgfSkuYWRkVG8obWFwKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLmFkZFRvKG1hcCk7XHJcbiAgICB9XHJcbiAgICByZWFkZXIucmVhZEFzVGV4dChmaWxlc1swXSk7XHJcbn1cclxub3BlbmdweC5hZGRFdmVudExpc3RlbmVyKFwiY2hhbmdlXCIsbG9hZEdweEZpbGUsIGZhbHNlKTsiXSwic291cmNlUm9vdCI6IiJ9