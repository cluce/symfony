(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["circuits"],{

/***/ "./assets/js/pages/circuits.js":
/*!*************************************!*\
  !*** ./assets/js/pages/circuits.js ***!
  \*************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.concat.js */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.filter.js */ "./node_modules/core-js/modules/es.array.filter.js");
/* harmony import */ var core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_array_includes_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.array.includes.js */ "./node_modules/core-js/modules/es.array.includes.js");
/* harmony import */ var core_js_modules_es_array_includes_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_includes_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_index_of_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.index-of.js */ "./node_modules/core-js/modules/es.array.index-of.js");
/* harmony import */ var core_js_modules_es_array_index_of_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_index_of_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_join_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.join.js */ "./node_modules/core-js/modules/es.array.join.js");
/* harmony import */ var core_js_modules_es_array_join_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_join_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_array_reverse_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.array.reverse.js */ "./node_modules/core-js/modules/es.array.reverse.js");
/* harmony import */ var core_js_modules_es_array_reverse_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_reverse_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_array_splice_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.array.splice.js */ "./node_modules/core-js/modules/es.array.splice.js");
/* harmony import */ var core_js_modules_es_array_splice_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_splice_js__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.regexp.exec.js */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_string_includes_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.string.includes.js */ "./node_modules/core-js/modules/es.string.includes.js");
/* harmony import */ var core_js_modules_es_string_includes_js__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_includes_js__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_string_search_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.string.search.js */ "./node_modules/core-js/modules/es.string.search.js");
/* harmony import */ var core_js_modules_es_string_search_js__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_search_js__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.string.split.js */ "./node_modules/core-js/modules/es.string.split.js");
/* harmony import */ var core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../services/mapConfig */ "./assets/js/services/mapConfig.js");
/* harmony import */ var _components_popup__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../components/popup */ "./assets/js/components/popup.js");
/* harmony import */ var _services_whiteList__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../services/whiteList */ "./assets/js/services/whiteList.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _components_InfoFeature__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../components/InfoFeature */ "./assets/js/components/InfoFeature.jsx");
/* harmony import */ var _yaireo_tagify__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @yaireo/tagify */ "./node_modules/@yaireo/tagify/dist/tagify.min.js");
/* harmony import */ var _yaireo_tagify__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(_yaireo_tagify__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var nouislider__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! nouislider */ "./node_modules/nouislider/distribute/nouislider.js");
/* harmony import */ var nouislider__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(nouislider__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var wnumb__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! wnumb */ "./node_modules/wnumb/wNumb.js");
/* harmony import */ var wnumb__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(wnumb__WEBPACK_IMPORTED_MODULE_20__);













function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }











(function mapCircuits() {
  //map
  var map = L.map('map', {
    center: [47, 0],
    zoom: 6,
    minZoom: 1,
    zoomControl: true
  });
  L.control.scale().addTo(map); //barre echelle

  var defaultLayer = L.esri.basemapLayer("Topographic").addTo(map);
  map.attributionControl.addAttribution('<a target="_blank" href="https://ffvelo.fr">Fédération française de cyclotourisme</a>'); //variables globales

  var mapElement = document.getElementById("map");
  var sidebarElement = document.getElementById("sidebar");
  var mapWidth = document.getElementById("map").offsetWidth;
  var defaultWhereClause = location.search ? Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["queryString"])(location.search) : "1=1"; //bouton toggle sidebar

  Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["sidebarToggleControl"])(L.Control, map, sidebarElement, mapElement, mapWidth); // controle des couches et changement de fond de carte

  var baseLayers = {
    'Topographique (Esri)': defaultLayer,
    'Satellite (Esri)': L.esri.basemapLayer('Imagery'),
    'OpenStreetMap': L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    })
  };
  var overlayMaps = {
    "Cols": Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["colsLayer"])()
  };
  L.control.layers(baseLayers, overlayMaps, {
    position: "bottomleft"
  }).addTo(map);
  Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["layerControlTitles"])();
  map.addControl(new _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["geolocationButton"](map)); //layer points departs circuits

  var pointDepart = L.esri.Cluster.featureLayer({
    url: _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["agolFeatureServices"].pointDepartCircuit,
    where: defaultWhereClause,
    pointToLayer: function pointToLayer(feature, latlng) {
      var difficulte = feature.properties.DIFFICULTE;
      return L.marker(latlng, {
        icon: _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["iconPointDepart"][difficulte]
      });
    },
    iconCreateFunction: function iconCreateFunction(cluster) {
      var count = cluster.getChildCount();
      var digits = (count + '').length;
      return L.divIcon({
        html: count,
        className: 'cluster digits-' + digits,
        iconSize: null
      });
    }
  }).addTo(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["pointDepartLayerGroup"]).addTo(map); //popups points departs circuits

  pointDepart.bindPopup(function (layer) {
    var nom = layer.feature.properties.NOM;
    var id = layer.feature.properties.IDENTIFIANT_CIRCUIT;
    var distance = layer.feature.properties.DISTANCE;
    var typevelo = _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["typeVeloName"][layer.feature.properties.TYPE_VELO];
    var denivele = layer.feature.properties.DENIVELE;
    var difficulte = _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["difficulteName"][layer.feature.properties.DIFFICULTE];
    var difficulteIcon = _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["difficulteFontAwesome"][layer.feature.properties.DIFFICULTE];
    var sousType = layer.feature.properties.SOUS_TYPE_CIRCUIT;
    return L.Util.template(Object(_components_popup__WEBPACK_IMPORTED_MODULE_13__["popupTemplateCircuit"])(nom, id, typevelo, distance, denivele, difficulte, difficulteIcon, sousType));
  });
  /*evenements carte et layer point depart*/

  pointDepart.once('load', function () {
    if (location.pathname != menuUrl) {
      var parts = location.href.split(menuUrl + "/");
      var urlid = parts[1].split("-");
      renderInfoFeature(pointDepart, urlid[0]);
    } else {
      Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["zoomToMarkersAfterLoad"])(map, pointDepart);
    }

    ; //syncSidebar();
  });
  pointDepart.on('popupopen', function (e) {
    document.getElementById("buttonPopup").addEventListener('click', function () {
      renderInfoFeature(pointDepart, e.layer.feature.properties.IDENTIFIANT_CIRCUIT);
    }, true);
  });
  pointDepart.on('click', function (e) {
    Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["showCircuit"])(map, e.layer.feature.properties.IDENTIFIANT_CIRCUIT, e.layer.feature.properties.DIFFICULTE);
  });
  map.on("moveend", function () {
    pointDepart.setWhere(pointDepart.getWhere());
    syncSidebar();
  });
  map.on('overlayremove', function () {
    _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["circuitLayerGroup"].clearLayers();
  });

  window.onpopstate = function () {
    distSlider.noUiSlider.reset();
    denivSlider.noUiSlider.reset();
    Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["backButtonNavigation"])(menuUrl, map, mapElement, mapWidth, pointDepart, renderInfoFeature);
  };
  /*fin evenements carte et layer point depart*/

  /*filtres de recherches */


  var distSlider = document.getElementById('distSlider');
  nouislider__WEBPACK_IMPORTED_MODULE_19___default.a.create(distSlider, {
    start: [0, 150],
    connect: true,
    range: {
      'min': 0,
      'max': 150
    },
    format: wnumb__WEBPACK_IMPORTED_MODULE_20___default()({
      decimals: 0 // default is 2

    }),
    pips: {
      mode: 'count',
      values: 5
    }
  });
  var denivSlider = document.getElementById('denivSlider');
  nouislider__WEBPACK_IMPORTED_MODULE_19___default.a.create(denivSlider, {
    start: [0, 1000],
    connect: true,
    range: {
      'min': 0,
      'max': 1000
    },
    format: wnumb__WEBPACK_IMPORTED_MODULE_20___default()({
      decimals: 0 // default is 2

    }),
    pips: {
      mode: 'count',
      values: 3
    }
  });
  var tagify = new _yaireo_tagify__WEBPACK_IMPORTED_MODULE_18___default.a(document.querySelector("textarea[name=tagsDepts]"), {
    enforeWhitelist: true,
    whitelist: _services_whiteList__WEBPACK_IMPORTED_MODULE_14__["deptList"]
  });

  var getFilters = function getFilters() {
    var filters = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var elements = document.querySelectorAll(".typeVeloFilter, .difficulteFilter, .tempsFilter, .sousTypeCircuitFilter, .typeCircuitFilter");

    _toConsumableArray(elements).map(function (item) {
      item.addEventListener('change', function (e) {
        if (item.checked === true) {
          filters.push(item.value);
        } else {
          filters.splice(filters.indexOf(item.value), 1);
        }

        setFilters(filters);
      });
    });

    tagify.on('add', function (e) {
      filters.push(e.detail.data.queryCircuit);
      setFilters(filters);
    }).on('remove', function (e) {
      filters.splice(filters.indexOf(e.detail.data.queryCircuit), 1);
      setFilters(filters);
    });
    distSlider.noUiSlider.on("change", function (data) {
      if (data[0] > 0 && data[1] < 250) {
        filters.distance = "DISTANCE >= " + data[0] + " AND DISTANCE <= " + data[1];
      } else if (data[0] > 0 && data[1] == 250) {
        filters.distance = "DISTANCE >=" + data[0];
      } else if (data[0] == 0 && data[1] < 250) {
        filters.distance = "DISTANCE <= " + data[1];
      } else {
        filters.distance = null;
      }

      setFilters(filters);
    });
    denivSlider.noUiSlider.on("change", function (data) {
      if (data[0] > 0 && data[1] < 1000) {
        filters.denivele = "DENIVELE >= " + data[0] + " AND DENIVELE <= " + data[1];
      } else if (data[0] > 0 && data[1] == 1000) {
        filters.denivele = "DENIVELE >=" + data[0];
      } else if (data[0] == 0 && data[1] < 1000) {
        filters.denivele = "DENIVELE <= " + data[1];
      } else {
        filters.denivele = null;
      }

      setFilters(filters);
    });
  };

  getFilters();

  var setFilters = function setFilters(filters) {
    var arrResult = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    var dept = filters.filter(function (f) {
      return f.includes("NUMERO_DEPARTEMENT");
    }).join(" OR ");
    var typeVelo = filters.filter(function (f) {
      return f.includes("TYPE_VELO");
    }).join(" OR ");
    var difficulte = filters.filter(function (f) {
      return f.includes("DIFFICULTE");
    }).join(" OR ");
    var temps = filters.filter(function (f) {
      return f.includes("TEMPS_PARCOURS");
    }).join(" OR ");
    var type = filters.filter(function (f) {
      return f.includes("TYPE_CIRCUIT");
    }).join(" OR ");
    var sousType = filters.filter(function (f) {
      return f.includes("SOUS_TYPE_CIRCUIT");
    }).join(" OR ");
    dept.length > 0 ? arrResult.push("(".concat(dept, ")")) : false;
    typeVelo.length > 0 ? arrResult.push("(".concat(typeVelo, ")")) : false;
    difficulte.length > 0 ? arrResult.push("(".concat(difficulte, ")")) : false;
    temps.length > 0 ? arrResult.push("(".concat(temps, ")")) : false;
    type.length > 0 ? arrResult.push("(".concat(type, ")")) : false;
    sousType.length > 0 ? arrResult.push("(".concat(sousType, ")")) : false;
    filters.distance ? arrResult.push("(".concat(filters.distance, ")")) : false;
    filters.denivele ? arrResult.push("(".concat(filters.denivele, ")")) : false;
    var result = arrResult.join(" AND ");
    console.log(result);
    pointDepart.setWhere(result);
    Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["zoomToMarkersAfterLoad"])(map, pointDepart);
  };
  /*fin des filtres de recherches */

  /* barre de recherche adresse et circuits (geocodeur en haut a gauche de la carte) */


  var searchControl = L.esri.Geocoding.geosearch({
    useMapBounds: false,
    title: 'Rechercher un lieu / un circuit',
    placeholder: 'Trouver un lieu / un circuit',
    expanded: true,
    collapseAfterResult: false,
    position: 'topleft',
    providers: [_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["arcgisOnlineProvider"], L.esri.Geocoding.featureLayerProvider({
      //recherche des couches dans la gdb vef
      url: _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["agolFeatureServices"].pointDepartCircuit,
      where: pointDepart.getWhere(),
      searchFields: ['NOM'],
      label: 'CIRCUITS VELOENFRANCE:',
      maxResults: 7,
      bufferRadius: 1000,
      formatSuggestion: function formatSuggestion(feature) {
        return "".concat(feature.properties.NOM, " - ").concat(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["ssTypeCircuit"][feature.properties.SOUS_TYPE_CIRCUIT]);
      }
    })]
  }).addTo(map); //geocodeur en dehors de la carte

  var esriGeocoder = searchControl.getContainer();
  Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["setEsriGeocoder"])(esriGeocoder, document.getElementById('geocoder')); //recup la requete en cours et applique aux resultats du geocodeur

  searchControl.on("requestend", function () {
    searchControl.options.providers[1].options.where = pointDepart.getWhere();
  }); //affiche le trace du circuit selectionne avec le geocodeur

  searchControl.on("results", function (e) {
    _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["circuitLayerGroup"].clearLayers();
    var idCircuitResult = e.results[0].properties.IDENTIFIANT_CIRCUIT;

    for (var i = e.results.length - 1; i >= 0; i--) {
      var marker = e.results[i].latlng;

      if (e.results[0].properties.IDENTIFIANT_CIRCUIT == null) {
        L.popup().setLatLng(marker).setContent(e.text).openOn(map);
      }
    }

    renderInfoFeature(pointDepart, idCircuitResult);
  });
  /* fin barre de recherche adresse et circuits (geocodeur en haut a gauche de la carte) */

  /*cards dans la sidebar*/

  var syncSidebar = function syncSidebar() {
    var arrResultsSidebar = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var featureCards = document.getElementById('features');

    while (featureCards.firstChild) {
      featureCards.removeChild(featureCards.firstChild);
    }

    pointDepart.eachActiveFeature(function (layer) {
      //eachActiveFeature = fonction a ajouter dans esri-leaflet-cluster.js - attention si maj de la librairie
      if (map.hasLayer(pointDepart)) {
        if (map.getBounds().contains(layer.getLatLng())) {
          arrResultsSidebar.push(sidebarTemplateCircuits(layer.feature.properties.NOM, _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["typeVeloName"][layer.feature.properties.TYPE_VELO], _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["difficulteName"][layer.feature.properties.DIFFICULTE], layer.feature.properties.DISTANCE, layer.feature.properties.DENIVELE, layer.feature.properties.SOUS_TYPE_CIRCUIT, layer.feature.properties.COMMUNE_DEPART, layer.feature.properties.Nom_Dep, layer.feature.properties.IDENTIFIANT_CIRCUIT, layer.feature.geometry.coordinates[1], layer.feature.geometry.coordinates[0]));
        }
      }
    });
    arrResultsSidebar.reverse();
    Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["addResultsToSidebar"])(arrResultsSidebar, pointDepart, renderInfoFeature);
  };

  var sidebarTemplateCircuits = function sidebarTemplateCircuits(nom, typevelo, difficulte, distance, denivele, soustype, ville, dept, id, lat, lon) {
    //template cards
    return "<div class=\"card feature-card my-2 p-2\" id=\"".concat(id, "\" data-lat=\"").concat(lat, "\" data-lon=\"").concat(lon, "\"> \n\t\t\t\t<div class=\"col align-self-center rounded-right\">\n\t\t\t\t\t<h6 class=\"d-block text-uppercase\">").concat(nom, "</h6>\n\t\t\t\t\t<span class=\"badge badge-pill badge-light my-1 mr-1\"><i class=\"fas fa-bicycle\"></i> ").concat(typevelo, "</span>\n\t\t\t\t\t").concat(difficulte ? "<span class=\"badge badge-pill badge-light my-1 mr-1\"><i class=\"far fa-star\"></i> ".concat(difficulte, "</span>") : "", "\n\t\t\t\t\t").concat(distance ? "<span class=\"badge badge-pill badge-light my-1 mr-1\"><i class=\"fas fa-fw fa-road\"></i> ".concat(distance, " km</span>") : "", "\n\t\t\t\t\t").concat(denivele ? "<span class=\"badge badge-pill badge-light my-1 mr-1\"><i class=\"fas fa-chart-line\"></i> ".concat(denivele, " m</span>") : "", "\n\t\t\t\t\t").concat(soustype ? "<span class=\"badge badge-pill badge-light my-1 mr-1\"><i className=\"fas fa-tags\"></i> ".concat(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["ssTypeCircuit"][soustype], "</span>") : "", "\n\t\t\t\t\t<span class=\"my-1 mr-1\"><br/><i class=\"fas fa-map-marked-alt\"></i> ").concat(ville, ", ").concat(dept, "</span>\n\t\t\t\t</div>\n\t\t\t</div>");
  };
  /* fin des cards dans la sidebar*/
  //composant fiche descriptive


  var renderInfoFeature = function renderInfoFeature(Layer, id) {
    var queryCircuitFournisseur = L.esri.query({
      url: _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["agolFeatureServices"].circuitFournisseur
    });
    var queryFournisseur = L.esri.query({
      url: _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["agolFeatureServices"].fournisseur
    });
    mapElement.style.width = mapWidth + "px";
    var infofeatureElement = document.getElementById('infofeature');
    var locationSearch = location.search;
    infofeatureElement.value = Layer.getWhere();
    /*let toggleButton = document.getElementById('toggleButton');
    toggleButton.style.display = "none";*/

    Layer.eachFeature(function (layer) {
      if (layer.feature.properties.IDENTIFIANT_CIRCUIT == id) {
        Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["showCircuit"])(map, layer.feature.properties.IDENTIFIANT_CIRCUIT, layer.feature.properties.DIFFICULTE, infofeatureElement, layer.feature.geometry.coordinates);
        Object(react_dom__WEBPACK_IMPORTED_MODULE_16__["unmountComponentAtNode"])(infofeatureElement);
        Object(react_dom__WEBPACK_IMPORTED_MODULE_16__["render"])( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_15___default.a.createElement(_components_InfoFeature__WEBPACK_IMPORTED_MODULE_17__["default"], {
          idCircuit: id,
          nom: layer.feature.properties.NOM,
          descr: layer.feature.properties.DESCRIPTION,
          distance: layer.feature.properties.DISTANCE,
          denivele: layer.feature.properties.DENIVELE,
          difficulte: _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["difficulteName"][layer.feature.properties.DIFFICULTE],
          typevelo: _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["typeVeloName"][layer.feature.properties.TYPE_VELO],
          soustype: layer.feature.properties.SOUS_TYPE_CIRCUIT,
          rootContainer: infofeatureElement,
          map: map,
          mapWidth: mapWidth,
          Layer: Layer,
          queryCircuitFournisseur: queryCircuitFournisseur,
          queryFournisseur: queryFournisseur,
          layer: layer,
          params: defaultWhereClause,
          locationSearch: locationSearch,
          whereClause: infofeatureElement.value,
          menuUrl: menuUrl,
          circuitLayer: _services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["circuitLayerGroup"],
          toggleButton: toggleButton
        }), infofeatureElement);
        Object(_services_mapConfig__WEBPACK_IMPORTED_MODULE_12__["navigationHistory"])(id, menuUrl, layer.feature.properties.SLUG); //gestion de la carte

        var resize = mapWidth - document.getElementById("info-feature-content").offsetWidth;
        mapElement.style.width = resize + "px";
        map.invalidateSize();
      }
    });
  };
})();

/***/ })

},[["./assets/js/pages/circuits.js","runtime","vendors~app~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_velor~10347b53","vendors~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~61364f2f","vendors~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~cotatio~d7e263c1","vendors~bonnes_adresses~calendrier~circuits~clubs~labels_velo~login_licencie","vendors~bonnes_adresses~calendrier~circuits~clubs~labels_velo","vendors~calendrier~circuits~clubs~labels_velo","vendors~calendrier~circuits","bonnes_adresses~calendrier~circuits~clubs~labels_velo","circuits~labels_velo"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvcGFnZXMvY2lyY3VpdHMuanMiXSwibmFtZXMiOlsibWFwQ2lyY3VpdHMiLCJtYXAiLCJMIiwiY2VudGVyIiwiem9vbSIsIm1pblpvb20iLCJ6b29tQ29udHJvbCIsImNvbnRyb2wiLCJzY2FsZSIsImFkZFRvIiwiZGVmYXVsdExheWVyIiwiZXNyaSIsImJhc2VtYXBMYXllciIsImF0dHJpYnV0aW9uQ29udHJvbCIsImFkZEF0dHJpYnV0aW9uIiwibWFwRWxlbWVudCIsImRvY3VtZW50IiwiZ2V0RWxlbWVudEJ5SWQiLCJzaWRlYmFyRWxlbWVudCIsIm1hcFdpZHRoIiwib2Zmc2V0V2lkdGgiLCJkZWZhdWx0V2hlcmVDbGF1c2UiLCJsb2NhdGlvbiIsInNlYXJjaCIsInF1ZXJ5U3RyaW5nIiwic2lkZWJhclRvZ2dsZUNvbnRyb2wiLCJDb250cm9sIiwiYmFzZUxheWVycyIsInRpbGVMYXllciIsImF0dHJpYnV0aW9uIiwib3ZlcmxheU1hcHMiLCJjb2xzTGF5ZXIiLCJsYXllcnMiLCJwb3NpdGlvbiIsImxheWVyQ29udHJvbFRpdGxlcyIsImFkZENvbnRyb2wiLCJnZW9sb2NhdGlvbkJ1dHRvbiIsInBvaW50RGVwYXJ0IiwiQ2x1c3RlciIsImZlYXR1cmVMYXllciIsInVybCIsImFnb2xGZWF0dXJlU2VydmljZXMiLCJwb2ludERlcGFydENpcmN1aXQiLCJ3aGVyZSIsInBvaW50VG9MYXllciIsImZlYXR1cmUiLCJsYXRsbmciLCJkaWZmaWN1bHRlIiwicHJvcGVydGllcyIsIkRJRkZJQ1VMVEUiLCJtYXJrZXIiLCJpY29uIiwiaWNvblBvaW50RGVwYXJ0IiwiaWNvbkNyZWF0ZUZ1bmN0aW9uIiwiY2x1c3RlciIsImNvdW50IiwiZ2V0Q2hpbGRDb3VudCIsImRpZ2l0cyIsImxlbmd0aCIsImRpdkljb24iLCJodG1sIiwiY2xhc3NOYW1lIiwiaWNvblNpemUiLCJwb2ludERlcGFydExheWVyR3JvdXAiLCJiaW5kUG9wdXAiLCJsYXllciIsIm5vbSIsIk5PTSIsImlkIiwiSURFTlRJRklBTlRfQ0lSQ1VJVCIsImRpc3RhbmNlIiwiRElTVEFOQ0UiLCJ0eXBldmVsbyIsInR5cGVWZWxvTmFtZSIsIlRZUEVfVkVMTyIsImRlbml2ZWxlIiwiREVOSVZFTEUiLCJkaWZmaWN1bHRlTmFtZSIsImRpZmZpY3VsdGVJY29uIiwiZGlmZmljdWx0ZUZvbnRBd2Vzb21lIiwic291c1R5cGUiLCJTT1VTX1RZUEVfQ0lSQ1VJVCIsIlV0aWwiLCJ0ZW1wbGF0ZSIsInBvcHVwVGVtcGxhdGVDaXJjdWl0Iiwib25jZSIsInBhdGhuYW1lIiwibWVudVVybCIsInBhcnRzIiwiaHJlZiIsInNwbGl0IiwidXJsaWQiLCJyZW5kZXJJbmZvRmVhdHVyZSIsInpvb21Ub01hcmtlcnNBZnRlckxvYWQiLCJvbiIsImUiLCJhZGRFdmVudExpc3RlbmVyIiwic2hvd0NpcmN1aXQiLCJzZXRXaGVyZSIsImdldFdoZXJlIiwic3luY1NpZGViYXIiLCJjaXJjdWl0TGF5ZXJHcm91cCIsImNsZWFyTGF5ZXJzIiwid2luZG93Iiwib25wb3BzdGF0ZSIsImRpc3RTbGlkZXIiLCJub1VpU2xpZGVyIiwicmVzZXQiLCJkZW5pdlNsaWRlciIsImJhY2tCdXR0b25OYXZpZ2F0aW9uIiwiY3JlYXRlIiwic3RhcnQiLCJjb25uZWN0IiwicmFuZ2UiLCJmb3JtYXQiLCJ3TnVtYiIsImRlY2ltYWxzIiwicGlwcyIsIm1vZGUiLCJ2YWx1ZXMiLCJ0YWdpZnkiLCJUYWdpZnkiLCJxdWVyeVNlbGVjdG9yIiwiZW5mb3JlV2hpdGVsaXN0Iiwid2hpdGVsaXN0IiwiZGVwdExpc3QiLCJnZXRGaWx0ZXJzIiwiZmlsdGVycyIsImVsZW1lbnRzIiwicXVlcnlTZWxlY3RvckFsbCIsIml0ZW0iLCJjaGVja2VkIiwicHVzaCIsInZhbHVlIiwic3BsaWNlIiwiaW5kZXhPZiIsInNldEZpbHRlcnMiLCJkZXRhaWwiLCJkYXRhIiwicXVlcnlDaXJjdWl0IiwiYXJyUmVzdWx0IiwiZGVwdCIsImZpbHRlciIsImYiLCJpbmNsdWRlcyIsImpvaW4iLCJ0eXBlVmVsbyIsInRlbXBzIiwidHlwZSIsInJlc3VsdCIsImNvbnNvbGUiLCJsb2ciLCJzZWFyY2hDb250cm9sIiwiR2VvY29kaW5nIiwiZ2Vvc2VhcmNoIiwidXNlTWFwQm91bmRzIiwidGl0bGUiLCJwbGFjZWhvbGRlciIsImV4cGFuZGVkIiwiY29sbGFwc2VBZnRlclJlc3VsdCIsInByb3ZpZGVycyIsImFyY2dpc09ubGluZVByb3ZpZGVyIiwiZmVhdHVyZUxheWVyUHJvdmlkZXIiLCJzZWFyY2hGaWVsZHMiLCJsYWJlbCIsIm1heFJlc3VsdHMiLCJidWZmZXJSYWRpdXMiLCJmb3JtYXRTdWdnZXN0aW9uIiwic3NUeXBlQ2lyY3VpdCIsImVzcmlHZW9jb2RlciIsImdldENvbnRhaW5lciIsInNldEVzcmlHZW9jb2RlciIsIm9wdGlvbnMiLCJpZENpcmN1aXRSZXN1bHQiLCJyZXN1bHRzIiwiaSIsInBvcHVwIiwic2V0TGF0TG5nIiwic2V0Q29udGVudCIsInRleHQiLCJvcGVuT24iLCJhcnJSZXN1bHRzU2lkZWJhciIsImZlYXR1cmVDYXJkcyIsImZpcnN0Q2hpbGQiLCJyZW1vdmVDaGlsZCIsImVhY2hBY3RpdmVGZWF0dXJlIiwiaGFzTGF5ZXIiLCJnZXRCb3VuZHMiLCJjb250YWlucyIsImdldExhdExuZyIsInNpZGViYXJUZW1wbGF0ZUNpcmN1aXRzIiwiQ09NTVVORV9ERVBBUlQiLCJOb21fRGVwIiwiZ2VvbWV0cnkiLCJjb29yZGluYXRlcyIsInJldmVyc2UiLCJhZGRSZXN1bHRzVG9TaWRlYmFyIiwic291c3R5cGUiLCJ2aWxsZSIsImxhdCIsImxvbiIsIkxheWVyIiwicXVlcnlDaXJjdWl0Rm91cm5pc3NldXIiLCJxdWVyeSIsImNpcmN1aXRGb3Vybmlzc2V1ciIsInF1ZXJ5Rm91cm5pc3NldXIiLCJmb3Vybmlzc2V1ciIsInN0eWxlIiwid2lkdGgiLCJpbmZvZmVhdHVyZUVsZW1lbnQiLCJsb2NhdGlvblNlYXJjaCIsImVhY2hGZWF0dXJlIiwidW5tb3VudENvbXBvbmVudEF0Tm9kZSIsInJlbmRlciIsIkRFU0NSSVBUSU9OIiwidG9nZ2xlQnV0dG9uIiwibmF2aWdhdGlvbkhpc3RvcnkiLCJTTFVHIiwicmVzaXplIiwiaW52YWxpZGF0ZVNpemUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUF1QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxDQUFDLFNBQVNBLFdBQVQsR0FBd0I7QUFFeEI7QUFDQSxNQUFNQyxHQUFHLEdBQUdDLENBQUMsQ0FBQ0QsR0FBRixDQUFNLEtBQU4sRUFBYTtBQUN4QkUsVUFBTSxFQUFFLENBQUMsRUFBRCxFQUFLLENBQUwsQ0FEZ0I7QUFFeEJDLFFBQUksRUFBRSxDQUZrQjtBQUd4QkMsV0FBTyxFQUFFLENBSGU7QUFJeEJDLGVBQVcsRUFBRTtBQUpXLEdBQWIsQ0FBWjtBQU1BSixHQUFDLENBQUNLLE9BQUYsQ0FBVUMsS0FBVixHQUFrQkMsS0FBbEIsQ0FBd0JSLEdBQXhCLEVBVHdCLENBU0s7O0FBQzdCLE1BQU1TLFlBQVksR0FBR1IsQ0FBQyxDQUFDUyxJQUFGLENBQU9DLFlBQVAsQ0FBb0IsYUFBcEIsRUFBbUNILEtBQW5DLENBQXlDUixHQUF6QyxDQUFyQjtBQUNBQSxLQUFHLENBQUNZLGtCQUFKLENBQXVCQyxjQUF2QixDQUFzQyx1RkFBdEMsRUFYd0IsQ0FheEI7O0FBQ0EsTUFBTUMsVUFBVSxHQUFHQyxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsS0FBeEIsQ0FBbkI7QUFDQSxNQUFNQyxjQUFjLEdBQUdGLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixTQUF4QixDQUF2QjtBQUNBLE1BQU1FLFFBQVEsR0FBR0gsUUFBUSxDQUFDQyxjQUFULENBQXdCLEtBQXhCLEVBQStCRyxXQUFoRDtBQUNBLE1BQU1DLGtCQUFrQixHQUFHQyxRQUFRLENBQUNDLE1BQVQsR0FBa0JDLHdFQUFXLENBQUNGLFFBQVEsQ0FBQ0MsTUFBVixDQUE3QixHQUFpRCxLQUE1RSxDQWpCd0IsQ0FtQnhCOztBQUNBRSxtRkFBb0IsQ0FBQ3ZCLENBQUMsQ0FBQ3dCLE9BQUgsRUFBWXpCLEdBQVosRUFBaUJpQixjQUFqQixFQUFpQ0gsVUFBakMsRUFBNkNJLFFBQTdDLENBQXBCLENBcEJ3QixDQXNCeEI7O0FBQ0EsTUFBTVEsVUFBVSxHQUFHO0FBQ2xCLDRCQUF3QmpCLFlBRE47QUFFbEIsd0JBQW9CUixDQUFDLENBQUNTLElBQUYsQ0FBT0MsWUFBUCxDQUFvQixTQUFwQixDQUZGO0FBR2xCLHFCQUFrQlYsQ0FBQyxDQUFDMEIsU0FBRixDQUFZLG9EQUFaLEVBQWtFO0FBQ25GQyxpQkFBVyxFQUFFO0FBRHNFLEtBQWxFO0FBSEEsR0FBbkI7QUFPQSxNQUFNQyxXQUFXLEdBQUc7QUFDbkIsWUFBUUMsc0VBQVM7QUFERSxHQUFwQjtBQUdBN0IsR0FBQyxDQUFDSyxPQUFGLENBQVV5QixNQUFWLENBQWlCTCxVQUFqQixFQUE2QkcsV0FBN0IsRUFBMEM7QUFDekNHLFlBQVEsRUFBRTtBQUQrQixHQUExQyxFQUVHeEIsS0FGSCxDQUVTUixHQUZUO0FBR0FpQyxpRkFBa0I7QUFDbEJqQyxLQUFHLENBQUNrQyxVQUFKLENBQWUsSUFBSUMsc0VBQUosQ0FBc0JuQyxHQUF0QixDQUFmLEVBckN3QixDQXVDeEI7O0FBQ0EsTUFBTW9DLFdBQVcsR0FBR25DLENBQUMsQ0FBQ1MsSUFBRixDQUFPMkIsT0FBUCxDQUFlQyxZQUFmLENBQTRCO0FBQy9DQyxPQUFHLEVBQUVDLHdFQUFtQixDQUFDQyxrQkFEc0I7QUFFL0NDLFNBQUssRUFBRXRCLGtCQUZ3QztBQUcvQ3VCLGdCQUFZLEVBQUUsc0JBQUNDLE9BQUQsRUFBVUMsTUFBVixFQUFxQjtBQUNsQyxVQUFJQyxVQUFVLEdBQUdGLE9BQU8sQ0FBQ0csVUFBUixDQUFtQkMsVUFBcEM7QUFDQSxhQUFPL0MsQ0FBQyxDQUFDZ0QsTUFBRixDQUFTSixNQUFULEVBQWlCO0FBQ3ZCSyxZQUFJLEVBQUVDLG9FQUFlLENBQUNMLFVBQUQ7QUFERSxPQUFqQixDQUFQO0FBR0EsS0FSOEM7QUFTL0NNLHNCQUFrQixFQUFFLDRCQUFDQyxPQUFELEVBQWE7QUFDaEMsVUFBSUMsS0FBSyxHQUFHRCxPQUFPLENBQUNFLGFBQVIsRUFBWjtBQUNBLFVBQUlDLE1BQU0sR0FBRyxDQUFDRixLQUFLLEdBQUcsRUFBVCxFQUFhRyxNQUExQjtBQUNBLGFBQU94RCxDQUFDLENBQUN5RCxPQUFGLENBQVU7QUFDZkMsWUFBSSxFQUFFTCxLQURTO0FBRWZNLGlCQUFTLEVBQUUsb0JBQW9CSixNQUZoQjtBQUdmSyxnQkFBUSxFQUFFO0FBSEssT0FBVixDQUFQO0FBS0U7QUFqQjRDLEdBQTVCLEVBa0JqQnJELEtBbEJpQixDQWtCWHNELDBFQWxCVyxFQWtCWXRELEtBbEJaLENBa0JrQlIsR0FsQmxCLENBQXBCLENBeEN3QixDQTREeEI7O0FBQ0FvQyxhQUFXLENBQUMyQixTQUFaLENBQXNCLFVBQUNDLEtBQUQsRUFBUztBQUM5QixRQUFJQyxHQUFHLEdBQUdELEtBQUssQ0FBQ3BCLE9BQU4sQ0FBY0csVUFBZCxDQUF5Qm1CLEdBQW5DO0FBQ0EsUUFBSUMsRUFBRSxHQUFHSCxLQUFLLENBQUNwQixPQUFOLENBQWNHLFVBQWQsQ0FBeUJxQixtQkFBbEM7QUFDQSxRQUFJQyxRQUFRLEdBQUdMLEtBQUssQ0FBQ3BCLE9BQU4sQ0FBY0csVUFBZCxDQUF5QnVCLFFBQXhDO0FBQ0EsUUFBSUMsUUFBUSxHQUFHQyxpRUFBWSxDQUFDUixLQUFLLENBQUNwQixPQUFOLENBQWNHLFVBQWQsQ0FBeUIwQixTQUExQixDQUEzQjtBQUNBLFFBQUlDLFFBQVEsR0FBR1YsS0FBSyxDQUFDcEIsT0FBTixDQUFjRyxVQUFkLENBQXlCNEIsUUFBeEM7QUFDQSxRQUFJN0IsVUFBVSxHQUFHOEIsbUVBQWMsQ0FBQ1osS0FBSyxDQUFDcEIsT0FBTixDQUFjRyxVQUFkLENBQXlCQyxVQUExQixDQUEvQjtBQUNBLFFBQUk2QixjQUFjLEdBQUdDLDBFQUFxQixDQUFDZCxLQUFLLENBQUNwQixPQUFOLENBQWNHLFVBQWQsQ0FBeUJDLFVBQTFCLENBQTFDO0FBQ0EsUUFBSStCLFFBQVEsR0FBR2YsS0FBSyxDQUFDcEIsT0FBTixDQUFjRyxVQUFkLENBQXlCaUMsaUJBQXhDO0FBQ0EsV0FBTy9FLENBQUMsQ0FBQ2dGLElBQUYsQ0FBT0MsUUFBUCxDQUNOQywrRUFBb0IsQ0FBQ2xCLEdBQUQsRUFBTUUsRUFBTixFQUFVSSxRQUFWLEVBQW9CRixRQUFwQixFQUE4QkssUUFBOUIsRUFBd0M1QixVQUF4QyxFQUFvRCtCLGNBQXBELEVBQW9FRSxRQUFwRSxDQURkLENBQVA7QUFHQSxHQVpEO0FBY0E7O0FBQ0EzQyxhQUFXLENBQUNnRCxJQUFaLENBQWlCLE1BQWpCLEVBQXlCLFlBQU07QUFDOUIsUUFBRy9ELFFBQVEsQ0FBQ2dFLFFBQVQsSUFBcUJDLE9BQXhCLEVBQWdDO0FBQy9CLFVBQUlDLEtBQUssR0FBR2xFLFFBQVEsQ0FBQ21FLElBQVQsQ0FBY0MsS0FBZCxDQUFvQkgsT0FBTyxHQUFDLEdBQTVCLENBQVo7QUFDQSxVQUFJSSxLQUFLLEdBQUdILEtBQUssQ0FBQyxDQUFELENBQUwsQ0FBU0UsS0FBVCxDQUFlLEdBQWYsQ0FBWjtBQUNBRSx1QkFBaUIsQ0FBQ3ZELFdBQUQsRUFBY3NELEtBQUssQ0FBQyxDQUFELENBQW5CLENBQWpCO0FBQ0EsS0FKRCxNQUlPO0FBQ05FLHlGQUFzQixDQUFDNUYsR0FBRCxFQUFNb0MsV0FBTixDQUF0QjtBQUNBOztBQUFBLEtBUDZCLENBUTlCO0FBQ0EsR0FURDtBQVVBQSxhQUFXLENBQUN5RCxFQUFaLENBQWUsV0FBZixFQUE0QixVQUFBQyxDQUFDLEVBQUk7QUFDaEMvRSxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsYUFBeEIsRUFBdUMrRSxnQkFBdkMsQ0FBd0QsT0FBeEQsRUFBaUUsWUFBTTtBQUN0RUosdUJBQWlCLENBQUN2RCxXQUFELEVBQWMwRCxDQUFDLENBQUM5QixLQUFGLENBQVFwQixPQUFSLENBQWdCRyxVQUFoQixDQUEyQnFCLG1CQUF6QyxDQUFqQjtBQUNBLEtBRkQsRUFFRyxJQUZIO0FBR0EsR0FKRDtBQUtBaEMsYUFBVyxDQUFDeUQsRUFBWixDQUFlLE9BQWYsRUFBd0IsVUFBQ0MsQ0FBRCxFQUFPO0FBQzlCRSw0RUFBVyxDQUFDaEcsR0FBRCxFQUFNOEYsQ0FBQyxDQUFDOUIsS0FBRixDQUFRcEIsT0FBUixDQUFnQkcsVUFBaEIsQ0FBMkJxQixtQkFBakMsRUFBc0QwQixDQUFDLENBQUM5QixLQUFGLENBQVFwQixPQUFSLENBQWdCRyxVQUFoQixDQUEyQkMsVUFBakYsQ0FBWDtBQUNBLEdBRkQ7QUFHQWhELEtBQUcsQ0FBQzZGLEVBQUosQ0FBTyxTQUFQLEVBQWtCLFlBQU07QUFDdkJ6RCxlQUFXLENBQUM2RCxRQUFaLENBQXFCN0QsV0FBVyxDQUFDOEQsUUFBWixFQUFyQjtBQUNBQyxlQUFXO0FBQ1gsR0FIRDtBQUlBbkcsS0FBRyxDQUFDNkYsRUFBSixDQUFPLGVBQVAsRUFBd0IsWUFBTTtBQUM3Qk8sMEVBQWlCLENBQUNDLFdBQWxCO0FBQ0EsR0FGRDs7QUFHQUMsUUFBTSxDQUFDQyxVQUFQLEdBQW9CLFlBQU07QUFDekJDLGNBQVUsQ0FBQ0MsVUFBWCxDQUFzQkMsS0FBdEI7QUFDQUMsZUFBVyxDQUFDRixVQUFaLENBQXVCQyxLQUF2QjtBQUNBRSxxRkFBb0IsQ0FBQ3RCLE9BQUQsRUFBVXRGLEdBQVYsRUFBZWMsVUFBZixFQUEyQkksUUFBM0IsRUFBcUNrQixXQUFyQyxFQUFrRHVELGlCQUFsRCxDQUFwQjtBQUNBLEdBSkQ7QUFLQTs7QUFHQTs7O0FBQ0EsTUFBTWEsVUFBVSxHQUFHekYsUUFBUSxDQUFDQyxjQUFULENBQXdCLFlBQXhCLENBQW5CO0FBQ0F5RixvREFBVSxDQUFDSSxNQUFYLENBQWtCTCxVQUFsQixFQUE4QjtBQUM3Qk0sU0FBSyxFQUFFLENBQUMsQ0FBRCxFQUFJLEdBQUosQ0FEc0I7QUFFN0JDLFdBQU8sRUFBRSxJQUZvQjtBQUc3QkMsU0FBSyxFQUFFO0FBQ04sYUFBTyxDQUREO0FBRU4sYUFBTztBQUZELEtBSHNCO0FBTzdCQyxVQUFNLEVBQUVDLDZDQUFLLENBQUM7QUFDYkMsY0FBUSxFQUFFLENBREcsQ0FDQTs7QUFEQSxLQUFELENBUGdCO0FBVTdCQyxRQUFJLEVBQUU7QUFBQ0MsVUFBSSxFQUFFLE9BQVA7QUFBZ0JDLFlBQU0sRUFBRTtBQUF4QjtBQVZ1QixHQUE5QjtBQVlBLE1BQU1YLFdBQVcsR0FBRzVGLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixhQUF4QixDQUFwQjtBQUNBeUYsb0RBQVUsQ0FBQ0ksTUFBWCxDQUFrQkYsV0FBbEIsRUFBK0I7QUFDOUJHLFNBQUssRUFBRSxDQUFDLENBQUQsRUFBSSxJQUFKLENBRHVCO0FBRTlCQyxXQUFPLEVBQUUsSUFGcUI7QUFHOUJDLFNBQUssRUFBRTtBQUNOLGFBQU8sQ0FERDtBQUVOLGFBQU87QUFGRCxLQUh1QjtBQU85QkMsVUFBTSxFQUFFQyw2Q0FBSyxDQUFDO0FBQ2JDLGNBQVEsRUFBRSxDQURHLENBQ0E7O0FBREEsS0FBRCxDQVBpQjtBQVU5QkMsUUFBSSxFQUFFO0FBQUNDLFVBQUksRUFBRSxPQUFQO0FBQWdCQyxZQUFNLEVBQUU7QUFBeEI7QUFWd0IsR0FBL0I7QUFZQSxNQUFNQyxNQUFNLEdBQUcsSUFBSUMsc0RBQUosQ0FBV3pHLFFBQVEsQ0FBQzBHLGFBQVQsQ0FBdUIsMEJBQXZCLENBQVgsRUFBK0Q7QUFDN0VDLG1CQUFlLEVBQUUsSUFENEQ7QUFFN0VDLGFBQVMsRUFBR0MsNkRBQVFBO0FBRnlELEdBQS9ELENBQWY7O0FBS0EsTUFBTUMsVUFBVSxHQUFDLFNBQVhBLFVBQVcsR0FBYztBQUFBLFFBQWJDLE9BQWEsdUVBQUwsRUFBSztBQUM5QixRQUFNQyxRQUFRLEdBQUdoSCxRQUFRLENBQUNpSCxnQkFBVCxDQUEwQiw4RkFBMUIsQ0FBakI7O0FBQ0EsdUJBQUlELFFBQUosRUFBYy9ILEdBQWQsQ0FBa0IsVUFBQWlJLElBQUksRUFBSTtBQUN6QkEsVUFBSSxDQUFDbEMsZ0JBQUwsQ0FBc0IsUUFBdEIsRUFBZ0MsVUFBQ0QsQ0FBRCxFQUFPO0FBQ3RDLFlBQUltQyxJQUFJLENBQUNDLE9BQUwsS0FBaUIsSUFBckIsRUFBMkI7QUFDMUJKLGlCQUFPLENBQUNLLElBQVIsQ0FBYUYsSUFBSSxDQUFDRyxLQUFsQjtBQUNBLFNBRkQsTUFFTztBQUNOTixpQkFBTyxDQUFDTyxNQUFSLENBQWVQLE9BQU8sQ0FBQ1EsT0FBUixDQUFnQkwsSUFBSSxDQUFDRyxLQUFyQixDQUFmLEVBQTJDLENBQTNDO0FBQ0E7O0FBQ0RHLGtCQUFVLENBQUNULE9BQUQsQ0FBVjtBQUNBLE9BUEQ7QUFRQSxLQVREOztBQVVBUCxVQUFNLENBQ0oxQixFQURGLENBQ0ssS0FETCxFQUNZLFVBQUNDLENBQUQsRUFBTztBQUNqQmdDLGFBQU8sQ0FBQ0ssSUFBUixDQUFhckMsQ0FBQyxDQUFDMEMsTUFBRixDQUFTQyxJQUFULENBQWNDLFlBQTNCO0FBQ0FILGdCQUFVLENBQUNULE9BQUQsQ0FBVjtBQUNBLEtBSkYsRUFLRWpDLEVBTEYsQ0FLSyxRQUxMLEVBS2UsVUFBQ0MsQ0FBRCxFQUFPO0FBQ3BCZ0MsYUFBTyxDQUFDTyxNQUFSLENBQWVQLE9BQU8sQ0FBQ1EsT0FBUixDQUFnQnhDLENBQUMsQ0FBQzBDLE1BQUYsQ0FBU0MsSUFBVCxDQUFjQyxZQUE5QixDQUFmLEVBQTJELENBQTNEO0FBQ0FILGdCQUFVLENBQUNULE9BQUQsQ0FBVjtBQUNELEtBUkQ7QUFTQXRCLGNBQVUsQ0FBQ0MsVUFBWCxDQUFzQlosRUFBdEIsQ0FBeUIsUUFBekIsRUFBbUMsVUFBQzRDLElBQUQsRUFBVTtBQUM1QyxVQUFLQSxJQUFJLENBQUMsQ0FBRCxDQUFKLEdBQVUsQ0FBWCxJQUFrQkEsSUFBSSxDQUFDLENBQUQsQ0FBSixHQUFVLEdBQWhDLEVBQXNDO0FBQ3JDWCxlQUFPLENBQUN6RCxRQUFSLEdBQW1CLGlCQUFpQm9FLElBQUksQ0FBQyxDQUFELENBQXJCLEdBQTJCLG1CQUEzQixHQUFpREEsSUFBSSxDQUFDLENBQUQsQ0FBeEU7QUFDQSxPQUZELE1BRU8sSUFBS0EsSUFBSSxDQUFDLENBQUQsQ0FBSixHQUFVLENBQVgsSUFBa0JBLElBQUksQ0FBQyxDQUFELENBQUosSUFBVyxHQUFqQyxFQUF1QztBQUM3Q1gsZUFBTyxDQUFDekQsUUFBUixHQUFvQixnQkFBZ0JvRSxJQUFJLENBQUMsQ0FBRCxDQUF4QztBQUNBLE9BRk0sTUFFQSxJQUFLQSxJQUFJLENBQUMsQ0FBRCxDQUFKLElBQVcsQ0FBWixJQUFtQkEsSUFBSSxDQUFDLENBQUQsQ0FBSixHQUFVLEdBQWpDLEVBQXVDO0FBQzdDWCxlQUFPLENBQUN6RCxRQUFSLEdBQW1CLGlCQUFpQm9FLElBQUksQ0FBQyxDQUFELENBQXhDO0FBQ0EsT0FGTSxNQUVBO0FBQ05YLGVBQU8sQ0FBQ3pELFFBQVIsR0FBaUIsSUFBakI7QUFDQTs7QUFDRGtFLGdCQUFVLENBQUNULE9BQUQsQ0FBVjtBQUNBLEtBWEQ7QUFZQW5CLGVBQVcsQ0FBQ0YsVUFBWixDQUF1QlosRUFBdkIsQ0FBMEIsUUFBMUIsRUFBb0MsVUFBQzRDLElBQUQsRUFBVTtBQUM3QyxVQUFLQSxJQUFJLENBQUMsQ0FBRCxDQUFKLEdBQVUsQ0FBWCxJQUFrQkEsSUFBSSxDQUFDLENBQUQsQ0FBSixHQUFVLElBQWhDLEVBQXVDO0FBQ3RDWCxlQUFPLENBQUNwRCxRQUFSLEdBQW1CLGlCQUFpQitELElBQUksQ0FBQyxDQUFELENBQXJCLEdBQTJCLG1CQUEzQixHQUFpREEsSUFBSSxDQUFDLENBQUQsQ0FBeEU7QUFDQSxPQUZELE1BRU8sSUFBS0EsSUFBSSxDQUFDLENBQUQsQ0FBSixHQUFVLENBQVgsSUFBa0JBLElBQUksQ0FBQyxDQUFELENBQUosSUFBVyxJQUFqQyxFQUF3QztBQUM5Q1gsZUFBTyxDQUFDcEQsUUFBUixHQUFvQixnQkFBZ0IrRCxJQUFJLENBQUMsQ0FBRCxDQUF4QztBQUNBLE9BRk0sTUFFQSxJQUFLQSxJQUFJLENBQUMsQ0FBRCxDQUFKLElBQVcsQ0FBWixJQUFtQkEsSUFBSSxDQUFDLENBQUQsQ0FBSixHQUFVLElBQWpDLEVBQXdDO0FBQzlDWCxlQUFPLENBQUNwRCxRQUFSLEdBQW1CLGlCQUFpQitELElBQUksQ0FBQyxDQUFELENBQXhDO0FBQ0EsT0FGTSxNQUVBO0FBQ05YLGVBQU8sQ0FBQ3BELFFBQVIsR0FBaUIsSUFBakI7QUFDQTs7QUFDRDZELGdCQUFVLENBQUNULE9BQUQsQ0FBVjtBQUNBLEtBWEQ7QUFZQSxHQTdDRDs7QUE2Q0VELFlBQVU7O0FBQ1osTUFBTVUsVUFBVSxHQUFHLFNBQWJBLFVBQWEsQ0FBQ1QsT0FBRCxFQUEyQjtBQUFBLFFBQWpCYSxTQUFpQix1RUFBUCxFQUFPO0FBQzdDLFFBQUlDLElBQUksR0FBR2QsT0FBTyxDQUFDZSxNQUFSLENBQWUsVUFBQUMsQ0FBQztBQUFBLGFBQUlBLENBQUMsQ0FBQ0MsUUFBRixDQUFXLG9CQUFYLENBQUo7QUFBQSxLQUFoQixFQUFzREMsSUFBdEQsQ0FBMkQsTUFBM0QsQ0FBWDtBQUNBLFFBQUlDLFFBQVEsR0FBR25CLE9BQU8sQ0FBQ2UsTUFBUixDQUFlLFVBQUFDLENBQUM7QUFBQSxhQUFJQSxDQUFDLENBQUNDLFFBQUYsQ0FBVyxXQUFYLENBQUo7QUFBQSxLQUFoQixFQUE2Q0MsSUFBN0MsQ0FBa0QsTUFBbEQsQ0FBZjtBQUNBLFFBQUlsRyxVQUFVLEdBQUdnRixPQUFPLENBQUNlLE1BQVIsQ0FBZSxVQUFBQyxDQUFDO0FBQUEsYUFBSUEsQ0FBQyxDQUFDQyxRQUFGLENBQVcsWUFBWCxDQUFKO0FBQUEsS0FBaEIsRUFBOENDLElBQTlDLENBQW1ELE1BQW5ELENBQWpCO0FBQ0EsUUFBSUUsS0FBSyxHQUFHcEIsT0FBTyxDQUFDZSxNQUFSLENBQWUsVUFBQUMsQ0FBQztBQUFBLGFBQUlBLENBQUMsQ0FBQ0MsUUFBRixDQUFXLGdCQUFYLENBQUo7QUFBQSxLQUFoQixFQUFrREMsSUFBbEQsQ0FBdUQsTUFBdkQsQ0FBWjtBQUNBLFFBQUlHLElBQUksR0FBR3JCLE9BQU8sQ0FBQ2UsTUFBUixDQUFlLFVBQUFDLENBQUM7QUFBQSxhQUFJQSxDQUFDLENBQUNDLFFBQUYsQ0FBVyxjQUFYLENBQUo7QUFBQSxLQUFoQixFQUFnREMsSUFBaEQsQ0FBcUQsTUFBckQsQ0FBWDtBQUNBLFFBQUlqRSxRQUFRLEdBQUcrQyxPQUFPLENBQUNlLE1BQVIsQ0FBZSxVQUFBQyxDQUFDO0FBQUEsYUFBSUEsQ0FBQyxDQUFDQyxRQUFGLENBQVcsbUJBQVgsQ0FBSjtBQUFBLEtBQWhCLEVBQXFEQyxJQUFyRCxDQUEwRCxNQUExRCxDQUFmO0FBRUFKLFFBQUksQ0FBQ25GLE1BQUwsR0FBWSxDQUFaLEdBQWdCa0YsU0FBUyxDQUFDUixJQUFWLFlBQW1CUyxJQUFuQixPQUFoQixHQUE4QyxLQUE5QztBQUNBSyxZQUFRLENBQUN4RixNQUFULEdBQWdCLENBQWhCLEdBQW9Ca0YsU0FBUyxDQUFDUixJQUFWLFlBQW1CYyxRQUFuQixPQUFwQixHQUFzRCxLQUF0RDtBQUNBbkcsY0FBVSxDQUFDVyxNQUFYLEdBQWtCLENBQWxCLEdBQXNCa0YsU0FBUyxDQUFDUixJQUFWLFlBQW1CckYsVUFBbkIsT0FBdEIsR0FBMEQsS0FBMUQ7QUFDQW9HLFNBQUssQ0FBQ3pGLE1BQU4sR0FBYSxDQUFiLEdBQWlCa0YsU0FBUyxDQUFDUixJQUFWLFlBQW1CZSxLQUFuQixPQUFqQixHQUFnRCxLQUFoRDtBQUNBQyxRQUFJLENBQUMxRixNQUFMLEdBQVksQ0FBWixHQUFnQmtGLFNBQVMsQ0FBQ1IsSUFBVixZQUFtQmdCLElBQW5CLE9BQWhCLEdBQThDLEtBQTlDO0FBQ0FwRSxZQUFRLENBQUN0QixNQUFULEdBQWdCLENBQWhCLEdBQW9Ca0YsU0FBUyxDQUFDUixJQUFWLFlBQW1CcEQsUUFBbkIsT0FBcEIsR0FBc0QsS0FBdEQ7QUFDQStDLFdBQU8sQ0FBQ3pELFFBQVIsR0FBbUJzRSxTQUFTLENBQUNSLElBQVYsWUFBbUJMLE9BQU8sQ0FBQ3pELFFBQTNCLE9BQW5CLEdBQTZELEtBQTdEO0FBQ0F5RCxXQUFPLENBQUNwRCxRQUFSLEdBQW1CaUUsU0FBUyxDQUFDUixJQUFWLFlBQW1CTCxPQUFPLENBQUNwRCxRQUEzQixPQUFuQixHQUE2RCxLQUE3RDtBQUVBLFFBQUkwRSxNQUFNLEdBQUdULFNBQVMsQ0FBQ0ssSUFBVixDQUFlLE9BQWYsQ0FBYjtBQUNBSyxXQUFPLENBQUNDLEdBQVIsQ0FBWUYsTUFBWjtBQUNBaEgsZUFBVyxDQUFDNkQsUUFBWixDQUFxQm1ELE1BQXJCO0FBQ0F4RCx1RkFBc0IsQ0FBRTVGLEdBQUYsRUFBT29DLFdBQVAsQ0FBdEI7QUFDQSxHQXJCRDtBQXNCQTs7QUFJQTs7O0FBQ0EsTUFBTW1ILGFBQWEsR0FBR3RKLENBQUMsQ0FBQ1MsSUFBRixDQUFPOEksU0FBUCxDQUFpQkMsU0FBakIsQ0FBMkI7QUFDaERDLGdCQUFZLEVBQUUsS0FEa0M7QUFFaERDLFNBQUssRUFBRSxpQ0FGeUM7QUFHaERDLGVBQVcsRUFBRSw4QkFIbUM7QUFJaERDLFlBQVEsRUFBRSxJQUpzQztBQUtoREMsdUJBQW1CLEVBQUUsS0FMMkI7QUFNaEQ5SCxZQUFRLEVBQUUsU0FOc0M7QUFPaEQrSCxhQUFTLEVBQUUsQ0FDVkMseUVBRFUsRUFFVi9KLENBQUMsQ0FBQ1MsSUFBRixDQUFPOEksU0FBUCxDQUFpQlMsb0JBQWpCLENBQXNDO0FBQUU7QUFDdkMxSCxTQUFHLEVBQUVDLHdFQUFtQixDQUFDQyxrQkFEWTtBQUVyQ0MsV0FBSyxFQUFFTixXQUFXLENBQUM4RCxRQUFaLEVBRjhCO0FBR3JDZ0Usa0JBQVksRUFBRSxDQUFDLEtBQUQsQ0FIdUI7QUFJckNDLFdBQUssRUFBRSx3QkFKOEI7QUFLckNDLGdCQUFVLEVBQUUsQ0FMeUI7QUFNckNDLGtCQUFZLEVBQUUsSUFOdUI7QUFPckNDLHNCQUFnQixFQUFFLDBCQUFVMUgsT0FBVixFQUFtQjtBQUNwQyx5QkFBVUEsT0FBTyxDQUFDRyxVQUFSLENBQW1CbUIsR0FBN0IsZ0JBQXNDcUcsa0VBQWEsQ0FBQzNILE9BQU8sQ0FBQ0csVUFBUixDQUFtQmlDLGlCQUFwQixDQUFuRDtBQUNBO0FBVG9DLEtBQXRDLENBRlU7QUFQcUMsR0FBM0IsRUFxQm5CeEUsS0FyQm1CLENBcUJiUixHQXJCYSxDQUF0QixDQXROd0IsQ0E0T3hCOztBQUNBLE1BQU13SyxZQUFZLEdBQUdqQixhQUFhLENBQUNrQixZQUFkLEVBQXJCO0FBQ0FDLDhFQUFlLENBQUNGLFlBQUQsRUFBZXpKLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixVQUF4QixDQUFmLENBQWYsQ0E5T3dCLENBK094Qjs7QUFDQXVJLGVBQWEsQ0FBQzFELEVBQWQsQ0FBaUIsWUFBakIsRUFBK0IsWUFBWTtBQUMxQzBELGlCQUFhLENBQUNvQixPQUFkLENBQXNCWixTQUF0QixDQUFnQyxDQUFoQyxFQUFtQ1ksT0FBbkMsQ0FBMkNqSSxLQUEzQyxHQUFtRE4sV0FBVyxDQUFDOEQsUUFBWixFQUFuRDtBQUNBLEdBRkQsRUFoUHdCLENBbVB4Qjs7QUFDQXFELGVBQWEsQ0FBQzFELEVBQWQsQ0FBaUIsU0FBakIsRUFBNEIsVUFBVUMsQ0FBVixFQUFhO0FBQ3hDTSwwRUFBaUIsQ0FBQ0MsV0FBbEI7QUFDQSxRQUFJdUUsZUFBZSxHQUFHOUUsQ0FBQyxDQUFDK0UsT0FBRixDQUFVLENBQVYsRUFBYTlILFVBQWIsQ0FBd0JxQixtQkFBOUM7O0FBQ0EsU0FBSyxJQUFJMEcsQ0FBQyxHQUFHaEYsQ0FBQyxDQUFDK0UsT0FBRixDQUFVcEgsTUFBVixHQUFtQixDQUFoQyxFQUFtQ3FILENBQUMsSUFBSSxDQUF4QyxFQUEyQ0EsQ0FBQyxFQUE1QyxFQUFnRDtBQUMvQyxVQUFJN0gsTUFBTSxHQUFHNkMsQ0FBQyxDQUFDK0UsT0FBRixDQUFVQyxDQUFWLEVBQWFqSSxNQUExQjs7QUFDQSxVQUFJaUQsQ0FBQyxDQUFDK0UsT0FBRixDQUFVLENBQVYsRUFBYTlILFVBQWIsQ0FBd0JxQixtQkFBeEIsSUFBK0MsSUFBbkQsRUFBeUQ7QUFDeERuRSxTQUFDLENBQUM4SyxLQUFGLEdBQVVDLFNBQVYsQ0FBb0IvSCxNQUFwQixFQUE0QmdJLFVBQTVCLENBQXVDbkYsQ0FBQyxDQUFDb0YsSUFBekMsRUFBK0NDLE1BQS9DLENBQXNEbkwsR0FBdEQ7QUFDQTtBQUNEOztBQUNEMkYscUJBQWlCLENBQUN2RCxXQUFELEVBQWN3SSxlQUFkLENBQWpCO0FBQ0EsR0FWRDtBQVdBOztBQUVBOztBQUNBLE1BQU16RSxXQUFXLEdBQUcsU0FBZEEsV0FBYyxHQUEwQjtBQUFBLFFBQXpCaUYsaUJBQXlCLHVFQUFQLEVBQU87QUFDN0MsUUFBSUMsWUFBWSxHQUFHdEssUUFBUSxDQUFDQyxjQUFULENBQXdCLFVBQXhCLENBQW5COztBQUNBLFdBQU1xSyxZQUFZLENBQUNDLFVBQW5CLEVBQThCO0FBQzdCRCxrQkFBWSxDQUFDRSxXQUFiLENBQXlCRixZQUFZLENBQUNDLFVBQXRDO0FBQ0E7O0FBQ0RsSixlQUFXLENBQUNvSixpQkFBWixDQUE4QixVQUFVeEgsS0FBVixFQUFpQjtBQUFFO0FBQ2hELFVBQUloRSxHQUFHLENBQUN5TCxRQUFKLENBQWFySixXQUFiLENBQUosRUFBK0I7QUFDOUIsWUFBSXBDLEdBQUcsQ0FBQzBMLFNBQUosR0FBZ0JDLFFBQWhCLENBQXlCM0gsS0FBSyxDQUFDNEgsU0FBTixFQUF6QixDQUFKLEVBQWlEO0FBQ2hEUiwyQkFBaUIsQ0FBQ2pELElBQWxCLENBQ0MwRCx1QkFBdUIsQ0FDdEI3SCxLQUFLLENBQUNwQixPQUFOLENBQWNHLFVBQWQsQ0FBeUJtQixHQURILEVBRXRCTSxpRUFBWSxDQUFDUixLQUFLLENBQUNwQixPQUFOLENBQWNHLFVBQWQsQ0FBeUIwQixTQUExQixDQUZVLEVBR3RCRyxtRUFBYyxDQUFDWixLQUFLLENBQUNwQixPQUFOLENBQWNHLFVBQWQsQ0FBeUJDLFVBQTFCLENBSFEsRUFJdEJnQixLQUFLLENBQUNwQixPQUFOLENBQWNHLFVBQWQsQ0FBeUJ1QixRQUpILEVBS3RCTixLQUFLLENBQUNwQixPQUFOLENBQWNHLFVBQWQsQ0FBeUI0QixRQUxILEVBTXRCWCxLQUFLLENBQUNwQixPQUFOLENBQWNHLFVBQWQsQ0FBeUJpQyxpQkFOSCxFQU90QmhCLEtBQUssQ0FBQ3BCLE9BQU4sQ0FBY0csVUFBZCxDQUF5QitJLGNBUEgsRUFRdEI5SCxLQUFLLENBQUNwQixPQUFOLENBQWNHLFVBQWQsQ0FBeUJnSixPQVJILEVBU3RCL0gsS0FBSyxDQUFDcEIsT0FBTixDQUFjRyxVQUFkLENBQXlCcUIsbUJBVEgsRUFVdEJKLEtBQUssQ0FBQ3BCLE9BQU4sQ0FBY29KLFFBQWQsQ0FBdUJDLFdBQXZCLENBQW1DLENBQW5DLENBVnNCLEVBV3RCakksS0FBSyxDQUFDcEIsT0FBTixDQUFjb0osUUFBZCxDQUF1QkMsV0FBdkIsQ0FBbUMsQ0FBbkMsQ0FYc0IsQ0FEeEI7QUFlQTtBQUNEO0FBQ0QsS0FwQkQ7QUFxQkFiLHFCQUFpQixDQUFDYyxPQUFsQjtBQUNBQyxvRkFBbUIsQ0FBQ2YsaUJBQUQsRUFBb0JoSixXQUFwQixFQUFpQ3VELGlCQUFqQyxDQUFuQjtBQUNBLEdBNUJEOztBQTZCQSxNQUFNa0csdUJBQXVCLEdBQUcsU0FBMUJBLHVCQUEwQixDQUFDNUgsR0FBRCxFQUFLTSxRQUFMLEVBQWN6QixVQUFkLEVBQXlCdUIsUUFBekIsRUFBa0NLLFFBQWxDLEVBQTJDMEgsUUFBM0MsRUFBb0RDLEtBQXBELEVBQTBEekQsSUFBMUQsRUFBK0R6RSxFQUEvRCxFQUFrRW1JLEdBQWxFLEVBQXNFQyxHQUF0RSxFQUE4RTtBQUFFO0FBQy9HLG9FQUNnRHBJLEVBRGhELDJCQUNpRW1JLEdBRGpFLDJCQUNtRkMsR0FEbkYsK0hBR3dDdEksR0FIeEMsc0hBSXlGTSxRQUp6RixnQ0FLS3pCLFVBQVUsa0dBQXVGQSxVQUF2RixpQkFMZix5QkFNS3VCLFFBQVEsd0dBQTZGQSxRQUE3RixvQkFOYix5QkFPS0ssUUFBUSx3R0FBNkZBLFFBQTdGLG1CQVBiLHlCQVFLMEgsUUFBUSxzR0FBMkY3QixrRUFBYSxDQUFDNkIsUUFBRCxDQUF4RyxpQkFSYixnR0FTd0VDLEtBVHhFLGVBU2tGekQsSUFUbEY7QUFhQSxHQWREO0FBZUE7QUFFQTs7O0FBQ0EsTUFBTWpELGlCQUFpQixHQUFHLFNBQXBCQSxpQkFBb0IsQ0FBQzZHLEtBQUQsRUFBUXJJLEVBQVIsRUFBZTtBQUN4QyxRQUFNc0ksdUJBQXVCLEdBQUd4TSxDQUFDLENBQUNTLElBQUYsQ0FBT2dNLEtBQVAsQ0FBYTtBQUNuQ25LLFNBQUcsRUFBRUMsd0VBQW1CLENBQUNtSztBQURVLEtBQWIsQ0FBaEM7QUFHQSxRQUFNQyxnQkFBZ0IsR0FBRzNNLENBQUMsQ0FBQ1MsSUFBRixDQUFPZ00sS0FBUCxDQUFhO0FBQ3JDbkssU0FBRyxFQUFFQyx3RUFBbUIsQ0FBQ3FLO0FBRFksS0FBYixDQUF6QjtBQUdBL0wsY0FBVSxDQUFDZ00sS0FBWCxDQUFpQkMsS0FBakIsR0FBd0I3TCxRQUFRLEdBQUMsSUFBakM7QUFDQSxRQUFJOEwsa0JBQWtCLEdBQUdqTSxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsYUFBeEIsQ0FBekI7QUFDQSxRQUFJaU0sY0FBYyxHQUFHNUwsUUFBUSxDQUFDQyxNQUE5QjtBQUNBMEwsc0JBQWtCLENBQUM1RSxLQUFuQixHQUF5Qm9FLEtBQUssQ0FBQ3RHLFFBQU4sRUFBekI7QUFDQTtBQUNGOztBQUNFc0csU0FBSyxDQUFDVSxXQUFOLENBQWtCLFVBQVVsSixLQUFWLEVBQWlCO0FBQ2xDLFVBQUdBLEtBQUssQ0FBQ3BCLE9BQU4sQ0FBY0csVUFBZCxDQUF5QnFCLG1CQUF6QixJQUFnREQsRUFBbkQsRUFBc0Q7QUFDckQ2QixnRkFBVyxDQUNWaEcsR0FEVSxFQUVWZ0UsS0FBSyxDQUFDcEIsT0FBTixDQUFjRyxVQUFkLENBQXlCcUIsbUJBRmYsRUFHVkosS0FBSyxDQUFDcEIsT0FBTixDQUFjRyxVQUFkLENBQXlCQyxVQUhmLEVBSVZnSyxrQkFKVSxFQUtWaEosS0FBSyxDQUFDcEIsT0FBTixDQUFjb0osUUFBZCxDQUF1QkMsV0FMYixDQUFYO0FBT0FrQixpRkFBc0IsQ0FBQ0gsa0JBQUQsQ0FBdEI7QUFDQUksaUVBQU0sZUFDTCw0REFBQyxnRUFBRDtBQUNDLG1CQUFTLEVBQUVqSixFQURaO0FBRUMsYUFBRyxFQUFFSCxLQUFLLENBQUNwQixPQUFOLENBQWNHLFVBQWQsQ0FBeUJtQixHQUYvQjtBQUdDLGVBQUssRUFBRUYsS0FBSyxDQUFDcEIsT0FBTixDQUFjRyxVQUFkLENBQXlCc0ssV0FIakM7QUFJQyxrQkFBUSxFQUFFckosS0FBSyxDQUFDcEIsT0FBTixDQUFjRyxVQUFkLENBQXlCdUIsUUFKcEM7QUFLQyxrQkFBUSxFQUFFTixLQUFLLENBQUNwQixPQUFOLENBQWNHLFVBQWQsQ0FBeUI0QixRQUxwQztBQU1DLG9CQUFVLEVBQUVDLG1FQUFjLENBQUNaLEtBQUssQ0FBQ3BCLE9BQU4sQ0FBY0csVUFBZCxDQUF5QkMsVUFBMUIsQ0FOM0I7QUFPQyxrQkFBUSxFQUFFd0IsaUVBQVksQ0FBQ1IsS0FBSyxDQUFDcEIsT0FBTixDQUFjRyxVQUFkLENBQXlCMEIsU0FBMUIsQ0FQdkI7QUFRQyxrQkFBUSxFQUFFVCxLQUFLLENBQUNwQixPQUFOLENBQWNHLFVBQWQsQ0FBeUJpQyxpQkFScEM7QUFTQyx1QkFBYSxFQUFFZ0ksa0JBVGhCO0FBVUMsYUFBRyxFQUFFaE4sR0FWTjtBQVdDLGtCQUFRLEVBQUlrQixRQVhiO0FBWUMsZUFBSyxFQUFFc0wsS0FaUjtBQWFDLGlDQUF1QixFQUFFQyx1QkFiMUI7QUFjQywwQkFBZ0IsRUFBRUcsZ0JBZG5CO0FBZUMsZUFBSyxFQUFFNUksS0FmUjtBQWdCQyxnQkFBTSxFQUFFNUMsa0JBaEJUO0FBaUJDLHdCQUFjLEVBQUU2TCxjQWpCakI7QUFrQkMscUJBQVcsRUFBRUQsa0JBQWtCLENBQUM1RSxLQWxCakM7QUFtQkMsaUJBQU8sRUFBRTlDLE9BbkJWO0FBb0JDLHNCQUFZLEVBQUVjLHNFQXBCZjtBQXFCQyxzQkFBWSxFQUFFa0g7QUFyQmYsVUFESyxFQXVCRE4sa0JBdkJDLENBQU47QUF5QkFPLHNGQUFpQixDQUFDcEosRUFBRCxFQUFLbUIsT0FBTCxFQUFhdEIsS0FBSyxDQUFDcEIsT0FBTixDQUFjRyxVQUFkLENBQXlCeUssSUFBdEMsQ0FBakIsQ0FsQ3FELENBbUNyRDs7QUFDQSxZQUFJQyxNQUFNLEdBQUd2TSxRQUFRLEdBQUNILFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixzQkFBeEIsRUFBZ0RHLFdBQXRFO0FBQ0FMLGtCQUFVLENBQUNnTSxLQUFYLENBQWlCQyxLQUFqQixHQUF1QlUsTUFBTSxHQUFDLElBQTlCO0FBQ0F6TixXQUFHLENBQUMwTixjQUFKO0FBQ0E7QUFDRCxLQXpDRDtBQTBDQSxHQXZERDtBQTBEQSxDQTNXRCxJIiwiZmlsZSI6ImNpcmN1aXRzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuXHRjaXJjdWl0TGF5ZXJHcm91cCxcclxuXHRwb2ludERlcGFydExheWVyR3JvdXAsXHJcblx0em9vbVRvTWFya2Vyc0FmdGVyTG9hZCxcclxuXHRxdWVyeVN0cmluZyxcclxuXHRhZGRSZXN1bHRzVG9TaWRlYmFyLFxyXG5cdGFnb2xGZWF0dXJlU2VydmljZXMsXHJcblx0YXJjZ2lzT25saW5lUHJvdmlkZXIsXHJcblx0aWNvblBvaW50RGVwYXJ0LFxyXG5cdHNzVHlwZUNpcmN1aXQsIFxyXG5cdHR5cGVWZWxvTmFtZSwgXHJcblx0ZGlmZmljdWx0ZU5hbWUsXHJcblx0ZGlmZmljdWx0ZUZvbnRBd2Vzb21lLFxyXG5cdGdlb2xvY2F0aW9uQnV0dG9uLCBcclxuXHRzZXRFc3JpR2VvY29kZXIsIFxyXG5cdHNob3dDaXJjdWl0LFxyXG5cdGJhY2tCdXR0b25OYXZpZ2F0aW9uLCBcclxuXHRuYXZpZ2F0aW9uSGlzdG9yeSxcclxuXHRjb2xzTGF5ZXIsXHJcblx0bGF5ZXJDb250cm9sVGl0bGVzLFxyXG5cdHNpZGViYXJUb2dnbGVDb250cm9sXHJcbn0gZnJvbSBcIi4uL3NlcnZpY2VzL21hcENvbmZpZ1wiO1xyXG5cclxuaW1wb3J0IHtwb3B1cFRlbXBsYXRlUG9pLCBwb3B1cFRlbXBsYXRlQ2lyY3VpdH0gZnJvbSBcIi4uL2NvbXBvbmVudHMvcG9wdXBcIjtcclxuaW1wb3J0IHtkZXB0TGlzdH0gZnJvbSAnLi4vc2VydmljZXMvd2hpdGVMaXN0J1xyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQge3JlbmRlciwgdW5tb3VudENvbXBvbmVudEF0Tm9kZX0gZnJvbSAncmVhY3QtZG9tJztcclxuaW1wb3J0IEluZm9GZWF0dXJlIGZyb20gJy4uL2NvbXBvbmVudHMvSW5mb0ZlYXR1cmUnO1xyXG5pbXBvcnQgVGFnaWZ5IGZyb20gJ0B5YWlyZW8vdGFnaWZ5JztcclxuaW1wb3J0IG5vVWlTbGlkZXIgZnJvbSAnbm91aXNsaWRlcic7XHJcbmltcG9ydCB3TnVtYiBmcm9tIFwid251bWJcIjtcclxuXHJcbihmdW5jdGlvbiBtYXBDaXJjdWl0cyAoKSB7XHJcblx0XHRcclxuXHQvL21hcFxyXG5cdGNvbnN0IG1hcCA9IEwubWFwKCdtYXAnLCB7XHJcblx0XHRjZW50ZXI6IFs0NywgMF0sXHJcblx0XHR6b29tOiA2LFxyXG5cdFx0bWluWm9vbTogMSxcclxuXHRcdHpvb21Db250cm9sOiB0cnVlLFxyXG5cdH0pO1xyXG5cdEwuY29udHJvbC5zY2FsZSgpLmFkZFRvKG1hcCk7Ly9iYXJyZSBlY2hlbGxlXHJcblx0Y29uc3QgZGVmYXVsdExheWVyID0gTC5lc3JpLmJhc2VtYXBMYXllcihcIlRvcG9ncmFwaGljXCIpLmFkZFRvKG1hcCk7XHJcblx0bWFwLmF0dHJpYnV0aW9uQ29udHJvbC5hZGRBdHRyaWJ1dGlvbignPGEgdGFyZ2V0PVwiX2JsYW5rXCIgaHJlZj1cImh0dHBzOi8vZmZ2ZWxvLmZyXCI+RsOpZMOpcmF0aW9uIGZyYW7Dp2Fpc2UgZGUgY3ljbG90b3VyaXNtZTwvYT4nKTtcclxuXHRcclxuXHQvL3ZhcmlhYmxlcyBnbG9iYWxlc1xyXG5cdGNvbnN0IG1hcEVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1hcFwiKTtcclxuXHRjb25zdCBzaWRlYmFyRWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwic2lkZWJhclwiKTtcclxuXHRjb25zdCBtYXBXaWR0aCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibWFwXCIpLm9mZnNldFdpZHRoO1xyXG5cdGNvbnN0IGRlZmF1bHRXaGVyZUNsYXVzZSA9IGxvY2F0aW9uLnNlYXJjaCA/IHF1ZXJ5U3RyaW5nKGxvY2F0aW9uLnNlYXJjaCkgOiBcIjE9MVwiO1xyXG5cclxuXHQvL2JvdXRvbiB0b2dnbGUgc2lkZWJhclxyXG5cdHNpZGViYXJUb2dnbGVDb250cm9sKEwuQ29udHJvbCwgbWFwLCBzaWRlYmFyRWxlbWVudCwgbWFwRWxlbWVudCwgbWFwV2lkdGgpO1xyXG5cclxuXHQvLyBjb250cm9sZSBkZXMgY291Y2hlcyBldCBjaGFuZ2VtZW50IGRlIGZvbmQgZGUgY2FydGVcclxuXHRjb25zdCBiYXNlTGF5ZXJzID0ge1xyXG5cdFx0J1RvcG9ncmFwaGlxdWUgKEVzcmkpJzogZGVmYXVsdExheWVyLFxyXG5cdFx0J1NhdGVsbGl0ZSAoRXNyaSknOiBMLmVzcmkuYmFzZW1hcExheWVyKCdJbWFnZXJ5JyksXHJcblx0XHQnT3BlblN0cmVldE1hcCc6ICBMLnRpbGVMYXllcignaHR0cHM6Ly97c30udGlsZS5vcGVuc3RyZWV0bWFwLm9yZy97en0ve3h9L3t5fS5wbmcnLCB7XHJcblx0XHRcdGF0dHJpYnV0aW9uOiAnJmNvcHk7IDxhIGhyZWY9XCJodHRwczovL3d3dy5vcGVuc3RyZWV0bWFwLm9yZy9jb3B5cmlnaHRcIj5PcGVuU3RyZWV0TWFwPC9hPicsXHJcblx0XHR9KSxcclxuXHR9O1xyXG5cdGNvbnN0IG92ZXJsYXlNYXBzID0ge1xyXG5cdFx0XCJDb2xzXCI6IGNvbHNMYXllcigpXHJcblx0fTtcclxuXHRMLmNvbnRyb2wubGF5ZXJzKGJhc2VMYXllcnMsIG92ZXJsYXlNYXBzLCB7XHJcblx0XHRwb3NpdGlvbjogXCJib3R0b21sZWZ0XCJcclxuXHR9KS5hZGRUbyhtYXApO1xyXG5cdGxheWVyQ29udHJvbFRpdGxlcygpO1xyXG5cdG1hcC5hZGRDb250cm9sKG5ldyBnZW9sb2NhdGlvbkJ1dHRvbihtYXApKTtcclxuXHJcblx0Ly9sYXllciBwb2ludHMgZGVwYXJ0cyBjaXJjdWl0c1xyXG5cdGNvbnN0IHBvaW50RGVwYXJ0ID0gTC5lc3JpLkNsdXN0ZXIuZmVhdHVyZUxheWVyKHtcclxuXHRcdHVybDogYWdvbEZlYXR1cmVTZXJ2aWNlcy5wb2ludERlcGFydENpcmN1aXQsXHJcblx0XHR3aGVyZTogZGVmYXVsdFdoZXJlQ2xhdXNlLFxyXG5cdFx0cG9pbnRUb0xheWVyOiAoZmVhdHVyZSwgbGF0bG5nKSA9PiB7XHJcblx0XHRcdGxldCBkaWZmaWN1bHRlID0gZmVhdHVyZS5wcm9wZXJ0aWVzLkRJRkZJQ1VMVEU7XHJcblx0XHRcdHJldHVybiBMLm1hcmtlcihsYXRsbmcsIHtcclxuXHRcdFx0XHRpY29uOiBpY29uUG9pbnREZXBhcnRbZGlmZmljdWx0ZV1cclxuXHRcdFx0fSk7XHJcblx0XHR9LFxyXG5cdFx0aWNvbkNyZWF0ZUZ1bmN0aW9uOiAoY2x1c3RlcikgPT4ge1xyXG5cdFx0XHRsZXQgY291bnQgPSBjbHVzdGVyLmdldENoaWxkQ291bnQoKTtcclxuXHRcdFx0bGV0IGRpZ2l0cyA9IChjb3VudCArICcnKS5sZW5ndGg7XHJcblx0XHRcdHJldHVybiBMLmRpdkljb24oe1xyXG5cdFx0XHQgIGh0bWw6IGNvdW50LFxyXG5cdFx0XHQgIGNsYXNzTmFtZTogJ2NsdXN0ZXIgZGlnaXRzLScgKyBkaWdpdHMsXHJcblx0XHRcdCAgaWNvblNpemU6IG51bGxcclxuXHRcdFx0fSk7XHJcblx0XHQgIH0sXHJcblx0fSkuYWRkVG8ocG9pbnREZXBhcnRMYXllckdyb3VwKS5hZGRUbyhtYXApO1xyXG5cclxuXHQvL3BvcHVwcyBwb2ludHMgZGVwYXJ0cyBjaXJjdWl0c1xyXG5cdHBvaW50RGVwYXJ0LmJpbmRQb3B1cCgobGF5ZXIpPT57XHJcblx0XHRsZXQgbm9tID0gbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk5PTTtcclxuXHRcdGxldCBpZCA9IGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5JREVOVElGSUFOVF9DSVJDVUlUO1xyXG5cdFx0bGV0IGRpc3RhbmNlID0gbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkRJU1RBTkNFO1xyXG5cdFx0bGV0IHR5cGV2ZWxvID0gdHlwZVZlbG9OYW1lW2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5UWVBFX1ZFTE9dO1xyXG5cdFx0bGV0IGRlbml2ZWxlID0gbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkRFTklWRUxFO1xyXG5cdFx0bGV0IGRpZmZpY3VsdGUgPSBkaWZmaWN1bHRlTmFtZVtsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuRElGRklDVUxURV07XHJcblx0XHRsZXQgZGlmZmljdWx0ZUljb24gPSBkaWZmaWN1bHRlRm9udEF3ZXNvbWVbbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkRJRkZJQ1VMVEVdO1xyXG5cdFx0bGV0IHNvdXNUeXBlID0gbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLlNPVVNfVFlQRV9DSVJDVUlUO1xyXG5cdFx0cmV0dXJuIEwuVXRpbC50ZW1wbGF0ZShcclxuXHRcdFx0cG9wdXBUZW1wbGF0ZUNpcmN1aXQobm9tLCBpZCwgdHlwZXZlbG8sIGRpc3RhbmNlLCBkZW5pdmVsZSwgZGlmZmljdWx0ZSwgZGlmZmljdWx0ZUljb24sIHNvdXNUeXBlKVxyXG5cdFx0KTtcclxuXHR9KTtcclxuXHJcblx0LypldmVuZW1lbnRzIGNhcnRlIGV0IGxheWVyIHBvaW50IGRlcGFydCovXHJcblx0cG9pbnREZXBhcnQub25jZSgnbG9hZCcsICgpID0+IHtcclxuXHRcdGlmKGxvY2F0aW9uLnBhdGhuYW1lICE9IG1lbnVVcmwpe1xyXG5cdFx0XHRsZXQgcGFydHMgPSBsb2NhdGlvbi5ocmVmLnNwbGl0KG1lbnVVcmwrXCIvXCIpO1xyXG5cdFx0XHRsZXQgdXJsaWQgPSBwYXJ0c1sxXS5zcGxpdChcIi1cIik7XHJcblx0XHRcdHJlbmRlckluZm9GZWF0dXJlKHBvaW50RGVwYXJ0LCB1cmxpZFswXSk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHR6b29tVG9NYXJrZXJzQWZ0ZXJMb2FkKG1hcCwgcG9pbnREZXBhcnQpO1xyXG5cdFx0fTtcclxuXHRcdC8vc3luY1NpZGViYXIoKTtcclxuXHR9KTtcclxuXHRwb2ludERlcGFydC5vbigncG9wdXBvcGVuJywgZSA9PiB7XHJcblx0XHRkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImJ1dHRvblBvcHVwXCIpLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4ge1xyXG5cdFx0XHRyZW5kZXJJbmZvRmVhdHVyZShwb2ludERlcGFydCwgZS5sYXllci5mZWF0dXJlLnByb3BlcnRpZXMuSURFTlRJRklBTlRfQ0lSQ1VJVClcclxuXHRcdH0sIHRydWUpO1xyXG5cdH0pO1xyXG5cdHBvaW50RGVwYXJ0Lm9uKCdjbGljaycsIChlKSA9PiB7XHJcblx0XHRzaG93Q2lyY3VpdChtYXAsIGUubGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLklERU5USUZJQU5UX0NJUkNVSVQsIGUubGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkRJRkZJQ1VMVEUpO1xyXG5cdH0pO1xyXG5cdG1hcC5vbihcIm1vdmVlbmRcIiwgKCkgPT4ge1xyXG5cdFx0cG9pbnREZXBhcnQuc2V0V2hlcmUocG9pbnREZXBhcnQuZ2V0V2hlcmUoKSk7XHJcblx0XHRzeW5jU2lkZWJhcigpO1xyXG5cdH0pO1xyXG5cdG1hcC5vbignb3ZlcmxheXJlbW92ZScsICgpID0+IHtcclxuXHRcdGNpcmN1aXRMYXllckdyb3VwLmNsZWFyTGF5ZXJzKCk7XHJcblx0fSk7XHJcblx0d2luZG93Lm9ucG9wc3RhdGUgPSAoKSA9PiB7XHJcblx0XHRkaXN0U2xpZGVyLm5vVWlTbGlkZXIucmVzZXQoKTtcclxuXHRcdGRlbml2U2xpZGVyLm5vVWlTbGlkZXIucmVzZXQoKTtcclxuXHRcdGJhY2tCdXR0b25OYXZpZ2F0aW9uKG1lbnVVcmwsIG1hcCwgbWFwRWxlbWVudCwgbWFwV2lkdGgsIHBvaW50RGVwYXJ0LCByZW5kZXJJbmZvRmVhdHVyZSk7XHJcblx0fVxyXG5cdC8qZmluIGV2ZW5lbWVudHMgY2FydGUgZXQgbGF5ZXIgcG9pbnQgZGVwYXJ0Ki9cclxuXHJcblx0XHJcblx0LypmaWx0cmVzIGRlIHJlY2hlcmNoZXMgKi9cclxuXHRjb25zdCBkaXN0U2xpZGVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2Rpc3RTbGlkZXInKTtcclxuXHRub1VpU2xpZGVyLmNyZWF0ZShkaXN0U2xpZGVyLCB7XHJcblx0XHRzdGFydDogWzAsIDE1MF0sXHJcblx0XHRjb25uZWN0OiB0cnVlLFxyXG5cdFx0cmFuZ2U6IHtcclxuXHRcdFx0J21pbic6IDAsXHJcblx0XHRcdCdtYXgnOiAxNTBcclxuXHRcdH0sXHJcblx0XHRmb3JtYXQ6IHdOdW1iKHtcclxuXHRcdFx0ZGVjaW1hbHM6IDAsIC8vIGRlZmF1bHQgaXMgMlxyXG5cdFx0fSksXHJcblx0XHRwaXBzOiB7bW9kZTogJ2NvdW50JywgdmFsdWVzOiA1fSxcclxuXHR9KTtcclxuXHRjb25zdCBkZW5pdlNsaWRlciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdkZW5pdlNsaWRlcicpO1xyXG5cdG5vVWlTbGlkZXIuY3JlYXRlKGRlbml2U2xpZGVyLCB7XHJcblx0XHRzdGFydDogWzAsIDEwMDBdLFxyXG5cdFx0Y29ubmVjdDogdHJ1ZSxcclxuXHRcdHJhbmdlOiB7XHJcblx0XHRcdCdtaW4nOiAwLFxyXG5cdFx0XHQnbWF4JzogMTAwMFxyXG5cdFx0fSxcclxuXHRcdGZvcm1hdDogd051bWIoe1xyXG5cdFx0XHRkZWNpbWFsczogMCwgLy8gZGVmYXVsdCBpcyAyXHJcblx0XHR9KSxcclxuXHRcdHBpcHM6IHttb2RlOiAnY291bnQnLCB2YWx1ZXM6IDN9LFxyXG5cdH0pO1xyXG5cdGNvbnN0IHRhZ2lmeSA9IG5ldyBUYWdpZnkoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInRleHRhcmVhW25hbWU9dGFnc0RlcHRzXVwiKSwge1xyXG5cdFx0ZW5mb3JlV2hpdGVsaXN0OiB0cnVlLFxyXG5cdFx0d2hpdGVsaXN0IDogZGVwdExpc3RcclxuXHR9KTtcclxuXHJcblx0Y29uc3QgZ2V0RmlsdGVycz0oZmlsdGVycz1bXSk9PntcclxuXHRcdGNvbnN0IGVsZW1lbnRzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi50eXBlVmVsb0ZpbHRlciwgLmRpZmZpY3VsdGVGaWx0ZXIsIC50ZW1wc0ZpbHRlciwgLnNvdXNUeXBlQ2lyY3VpdEZpbHRlciwgLnR5cGVDaXJjdWl0RmlsdGVyXCIpO1xyXG5cdFx0Wy4uLmVsZW1lbnRzXS5tYXAoaXRlbSA9PiB7XHJcblx0XHRcdGl0ZW0uYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgKGUpID0+IHtcclxuXHRcdFx0XHRpZiAoaXRlbS5jaGVja2VkID09PSB0cnVlKSB7XHJcblx0XHRcdFx0XHRmaWx0ZXJzLnB1c2goaXRlbS52YWx1ZSk7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdGZpbHRlcnMuc3BsaWNlKGZpbHRlcnMuaW5kZXhPZihpdGVtLnZhbHVlKSwxKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0c2V0RmlsdGVycyhmaWx0ZXJzKTtcclxuXHRcdFx0fSk7XHJcblx0XHR9KTtcclxuXHRcdHRhZ2lmeVxyXG5cdFx0XHQub24oJ2FkZCcsIChlKSA9PiB7XHJcblx0XHRcdFx0ZmlsdGVycy5wdXNoKGUuZGV0YWlsLmRhdGEucXVlcnlDaXJjdWl0KTtcclxuXHRcdFx0XHRzZXRGaWx0ZXJzKGZpbHRlcnMpO1xyXG5cdFx0XHR9KVxyXG5cdFx0XHQub24oJ3JlbW92ZScsIChlKSA9PiB7XHJcblx0XHRcdFx0ZmlsdGVycy5zcGxpY2UoZmlsdGVycy5pbmRleE9mKGUuZGV0YWlsLmRhdGEucXVlcnlDaXJjdWl0KSwxKTtcclxuXHRcdFx0XHRzZXRGaWx0ZXJzKGZpbHRlcnMpO1xyXG5cdFx0fSk7XHJcblx0XHRkaXN0U2xpZGVyLm5vVWlTbGlkZXIub24oXCJjaGFuZ2VcIiwgKGRhdGEpID0+IHtcclxuXHRcdFx0aWYgKChkYXRhWzBdID4gMCkgJiYgKGRhdGFbMV0gPCAyNTApKSB7XHJcblx0XHRcdFx0ZmlsdGVycy5kaXN0YW5jZSA9IFwiRElTVEFOQ0UgPj0gXCIgKyBkYXRhWzBdICsgXCIgQU5EIERJU1RBTkNFIDw9IFwiICsgZGF0YVsxXTtcclxuXHRcdFx0fSBlbHNlIGlmICgoZGF0YVswXSA+IDApICYmIChkYXRhWzFdID09IDI1MCkpIHtcclxuXHRcdFx0XHRmaWx0ZXJzLmRpc3RhbmNlID0gIFwiRElTVEFOQ0UgPj1cIiArIGRhdGFbMF07XHJcblx0XHRcdH0gZWxzZSBpZiAoKGRhdGFbMF0gPT0gMCkgJiYgKGRhdGFbMV0gPCAyNTApKSB7XHJcblx0XHRcdFx0ZmlsdGVycy5kaXN0YW5jZSA9IFwiRElTVEFOQ0UgPD0gXCIgKyBkYXRhWzFdO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdGZpbHRlcnMuZGlzdGFuY2U9bnVsbDtcclxuXHRcdFx0fVxyXG5cdFx0XHRzZXRGaWx0ZXJzKGZpbHRlcnMpO1xyXG5cdFx0fSk7XHJcblx0XHRkZW5pdlNsaWRlci5ub1VpU2xpZGVyLm9uKFwiY2hhbmdlXCIsIChkYXRhKSA9PiB7XHJcblx0XHRcdGlmICgoZGF0YVswXSA+IDApICYmIChkYXRhWzFdIDwgMTAwMCkpIHtcclxuXHRcdFx0XHRmaWx0ZXJzLmRlbml2ZWxlID0gXCJERU5JVkVMRSA+PSBcIiArIGRhdGFbMF0gKyBcIiBBTkQgREVOSVZFTEUgPD0gXCIgKyBkYXRhWzFdO1xyXG5cdFx0XHR9IGVsc2UgaWYgKChkYXRhWzBdID4gMCkgJiYgKGRhdGFbMV0gPT0gMTAwMCkpIHtcclxuXHRcdFx0XHRmaWx0ZXJzLmRlbml2ZWxlID0gIFwiREVOSVZFTEUgPj1cIiArIGRhdGFbMF07XHJcblx0XHRcdH0gZWxzZSBpZiAoKGRhdGFbMF0gPT0gMCkgJiYgKGRhdGFbMV0gPCAxMDAwKSkge1xyXG5cdFx0XHRcdGZpbHRlcnMuZGVuaXZlbGUgPSBcIkRFTklWRUxFIDw9IFwiICsgZGF0YVsxXTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRmaWx0ZXJzLmRlbml2ZWxlPW51bGw7XHJcblx0XHRcdH1cclxuXHRcdFx0c2V0RmlsdGVycyhmaWx0ZXJzKTtcclxuXHRcdH0pO1xyXG5cdH07Z2V0RmlsdGVycygpO1xyXG5cdGNvbnN0IHNldEZpbHRlcnMgPSAoZmlsdGVycywgYXJyUmVzdWx0PVtdKSA9PiB7XHJcblx0XHRsZXQgZGVwdCA9IGZpbHRlcnMuZmlsdGVyKGYgPT4gZi5pbmNsdWRlcyhcIk5VTUVST19ERVBBUlRFTUVOVFwiKSkuam9pbihcIiBPUiBcIik7XHJcblx0XHRsZXQgdHlwZVZlbG8gPSBmaWx0ZXJzLmZpbHRlcihmID0+IGYuaW5jbHVkZXMoXCJUWVBFX1ZFTE9cIikpLmpvaW4oXCIgT1IgXCIpO1xyXG5cdFx0bGV0IGRpZmZpY3VsdGUgPSBmaWx0ZXJzLmZpbHRlcihmID0+IGYuaW5jbHVkZXMoXCJESUZGSUNVTFRFXCIpKS5qb2luKFwiIE9SIFwiKTtcclxuXHRcdGxldCB0ZW1wcyA9IGZpbHRlcnMuZmlsdGVyKGYgPT4gZi5pbmNsdWRlcyhcIlRFTVBTX1BBUkNPVVJTXCIpKS5qb2luKFwiIE9SIFwiKTtcclxuXHRcdGxldCB0eXBlID0gZmlsdGVycy5maWx0ZXIoZiA9PiBmLmluY2x1ZGVzKFwiVFlQRV9DSVJDVUlUXCIpKS5qb2luKFwiIE9SIFwiKTtcclxuXHRcdGxldCBzb3VzVHlwZSA9IGZpbHRlcnMuZmlsdGVyKGYgPT4gZi5pbmNsdWRlcyhcIlNPVVNfVFlQRV9DSVJDVUlUXCIpKS5qb2luKFwiIE9SIFwiKTtcclxuXHJcblx0XHRkZXB0Lmxlbmd0aD4wID8gYXJyUmVzdWx0LnB1c2goYCgke2RlcHR9KWApIDogZmFsc2U7XHJcblx0XHR0eXBlVmVsby5sZW5ndGg+MCA/IGFyclJlc3VsdC5wdXNoKGAoJHt0eXBlVmVsb30pYCkgOiBmYWxzZTtcclxuXHRcdGRpZmZpY3VsdGUubGVuZ3RoPjAgPyBhcnJSZXN1bHQucHVzaChgKCR7ZGlmZmljdWx0ZX0pYCkgOiBmYWxzZTtcclxuXHRcdHRlbXBzLmxlbmd0aD4wID8gYXJyUmVzdWx0LnB1c2goYCgke3RlbXBzfSlgKSA6IGZhbHNlO1xyXG5cdFx0dHlwZS5sZW5ndGg+MCA/IGFyclJlc3VsdC5wdXNoKGAoJHt0eXBlfSlgKSA6IGZhbHNlO1xyXG5cdFx0c291c1R5cGUubGVuZ3RoPjAgPyBhcnJSZXN1bHQucHVzaChgKCR7c291c1R5cGV9KWApIDogZmFsc2U7XHJcblx0XHRmaWx0ZXJzLmRpc3RhbmNlID8gYXJyUmVzdWx0LnB1c2goYCgke2ZpbHRlcnMuZGlzdGFuY2V9KWApIDogZmFsc2U7XHJcblx0XHRmaWx0ZXJzLmRlbml2ZWxlID8gYXJyUmVzdWx0LnB1c2goYCgke2ZpbHRlcnMuZGVuaXZlbGV9KWApIDogZmFsc2U7XHJcblxyXG5cdFx0bGV0IHJlc3VsdCA9IGFyclJlc3VsdC5qb2luKFwiIEFORCBcIik7XHJcblx0XHRjb25zb2xlLmxvZyhyZXN1bHQpO1xyXG5cdFx0cG9pbnREZXBhcnQuc2V0V2hlcmUocmVzdWx0KTtcclxuXHRcdHpvb21Ub01hcmtlcnNBZnRlckxvYWQgKG1hcCwgcG9pbnREZXBhcnQpO1xyXG5cdH1cclxuXHQvKmZpbiBkZXMgZmlsdHJlcyBkZSByZWNoZXJjaGVzICovXHJcblxyXG5cclxuXHRcclxuXHQvKiBiYXJyZSBkZSByZWNoZXJjaGUgYWRyZXNzZSBldCBjaXJjdWl0cyAoZ2VvY29kZXVyIGVuIGhhdXQgYSBnYXVjaGUgZGUgbGEgY2FydGUpICovXHJcblx0Y29uc3Qgc2VhcmNoQ29udHJvbCA9IEwuZXNyaS5HZW9jb2RpbmcuZ2Vvc2VhcmNoKHtcclxuXHRcdHVzZU1hcEJvdW5kczogZmFsc2UsXHJcblx0XHR0aXRsZTogJ1JlY2hlcmNoZXIgdW4gbGlldSAvIHVuIGNpcmN1aXQnLFxyXG5cdFx0cGxhY2Vob2xkZXI6ICdUcm91dmVyIHVuIGxpZXUgLyB1biBjaXJjdWl0JyxcclxuXHRcdGV4cGFuZGVkOiB0cnVlLFxyXG5cdFx0Y29sbGFwc2VBZnRlclJlc3VsdDogZmFsc2UsXHJcblx0XHRwb3NpdGlvbjogJ3RvcGxlZnQnLFxyXG5cdFx0cHJvdmlkZXJzOiBbXHJcblx0XHRcdGFyY2dpc09ubGluZVByb3ZpZGVyLFxyXG5cdFx0XHRMLmVzcmkuR2VvY29kaW5nLmZlYXR1cmVMYXllclByb3ZpZGVyKHsgLy9yZWNoZXJjaGUgZGVzIGNvdWNoZXMgZGFucyBsYSBnZGIgdmVmXHJcblx0XHRcdFx0dXJsOiBhZ29sRmVhdHVyZVNlcnZpY2VzLnBvaW50RGVwYXJ0Q2lyY3VpdCxcclxuXHRcdFx0XHR3aGVyZTogcG9pbnREZXBhcnQuZ2V0V2hlcmUoKSxcclxuXHRcdFx0XHRzZWFyY2hGaWVsZHM6IFsnTk9NJ10sXHJcblx0XHRcdFx0bGFiZWw6ICdDSVJDVUlUUyBWRUxPRU5GUkFOQ0U6JyxcclxuXHRcdFx0XHRtYXhSZXN1bHRzOiA3LFxyXG5cdFx0XHRcdGJ1ZmZlclJhZGl1czogMTAwMCxcclxuXHRcdFx0XHRmb3JtYXRTdWdnZXN0aW9uOiBmdW5jdGlvbiAoZmVhdHVyZSkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIGAke2ZlYXR1cmUucHJvcGVydGllcy5OT019IC0gJHtzc1R5cGVDaXJjdWl0W2ZlYXR1cmUucHJvcGVydGllcy5TT1VTX1RZUEVfQ0lSQ1VJVF19YDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pXHJcblx0XHRdXHJcblx0fSkuYWRkVG8obWFwKTtcclxuXHQvL2dlb2NvZGV1ciBlbiBkZWhvcnMgZGUgbGEgY2FydGVcclxuXHRjb25zdCBlc3JpR2VvY29kZXIgPSBzZWFyY2hDb250cm9sLmdldENvbnRhaW5lcigpO1xyXG5cdHNldEVzcmlHZW9jb2Rlcihlc3JpR2VvY29kZXIsIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdnZW9jb2RlcicpKTtcclxuXHQvL3JlY3VwIGxhIHJlcXVldGUgZW4gY291cnMgZXQgYXBwbGlxdWUgYXV4IHJlc3VsdGF0cyBkdSBnZW9jb2RldXJcclxuXHRzZWFyY2hDb250cm9sLm9uKFwicmVxdWVzdGVuZFwiLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRzZWFyY2hDb250cm9sLm9wdGlvbnMucHJvdmlkZXJzWzFdLm9wdGlvbnMud2hlcmUgPSBwb2ludERlcGFydC5nZXRXaGVyZSgpO1xyXG5cdH0pO1xyXG5cdC8vYWZmaWNoZSBsZSB0cmFjZSBkdSBjaXJjdWl0IHNlbGVjdGlvbm5lIGF2ZWMgbGUgZ2VvY29kZXVyXHJcblx0c2VhcmNoQ29udHJvbC5vbihcInJlc3VsdHNcIiwgZnVuY3Rpb24gKGUpIHtcclxuXHRcdGNpcmN1aXRMYXllckdyb3VwLmNsZWFyTGF5ZXJzKCk7XHJcblx0XHR2YXIgaWRDaXJjdWl0UmVzdWx0ID0gZS5yZXN1bHRzWzBdLnByb3BlcnRpZXMuSURFTlRJRklBTlRfQ0lSQ1VJVDtcclxuXHRcdGZvciAobGV0IGkgPSBlLnJlc3VsdHMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHtcclxuXHRcdFx0bGV0IG1hcmtlciA9IGUucmVzdWx0c1tpXS5sYXRsbmc7XHJcblx0XHRcdGlmIChlLnJlc3VsdHNbMF0ucHJvcGVydGllcy5JREVOVElGSUFOVF9DSVJDVUlUID09IG51bGwpIHtcclxuXHRcdFx0XHRMLnBvcHVwKCkuc2V0TGF0TG5nKG1hcmtlcikuc2V0Q29udGVudChlLnRleHQpLm9wZW5PbihtYXApO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRyZW5kZXJJbmZvRmVhdHVyZShwb2ludERlcGFydCwgaWRDaXJjdWl0UmVzdWx0KTtcclxuXHR9KTtcclxuXHQvKiBmaW4gYmFycmUgZGUgcmVjaGVyY2hlIGFkcmVzc2UgZXQgY2lyY3VpdHMgKGdlb2NvZGV1ciBlbiBoYXV0IGEgZ2F1Y2hlIGRlIGxhIGNhcnRlKSAqL1xyXG5cclxuXHQvKmNhcmRzIGRhbnMgbGEgc2lkZWJhciovXHJcblx0Y29uc3Qgc3luY1NpZGViYXIgPSAoYXJyUmVzdWx0c1NpZGViYXI9W10pID0+IHtcclxuXHRcdGxldCBmZWF0dXJlQ2FyZHMgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZmVhdHVyZXMnKTtcclxuXHRcdHdoaWxlKGZlYXR1cmVDYXJkcy5maXJzdENoaWxkKXtcclxuXHRcdFx0ZmVhdHVyZUNhcmRzLnJlbW92ZUNoaWxkKGZlYXR1cmVDYXJkcy5maXJzdENoaWxkKVxyXG5cdFx0fVxyXG5cdFx0cG9pbnREZXBhcnQuZWFjaEFjdGl2ZUZlYXR1cmUoZnVuY3Rpb24gKGxheWVyKSB7IC8vZWFjaEFjdGl2ZUZlYXR1cmUgPSBmb25jdGlvbiBhIGFqb3V0ZXIgZGFucyBlc3JpLWxlYWZsZXQtY2x1c3Rlci5qcyAtIGF0dGVudGlvbiBzaSBtYWogZGUgbGEgbGlicmFpcmllXHJcblx0XHRcdGlmIChtYXAuaGFzTGF5ZXIocG9pbnREZXBhcnQpKSB7XHJcblx0XHRcdFx0aWYgKG1hcC5nZXRCb3VuZHMoKS5jb250YWlucyhsYXllci5nZXRMYXRMbmcoKSkpIHtcclxuXHRcdFx0XHRcdGFyclJlc3VsdHNTaWRlYmFyLnB1c2goXHJcblx0XHRcdFx0XHRcdHNpZGViYXJUZW1wbGF0ZUNpcmN1aXRzKFxyXG5cdFx0XHRcdFx0XHRcdGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5OT00sXHJcblx0XHRcdFx0XHRcdFx0dHlwZVZlbG9OYW1lW2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5UWVBFX1ZFTE9dLFxyXG5cdFx0XHRcdFx0XHRcdGRpZmZpY3VsdGVOYW1lW2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5ESUZGSUNVTFRFXSxcclxuXHRcdFx0XHRcdFx0XHRsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuRElTVEFOQ0UsXHJcblx0XHRcdFx0XHRcdFx0bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkRFTklWRUxFLFxyXG5cdFx0XHRcdFx0XHRcdGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5TT1VTX1RZUEVfQ0lSQ1VJVCxcclxuXHRcdFx0XHRcdFx0XHRsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuQ09NTVVORV9ERVBBUlQsXHJcblx0XHRcdFx0XHRcdFx0bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk5vbV9EZXAsXHJcblx0XHRcdFx0XHRcdFx0bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLklERU5USUZJQU5UX0NJUkNVSVQsXHJcblx0XHRcdFx0XHRcdFx0bGF5ZXIuZmVhdHVyZS5nZW9tZXRyeS5jb29yZGluYXRlc1sxXSxcclxuXHRcdFx0XHRcdFx0XHRsYXllci5mZWF0dXJlLmdlb21ldHJ5LmNvb3JkaW5hdGVzWzBdXHJcblx0XHRcdFx0XHRcdClcclxuXHRcdFx0XHRcdCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHRcdGFyclJlc3VsdHNTaWRlYmFyLnJldmVyc2UoKTtcclxuXHRcdGFkZFJlc3VsdHNUb1NpZGViYXIoYXJyUmVzdWx0c1NpZGViYXIsIHBvaW50RGVwYXJ0LCByZW5kZXJJbmZvRmVhdHVyZSk7XHJcblx0fVxyXG5cdGNvbnN0IHNpZGViYXJUZW1wbGF0ZUNpcmN1aXRzID0gKG5vbSx0eXBldmVsbyxkaWZmaWN1bHRlLGRpc3RhbmNlLGRlbml2ZWxlLHNvdXN0eXBlLHZpbGxlLGRlcHQsaWQsbGF0LGxvbikgPT4geyAvL3RlbXBsYXRlIGNhcmRzXHJcblx0XHRyZXR1cm4gKFxyXG5cdFx0XHRgPGRpdiBjbGFzcz1cImNhcmQgZmVhdHVyZS1jYXJkIG15LTIgcC0yXCIgaWQ9XCIke2lkfVwiIGRhdGEtbGF0PVwiJHtsYXR9XCIgZGF0YS1sb249XCIke2xvbn1cIj4gXHJcblx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbCBhbGlnbi1zZWxmLWNlbnRlciByb3VuZGVkLXJpZ2h0XCI+XHJcblx0XHRcdFx0XHQ8aDYgY2xhc3M9XCJkLWJsb2NrIHRleHQtdXBwZXJjYXNlXCI+JHtub219PC9oNj5cclxuXHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwiYmFkZ2UgYmFkZ2UtcGlsbCBiYWRnZS1saWdodCBteS0xIG1yLTFcIj48aSBjbGFzcz1cImZhcyBmYS1iaWN5Y2xlXCI+PC9pPiAke3R5cGV2ZWxvfTwvc3Bhbj5cclxuXHRcdFx0XHRcdCR7ZGlmZmljdWx0ZSA/IGA8c3BhbiBjbGFzcz1cImJhZGdlIGJhZGdlLXBpbGwgYmFkZ2UtbGlnaHQgbXktMSBtci0xXCI+PGkgY2xhc3M9XCJmYXIgZmEtc3RhclwiPjwvaT4gJHtkaWZmaWN1bHRlfTwvc3Bhbj5gIDogYGB9XHJcblx0XHRcdFx0XHQke2Rpc3RhbmNlID8gYDxzcGFuIGNsYXNzPVwiYmFkZ2UgYmFkZ2UtcGlsbCBiYWRnZS1saWdodCBteS0xIG1yLTFcIj48aSBjbGFzcz1cImZhcyBmYS1mdyBmYS1yb2FkXCI+PC9pPiAke2Rpc3RhbmNlfSBrbTwvc3Bhbj5gIDogYGB9XHJcblx0XHRcdFx0XHQke2Rlbml2ZWxlID8gYDxzcGFuIGNsYXNzPVwiYmFkZ2UgYmFkZ2UtcGlsbCBiYWRnZS1saWdodCBteS0xIG1yLTFcIj48aSBjbGFzcz1cImZhcyBmYS1jaGFydC1saW5lXCI+PC9pPiAke2Rlbml2ZWxlfSBtPC9zcGFuPmAgOiBgYH1cclxuXHRcdFx0XHRcdCR7c291c3R5cGUgPyBgPHNwYW4gY2xhc3M9XCJiYWRnZSBiYWRnZS1waWxsIGJhZGdlLWxpZ2h0IG15LTEgbXItMVwiPjxpIGNsYXNzTmFtZT1cImZhcyBmYS10YWdzXCI+PC9pPiAke3NzVHlwZUNpcmN1aXRbc291c3R5cGVdfTwvc3Bhbj5gIDogYGB9XHJcblx0XHRcdFx0XHQ8c3BhbiBjbGFzcz1cIm15LTEgbXItMVwiPjxici8+PGkgY2xhc3M9XCJmYXMgZmEtbWFwLW1hcmtlZC1hbHRcIj48L2k+ICR7dmlsbGV9LCAke2RlcHR9PC9zcGFuPlxyXG5cdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHQ8L2Rpdj5gXHJcblx0XHQpO1xyXG5cdH1cclxuXHQvKiBmaW4gZGVzIGNhcmRzIGRhbnMgbGEgc2lkZWJhciovXHJcblx0XHJcblx0Ly9jb21wb3NhbnQgZmljaGUgZGVzY3JpcHRpdmVcclxuXHRjb25zdCByZW5kZXJJbmZvRmVhdHVyZSA9IChMYXllciwgaWQpID0+IHtcclxuXHRcdGNvbnN0IHF1ZXJ5Q2lyY3VpdEZvdXJuaXNzZXVyID0gTC5lc3JpLnF1ZXJ5KHtcclxuICAgICAgICAgICAgdXJsOiBhZ29sRmVhdHVyZVNlcnZpY2VzLmNpcmN1aXRGb3Vybmlzc2V1cixcclxuICAgICAgICB9KTtcclxuXHRcdGNvbnN0IHF1ZXJ5Rm91cm5pc3NldXIgPSBMLmVzcmkucXVlcnkoe1xyXG5cdFx0XHR1cmw6IGFnb2xGZWF0dXJlU2VydmljZXMuZm91cm5pc3NldXJcclxuXHRcdH0pO1xyXG5cdFx0bWFwRWxlbWVudC5zdHlsZS53aWR0aD0gbWFwV2lkdGgrXCJweFwiO1xyXG5cdFx0bGV0IGluZm9mZWF0dXJlRWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdpbmZvZmVhdHVyZScpO1xyXG5cdFx0bGV0IGxvY2F0aW9uU2VhcmNoID0gbG9jYXRpb24uc2VhcmNoO1xyXG5cdFx0aW5mb2ZlYXR1cmVFbGVtZW50LnZhbHVlPUxheWVyLmdldFdoZXJlKCk7XHJcblx0XHQvKmxldCB0b2dnbGVCdXR0b24gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgndG9nZ2xlQnV0dG9uJyk7XHJcblx0XHR0b2dnbGVCdXR0b24uc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiOyovXHJcblx0XHRMYXllci5lYWNoRmVhdHVyZShmdW5jdGlvbiAobGF5ZXIpIHtcclxuXHRcdFx0aWYobGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLklERU5USUZJQU5UX0NJUkNVSVQgPT0gaWQpe1xyXG5cdFx0XHRcdHNob3dDaXJjdWl0KFxyXG5cdFx0XHRcdFx0bWFwLCBcclxuXHRcdFx0XHRcdGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5JREVOVElGSUFOVF9DSVJDVUlULCBcclxuXHRcdFx0XHRcdGxheWVyLmZlYXR1cmUucHJvcGVydGllcy5ESUZGSUNVTFRFLCBcclxuXHRcdFx0XHRcdGluZm9mZWF0dXJlRWxlbWVudCwgXHJcblx0XHRcdFx0XHRsYXllci5mZWF0dXJlLmdlb21ldHJ5LmNvb3JkaW5hdGVzXHJcblx0XHRcdFx0KTtcclxuXHRcdFx0XHR1bm1vdW50Q29tcG9uZW50QXROb2RlKGluZm9mZWF0dXJlRWxlbWVudCk7XHJcblx0XHRcdFx0cmVuZGVyKFx0XHJcblx0XHRcdFx0XHQ8SW5mb0ZlYXR1cmUgXHJcblx0XHRcdFx0XHRcdGlkQ2lyY3VpdD17aWR9XHJcblx0XHRcdFx0XHRcdG5vbT17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLk5PTX1cclxuXHRcdFx0XHRcdFx0ZGVzY3I9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5ERVNDUklQVElPTn1cclxuXHRcdFx0XHRcdFx0ZGlzdGFuY2U9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5ESVNUQU5DRX1cclxuXHRcdFx0XHRcdFx0ZGVuaXZlbGU9e2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5ERU5JVkVMRX1cclxuXHRcdFx0XHRcdFx0ZGlmZmljdWx0ZT17ZGlmZmljdWx0ZU5hbWVbbGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkRJRkZJQ1VMVEVdfVxyXG5cdFx0XHRcdFx0XHR0eXBldmVsbz17dHlwZVZlbG9OYW1lW2xheWVyLmZlYXR1cmUucHJvcGVydGllcy5UWVBFX1ZFTE9dfVxyXG5cdFx0XHRcdFx0XHRzb3VzdHlwZT17bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLlNPVVNfVFlQRV9DSVJDVUlUfVxyXG5cdFx0XHRcdFx0XHRyb290Q29udGFpbmVyPXtpbmZvZmVhdHVyZUVsZW1lbnR9XHJcblx0XHRcdFx0XHRcdG1hcD17bWFwfVxyXG5cdFx0XHRcdFx0XHRtYXBXaWR0aCA9IHttYXBXaWR0aH1cclxuXHRcdFx0XHRcdFx0TGF5ZXI9e0xheWVyfVxyXG5cdFx0XHRcdFx0XHRxdWVyeUNpcmN1aXRGb3Vybmlzc2V1cj17cXVlcnlDaXJjdWl0Rm91cm5pc3NldXJ9XHJcblx0XHRcdFx0XHRcdHF1ZXJ5Rm91cm5pc3NldXI9e3F1ZXJ5Rm91cm5pc3NldXJ9XHJcblx0XHRcdFx0XHRcdGxheWVyPXtsYXllcn1cclxuXHRcdFx0XHRcdFx0cGFyYW1zPXtkZWZhdWx0V2hlcmVDbGF1c2V9XHJcblx0XHRcdFx0XHRcdGxvY2F0aW9uU2VhcmNoPXtsb2NhdGlvblNlYXJjaH1cclxuXHRcdFx0XHRcdFx0d2hlcmVDbGF1c2U9e2luZm9mZWF0dXJlRWxlbWVudC52YWx1ZX1cclxuXHRcdFx0XHRcdFx0bWVudVVybD17bWVudVVybH1cclxuXHRcdFx0XHRcdFx0Y2lyY3VpdExheWVyPXtjaXJjdWl0TGF5ZXJHcm91cH1cclxuXHRcdFx0XHRcdFx0dG9nZ2xlQnV0dG9uPXt0b2dnbGVCdXR0b259XHJcblx0XHRcdFx0XHQvPiwgaW5mb2ZlYXR1cmVFbGVtZW50XHJcblx0XHRcdFx0KTtcclxuXHRcdFx0XHRuYXZpZ2F0aW9uSGlzdG9yeShpZCwgbWVudVVybCxsYXllci5mZWF0dXJlLnByb3BlcnRpZXMuU0xVRyk7XHJcblx0XHRcdFx0Ly9nZXN0aW9uIGRlIGxhIGNhcnRlXHJcblx0XHRcdFx0bGV0IHJlc2l6ZSA9IG1hcFdpZHRoLWRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiaW5mby1mZWF0dXJlLWNvbnRlbnRcIikub2Zmc2V0V2lkdGg7XHJcblx0XHRcdFx0bWFwRWxlbWVudC5zdHlsZS53aWR0aD1yZXNpemUrXCJweFwiO1xyXG5cdFx0XHRcdG1hcC5pbnZhbGlkYXRlU2l6ZSgpO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cdFxyXG59KSgpO1xyXG5cclxuXHJcbiJdLCJzb3VyY2VSb290IjoiIn0=