(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cotation_veloroute"],{

/***/ "./assets/js/pages/cotation/cotation.js":
/*!**********************************************!*\
  !*** ./assets/js/pages/cotation/cotation.js ***!
  \**********************************************/
/*! exports provided: convertMinsToHrsMins, addValueToInput */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "convertMinsToHrsMins", function() { return convertMinsToHrsMins; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addValueToInput", function() { return addValueToInput; });
/* harmony import */ var core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.parse-float.js */ "./node_modules/core-js/modules/es.parse-float.js");
/* harmony import */ var core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.regexp.exec.js */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.string.replace.js */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2__);



//conversion minutes en heures
var convertMinsToHrsMins = function convertMinsToHrsMins(minutes) {
  var h = Math.floor(minutes / 60);
  var m = minutes % 60;
  h = h < 10 ? '0' + h : h;
  m = m < 10 ? '0' + m : m;
  return h + ':' + m;
}; //remplissage inputs

function addValueToInput(inputVal, id) {
  //document.getElementById(id).value = inputVal;
  $(id).val(inputVal);
} //recup coordonnees points departs

/*const getCoordsDepart = (xml) => {
    parser = new DOMParser();
    xmlDoc = parser.parseFromString(xml, "text/xml");
    var lat = xmlDoc.getElementsByTagName("trkpt")[0].getAttribute('lat');
    var lon = xmlDoc.getElementsByTagName("trkpt")[0].getAttribute('lon');
    return [lon, lat];
}*/
//calcul de la difficulte avec radios buttons

$(":radio").change(function () {
  var names = {};
  $(':radio').each(function () {
    names[$(this).attr('name')] = true;
  });
  var count = 0;
  $.each(names, function () {
    count++;
  });

  if ($(':radio:checked').length === count) {
    var total = 0;
    $("input[type=radio]:checked").each(function () {
      total += parseFloat($(this).val());
    });
    outputDiff(total);
  }
}); //input difficulte

function outputDiff(total) {
  var diff, color;

  if (total >= 4 && total <= 5) {
    diff = total + " - Très facile";
    color = "green";
  } else if (total >= 6 && total <= 8) {
    diff = total + " - Facile";
    color = "blue";
  } else if (total >= 9 && total <= 12) {
    diff = total + " - Difficile";
    color = "red";
  } else {
    diff = total + " - Très Difficile";
    color = "black";
  }

  $("#cotation_form_difficulte").val(diff);
  $("#cotation_form_difficulte").css({
    "color": color,
    "font-size": "25px"
  });
} //compte le nombre de caracteres dans le textarea


var textInit = 0;
$('#count_text').html(textInit + ' caractère /500');
$('#cotation_form_textDescr').keyup(function () {
  var text_length = $('#cotation_form_textDescr').val().length;
  $('#count_text').html(text_length + ' caractères /500');

  if (text_length <= 1) {
    $('#count_text').html(text_length + ' caractère /500');
  }

  if (text_length > 500) {
    document.getElementById("count_text").style.color = 'red';
  } else {
    document.getElementById("count_text").style.color = 'black';
  }
}); //empecher les retours charriots du textarea

$('#cotation_form_textDescr').on('keyup', function () {
  $(this).val($(this).val().replace(/[\r\n\v]+/g, ''));
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/pages/cotation/cotationVeloroute.js":
/*!*******************************************************!*\
  !*** ./assets/js/pages/cotation/cotationVeloroute.js ***!
  \*******************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var esri_leaflet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! esri-leaflet */ "./node_modules/esri-leaflet/src/EsriLeaflet.js");
/* harmony import */ var leaflet_gpx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! leaflet-gpx */ "./node_modules/leaflet-gpx/gpx.js");
/* harmony import */ var leaflet_gpx__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(leaflet_gpx__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _cotation__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cotation */ "./assets/js/pages/cotation/cotation.js");





var toGeoJSON = __webpack_require__(/*! @tmcw/togeojson */ "./node_modules/@tmcw/togeojson/dist/togeojson.umd.js");

__webpack_require__(/*! leaflet.heightgraph */ "./node_modules/leaflet.heightgraph/dist/L.Control.Heightgraph.min.js");

__webpack_require__(/*! leaflet-polylinedecorator */ "./node_modules/leaflet-polylinedecorator/dist/leaflet.polylineDecorator.js"); //coche bouton radio distance au chargement du gpx


function radioRatioCheck(deniv, dist) {
  var ratio = Math.round(deniv / dist);

  if (ratio >= 0 && ratio <= 5) {
    document.getElementById("cotation_form_ratioChoice_0").checked = true;
  }

  if (ratio >= 6 && ratio <= 10) {
    document.getElementById("cotation_form_ratioChoice_1").checked = true;
  }

  if (ratio >= 11 && ratio <= 15) {
    document.getElementById("cotation_form_ratioChoice_2").checked = true;
  }

  if (ratio >= 16) {
    document.getElementById("cotation_form_ratioChoice_3").checked = true;
  }
} // fonds de carte


var map = L.map('cotation-map', {
  center: [47, 2],
  zoom: 5,
  minZoom: 3,
  zoomControl: false,
  layers: [Object(esri_leaflet__WEBPACK_IMPORTED_MODULE_1__["basemapLayer"])('Topographic')]
});
L.control.scale().addTo(map); //ouverture et affichage du gpx

var cpt = 0;

function loadGpxFile(g) {
  var files = g.target.files,
      reader = new FileReader();
  cpt += 1;

  if (cpt > 1) {
    map.remove();
    map = L.map('cotation-map', {
      center: [47, 2],
      zoom: 5,
      minZoom: 3,
      zoomControl: true,
      layers: [Object(esri_leaflet__WEBPACK_IMPORTED_MODULE_1__["basemapLayer"])('Topographic')]
    });
  }

  reader.onload = function (e) {
    var gpxString = reader.result;
    new leaflet_gpx__WEBPACK_IMPORTED_MODULE_2__["GPX"](gpxString, {
      async: true,
      marker_options: {
        startIconUrl: '../build/images/icon/pin-icon-start.png',
        endIconUrl: '../build/images/icon/pin-icon-end.png',
        shadowUrl: '',
        wptIconUrls: ''
      }
    }).on('loaded', function (e) {
      map.fitBounds(e.target.getBounds());
      var nom = e.target.get_name();
      var dist = Math.round(e.target.get_distance() / 1000);
      var temps = Math.round(dist / 15 * 60);
      var deniv = Math.round(e.target.get_elevation_gain());
      _cotation__WEBPACK_IMPORTED_MODULE_3__["addValueToInput"](nom, "#cotation_form_nom_circuit");
      _cotation__WEBPACK_IMPORTED_MODULE_3__["addValueToInput"](dist, "#cotation_form_distance");
      _cotation__WEBPACK_IMPORTED_MODULE_3__["addValueToInput"](_cotation__WEBPACK_IMPORTED_MODULE_3__["convertMinsToHrsMins"](temps), "#cotation_form_temps");
      _cotation__WEBPACK_IMPORTED_MODULE_3__["addValueToInput"](deniv, "#cotation_form_denivele");
      radioRatioCheck(deniv, dist);
    }).addTo(map);
    var gpxDoc = new DOMParser().parseFromString(gpxString, 'text/xml');
    var geoJson = [];
    geoJson.push(toGeoJSON.gpx(gpxDoc));
    geoJson[0]['properties'] = {
      "Creator": "FFVélo",
      "summary": ""
    };
    var hg = L.control.heightgraph({
      width: 800,
      height: 280,
      margins: {
        top: 10,
        right: 30,
        bottom: 55,
        left: 50
      },
      position: "bottomright",
      mappings: undefined
    });
    hg.addTo(map);
    hg.addData(geoJson); //Geojson pour fleches directionnelles sur le tracé du circuit

    var arrowLayer = L.geoJson(geoJson, {
      onEachFeature: function onEachFeature(feature, layer) {
        var coords = layer.getLatLngs();
        L.polylineDecorator(coords, {
          patterns: [{
            offset: 25,
            repeat: 100,
            symbol: L.Symbol.arrowHead({
              pixelSize: 15,
              pathOptions: {
                fillOpacity: 1,
                weight: 0,
                color: '#0000FF'
              }
            })
          }]
        }).addTo(map);
      }
    }).addTo(map);
  };

  reader.readAsText(files[0]);
}

opengpx.addEventListener("change", loadGpxFile, false);

/***/ })

},[["./assets/js/pages/cotation/cotationVeloroute.js","runtime","vendors~app~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_velor~10347b53","vendors~bcn_bpf~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~61364f2f","vendors~bonnes_adresses~calendrier~circuits~clubs~cotation~cotation_route~cotation_veloroute~cotatio~d7e263c1","vendors~cotation_route~cotation_veloroute~cotation_vtt~cotation_vttae"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvcGFnZXMvY290YXRpb24vY290YXRpb24uanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL3BhZ2VzL2NvdGF0aW9uL2NvdGF0aW9uVmVsb3JvdXRlLmpzIl0sIm5hbWVzIjpbImNvbnZlcnRNaW5zVG9IcnNNaW5zIiwibWludXRlcyIsImgiLCJNYXRoIiwiZmxvb3IiLCJtIiwiYWRkVmFsdWVUb0lucHV0IiwiaW5wdXRWYWwiLCJpZCIsIiQiLCJ2YWwiLCJjaGFuZ2UiLCJuYW1lcyIsImVhY2giLCJhdHRyIiwiY291bnQiLCJsZW5ndGgiLCJ0b3RhbCIsInBhcnNlRmxvYXQiLCJvdXRwdXREaWZmIiwiZGlmZiIsImNvbG9yIiwiY3NzIiwidGV4dEluaXQiLCJodG1sIiwia2V5dXAiLCJ0ZXh0X2xlbmd0aCIsImRvY3VtZW50IiwiZ2V0RWxlbWVudEJ5SWQiLCJzdHlsZSIsIm9uIiwicmVwbGFjZSIsInRvR2VvSlNPTiIsInJlcXVpcmUiLCJyYWRpb1JhdGlvQ2hlY2siLCJkZW5pdiIsImRpc3QiLCJyYXRpbyIsInJvdW5kIiwiY2hlY2tlZCIsIm1hcCIsIkwiLCJjZW50ZXIiLCJ6b29tIiwibWluWm9vbSIsInpvb21Db250cm9sIiwibGF5ZXJzIiwiYmFzZW1hcExheWVyIiwiY29udHJvbCIsInNjYWxlIiwiYWRkVG8iLCJjcHQiLCJsb2FkR3B4RmlsZSIsImciLCJmaWxlcyIsInRhcmdldCIsInJlYWRlciIsIkZpbGVSZWFkZXIiLCJyZW1vdmUiLCJvbmxvYWQiLCJlIiwiZ3B4U3RyaW5nIiwicmVzdWx0IiwiR1BYIiwiYXN5bmMiLCJtYXJrZXJfb3B0aW9ucyIsInN0YXJ0SWNvblVybCIsImVuZEljb25VcmwiLCJzaGFkb3dVcmwiLCJ3cHRJY29uVXJscyIsImZpdEJvdW5kcyIsImdldEJvdW5kcyIsIm5vbSIsImdldF9uYW1lIiwiZ2V0X2Rpc3RhbmNlIiwidGVtcHMiLCJnZXRfZWxldmF0aW9uX2dhaW4iLCJDb3RhdGlvbiIsImdweERvYyIsIkRPTVBhcnNlciIsInBhcnNlRnJvbVN0cmluZyIsImdlb0pzb24iLCJwdXNoIiwiZ3B4IiwiaGciLCJoZWlnaHRncmFwaCIsIndpZHRoIiwiaGVpZ2h0IiwibWFyZ2lucyIsInRvcCIsInJpZ2h0IiwiYm90dG9tIiwibGVmdCIsInBvc2l0aW9uIiwibWFwcGluZ3MiLCJ1bmRlZmluZWQiLCJhZGREYXRhIiwiYXJyb3dMYXllciIsIm9uRWFjaEZlYXR1cmUiLCJmZWF0dXJlIiwibGF5ZXIiLCJjb29yZHMiLCJnZXRMYXRMbmdzIiwicG9seWxpbmVEZWNvcmF0b3IiLCJwYXR0ZXJucyIsIm9mZnNldCIsInJlcGVhdCIsInN5bWJvbCIsIlN5bWJvbCIsImFycm93SGVhZCIsInBpeGVsU2l6ZSIsInBhdGhPcHRpb25zIiwiZmlsbE9wYWNpdHkiLCJ3ZWlnaHQiLCJyZWFkQXNUZXh0Iiwib3BlbmdweCIsImFkZEV2ZW50TGlzdGVuZXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNPLElBQU1BLG9CQUFvQixHQUFHLFNBQXZCQSxvQkFBdUIsQ0FBQ0MsT0FBRCxFQUFhO0FBQzdDLE1BQUlDLENBQUMsR0FBR0MsSUFBSSxDQUFDQyxLQUFMLENBQVdILE9BQU8sR0FBRyxFQUFyQixDQUFSO0FBQ0EsTUFBSUksQ0FBQyxHQUFHSixPQUFPLEdBQUcsRUFBbEI7QUFDQUMsR0FBQyxHQUFHQSxDQUFDLEdBQUcsRUFBSixHQUFTLE1BQU1BLENBQWYsR0FBbUJBLENBQXZCO0FBQ0FHLEdBQUMsR0FBR0EsQ0FBQyxHQUFHLEVBQUosR0FBUyxNQUFNQSxDQUFmLEdBQW1CQSxDQUF2QjtBQUNBLFNBQU9ILENBQUMsR0FBRyxHQUFKLEdBQVVHLENBQWpCO0FBQ0gsQ0FOTSxDLENBUVA7O0FBQ08sU0FBU0MsZUFBVCxDQUF5QkMsUUFBekIsRUFBbUNDLEVBQW5DLEVBQXNDO0FBQ3pDO0FBQ0FDLEdBQUMsQ0FBQ0QsRUFBRCxDQUFELENBQU1FLEdBQU4sQ0FBVUgsUUFBVjtBQUNILEMsQ0FFRDs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOztBQUNBRSxDQUFDLENBQUMsUUFBRCxDQUFELENBQVlFLE1BQVosQ0FBbUIsWUFBWTtBQUMzQixNQUFJQyxLQUFLLEdBQUcsRUFBWjtBQUNBSCxHQUFDLENBQUMsUUFBRCxDQUFELENBQVlJLElBQVosQ0FBaUIsWUFBWTtBQUN6QkQsU0FBSyxDQUFDSCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFLLElBQVIsQ0FBYSxNQUFiLENBQUQsQ0FBTCxHQUE4QixJQUE5QjtBQUNILEdBRkQ7QUFHQSxNQUFJQyxLQUFLLEdBQUcsQ0FBWjtBQUNBTixHQUFDLENBQUNJLElBQUYsQ0FBT0QsS0FBUCxFQUFjLFlBQVk7QUFDdEJHLFNBQUs7QUFDUixHQUZEOztBQUdBLE1BQUlOLENBQUMsQ0FBQyxnQkFBRCxDQUFELENBQW9CTyxNQUFwQixLQUErQkQsS0FBbkMsRUFBMEM7QUFDdEMsUUFBSUUsS0FBSyxHQUFHLENBQVo7QUFDQVIsS0FBQyxDQUFDLDJCQUFELENBQUQsQ0FBK0JJLElBQS9CLENBQW9DLFlBQVk7QUFDNUNJLFdBQUssSUFBSUMsVUFBVSxDQUFDVCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFDLEdBQVIsRUFBRCxDQUFuQjtBQUNILEtBRkQ7QUFHQVMsY0FBVSxDQUFDRixLQUFELENBQVY7QUFDSDtBQUNKLENBaEJELEUsQ0FtQkE7O0FBQ0EsU0FBU0UsVUFBVCxDQUFvQkYsS0FBcEIsRUFBMEI7QUFDdEIsTUFBSUcsSUFBSixFQUFVQyxLQUFWOztBQUNBLE1BQUlKLEtBQUssSUFBSSxDQUFULElBQWNBLEtBQUssSUFBSSxDQUEzQixFQUE4QjtBQUMxQkcsUUFBSSxHQUFHSCxLQUFLLEdBQUcsZ0JBQWY7QUFDQUksU0FBSyxHQUFHLE9BQVI7QUFDSCxHQUhELE1BR08sSUFBSUosS0FBSyxJQUFJLENBQVQsSUFBY0EsS0FBSyxJQUFJLENBQTNCLEVBQThCO0FBQ2pDRyxRQUFJLEdBQUdILEtBQUssR0FBRyxXQUFmO0FBQ0FJLFNBQUssR0FBRyxNQUFSO0FBQ0gsR0FITSxNQUdBLElBQUlKLEtBQUssSUFBSSxDQUFULElBQWNBLEtBQUssSUFBSSxFQUEzQixFQUErQjtBQUNsQ0csUUFBSSxHQUFHSCxLQUFLLEdBQUcsY0FBZjtBQUNBSSxTQUFLLEdBQUcsS0FBUjtBQUNILEdBSE0sTUFHQTtBQUNIRCxRQUFJLEdBQUdILEtBQUssR0FBRyxtQkFBZjtBQUNBSSxTQUFLLEdBQUcsT0FBUjtBQUNIOztBQUNEWixHQUFDLENBQUMsMkJBQUQsQ0FBRCxDQUErQkMsR0FBL0IsQ0FBbUNVLElBQW5DO0FBQ0FYLEdBQUMsQ0FBQywyQkFBRCxDQUFELENBQStCYSxHQUEvQixDQUFtQztBQUFDLGFBQVNELEtBQVY7QUFBaUIsaUJBQWE7QUFBOUIsR0FBbkM7QUFDSCxDLENBRUQ7OztBQUNBLElBQUlFLFFBQVEsR0FBRyxDQUFmO0FBQ0FkLENBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJlLElBQWpCLENBQXNCRCxRQUFRLEdBQUcsaUJBQWpDO0FBQ0FkLENBQUMsQ0FBQywwQkFBRCxDQUFELENBQThCZ0IsS0FBOUIsQ0FBb0MsWUFBWTtBQUM1QyxNQUFJQyxXQUFXLEdBQUdqQixDQUFDLENBQUMsMEJBQUQsQ0FBRCxDQUE4QkMsR0FBOUIsR0FBb0NNLE1BQXREO0FBQ0FQLEdBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJlLElBQWpCLENBQXNCRSxXQUFXLEdBQUcsa0JBQXBDOztBQUNBLE1BQUlBLFdBQVcsSUFBSSxDQUFuQixFQUFzQjtBQUNsQmpCLEtBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJlLElBQWpCLENBQXNCRSxXQUFXLEdBQUcsaUJBQXBDO0FBQ0g7O0FBQ0QsTUFBSUEsV0FBVyxHQUFHLEdBQWxCLEVBQXVCO0FBQ25CQyxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0NDLEtBQXRDLENBQTRDUixLQUE1QyxHQUFvRCxLQUFwRDtBQUNILEdBRkQsTUFFTztBQUNITSxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0NDLEtBQXRDLENBQTRDUixLQUE1QyxHQUFvRCxPQUFwRDtBQUNIO0FBQ0osQ0FYRCxFLENBWUE7O0FBQ0FaLENBQUMsQ0FBQywwQkFBRCxDQUFELENBQThCcUIsRUFBOUIsQ0FBaUMsT0FBakMsRUFBMEMsWUFBWTtBQUNsRHJCLEdBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUUMsR0FBUixDQUFZRCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFDLEdBQVIsR0FBY3FCLE9BQWQsQ0FBc0IsWUFBdEIsRUFBb0MsRUFBcEMsQ0FBWjtBQUNILENBRkQsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaEZBO0FBQ0E7QUFDQTs7QUFDQSxJQUFNQyxTQUFTLEdBQUdDLG1CQUFPLENBQUMsNkVBQUQsQ0FBekI7O0FBQ0FBLG1CQUFPLENBQUMsaUdBQUQsQ0FBUDs7QUFDQUEsbUJBQU8sQ0FBQyw2R0FBRCxDQUFQLEMsQ0FFQTs7O0FBQ0EsU0FBU0MsZUFBVCxDQUF5QkMsS0FBekIsRUFBZ0NDLElBQWhDLEVBQXFDO0FBQ2pDLE1BQUlDLEtBQUssR0FBRWxDLElBQUksQ0FBQ21DLEtBQUwsQ0FBV0gsS0FBSyxHQUFDQyxJQUFqQixDQUFYOztBQUNBLE1BQUtDLEtBQUssSUFBSSxDQUFWLElBQWlCQSxLQUFLLElBQUksQ0FBOUIsRUFBa0M7QUFBQ1YsWUFBUSxDQUFDQyxjQUFULENBQXdCLDZCQUF4QixFQUF1RFcsT0FBdkQsR0FBaUUsSUFBakU7QUFBc0U7O0FBQ3pHLE1BQUtGLEtBQUssSUFBSSxDQUFWLElBQWlCQSxLQUFLLElBQUksRUFBOUIsRUFBbUM7QUFBQ1YsWUFBUSxDQUFDQyxjQUFULENBQXdCLDZCQUF4QixFQUF1RFcsT0FBdkQsR0FBaUUsSUFBakU7QUFBc0U7O0FBQzFHLE1BQUtGLEtBQUssSUFBSSxFQUFWLElBQWtCQSxLQUFLLElBQUksRUFBL0IsRUFBb0M7QUFBQ1YsWUFBUSxDQUFDQyxjQUFULENBQXdCLDZCQUF4QixFQUF1RFcsT0FBdkQsR0FBaUUsSUFBakU7QUFBc0U7O0FBQzNHLE1BQUlGLEtBQUssSUFBSSxFQUFiLEVBQWdCO0FBQUNWLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3Qiw2QkFBeEIsRUFBdURXLE9BQXZELEdBQWlFLElBQWpFO0FBQXNFO0FBQzFGLEMsQ0FFRDs7O0FBQ0EsSUFBSUMsR0FBRyxHQUFHQyxDQUFDLENBQUNELEdBQUYsQ0FBTSxjQUFOLEVBQXNCO0FBQzVCRSxRQUFNLEVBQUUsQ0FBQyxFQUFELEVBQUssQ0FBTCxDQURvQjtBQUU1QkMsTUFBSSxFQUFFLENBRnNCO0FBRzVCQyxTQUFPLEVBQUUsQ0FIbUI7QUFJNUJDLGFBQVcsRUFBRSxLQUplO0FBSzVCQyxRQUFNLEVBQUUsQ0FBQ0MsaUVBQVksQ0FBQyxhQUFELENBQWI7QUFMb0IsQ0FBdEIsQ0FBVjtBQU9BTixDQUFDLENBQUNPLE9BQUYsQ0FBVUMsS0FBVixHQUFrQkMsS0FBbEIsQ0FBd0JWLEdBQXhCLEUsQ0FFQTs7QUFDQSxJQUFJVyxHQUFHLEdBQUcsQ0FBVjs7QUFDQSxTQUFTQyxXQUFULENBQXFCQyxDQUFyQixFQUF3QjtBQUNwQixNQUFJQyxLQUFLLEdBQUdELENBQUMsQ0FBQ0UsTUFBRixDQUFTRCxLQUFyQjtBQUFBLE1BQ0FFLE1BQU0sR0FBRyxJQUFJQyxVQUFKLEVBRFQ7QUFFQU4sS0FBRyxJQUFFLENBQUw7O0FBQ0EsTUFBSUEsR0FBRyxHQUFDLENBQVIsRUFBVTtBQUNOWCxPQUFHLENBQUNrQixNQUFKO0FBQ0FsQixPQUFHLEdBQUdDLENBQUMsQ0FBQ0QsR0FBRixDQUFNLGNBQU4sRUFBc0I7QUFDeEJFLFlBQU0sRUFBRSxDQUFDLEVBQUQsRUFBSyxDQUFMLENBRGdCO0FBRXhCQyxVQUFJLEVBQUUsQ0FGa0I7QUFHeEJDLGFBQU8sRUFBRSxDQUhlO0FBSXhCQyxpQkFBVyxFQUFFLElBSlc7QUFLeEJDLFlBQU0sRUFBRSxDQUFDQyxpRUFBWSxDQUFDLGFBQUQsQ0FBYjtBQUxnQixLQUF0QixDQUFOO0FBT0g7O0FBQ0RTLFFBQU0sQ0FBQ0csTUFBUCxHQUFnQixVQUFTQyxDQUFULEVBQVk7QUFDeEIsUUFBSUMsU0FBUyxHQUFHTCxNQUFNLENBQUNNLE1BQXZCO0FBQ0EsUUFBSUMsK0NBQUosQ0FBUUYsU0FBUixFQUFtQjtBQUFDRyxXQUFLLEVBQUUsSUFBUjtBQUNmQyxvQkFBYyxFQUFFO0FBQ1pDLG9CQUFZLEVBQUUseUNBREY7QUFFWkMsa0JBQVUsRUFBRSx1Q0FGQTtBQUdaQyxpQkFBUyxFQUFFLEVBSEM7QUFJWkMsbUJBQVcsRUFBRTtBQUpEO0FBREQsS0FBbkIsRUFPR3ZDLEVBUEgsQ0FPTSxRQVBOLEVBT2dCLFVBQVM4QixDQUFULEVBQVk7QUFDeEJwQixTQUFHLENBQUM4QixTQUFKLENBQWNWLENBQUMsQ0FBQ0wsTUFBRixDQUFTZ0IsU0FBVCxFQUFkO0FBQ0EsVUFBSUMsR0FBRyxHQUFDWixDQUFDLENBQUNMLE1BQUYsQ0FBU2tCLFFBQVQsRUFBUjtBQUNBLFVBQUlyQyxJQUFJLEdBQUNqQyxJQUFJLENBQUNtQyxLQUFMLENBQVdzQixDQUFDLENBQUNMLE1BQUYsQ0FBU21CLFlBQVQsS0FBd0IsSUFBbkMsQ0FBVDtBQUNBLFVBQUlDLEtBQUssR0FBQ3hFLElBQUksQ0FBQ21DLEtBQUwsQ0FBWUYsSUFBSSxHQUFDLEVBQU4sR0FBVSxFQUFyQixDQUFWO0FBQ0EsVUFBSUQsS0FBSyxHQUFHaEMsSUFBSSxDQUFDbUMsS0FBTCxDQUFXc0IsQ0FBQyxDQUFDTCxNQUFGLENBQVNxQixrQkFBVCxFQUFYLENBQVo7QUFDQUMsK0RBQUEsQ0FBeUJMLEdBQXpCLEVBQThCLDRCQUE5QjtBQUNBSywrREFBQSxDQUF5QnpDLElBQXpCLEVBQStCLHlCQUEvQjtBQUNBeUMsK0RBQUEsQ0FBeUJBLDhEQUFBLENBQThCRixLQUE5QixDQUF6QixFQUErRCxzQkFBL0Q7QUFDQUUsK0RBQUEsQ0FBeUIxQyxLQUF6QixFQUFnQyx5QkFBaEM7QUFDQUQscUJBQWUsQ0FBQ0MsS0FBRCxFQUFRQyxJQUFSLENBQWY7QUFDSCxLQWxCRCxFQWtCR2MsS0FsQkgsQ0FrQlNWLEdBbEJUO0FBb0JBLFFBQUlzQyxNQUFNLEdBQUcsSUFBSUMsU0FBSixHQUFnQkMsZUFBaEIsQ0FBZ0NuQixTQUFoQyxFQUEyQyxVQUEzQyxDQUFiO0FBQ0EsUUFBSW9CLE9BQU8sR0FBQyxFQUFaO0FBQ0FBLFdBQU8sQ0FBQ0MsSUFBUixDQUFhbEQsU0FBUyxDQUFDbUQsR0FBVixDQUFjTCxNQUFkLENBQWI7QUFFQUcsV0FBTyxDQUFDLENBQUQsQ0FBUCxDQUFXLFlBQVgsSUFDQTtBQUNJLGlCQUFXLFFBRGY7QUFFSSxpQkFBVztBQUZmLEtBREE7QUFLQSxRQUFJRyxFQUFFLEdBQUczQyxDQUFDLENBQUNPLE9BQUYsQ0FBVXFDLFdBQVYsQ0FBc0I7QUFDM0JDLFdBQUssRUFBRSxHQURvQjtBQUUzQkMsWUFBTSxFQUFFLEdBRm1CO0FBRzNCQyxhQUFPLEVBQUU7QUFDTEMsV0FBRyxFQUFFLEVBREE7QUFFTEMsYUFBSyxFQUFFLEVBRkY7QUFHTEMsY0FBTSxFQUFFLEVBSEg7QUFJTEMsWUFBSSxFQUFFO0FBSkQsT0FIa0I7QUFTM0JDLGNBQVEsRUFBRSxhQVRpQjtBQVUzQkMsY0FBUSxFQUFHQztBQVZnQixLQUF0QixDQUFUO0FBWUFYLE1BQUUsQ0FBQ2xDLEtBQUgsQ0FBU1YsR0FBVDtBQUNBNEMsTUFBRSxDQUFDWSxPQUFILENBQVdmLE9BQVgsRUE1Q3dCLENBNkN4Qjs7QUFDQSxRQUFJZ0IsVUFBVSxHQUFHeEQsQ0FBQyxDQUFDd0MsT0FBRixDQUFVQSxPQUFWLEVBQW1CO0FBQ2hDaUIsbUJBQWEsRUFBRSx1QkFBVUMsT0FBVixFQUFtQkMsS0FBbkIsRUFBMEI7QUFDckMsWUFBSUMsTUFBTSxHQUFHRCxLQUFLLENBQUNFLFVBQU4sRUFBYjtBQUNBN0QsU0FBQyxDQUFDOEQsaUJBQUYsQ0FBb0JGLE1BQXBCLEVBQTRCO0FBQ3hCRyxrQkFBUSxFQUFFLENBQUM7QUFDUEMsa0JBQU0sRUFBRSxFQUREO0FBRVBDLGtCQUFNLEVBQUUsR0FGRDtBQUdQQyxrQkFBTSxFQUFFbEUsQ0FBQyxDQUFDbUUsTUFBRixDQUFTQyxTQUFULENBQW1CO0FBQ3ZCQyx1QkFBUyxFQUFFLEVBRFk7QUFFdkJDLHlCQUFXLEVBQUU7QUFDVEMsMkJBQVcsRUFBRSxDQURKO0FBRVRDLHNCQUFNLEVBQUUsQ0FGQztBQUdUNUYscUJBQUssRUFBQztBQUhHO0FBRlUsYUFBbkI7QUFIRCxXQUFEO0FBRGMsU0FBNUIsRUFhRzZCLEtBYkgsQ0FhU1YsR0FiVDtBQWNIO0FBakIrQixLQUFuQixFQWtCZFUsS0FsQmMsQ0FrQlJWLEdBbEJRLENBQWpCO0FBbUJILEdBakVEOztBQWtFQWdCLFFBQU0sQ0FBQzBELFVBQVAsQ0FBa0I1RCxLQUFLLENBQUMsQ0FBRCxDQUF2QjtBQUNIOztBQUNENkQsT0FBTyxDQUFDQyxnQkFBUixDQUF5QixRQUF6QixFQUFrQ2hFLFdBQWxDLEVBQStDLEtBQS9DLEUiLCJmaWxlIjoiY290YXRpb25fdmVsb3JvdXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy9jb252ZXJzaW9uIG1pbnV0ZXMgZW4gaGV1cmVzXHJcbmV4cG9ydCBjb25zdCBjb252ZXJ0TWluc1RvSHJzTWlucyA9IChtaW51dGVzKSA9PiB7XHJcbiAgICB2YXIgaCA9IE1hdGguZmxvb3IobWludXRlcyAvIDYwKTtcclxuICAgIHZhciBtID0gbWludXRlcyAlIDYwO1xyXG4gICAgaCA9IGggPCAxMCA/ICcwJyArIGggOiBoO1xyXG4gICAgbSA9IG0gPCAxMCA/ICcwJyArIG0gOiBtO1xyXG4gICAgcmV0dXJuIGggKyAnOicgKyBtO1xyXG59XHJcblxyXG4vL3JlbXBsaXNzYWdlIGlucHV0c1xyXG5leHBvcnQgZnVuY3Rpb24gYWRkVmFsdWVUb0lucHV0KGlucHV0VmFsLCBpZCl7XHJcbiAgICAvL2RvY3VtZW50LmdldEVsZW1lbnRCeUlkKGlkKS52YWx1ZSA9IGlucHV0VmFsO1xyXG4gICAgJChpZCkudmFsKGlucHV0VmFsKTtcclxufVxyXG5cclxuLy9yZWN1cCBjb29yZG9ubmVlcyBwb2ludHMgZGVwYXJ0c1xyXG4vKmNvbnN0IGdldENvb3Jkc0RlcGFydCA9ICh4bWwpID0+IHtcclxuICAgIHBhcnNlciA9IG5ldyBET01QYXJzZXIoKTtcclxuICAgIHhtbERvYyA9IHBhcnNlci5wYXJzZUZyb21TdHJpbmcoeG1sLCBcInRleHQveG1sXCIpO1xyXG4gICAgdmFyIGxhdCA9IHhtbERvYy5nZXRFbGVtZW50c0J5VGFnTmFtZShcInRya3B0XCIpWzBdLmdldEF0dHJpYnV0ZSgnbGF0Jyk7XHJcbiAgICB2YXIgbG9uID0geG1sRG9jLmdldEVsZW1lbnRzQnlUYWdOYW1lKFwidHJrcHRcIilbMF0uZ2V0QXR0cmlidXRlKCdsb24nKTtcclxuICAgIHJldHVybiBbbG9uLCBsYXRdO1xyXG59Ki9cclxuXHJcbi8vY2FsY3VsIGRlIGxhIGRpZmZpY3VsdGUgYXZlYyByYWRpb3MgYnV0dG9uc1xyXG4kKFwiOnJhZGlvXCIpLmNoYW5nZShmdW5jdGlvbiAoKSB7XHJcbiAgICBsZXQgbmFtZXMgPSB7fTtcclxuICAgICQoJzpyYWRpbycpLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIG5hbWVzWyQodGhpcykuYXR0cignbmFtZScpXSA9IHRydWU7XHJcbiAgICB9KTtcclxuICAgIGxldCBjb3VudCA9IDA7XHJcbiAgICAkLmVhY2gobmFtZXMsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBjb3VudCsrO1xyXG4gICAgfSk7XHJcbiAgICBpZiAoJCgnOnJhZGlvOmNoZWNrZWQnKS5sZW5ndGggPT09IGNvdW50KSB7XHJcbiAgICAgICAgbGV0IHRvdGFsID0gMDtcclxuICAgICAgICAkKFwiaW5wdXRbdHlwZT1yYWRpb106Y2hlY2tlZFwiKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdG90YWwgKz0gcGFyc2VGbG9hdCgkKHRoaXMpLnZhbCgpKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICBvdXRwdXREaWZmKHRvdGFsKTtcclxuICAgIH1cclxufSk7XHJcblxyXG5cclxuLy9pbnB1dCBkaWZmaWN1bHRlXHJcbmZ1bmN0aW9uIG91dHB1dERpZmYodG90YWwpe1xyXG4gICAgdmFyIGRpZmYsIGNvbG9yO1xyXG4gICAgaWYgKHRvdGFsID49IDQgJiYgdG90YWwgPD0gNSkge1xyXG4gICAgICAgIGRpZmYgPSB0b3RhbCArIFwiIC0gVHLDqHMgZmFjaWxlXCI7XHJcbiAgICAgICAgY29sb3IgPSBcImdyZWVuXCI7XHJcbiAgICB9IGVsc2UgaWYgKHRvdGFsID49IDYgJiYgdG90YWwgPD0gOCkge1xyXG4gICAgICAgIGRpZmYgPSB0b3RhbCArIFwiIC0gRmFjaWxlXCI7XHJcbiAgICAgICAgY29sb3IgPSBcImJsdWVcIjtcclxuICAgIH0gZWxzZSBpZiAodG90YWwgPj0gOSAmJiB0b3RhbCA8PSAxMikge1xyXG4gICAgICAgIGRpZmYgPSB0b3RhbCArIFwiIC0gRGlmZmljaWxlXCI7XHJcbiAgICAgICAgY29sb3IgPSBcInJlZFwiO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBkaWZmID0gdG90YWwgKyBcIiAtIFRyw6hzIERpZmZpY2lsZVwiO1xyXG4gICAgICAgIGNvbG9yID0gXCJibGFja1wiO1xyXG4gICAgfVxyXG4gICAgJChcIiNjb3RhdGlvbl9mb3JtX2RpZmZpY3VsdGVcIikudmFsKGRpZmYpO1xyXG4gICAgJChcIiNjb3RhdGlvbl9mb3JtX2RpZmZpY3VsdGVcIikuY3NzKHtcImNvbG9yXCI6IGNvbG9yLCBcImZvbnQtc2l6ZVwiOiBcIjI1cHhcIn0pO1xyXG59XHJcblxyXG4vL2NvbXB0ZSBsZSBub21icmUgZGUgY2FyYWN0ZXJlcyBkYW5zIGxlIHRleHRhcmVhXHJcbnZhciB0ZXh0SW5pdCA9IDA7XHJcbiQoJyNjb3VudF90ZXh0JykuaHRtbCh0ZXh0SW5pdCArICcgY2FyYWN0w6hyZSAvNTAwJyk7XHJcbiQoJyNjb3RhdGlvbl9mb3JtX3RleHREZXNjcicpLmtleXVwKGZ1bmN0aW9uICgpIHtcclxuICAgIGxldCB0ZXh0X2xlbmd0aCA9ICQoJyNjb3RhdGlvbl9mb3JtX3RleHREZXNjcicpLnZhbCgpLmxlbmd0aDtcclxuICAgICQoJyNjb3VudF90ZXh0JykuaHRtbCh0ZXh0X2xlbmd0aCArICcgY2FyYWN0w6hyZXMgLzUwMCcpO1xyXG4gICAgaWYgKHRleHRfbGVuZ3RoIDw9IDEpIHtcclxuICAgICAgICAkKCcjY291bnRfdGV4dCcpLmh0bWwodGV4dF9sZW5ndGggKyAnIGNhcmFjdMOocmUgLzUwMCcpO1xyXG4gICAgfVxyXG4gICAgaWYgKHRleHRfbGVuZ3RoID4gNTAwKSB7XHJcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjb3VudF90ZXh0XCIpLnN0eWxlLmNvbG9yID0gJ3JlZCc7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY291bnRfdGV4dFwiKS5zdHlsZS5jb2xvciA9ICdibGFjayc7XHJcbiAgICB9XHJcbn0pO1xyXG4vL2VtcGVjaGVyIGxlcyByZXRvdXJzIGNoYXJyaW90cyBkdSB0ZXh0YXJlYVxyXG4kKCcjY290YXRpb25fZm9ybV90ZXh0RGVzY3InKS5vbigna2V5dXAnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAkKHRoaXMpLnZhbCgkKHRoaXMpLnZhbCgpLnJlcGxhY2UoL1tcXHJcXG5cXHZdKy9nLCAnJykpO1xyXG59KTtcclxuIiwiaW1wb3J0IHsgYmFzZW1hcExheWVyIH0gZnJvbSAnZXNyaS1sZWFmbGV0JztcclxuaW1wb3J0IHsgR1BYIH0gZnJvbSAnbGVhZmxldC1ncHgnO1xyXG5pbXBvcnQgKiBhcyBDb3RhdGlvbiBmcm9tICcuL2NvdGF0aW9uJztcclxuY29uc3QgdG9HZW9KU09OID0gcmVxdWlyZShcIkB0bWN3L3RvZ2VvanNvblwiKTtcclxucmVxdWlyZSgnbGVhZmxldC5oZWlnaHRncmFwaCcpO1xyXG5yZXF1aXJlKCdsZWFmbGV0LXBvbHlsaW5lZGVjb3JhdG9yJyk7XHJcblxyXG4vL2NvY2hlIGJvdXRvbiByYWRpbyBkaXN0YW5jZSBhdSBjaGFyZ2VtZW50IGR1IGdweFxyXG5mdW5jdGlvbiByYWRpb1JhdGlvQ2hlY2soZGVuaXYsIGRpc3Qpe1xyXG4gICAgdmFyIHJhdGlvPSBNYXRoLnJvdW5kKGRlbml2L2Rpc3QpO1xyXG4gICAgaWYgKChyYXRpbyA+PSAwKSAmJiAocmF0aW8gPD0gNSkpIHtkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdGF0aW9uX2Zvcm1fcmF0aW9DaG9pY2VfMFwiKS5jaGVja2VkID0gdHJ1ZX1cclxuICAgIGlmICgocmF0aW8gPj0gNikgJiYgKHJhdGlvIDw9IDEwKSkge2RvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY290YXRpb25fZm9ybV9yYXRpb0Nob2ljZV8xXCIpLmNoZWNrZWQgPSB0cnVlfVxyXG4gICAgaWYgKChyYXRpbyA+PSAxMSkgJiYgKHJhdGlvIDw9IDE1KSkge2RvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY290YXRpb25fZm9ybV9yYXRpb0Nob2ljZV8yXCIpLmNoZWNrZWQgPSB0cnVlfVxyXG4gICAgaWYgKHJhdGlvID49IDE2KXtkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdGF0aW9uX2Zvcm1fcmF0aW9DaG9pY2VfM1wiKS5jaGVja2VkID0gdHJ1ZX1cclxufVxyXG5cclxuLy8gZm9uZHMgZGUgY2FydGVcclxudmFyIG1hcCA9IEwubWFwKCdjb3RhdGlvbi1tYXAnLCB7XHJcbiAgICBjZW50ZXI6IFs0NywgMl0sXHJcbiAgICB6b29tOiA1LFxyXG4gICAgbWluWm9vbTogMyxcclxuICAgIHpvb21Db250cm9sOiBmYWxzZSxcclxuICAgIGxheWVyczogW2Jhc2VtYXBMYXllcignVG9wb2dyYXBoaWMnKV1cclxufSk7XHJcbkwuY29udHJvbC5zY2FsZSgpLmFkZFRvKG1hcCk7XHJcblxyXG4vL291dmVydHVyZSBldCBhZmZpY2hhZ2UgZHUgZ3B4XHJcbnZhciBjcHQgPSAwO1xyXG5mdW5jdGlvbiBsb2FkR3B4RmlsZShnKSB7XHJcbiAgICB2YXIgZmlsZXMgPSBnLnRhcmdldC5maWxlcyxcclxuICAgIHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XHJcbiAgICBjcHQrPTE7XHJcbiAgICBpZiAoY3B0PjEpe1xyXG4gICAgICAgIG1hcC5yZW1vdmUoKTtcclxuICAgICAgICBtYXAgPSBMLm1hcCgnY290YXRpb24tbWFwJywge1xyXG4gICAgICAgICAgICBjZW50ZXI6IFs0NywgMl0sXHJcbiAgICAgICAgICAgIHpvb206IDUsXHJcbiAgICAgICAgICAgIG1pblpvb206IDMsXHJcbiAgICAgICAgICAgIHpvb21Db250cm9sOiB0cnVlLFxyXG4gICAgICAgICAgICBsYXllcnM6IFtiYXNlbWFwTGF5ZXIoJ1RvcG9ncmFwaGljJyldXHJcbiAgICAgICAgfSk7XHRcdFxyXG4gICAgfVxyXG4gICAgcmVhZGVyLm9ubG9hZCA9IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICB2YXIgZ3B4U3RyaW5nID0gcmVhZGVyLnJlc3VsdDtcclxuICAgICAgICBuZXcgR1BYKGdweFN0cmluZywge2FzeW5jOiB0cnVlLFxyXG4gICAgICAgICAgICBtYXJrZXJfb3B0aW9uczoge1xyXG4gICAgICAgICAgICAgICAgc3RhcnRJY29uVXJsOiAnLi4vYnVpbGQvaW1hZ2VzL2ljb24vcGluLWljb24tc3RhcnQucG5nJyxcclxuICAgICAgICAgICAgICAgIGVuZEljb25Vcmw6ICcuLi9idWlsZC9pbWFnZXMvaWNvbi9waW4taWNvbi1lbmQucG5nJyxcclxuICAgICAgICAgICAgICAgIHNoYWRvd1VybDogJycsXHJcbiAgICAgICAgICAgICAgICB3cHRJY29uVXJsczogJydcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLm9uKCdsb2FkZWQnLCBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgICAgIG1hcC5maXRCb3VuZHMoZS50YXJnZXQuZ2V0Qm91bmRzKCkpO1xyXG4gICAgICAgICAgICBsZXQgbm9tPWUudGFyZ2V0LmdldF9uYW1lKCk7XHJcbiAgICAgICAgICAgIGxldCBkaXN0PU1hdGgucm91bmQoZS50YXJnZXQuZ2V0X2Rpc3RhbmNlKCkvMTAwMCk7XHJcbiAgICAgICAgICAgIGxldCB0ZW1wcz1NYXRoLnJvdW5kKChkaXN0LzE1KSo2MCk7XHJcbiAgICAgICAgICAgIGxldCBkZW5pdiA9IE1hdGgucm91bmQoZS50YXJnZXQuZ2V0X2VsZXZhdGlvbl9nYWluKCkpO1xyXG4gICAgICAgICAgICBDb3RhdGlvbi5hZGRWYWx1ZVRvSW5wdXQobm9tLCBcIiNjb3RhdGlvbl9mb3JtX25vbV9jaXJjdWl0XCIpO1xyXG4gICAgICAgICAgICBDb3RhdGlvbi5hZGRWYWx1ZVRvSW5wdXQoZGlzdCwgXCIjY290YXRpb25fZm9ybV9kaXN0YW5jZVwiKTtcclxuICAgICAgICAgICAgQ290YXRpb24uYWRkVmFsdWVUb0lucHV0KENvdGF0aW9uLmNvbnZlcnRNaW5zVG9IcnNNaW5zKHRlbXBzKSwgXCIjY290YXRpb25fZm9ybV90ZW1wc1wiKTtcclxuICAgICAgICAgICAgQ290YXRpb24uYWRkVmFsdWVUb0lucHV0KGRlbml2LCBcIiNjb3RhdGlvbl9mb3JtX2Rlbml2ZWxlXCIpO1xyXG4gICAgICAgICAgICByYWRpb1JhdGlvQ2hlY2soZGVuaXYsIGRpc3QpO1xyXG4gICAgICAgIH0pLmFkZFRvKG1hcCk7XHJcblxyXG4gICAgICAgIHZhciBncHhEb2MgPSBuZXcgRE9NUGFyc2VyKCkucGFyc2VGcm9tU3RyaW5nKGdweFN0cmluZywgJ3RleHQveG1sJyk7XHJcbiAgICAgICAgdmFyIGdlb0pzb249W107XHJcbiAgICAgICAgZ2VvSnNvbi5wdXNoKHRvR2VvSlNPTi5ncHgoZ3B4RG9jKSk7XHJcblxyXG4gICAgICAgIGdlb0pzb25bMF1bJ3Byb3BlcnRpZXMnXT1cclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIFwiQ3JlYXRvclwiOiBcIkZGVsOpbG9cIixcclxuICAgICAgICAgICAgXCJzdW1tYXJ5XCI6IFwiXCJcclxuICAgICAgICB9XHJcbiAgICAgICAgdmFyIGhnID0gTC5jb250cm9sLmhlaWdodGdyYXBoKHtcclxuICAgICAgICAgICAgd2lkdGg6IDgwMCxcclxuICAgICAgICAgICAgaGVpZ2h0OiAyODAsXHJcbiAgICAgICAgICAgIG1hcmdpbnM6IHtcclxuICAgICAgICAgICAgICAgIHRvcDogMTAsXHJcbiAgICAgICAgICAgICAgICByaWdodDogMzAsXHJcbiAgICAgICAgICAgICAgICBib3R0b206IDU1LFxyXG4gICAgICAgICAgICAgICAgbGVmdDogNTBcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgcG9zaXRpb246IFwiYm90dG9tcmlnaHRcIixcclxuICAgICAgICAgICAgbWFwcGluZ3M6ICB1bmRlZmluZWRcclxuICAgICAgICB9KTtcclxuICAgICAgICBoZy5hZGRUbyhtYXApO1xyXG4gICAgICAgIGhnLmFkZERhdGEoZ2VvSnNvbik7XHJcbiAgICAgICAgLy9HZW9qc29uIHBvdXIgZmxlY2hlcyBkaXJlY3Rpb25uZWxsZXMgc3VyIGxlIHRyYWPDqSBkdSBjaXJjdWl0XHJcbiAgICAgICAgdmFyIGFycm93TGF5ZXIgPSBMLmdlb0pzb24oZ2VvSnNvbiwge1xyXG4gICAgICAgICAgICBvbkVhY2hGZWF0dXJlOiBmdW5jdGlvbiAoZmVhdHVyZSwgbGF5ZXIpIHtcclxuICAgICAgICAgICAgICAgIHZhciBjb29yZHMgPSBsYXllci5nZXRMYXRMbmdzKCk7XHJcbiAgICAgICAgICAgICAgICBMLnBvbHlsaW5lRGVjb3JhdG9yKGNvb3Jkcywge1xyXG4gICAgICAgICAgICAgICAgICAgIHBhdHRlcm5zOiBbe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvZmZzZXQ6IDI1LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXBlYXQ6IDEwMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3ltYm9sOiBMLlN5bWJvbC5hcnJvd0hlYWQoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGl4ZWxTaXplOiAxNSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhdGhPcHRpb25zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsbE9wYWNpdHk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2VpZ2h0OiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOicjMDAwMEZGJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgIH1dXHJcbiAgICAgICAgICAgICAgICB9KS5hZGRUbyhtYXApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSkuYWRkVG8obWFwKTtcclxuICAgIH1cclxuICAgIHJlYWRlci5yZWFkQXNUZXh0KGZpbGVzWzBdKTtcclxufVxyXG5vcGVuZ3B4LmFkZEV2ZW50TGlzdGVuZXIoXCJjaGFuZ2VcIixsb2FkR3B4RmlsZSwgZmFsc2UpOyJdLCJzb3VyY2VSb290IjoiIn0=