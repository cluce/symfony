import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import * as JSZip from "jszip";
window.JSZip = JSZip;
import 'datatables.net-bs4';
import 'datatables.net-buttons-bs4';
import 'datatables.net-buttons/js/buttons.html5.min.js'

$(function() {
    $('#table_bcnbpf').DataTable( {
        ajax: {
            url: "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/VELOENFRANCE/FeatureServer/0/query?where=REFERENCE_SOUSTYPEPOI=101&f=json&outFields=*&returnGeometry=true&outSR=4326",
            dataSrc: 'features',
        },
        columns: [
            { data: "attributes.NOM"},
            { data: "attributes.COMMUNE"},
            { data: "attributes.Code_Dept"},
            { data: "attributes.Nom_Dept"},
            { data: "geometry.y"},
            { data: "geometry.x"},
            {"render": function(d, t, r) {
                return `<a href="${window.location.origin}/labels-velo/${r.attributes.IDENTIFIANT_POI}" target="_blank">${window.location.origin}/labels/${r.attributes.IDENTIFIANT_POI}</a>`;
            }},
            
        ],
        "pageLength": 15,
        "scrollX": true,
        "scrollCollapse": true,
        dom: 'Blfrtip',
        buttons: [
            { extend: 'copyHtml5', text: 'Copier <i class="far fa-fw fa-copy"></i>', className: 'btn btn-sm btn-bubble-blue rounded-pill mb-2 mr-1' },
            { extend: 'excelHtml5', text: 'Excel <i class="fas fa-fw fa-file-excel"></i>', className: 'btn btn-sm btn-bubble-blue rounded-pill mb-2 mr-1' },
            { extend: 'csvHtml5', text: 'CSV <i class="fas fa-fw fa-file-csv"></i>', className: 'btn btn-sm btn-bubble-blue rounded-pill mb-2 mr-1' },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                text: 'PDF <i class="fas fa-fw fa-file-pdf"></i>',
                className: 'btn btn-sm btn-bubble-blue rounded-pill mb-2 mr-1'
            },
        ],
        "language": {
            "search": "Rechercher",
            "info": "Éléments _START_ à _END_ sur _TOTAL_",
            "lengthMenu": "Afficher _MENU_ résultats",
            "paginate": {
                "first":      "Premier",
                "last":       "Dernier",
                "next":       "Suivant",
                "previous":   "Précédent"
            },
        },
        "columnDefs": [
            { "width": "15%", "targets": 6 },
            { render: ( data, type, row ) => {
                    return data.toFixed(5);
                },
                "targets": [4,5] 
            }
        ],   
    });
});