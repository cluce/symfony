import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import * as JSZip from "jszip";
window.JSZip = JSZip;
import 'datatables.net-bs4';
import 'datatables.net-buttons-bs4';
import 'datatables.net-buttons/js/buttons.html5.min.js'


$(function() {
    $('#table_rp').DataTable({
        data:  data,
        columns: [
            { title: "Id" },
            { title: "Nom" },
            { title: "Commune" },
            { title: "Code postal" },
            { title: "Accessible aux non licenciés" },
            { title: "Tarif adulte licencié" },
            { title: "Tarif jeune licencié" },
            { title: "Tarif adulte non licencié" },
            { title: "Tarif jeune non licencié" },
            { title: "Plus d'infos" }
        ],
        "pageLength": 15,
        "scrollX": true,
        "scrollCollapse": true,
        dom: 'Blfrtip',
        buttons: [
            { extend: 'copyHtml5', text: 'Copier <i class="far fa-fw fa-copy"></i>', className: 'btn btn-sm btn-bubble-blue rounded-pill mb-2 mr-1' },
            { extend: 'excelHtml5', text: 'Excel <i class="fas fa-fw fa-file-excel"></i>', className: 'btn btn-sm btn-bubble-blue rounded-pill mb-2 mr-1' },
            { extend: 'csvHtml5', text: 'CSV <i class="fas fa-fw fa-file-csv"></i>', className: 'btn btn-sm btn-bubble-blue rounded-pill mb-2 mr-1' },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                text: 'PDF <i class="fas fa-fw fa-file-pdf"></i>',
                className: 'btn btn-sm btn-bubble-blue rounded-pill mb-2 mr-1'
            },
            //{ extend: 'print', text: 'Imprimer <i class="fas fa-fw fa-print"></i>', className: 'btn btn-sm btn-bubble-blue rounded-pill mb-2 mr-1', orientation: 'landscape'},
        ],
        "language": {
            "search": "Rechercher",
            "info": "Éléments _START_ à _END_ sur _TOTAL_",
            "lengthMenu": "Afficher _MENU_ résultats",
            "paginate": {
                "first":      "Premier",
                "last":       "Dernier",
                "next":       "Suivant",
                "previous":   "Précédent"
            },
        },
        "columnDefs": [
            { "width": "15%", "targets": 7 }
        ],   
    });
});