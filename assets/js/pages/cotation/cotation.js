//conversion minutes en heures
export const convertMinsToHrsMins = (minutes) => {
    var h = Math.floor(minutes / 60);
    var m = minutes % 60;
    h = h < 10 ? '0' + h : h;
    m = m < 10 ? '0' + m : m;
    return h + ':' + m;
}

//remplissage inputs
export function addValueToInput(inputVal, id){
    //document.getElementById(id).value = inputVal;
    $(id).val(inputVal);
}

//recup coordonnees points departs
/*const getCoordsDepart = (xml) => {
    parser = new DOMParser();
    xmlDoc = parser.parseFromString(xml, "text/xml");
    var lat = xmlDoc.getElementsByTagName("trkpt")[0].getAttribute('lat');
    var lon = xmlDoc.getElementsByTagName("trkpt")[0].getAttribute('lon');
    return [lon, lat];
}*/

//calcul de la difficulte avec radios buttons
$(":radio").change(function () {
    let names = {};
    $(':radio').each(function () {
        names[$(this).attr('name')] = true;
    });
    let count = 0;
    $.each(names, function () {
        count++;
    });
    if ($(':radio:checked').length === count) {
        let total = 0;
        $("input[type=radio]:checked").each(function () {
            total += parseFloat($(this).val());
        });
        outputDiff(total);
    }
});


//input difficulte
function outputDiff(total){
    var diff, color;
    if (total >= 4 && total <= 5) {
        diff = total + " - Très facile";
        color = "green";
    } else if (total >= 6 && total <= 8) {
        diff = total + " - Facile";
        color = "blue";
    } else if (total >= 9 && total <= 12) {
        diff = total + " - Difficile";
        color = "red";
    } else {
        diff = total + " - Très Difficile";
        color = "black";
    }
    $("#cotation_form_difficulte").val(diff);
    $("#cotation_form_difficulte").css({"color": color, "font-size": "25px"});
}

//compte le nombre de caracteres dans le textarea
var textInit = 0;
$('#count_text').html(textInit + ' caractère /500');
$('#cotation_form_textDescr').keyup(function () {
    let text_length = $('#cotation_form_textDescr').val().length;
    $('#count_text').html(text_length + ' caractères /500');
    if (text_length <= 1) {
        $('#count_text').html(text_length + ' caractère /500');
    }
    if (text_length > 500) {
        document.getElementById("count_text").style.color = 'red';
    } else {
        document.getElementById("count_text").style.color = 'black';
    }
});
//empecher les retours charriots du textarea
$('#cotation_form_textDescr').on('keyup', function () {
    $(this).val($(this).val().replace(/[\r\n\v]+/g, ''));
});
