import { basemapLayer } from 'esri-leaflet';
import { GPX } from 'leaflet-gpx';
import * as Cotation from './cotation';
const toGeoJSON = require("@tmcw/togeojson");
require('leaflet.heightgraph');
require('leaflet-polylinedecorator');

 //coche bouton radio distance au chargement du gpx
 function radioDistCheck(dist){
    if ((dist >= 0) && (dist <= 10)) {document.getElementById("cotation_form_distChoice_0").checked = true}
    if ((dist >= 11) && (dist <= 20)) {document.getElementById("cotation_form_distChoice_1").checked = true}
    if ((dist >= 21) && (dist <= 40)) {document.getElementById("cotation_form_distChoice_2").checked = true}
    if (dist >= 41){document.getElementById("cotation_form_distChoice_3").checked = true}
}
//coche bouton radio denivele au chargement du gpx
function radioDenivCheck(deniv){
    if ((deniv >= 0) && (deniv <= 100)) {document.getElementById("cotation_form_denivChoice_0").checked = true}
    if ((deniv >= 101) && (deniv <= 250)) {document.getElementById("cotation_form_denivChoice_1").checked = true}
    if ((deniv >= 251) && (deniv <= 600)) {document.getElementById("cotation_form_denivChoice_2").checked = true}
    if (deniv >= 601){document.getElementById("cotation_form_denivChoice_3").checked = true}
}
// fonds de carte
var map = L.map('cotation-map', {
    center: [47, 2],
    zoom: 5,
    minZoom: 3,
    zoomControl: true,
    layers: [basemapLayer('Topographic')]
});
L.control.scale().addTo(map);

//ouverture et affichage du gpx
var cpt = 0;
function loadGpxFile(g) {
    var files = g.target.files,
    reader = new FileReader();
    cpt+=1;
    if (cpt>1){
        map.remove();
        map = L.map('cotation-map', {
            center: [47, 2],
            zoom: 5,
            minZoom: 3,
            zoomControl: true,
            layers: [basemapLayer('Topographic')]
        });
    }
    reader.onload = function(e) {
        var gpxString = reader.result;
        new GPX(gpxString, {async: true,
            marker_options: {
                startIconUrl: '../build/images/icon/pin-icon-start.png',
                endIconUrl: '../build/images/icon/pin-icon-end.png',
                shadowUrl: '',
                wptIconUrls: ''
            }
        }).on('loaded', function(e) {
            map.fitBounds(e.target.getBounds());
            let nom=e.target.get_name();
            let dist=Math.round(e.target.get_distance()/1000);
            let temps=Math.round((dist/15)*60);
            let deniv = Math.round(e.target.get_elevation_gain());
            Cotation.addValueToInput(nom, "#cotation_form_nom_circuit");
            Cotation.addValueToInput(dist, "#cotation_form_distance");
            Cotation.addValueToInput(Cotation.convertMinsToHrsMins(temps), "#cotation_form_temps");
            Cotation.addValueToInput(deniv, "#cotation_form_denivele");
            radioDistCheck(dist);
            radioDenivCheck(deniv);
            /*radioTypeVoieCheck(dist);
            radioPenteCheck(dist);*/
        }).addTo(map);

        var gpxDoc = new DOMParser().parseFromString(gpxString, 'text/xml');
        var geoJson=[];
        geoJson.push(toGeoJSON.gpx(gpxDoc));

        geoJson[0]['properties']=
        {
            "Creator": "FFVélo",
            "summary": ""
        }
        /*var hg = L.control.heightgraph({
            width: 800,
            height: 280,
            margins: {
                top: 10,
                right: 30,
                bottom: 55,
                left: 50
            },
            position: "bottomright",
            mappings:  undefined
        });
        hg.addTo(map);
        hg.addData(geoJson);*/
        //Geojson pour fleches directionnelles sur le tracé du circuit
        var arrowLayer = L.geoJson(geoJson, {
            onEachFeature: function (feature, layer) {
                var coords = layer.getLatLngs();
                L.polylineDecorator(coords, {
                    patterns: [{
                        offset: 25,
                        repeat: 100,
                        symbol: L.Symbol.arrowHead({
                            pixelSize: 15,
                            pathOptions: {
                                fillOpacity: 1,
                                weight: 0,
                                color:'#0000FF'
                            }
                        })
                    }]
                }).addTo(map);
            }
        }).addTo(map);
    }
    reader.readAsText(files[0]);
}
opengpx.addEventListener("change",loadGpxFile, false);