import {
	circuitLayerGroup,
	pointDepartLayerGroup,
	zoomToMarkersAfterLoad,
	queryString,
	addResultsToSidebar,
	agolFeatureServices,
	arcgisOnlineProvider,
	iconPointDepart,
	ssTypeCircuit, 
	typeVeloName, 
	difficulteName,
	difficulteFontAwesome,
	geolocationButton, 
	setEsriGeocoder, 
	showCircuit,
	backButtonNavigation, 
	navigationHistory,
	colsLayer,
	layerControlTitles,
	sidebarToggleControl
} from "../services/mapConfig";

import {popupTemplatePoi, popupTemplateCircuit} from "../components/popup";
import {deptList} from '../services/whiteList'
import React from 'react';
import {render, unmountComponentAtNode} from 'react-dom';
import InfoFeature from '../components/InfoFeature';
import Tagify from '@yaireo/tagify';
import noUiSlider from 'nouislider';
import wNumb from "wnumb";

(function mapCircuits () {
		
	//map
	const map = L.map('map', {
		center: [47, 0],
		zoom: 6,
		minZoom: 1,
		zoomControl: true,
	});
	L.control.scale().addTo(map);//barre echelle
	const defaultLayer = L.esri.basemapLayer("Topographic").addTo(map);
	map.attributionControl.addAttribution('<a target="_blank" href="https://ffvelo.fr">Fédération française de cyclotourisme</a>');
	
	//variables globales
	const mapElement = document.getElementById("map");
	const sidebarElement = document.getElementById("sidebar");
	const mapWidth = document.getElementById("map").offsetWidth;
	const defaultWhereClause = location.search ? queryString(location.search) : "1=1";

	//bouton toggle sidebar
	sidebarToggleControl(L.Control, map, sidebarElement, mapElement, mapWidth);

	// controle des couches et changement de fond de carte
	const baseLayers = {
		'Topographique (Esri)': defaultLayer,
		'Satellite (Esri)': L.esri.basemapLayer('Imagery'),
		'OpenStreetMap':  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
		}),
	};
	const overlayMaps = {
		"Cols": colsLayer()
	};
	L.control.layers(baseLayers, overlayMaps, {
		position: "bottomleft"
	}).addTo(map);
	layerControlTitles();
	map.addControl(new geolocationButton(map));

	//layer points departs circuits
	const pointDepart = L.esri.Cluster.featureLayer({
		url: agolFeatureServices.pointDepartCircuit,
		where: defaultWhereClause,
		pointToLayer: (feature, latlng) => {
			let difficulte = feature.properties.DIFFICULTE;
			return L.marker(latlng, {
				icon: iconPointDepart[difficulte]
			});
		},
		iconCreateFunction: (cluster) => {
			let count = cluster.getChildCount();
			let digits = (count + '').length;
			return L.divIcon({
			  html: count,
			  className: 'cluster digits-' + digits,
			  iconSize: null
			});
		  },
	}).addTo(pointDepartLayerGroup).addTo(map);

	//popups points departs circuits
	pointDepart.bindPopup((layer)=>{
		let nom = layer.feature.properties.NOM;
		let id = layer.feature.properties.IDENTIFIANT_CIRCUIT;
		let distance = layer.feature.properties.DISTANCE;
		let typevelo = typeVeloName[layer.feature.properties.TYPE_VELO];
		let denivele = layer.feature.properties.DENIVELE;
		let difficulte = difficulteName[layer.feature.properties.DIFFICULTE];
		let difficulteIcon = difficulteFontAwesome[layer.feature.properties.DIFFICULTE];
		let sousType = layer.feature.properties.SOUS_TYPE_CIRCUIT;
		return L.Util.template(
			popupTemplateCircuit(nom, id, typevelo, distance, denivele, difficulte, difficulteIcon, sousType)
		);
	});

	/*evenements carte et layer point depart*/
	pointDepart.once('load', () => {
		if(location.pathname != menuUrl){
			let parts = location.href.split(menuUrl+"/");
			let urlid = parts[1].split("-");
			renderInfoFeature(pointDepart, urlid[0]);
		} else {
			zoomToMarkersAfterLoad(map, pointDepart);
		};
		//syncSidebar();
	});
	pointDepart.on('popupopen', e => {
		document.getElementById("buttonPopup").addEventListener('click', () => {
			renderInfoFeature(pointDepart, e.layer.feature.properties.IDENTIFIANT_CIRCUIT)
		}, true);
	});
	pointDepart.on('click', (e) => {
		showCircuit(map, e.layer.feature.properties.IDENTIFIANT_CIRCUIT, e.layer.feature.properties.DIFFICULTE);
	});
	map.on("moveend", () => {
		pointDepart.setWhere(pointDepart.getWhere());
		syncSidebar();
	});
	map.on('overlayremove', () => {
		circuitLayerGroup.clearLayers();
	});
	window.onpopstate = () => {
		distSlider.noUiSlider.reset();
		denivSlider.noUiSlider.reset();
		backButtonNavigation(menuUrl, map, mapElement, mapWidth, pointDepart, renderInfoFeature);
	}
	/*fin evenements carte et layer point depart*/

	
	/*filtres de recherches */
	const distSlider = document.getElementById('distSlider');
	noUiSlider.create(distSlider, {
		start: [0, 150],
		connect: true,
		range: {
			'min': 0,
			'max': 150
		},
		format: wNumb({
			decimals: 0, // default is 2
		}),
		pips: {mode: 'count', values: 5},
	});
	const denivSlider = document.getElementById('denivSlider');
	noUiSlider.create(denivSlider, {
		start: [0, 1000],
		connect: true,
		range: {
			'min': 0,
			'max': 1000
		},
		format: wNumb({
			decimals: 0, // default is 2
		}),
		pips: {mode: 'count', values: 3},
	});
	const tagify = new Tagify(document.querySelector("textarea[name=tagsDepts]"), {
		enforeWhitelist: true,
		whitelist : deptList
	});

	const getFilters=(filters=[])=>{
		const elements = document.querySelectorAll(".typeVeloFilter, .difficulteFilter, .tempsFilter, .sousTypeCircuitFilter, .typeCircuitFilter");
		[...elements].map(item => {
			item.addEventListener('change', (e) => {
				if (item.checked === true) {
					filters.push(item.value);
				} else {
					filters.splice(filters.indexOf(item.value),1);
				}
				setFilters(filters);
			});
		});
		tagify
			.on('add', (e) => {
				filters.push(e.detail.data.queryCircuit);
				setFilters(filters);
			})
			.on('remove', (e) => {
				filters.splice(filters.indexOf(e.detail.data.queryCircuit),1);
				setFilters(filters);
		});
		distSlider.noUiSlider.on("change", (data) => {
			if ((data[0] > 0) && (data[1] < 250)) {
				filters.distance = "DISTANCE >= " + data[0] + " AND DISTANCE <= " + data[1];
			} else if ((data[0] > 0) && (data[1] == 250)) {
				filters.distance =  "DISTANCE >=" + data[0];
			} else if ((data[0] == 0) && (data[1] < 250)) {
				filters.distance = "DISTANCE <= " + data[1];
			} else {
				filters.distance=null;
			}
			setFilters(filters);
		});
		denivSlider.noUiSlider.on("change", (data) => {
			if ((data[0] > 0) && (data[1] < 1000)) {
				filters.denivele = "DENIVELE >= " + data[0] + " AND DENIVELE <= " + data[1];
			} else if ((data[0] > 0) && (data[1] == 1000)) {
				filters.denivele =  "DENIVELE >=" + data[0];
			} else if ((data[0] == 0) && (data[1] < 1000)) {
				filters.denivele = "DENIVELE <= " + data[1];
			} else {
				filters.denivele=null;
			}
			setFilters(filters);
		});
	};getFilters();
	const setFilters = (filters, arrResult=[]) => {
		let dept = filters.filter(f => f.includes("NUMERO_DEPARTEMENT")).join(" OR ");
		let typeVelo = filters.filter(f => f.includes("TYPE_VELO")).join(" OR ");
		let difficulte = filters.filter(f => f.includes("DIFFICULTE")).join(" OR ");
		let temps = filters.filter(f => f.includes("TEMPS_PARCOURS")).join(" OR ");
		let type = filters.filter(f => f.includes("TYPE_CIRCUIT")).join(" OR ");
		let sousType = filters.filter(f => f.includes("SOUS_TYPE_CIRCUIT")).join(" OR ");

		dept.length>0 ? arrResult.push(`(${dept})`) : false;
		typeVelo.length>0 ? arrResult.push(`(${typeVelo})`) : false;
		difficulte.length>0 ? arrResult.push(`(${difficulte})`) : false;
		temps.length>0 ? arrResult.push(`(${temps})`) : false;
		type.length>0 ? arrResult.push(`(${type})`) : false;
		sousType.length>0 ? arrResult.push(`(${sousType})`) : false;
		filters.distance ? arrResult.push(`(${filters.distance})`) : false;
		filters.denivele ? arrResult.push(`(${filters.denivele})`) : false;

		let result = arrResult.join(" AND ");
		console.log(result);
		pointDepart.setWhere(result);
		zoomToMarkersAfterLoad (map, pointDepart);
	}
	/*fin des filtres de recherches */


	
	/* barre de recherche adresse et circuits (geocodeur en haut a gauche de la carte) */
	const searchControl = L.esri.Geocoding.geosearch({
		useMapBounds: false,
		title: 'Rechercher un lieu / un circuit',
		placeholder: 'Trouver un lieu / un circuit',
		expanded: true,
		collapseAfterResult: false,
		position: 'topleft',
		providers: [
			arcgisOnlineProvider,
			L.esri.Geocoding.featureLayerProvider({ //recherche des couches dans la gdb vef
				url: agolFeatureServices.pointDepartCircuit,
				where: pointDepart.getWhere(),
				searchFields: ['NOM'],
				label: 'CIRCUITS VELOENFRANCE:',
				maxResults: 7,
				bufferRadius: 1000,
				formatSuggestion: function (feature) {
					return `${feature.properties.NOM} - ${ssTypeCircuit[feature.properties.SOUS_TYPE_CIRCUIT]}`;
				}
			})
		]
	}).addTo(map);
	//geocodeur en dehors de la carte
	const esriGeocoder = searchControl.getContainer();
	setEsriGeocoder(esriGeocoder, document.getElementById('geocoder'));
	//recup la requete en cours et applique aux resultats du geocodeur
	searchControl.on("requestend", function () {
		searchControl.options.providers[1].options.where = pointDepart.getWhere();
	});
	//affiche le trace du circuit selectionne avec le geocodeur
	searchControl.on("results", function (e) {
		circuitLayerGroup.clearLayers();
		var idCircuitResult = e.results[0].properties.IDENTIFIANT_CIRCUIT;
		for (let i = e.results.length - 1; i >= 0; i--) {
			let marker = e.results[i].latlng;
			if (e.results[0].properties.IDENTIFIANT_CIRCUIT == null) {
				L.popup().setLatLng(marker).setContent(e.text).openOn(map);
			}
		}
		renderInfoFeature(pointDepart, idCircuitResult);
	});
	/* fin barre de recherche adresse et circuits (geocodeur en haut a gauche de la carte) */

	/*cards dans la sidebar*/
	const syncSidebar = (arrResultsSidebar=[]) => {
		let featureCards = document.getElementById('features');
		while(featureCards.firstChild){
			featureCards.removeChild(featureCards.firstChild)
		}
		pointDepart.eachActiveFeature(function (layer) { //eachActiveFeature = fonction a ajouter dans esri-leaflet-cluster.js - attention si maj de la librairie
			if (map.hasLayer(pointDepart)) {
				if (map.getBounds().contains(layer.getLatLng())) {
					arrResultsSidebar.push(
						sidebarTemplateCircuits(
							layer.feature.properties.NOM,
							typeVeloName[layer.feature.properties.TYPE_VELO],
							difficulteName[layer.feature.properties.DIFFICULTE],
							layer.feature.properties.DISTANCE,
							layer.feature.properties.DENIVELE,
							layer.feature.properties.SOUS_TYPE_CIRCUIT,
							layer.feature.properties.COMMUNE_DEPART,
							layer.feature.properties.Nom_Dep,
							layer.feature.properties.IDENTIFIANT_CIRCUIT,
							layer.feature.geometry.coordinates[1],
							layer.feature.geometry.coordinates[0]
						)
					);
				}
			}
		});
		arrResultsSidebar.reverse();
		addResultsToSidebar(arrResultsSidebar, pointDepart, renderInfoFeature);
	}
	const sidebarTemplateCircuits = (nom,typevelo,difficulte,distance,denivele,soustype,ville,dept,id,lat,lon) => { //template cards
		return (
			`<div class="card feature-card my-2 p-2" id="${id}" data-lat="${lat}" data-lon="${lon}"> 
				<div class="col align-self-center rounded-right">
					<h6 class="d-block text-uppercase">${nom}</h6>
					<span class="badge badge-pill badge-light my-1 mr-1"><i class="fas fa-bicycle"></i> ${typevelo}</span>
					${difficulte ? `<span class="badge badge-pill badge-light my-1 mr-1"><i class="far fa-star"></i> ${difficulte}</span>` : ``}
					${distance ? `<span class="badge badge-pill badge-light my-1 mr-1"><i class="fas fa-fw fa-road"></i> ${distance} km</span>` : ``}
					${denivele ? `<span class="badge badge-pill badge-light my-1 mr-1"><i class="fas fa-chart-line"></i> ${denivele} m</span>` : ``}
					${soustype ? `<span class="badge badge-pill badge-light my-1 mr-1"><i className="fas fa-tags"></i> ${ssTypeCircuit[soustype]}</span>` : ``}
					<span class="my-1 mr-1"><br/><i class="fas fa-map-marked-alt"></i> ${ville}, ${dept}</span>
				</div>
			</div>`
		);
	}
	/* fin des cards dans la sidebar*/
	
	//composant fiche descriptive
	const renderInfoFeature = (Layer, id) => {
		const queryCircuitFournisseur = L.esri.query({
            url: agolFeatureServices.circuitFournisseur,
        });
		const queryFournisseur = L.esri.query({
			url: agolFeatureServices.fournisseur
		});
		mapElement.style.width= mapWidth+"px";
		let infofeatureElement = document.getElementById('infofeature');
		let locationSearch = location.search;
		infofeatureElement.value=Layer.getWhere();
		/*let toggleButton = document.getElementById('toggleButton');
		toggleButton.style.display = "none";*/
		Layer.eachFeature(function (layer) {
			if(layer.feature.properties.IDENTIFIANT_CIRCUIT == id){
				showCircuit(
					map, 
					layer.feature.properties.IDENTIFIANT_CIRCUIT, 
					layer.feature.properties.DIFFICULTE, 
					infofeatureElement, 
					layer.feature.geometry.coordinates
				);
				unmountComponentAtNode(infofeatureElement);
				render(	
					<InfoFeature 
						idCircuit={id}
						nom={layer.feature.properties.NOM}
						descr={layer.feature.properties.DESCRIPTION}
						distance={layer.feature.properties.DISTANCE}
						denivele={layer.feature.properties.DENIVELE}
						difficulte={difficulteName[layer.feature.properties.DIFFICULTE]}
						typevelo={typeVeloName[layer.feature.properties.TYPE_VELO]}
						soustype={layer.feature.properties.SOUS_TYPE_CIRCUIT}
						rootContainer={infofeatureElement}
						map={map}
						mapWidth = {mapWidth}
						Layer={Layer}
						queryCircuitFournisseur={queryCircuitFournisseur}
						queryFournisseur={queryFournisseur}
						layer={layer}
						params={defaultWhereClause}
						locationSearch={locationSearch}
						whereClause={infofeatureElement.value}
						menuUrl={menuUrl}
						circuitLayer={circuitLayerGroup}
						toggleButton={toggleButton}
					/>, infofeatureElement
				);
				navigationHistory(id, menuUrl,layer.feature.properties.SLUG);
				//gestion de la carte
				let resize = mapWidth-document.getElementById("info-feature-content").offsetWidth;
				mapElement.style.width=resize+"px";
				map.invalidateSize();
			}
		});
	}

	
})();


