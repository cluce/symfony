import {
	poiLayerGroup,
	queryString,
	addResultsToSidebar,
	agolFeatureServices,
	arcgisOnlineProvider,
	iconPoi,
	baName,
	poiName,
	geolocationButton, 
	setEsriGeocoder, 
	backButtonNavigation, 
	navigationHistory
} from "../services/mapConfig";

import {popupTemplateBonneAdresse} from "../components/popup";
import React from 'react';
import {render, unmountComponentAtNode} from 'react-dom';
import InfoFeature from '../components/InfoFeature';

(function mapBonnesAdresses () {
	//map
	const map = L.map('map', {
		center: [47, 0],
		zoom: 5,
		minZoom: 3,
		zoomControl: true,
		layers: [L.esri.basemapLayer('Topographic')]
	});
	map.addControl(new geolocationButton(map));

	//variables globales
	const mapElement = document.getElementById("map");
	const mapWidth = document.getElementById("map").offsetWidth;
	//const defaultWhereClause = location.search ? queryString(location.search) : "VISIBILITE=1";

	//layer points departs circuits
	const baLayer = L.esri.Cluster.featureLayer({
		url: agolFeatureServices.bonneAdresse, 
		where: "1=1",
		token: token,
		pointToLayer: function (feature, latlng) {
			let ssTypePoi = feature.properties.REFERENCE_SOUSTYPEPOI;
			return L.marker(latlng, {
				icon: iconPoi[301]
			});
		}
	}).addTo(poiLayerGroup).addTo(map);

	const renderSousTypeBonneAdresse = (layer) => {
		let hotel = layer.feature.properties.HOTEL == true ? `<span class="badge badge-pill badge-light my-1 mr-1">Hôtel</span>` : ``; 
		let hotelResto = layer.feature.properties.HOTEL_RESTAURANT == true ? `<span class="badge badge-pill badge-light my-1 mr-1">Hôtel-restaurant</span>` : ``;
		let appartHotel = layer.feature.properties.APPARTHOTEL == true ? `<span class="badge badge-pill badge-light my-1 mr-1">Appart'hôtel</span>` : ``;
		let camping = layer.feature.properties.CAMPING == true ? `<span class="badge badge-pill badge-light my-1 mr-1">Camping</span>` : ``;
		let lodge = layer.feature.properties.LODGE == true ? `<span class="badge badge-pill badge-light my-1 mr-1">Lodge</span>` : ``;
		let bungalow = layer.feature.properties.BUNGALOW == true ? `<span class="badge badge-pill badge-light my-1 mr-1">Bungalow</span>` : ``;
		let gite = layer.feature.properties.GITE == true ? `<span class="badge badge-pill badge-light my-1 mr-1">Gîte</span>` : ``;
		let chambreHote = layer.feature.properties.CHAMBRE_HOTE == true ? `<span class="badge badge-pill badge-light my-1 mr-1">Chambre d'hôte</span>` : ``;
		let cis = layer.feature.properties.CIS == true ? `<span class="badge badge-pill badge-light my-1 mr-1">Centre international de séjour (CIS)</span>` : ``;
		let villageVacance = layer.feature.properties.VILLAGE_VACANCES == true ? `<span class="badge badge-pill badge-light my-1 mr-1">Village vacances</span>` : ``;
		let aubergeJeunesse = layer.feature.properties.AUBERGE_JEUNESSE == true ? `<span class="badge badge-pill badge-light my-1 mr-1">Auberge de jeunesse</span>` : ``;
		let insolite = layer.feature.properties.INSOLITE == true ? `<span class="badge badge-pill badge-light my-1 mr-1">Hébergement insolite</span>` : ``;
		let resto = layer.feature.properties.RESTAURANT == true ? `<span class="badge badge-pill badge-light my-1 mr-1">Restaurant</span>` : ``;
		let velociste = layer.feature.properties.VELOCISTE == true ? `<span class="badge badge-pill badge-light my-1 mr-1">Vélociste</span>` : ``;
		let loueur = layer.feature.properties.LOUEUR_VELO == true ? `<span class="badge badge-pill badge-light my-1 mr-1">Loueur de vélos</span>` : ``;
		return `${hotel}${hotelResto}${appartHotel}${camping}${lodge}${bungalow}${gite}${chambreHote}${cis}${villageVacance}${aubergeJeunesse}${insolite}${resto}${velociste}${loueur}`;
	}

	//popup bonnes adresses
	baLayer.bindPopup(function (layer) {
		let nom = layer.feature.properties.NOM_ETABLISSEMENT;
		let id = layer.feature.properties.IDENTIFIANT_BONNE_ADRESSE;
		let soustype = renderSousTypeBonneAdresse(layer);
		let photo = layer.feature.properties.LIEN_PHOTO1;
		let email = layer.feature.properties.EMAIL_ETABLISSEMENT;
		let tel = layer.feature.properties.TELEPHONE_ETABLISSEMENT;
		let siteweb = layer.feature.properties.SITEWEB;
		return L.Util.template(popupTemplateBonneAdresse(nom, id, photo, soustype, tel, email, siteweb));
	});

	/*evenements carte et layer bonnes adresses*/
	baLayer.on('load', () => {
		syncSidebar();
	});
	baLayer.once('load', () => {
		if(location.pathname != menuUrl){
			let parts = location.href.split(menuUrl+"/");
			let urlid = parts[1].split("-");
			renderInfoFeature(baLayer, urlid[0]);
		};
	});
	baLayer.on('popupopen', () => {
		document.getElementById("buttonPopup").addEventListener('click', () => {
			renderInfoFeature(baLayer, document.getElementById("buttonPopup").getAttribute('value'))
		}, false);
	});
	map.on("moveend", function () {
		baLayer.setWhere(baLayer.getWhere());
		syncSidebar();
	});
	map.on('overlayremove', function(e) {
		poiLayerGroup.clearLayers();
	});
	/*fin evenements carte et layer point depart*/

	const searchControl = L.esri.Geocoding.geosearch({
		useMapBounds: false,
		title: 'Rechercher un lieu / une Bonne adresse',
		placeholder: 'Trouver un lieu / une Bonne adresse',
		expanded: true,
		collapseAfterResult: false,
		position: 'topleft',
		providers: [
			arcgisOnlineProvider,
			L.esri.Geocoding.featureLayerProvider({ //recherche des couches dans la gdb vef
				url: agolFeatureServices.bonneAdresse,
				token: token,
				where: baLayer.getWhere(),
				searchFields: ['NOM_ETABLISSEMENT'],
				label: 'BONNES ADRESSES:',
				maxResults: 7,
				bufferRadius: 1000,
				formatSuggestion: function (feature) {
					return `${feature.properties.NOM_ETABLISSEMENT}`;
				}
			})
		]
	}).addTo(map);
	//barre de recherche en dehors de la carte
	const esriGeocoder = searchControl.getContainer();
	setEsriGeocoder(esriGeocoder, document.getElementById('geocoder'));
	//recup la requete en cours et applique aux resultats du geocodeur
	searchControl.on("requestend", function () {
		searchControl.options.providers[1].options.where = baLayer.getWhere();
	});
	/* fin barre de recherche adresse et circuits (geocodeur en haut a gauche de la carte) */

	/*cards dans la sidebar*/
	const syncSidebar = (arrResultsSidebar=[]) => {
		let featureCards = document.getElementById('features');
		while(featureCards.firstChild){
			featureCards.removeChild(featureCards.firstChild)
		}
		baLayer.eachActiveFeature(function (layer) { //eachActiveFeature = fonction a ajouter dans esri-leaflet-cluster.js - attention si maj de la librairie
			if (map.hasLayer(baLayer)) {
				if (map.getBounds().contains(layer.getLatLng())) {
					arrResultsSidebar.push(
						sidebarTemplateBonneAdresse(
							layer.feature.properties.NOM_ETABLISSEMENT,
							layer.feature.properties.VILLE_ETABLISSEMENT,
							layer.feature.properties.NOMDEPT_ETABLISSEMENT,
							layer.feature.properties.EMAIL_ETABLISSEMENT,
							layer.feature.properties.TELEPHONE_ETABLISSEMENT,
							layer.feature.properties.SITEWEB,
							layer.feature.properties.IDENTIFIANT_BONNE_ADRESSE,
							layer.feature.geometry.coordinates[1],
							layer.feature.geometry.coordinates[0],
							layer
						)
					);
				}
			}
		});
		arrResultsSidebar.reverse();
		addResultsToSidebar(arrResultsSidebar,baLayer,renderInfoFeature);
	}

	//template cards
	const sidebarTemplateBonneAdresse = (nom,ville,dept,email,tel,siteweb,id,lat,lon,layer) => {
		let soustype=renderSousTypeBonneAdresse(layer);
		return (
			`<div class="card feature-card my-2 p-2" id="${id}" data-lat="${lat}" data-lon="${lon}"> 
				<div class="col align-self-center rounded-right">
					<h6 class="d-block text-uppercase">${nom}</h6>
					${soustype}
					${tel ? `<span class="my-1 mr-1"><br/><i class="fas fa-phone"></i> <a href='tel:${tel}'> ${tel}</a></span>` : "" }
					${email ? `<span class="my-1 mr-1"><br/><i class="fas fa-at"></i> <a href='mailto:${email}'> ${email}</a></span>` : "" }
					${siteweb ? `<span class="my-1 mr-1"><br/><i class="fas fa-globe"></i>  <a href='${siteweb}' target="_blank"> Site web</a></span>` : "" }
					<span class="my-1 mr-1"><br/><i class="fas fa-map-marked-alt"></i> ${ville}, ${dept}</span>
				</div>
			</div>`
		);
	}
	/* fin des cards dans la sidebar*/

	//composant fiche descriptive
	const renderInfoFeature = (Layer, id) => {
		mapElement.style.width= mapWidth+"px";
		let infofeatureElement = document.getElementById('infofeature');
		let locationSearch = location.search;
		infofeatureElement.value=Layer.getWhere();
		Layer.eachFeature(function (layer) {
			if(layer.feature.properties.IDENTIFIANT_BONNE_ADRESSE == id){
				Layer.setWhere("IDENTIFIANT_BONNE_ADRESSE="+id);
				unmountComponentAtNode(infofeatureElement);
				render(	
					<InfoFeature 
						idBonneAdresse = {layer.feature.properties.IDENTIFIANT_BONNE_ADRESSE}
						hebergement = {layer.feature.properties.HEBERGEMENT}
						restaurant = {layer.feature.properties.RESTAURANT}
						velociste = {layer.feature.properties.VELOCISTE}
						loueur_velo = {layer.feature.properties.LOUEUR_VELO}
						nom_etablissement = {layer.feature.properties.NOM_ETABLISSEMENT}
						adresse_etablissement = {layer.feature.properties.ADRESSE_ETABLISSEMENT}
						telephone_etablissement = {layer.feature.properties.TELEPHONE_ETABLISSEMENT}
						email_etablissement = {layer.feature.properties.EMAIL_ETABLISSEMENT}
						siteweb = {layer.feature.properties.SITEWEB}
						club_avantages = {layer.feature.properties.CLUB_AVANTAGES}
						fermeture_hebdomadaire = {layer.feature.properties.FERMETURE_HEBDOMADAIRE}
						fermeture_anuelle = {layer.feature.properties.FERMETURE_ANUELLE}
						descr = {layer.feature.properties.DESCRIPTION}
						lien_photo1 = {layer.feature.properties.LIEN_PHOTO1}
						lien_photo2 = {layer.feature.properties.LIEN_PHOTO2}
						lien_photo3 = {layer.feature.properties.LIEN_PHOTO3}
						lien_photo4 = {layer.feature.properties.LIEN_PHOTO4}
						lien_photo5 = {layer.feature.properties.LIEN_PHOTO5}
						lien_video = {layer.feature.properties.LIEN_VIDEO}
						hotel = {layer.feature.properties.HOTEL}
						hotel_restaurant = {layer.feature.properties.HOTEL_RESTAURANT}
						apparthotel = {layer.feature.properties.APPARTHOTEL}
						camping = {layer.feature.properties.CAMPING}
						lodge = {layer.feature.properties.LODGE}
						bungalow = {layer.feature.properties.BUNGALOW}
						gite = {layer.feature.properties.GITE}
						chambre_hote = {layer.feature.properties.CHAMBRE_HOTE}
						cis = {layer.feature.properties.CIS}
						village_vacances = {layer.feature.properties.VILLAGE_VACANCES}
						auberge_jeunesse = {layer.feature.properties.AUBERGE_JEUNESSE}
						insolite = {layer.feature.properties.INSOLITE}
						categorie = {layer.feature.properties.CATEGORIE}
						nombre_chambres = {layer.feature.properties.NOMBRE_CHAMBRES}
						nombre_personnes = {layer.feature.properties.NOMBRE_PERSONNES}
						nombre_emplacement_camping = {layer.feature.properties.NOMBRE_EMPLACEMENT_CAMPING}
						capacite_localvelo = {layer.feature.properties.CAPACITE_LOCALVELO}
						heure_petitdejeuner = {layer.feature.properties.HEURE_PETITDEJEUNER}
						heure_servicemidi = {layer.feature.properties.HEURE_SERVICEMIDI}
						heure_servicesoir = {layer.feature.properties.HEURE_SERVICESOIR}
						artisan_constructeur = {layer.feature.properties.ARTISAN_CONSTRUCTEUR}
						reparation_urgente = {layer.feature.properties.REPARATION_URGENTE}
						heure_ouverture_matin_velociste = {layer.feature.properties.HEURE_OUVERTURE_MATIN_VELOCISTE}
						heure_fermeture_midi_velociste = {layer.feature.properties.HEURE_FERMETURE_MIDI_VELOCISTE}
						heure_ouverture_aprem_velociste = {layer.feature.properties.HEURE_OUVERTURE_APREM_VELOCISTE}
						heure_fermeture_soir_velociste = {layer.feature.properties.HEURE_FERMETURE_SOIR_VELOCISTE}
						location_route = {layer.feature.properties.LOCATION_ROUTE}
						location_route_vae = {layer.feature.properties.LOCATION_ROUTE_VAE}
						location_vtt = {layer.feature.properties.LOCATION_VTT}
						location_vttae = {layer.feature.properties.LOCATION_VTTAE}
						location_vtc = {layer.feature.properties.LOCATION_VTC}
						location_vtcae = {layer.feature.properties.LOCATION_VTCAE}
						location_gravel = {layer.feature.properties.LOCATION_GRAVEL}
						location_gravel_vae = {layer.feature.properties.LOCATION_GRAVEL_VAE}
						location_ville = {layer.feature.properties.LOCATION_VILLE}
						location_ville_vae = {layer.feature.properties.LOCATION_VILLE_VAE}
						location_enfant = {layer.feature.properties.LOCATION_ENFANT}
						location_enfant_vae = {layer.feature.properties.LOCATION_ENFANT_VAE}
						offre_5pourcent = {layer.feature.properties.OFFRE_5POURCENT}
						offre_apero = {layer.feature.properties.OFFRE_APERO}
						offre_adhesion = {layer.feature.properties.OFFRE_ADHESION}
						repas_emporter = {layer.feature.properties.REPAS_EMPORTER}
						lave_linge = {layer.feature.properties.LAVE_LINGE}
						wifi = {layer.feature.properties.WIFI}
						cp_etablissement = {layer.feature.properties.CP_ETABLISSEMENT}
						ville_etablissement = {layer.feature.properties.VILLE_ETABLISSEMENT}
						codedept_etablissement = {layer.feature.properties.CODEDEPT_ETABLISSEMENT}
						nomdept_etablissement = {layer.feature.properties.NOMDEPT_ETABLISSEMENT}
						pays_etablissement = {layer.feature.properties.PAYS_ETABLISSEMENT}
						tarif_minimum = {layer.feature.properties.TARIF_MINIMUM}
						slug = {layer.feature.properties.SLUG}
						rootContainer={infofeatureElement}
						map={map}
						mapWidth = {mapWidth}
						Layer={baLayer}
						whereClause={infofeatureElement.value}
						locationSearch={locationSearch}
						menuUrl={menuUrl}
					/>, infofeatureElement
				);
				//gestion de la carte
				navigationHistory(id, menuUrl, layer.feature.properties.SLUG);
				let resize = mapWidth-document.getElementById("info-feature-content").offsetWidth;
				mapElement.style.width=resize+"px";
				map.invalidateSize();
				map.setView([layer.feature.geometry.coordinates[1],layer.feature.geometry.coordinates[0]], 10);
			}
		});
	}

	//gestion de la navigation avec boutons precedent/suivant
	window.onpopstate = () => {
		backButtonNavigation(menuUrl, map, mapElement, mapWidth, baLayer, renderInfoFeature);
	}

})();
