import {
	circuitLayerGroup, 
	pointDepartLayerGroup, 
	poiLayerGroup,
	queryString,
	addResultsToSidebar,
	zoomToMarkersAfterLoad,
	agolFeatureServices,
	arcgisOnlineProvider,
	iconPoi,
	ssTypePoi,
	baName,
	poiName,
	geolocationButton, 
	setEsriGeocoder, 
	showCircuit,
	backButtonNavigation, 
	navigationHistory
} from "../services/mapConfig";

import {popupTemplatePoi} from "../components/popup";
import {deptList} from '../services/whiteList'
import React from 'react';
import {render, unmountComponentAtNode} from 'react-dom';
import InfoFeature from '../components/InfoFeature';
import Tagify from '@yaireo/tagify';

(function mapLabelsVelo () {
	//map
	const map = L.map('map', {
		center: [47, 0],
		zoom: 5,
		minZoom: 3,
		zoomControl: true,
		layers: [L.esri.basemapLayer('Topographic')]
	});
	map.addControl(new geolocationButton(map));

	//variables globales
	const mapElement = document.getElementById("map");
	const mapWidth = document.getElementById("map").offsetWidth;
	let defaultWhereClause = location.search ? queryString(location.search) : "(REFERENCE_SOUSTYPEPOI IN (101,204,205)) AND VISIBILITE=1";

	//layer points departs circuits
	const poiLayer = L.esri.Cluster.featureLayer({
		url: agolFeatureServices.poi, 
		where: defaultWhereClause,
		pointToLayer: function (feature, latlng) {
			let ssTypePoi = feature.properties.REFERENCE_SOUSTYPEPOI;
			return L.marker(latlng, {
				icon: iconPoi[ssTypePoi]
			});
		},
		iconCreateFunction: function (cluster) {
            let count = cluster.getChildCount();
            let digits = (count + '').length;
            return L.divIcon({
              html: count,
              className: 'cluster digits-' + digits,
              iconSize: null
            });
        }
	}).addTo(poiLayerGroup).addTo(map);

	//popup bonnes adresses
	poiLayer.bindPopup(function (layer) {
		var nom = layer.feature.properties.NOM;
		var idPoi = layer.feature.properties.IDENTIFIANT_POI;
		var type = layer.feature.properties.REFERENCE_TYPEPOI;
		var refSousType = layer.feature.properties.REFERENCE_SOUSTYPEPOI;
		return L.Util.template(popupTemplatePoi(nom, idPoi, type, refSousType));
	});

	/*evenements carte et layer bonnes adresses*/
	poiLayer.on('load', () => {
		syncSidebar();
	});
	poiLayer.once('load', () => {
		if(location.pathname != menuUrl){
			let parts = location.href.split(menuUrl+"/");
			let urlid = parts[1].split("-");
			renderInfoFeature(poiLayer, urlid[0]);
		};
		zoomToMarkersAfterLoad(map, poiLayer);
	});
	poiLayer.on('popupopen', () => {
		document.getElementById("buttonPopup").addEventListener('click', () => {
			renderInfoFeature(poiLayer, document.getElementById("buttonPopup").getAttribute('value'))
		}, false);
	});
	map.on("moveend", function () {
		poiLayer.setWhere(poiLayer.getWhere());
		syncSidebar();
	});
	map.on('overlayremove', function(e) {
		poiLayerGroup.clearLayers();
	});
	/*fin evenements carte et layer point depart*/

	/*filtres de recherches */
	new Tagify(document.querySelector("textarea[name=tagsDepts]"), {
		enforeWhitelist: true,
		whitelist :deptList
	});
	const filters=(filters=[])=>{
		const elements = document.querySelectorAll(".typePoiFilter, textarea[name=tagsDepts]");
		elements.forEach(item => {
			item.addEventListener('change', (e) => {
				if (document.querySelectorAll("input:checked, textarea[name=tagsDepts]").length === 0){
					filters=[];
					globalFilter(filters);
				} else {
					if (item.checked === true && e.target.id !="deptFilter"){
						filters.push(item.value);
					} else if (e.target.id=="deptFilter" && e.target.value){
						let jsonDepts = JSON.parse(e.target.value);
						filters=filters.filter(f => f.indexOf("Code_Dept") !== 0);
						jsonDepts.forEach(function(itemDept){
							filters.push(itemDept.queryPoi);
						});
					} else {
						filters.splice(filters.indexOf(item.value),1);
					}
					globalFilter(filters);
				}
			});
		});
	}
	filters();
	const globalFilter = (filters) => {
		let dept = filters.filter(f => f.includes("Code_Dept")).join(" OR ");
		let typePoi = filters.filter(f => f.includes("REFERENCE_SOUSTYPEPOI")).join(" OR ");
		let sousTypePoi= filters.filter(f => f.includes("REFERENCE_TYPEPOI")).join(" OR ");
		let result = `${defaultWhereClause =! "(REFERENCE_SOUSTYPEPOI IN (101,204,205)) AND VISIBILITE=1" ? "("+defaultWhereClause+") AND " : ""}${typePoi ? "("+typePoi+") AND " : ""}${sousTypePoi ? "("+sousTypePoi+") AND " : ""}${dept ? "("+dept+") AND " : ""}`.slice(0,-4);
		//console.log(result);
		poiLayer.setWhere(result);
	}
	/*fin des filtres de recherches */

	/* barre de recherche adresse et poi (geocodeur en haut a gauche de la carte) */
	const searchControl = L.esri.Geocoding.geosearch({
		useMapBounds: false,
		title: 'Rechercher un lieu / un label vélo',
		placeholder: 'Trouver un lieu / un label vélo',
		expanded: true,
		collapseAfterResult: false,
		position: 'topleft',
		providers: [
			arcgisOnlineProvider,
			L.esri.Geocoding.featureLayerProvider({ //recherche des couches dans la gdb vef
				url: poiLayer.options.url,
				where: poiLayer.getWhere(),
				searchFields: ['NOM'],
				label: 'LABELS VELO:',
				maxResults: 7,
				bufferRadius: 1000,
				formatSuggestion: function (feature) {
					return `${feature.properties.NOM} - ${baName[feature.properties.REFERENCE_SOUSTYPEPOI]}`;
				}
			})
		]
	}).addTo(map);
	//barre de recherche en dehors de la carte
	const esriGeocoder = searchControl.getContainer();
	setEsriGeocoder(esriGeocoder, document.getElementById('geocoder'));
	//recup la requete en cours et applique aux resultats du geocodeur
	searchControl.on("requestend", function () {
		searchControl.options.providers[1].options.where = poiLayer.getWhere();
	});
	/* fin barre de recherche adresse et poi (geocodeur en haut a gauche de la carte) */

	/*cards dans la sidebar*/
	const syncSidebar = (arrResultsSidebar=[]) => {
		$("#features").empty();
		poiLayer.eachActiveFeature(function (layer) { //eachActiveFeature = fonction a ajouter dans esri-leaflet-cluster.js - attention si maj de la librairie
			if (map.hasLayer(poiLayer)) {
				if (map.getBounds().contains(layer.getLatLng())) {
					arrResultsSidebar.push(
						sidebarTemplatePoi(
							layer.feature.properties.NOM,
							poiName[layer.feature.properties.REFERENCE_SOUSTYPEPOI],
							layer.feature.properties.COMMUNE,
							layer.feature.properties.Nom_Dept,
							layer.feature.properties.IDENTIFIANT_POI,
							layer.feature.geometry.coordinates[1],
							layer.feature.geometry.coordinates[0]
						)
					);
				}
			}
		});
		arrResultsSidebar.reverse();
		addResultsToSidebar(arrResultsSidebar,poiLayer,renderInfoFeature);
	}

	//template cards
	const sidebarTemplatePoi = (nom,soustype,ville,dept,id,lat,lon) => {
		return (
			`<div class="card feature-card my-2 p-2" id="${id}" data-lat="${lat}" data-lon="${lon}"> 
				<div class="col align-self-center rounded-right">
					<h6 class="d-block text-uppercase">${nom}</h6>
					<span class="badge badge-pill badge-light my-1 mr-1">${soustype}</span>
					<span class="my-1 mr-1"><br/><i class="fas fa-map-marked-alt"></i> ${ville}, ${dept}</span>
				</div>
			</div>`
		);
	}
	/* fin des cards dans la sidebar*/

	//composant fiche descriptive
	const renderInfoFeature = (Layer, id) => {
		const queryContact = L.esri.query({
			url: agolFeatureServices.fournisseur
		});
		mapElement.style.width= mapWidth+"px";
		let params=location.search;
		let infofeatureElement = document.getElementById('infofeature');
		infofeatureElement.value=Layer.getWhere();
		Layer.eachFeature(function (layer) {
			if(layer.feature.properties.IDENTIFIANT_POI == id){
				Layer.setWhere("IDENTIFIANT_POI="+id);
				unmountComponentAtNode(infofeatureElement);
				render(	
					<InfoFeature 
						idPoi={id}
						nom={layer.feature.properties.NOM}
						queryContactLabels={queryContact}
						descr={layer.feature.properties.DESCRIPTION}
						soustype={poiName[layer.feature.properties.REFERENCE_SOUSTYPEPOI]}
						rootContainer={infofeatureElement}
						map={map}
						mapWidth = {mapWidth}
						Layer={poiLayer}
						whereClause={infofeatureElement.value}
						params={params}
					/>, infofeatureElement
				);
				navigationHistory(id, menuUrl,layer.feature.properties.SLUG);
				//gestion de la carte
				let resize = mapWidth-document.getElementById("info-feature-content").offsetWidth;
				mapElement.style.width=resize+"px";
				map.invalidateSize();
				zoomToMarkersAfterLoad (map, Layer);
				//map.setView([layer.feature.geometry.coordinates[1],layer.feature.geometry.coordinates[0]], 10);
			}
		});
	}

	//gestion de la navigation avec boutons precedent/suivant
	window.onpopstate = () => {
		backButtonNavigation(menuUrl, map, mapElement, mapWidth, poiLayer, renderInfoFeature);
	}

})();
