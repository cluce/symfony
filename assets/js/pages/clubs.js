import {
    agolFeatureServices,
    arcgisOnlineProvider,
    setEsriGeocoder,
    addResultsToSidebar,
    navigationHistory,
    zoomToMarkersAfterLoad,
    iconOin,
    structureType
} from "../services/mapConfig";

import {popupTemplateClubs} from "../components/popup";
import React from 'react';
import {render, unmountComponentAtNode} from 'react-dom';
import InfoFeature from '../components/InfoFeature';
import Tagify from '@yaireo/tagify';

(function mapClubs () {

    const map = L.map('map', {
        center: [47, 0],
        zoom: 5,
        minZoom: 3,
        zoomControl: false,
        layers: [L.esri.basemapLayer('Topographic')]
    });
    map.attributionControl.addAttribution('<a target="_blank" href="https://ffvelo.fr">Fédération française de cyclotourisme</a>');

    //variables globales
    const mapElement = document.getElementById("map");
    const mapWidth = document.getElementById("map").offsetWidth;
    const clubsLayer = L.markerClusterGroup({ 
        chunkedLoading: true,
        iconCreateFunction: function (cluster) {
            let count = cluster.getChildCount();
            let digits = (count + '').length;
            return L.divIcon({
            html: count,
            className: 'cluster digits-' + digits,
            iconSize: null
            });
        }
    });

     //layer clubs
	const clubs = L.esri.Cluster.featureLayer({
        url: agolFeatureServices.clubs,
        token: token,
		pointToLayer: (feature, latlng) => {
			return L.marker(latlng, {
				icon: iconOin[2]
			});
		},
		iconCreateFunction: (cluster) => {
			let count = cluster.getChildCount();
			let digits = (count + '').length;
			return L.divIcon({
			  html: count,
			  className: 'cluster digits-' + digits,
			  iconSize: null
			});
		  },
    }).addTo(clubsLayer).addTo(map);

	//popups clubs
	clubs.bindPopup((layer)=>{
        let nom = layer.feature.properties.StructureNom;
        let ville = layer.feature.properties.AdrVille;
        let id = layer.feature.properties.StructureCode;
        let pratiqueRoute=layer.feature.properties.Pratique_Route;
        let pratiqueVttVtc=layer.feature.properties.Pratique_VTT_VTC;
        let tel = layer.feature.properties.AdrTel;
        let email = layer.feature.properties.AdrMail;
        let siteweb = layer.feature.properties.AdrWeb;
        let logo = layer.feature.properties.Logo;
        return L.Util.template(
            popupTemplateClubs(nom, ville, id, pratiqueRoute, pratiqueVttVtc, tel, email, siteweb, logo)
        );
    });

    /*evenements carte et layer point depart*/
	clubs.once('load', () => {
		if(location.pathname != menuUrl){
			let parts = location.href.split(menuUrl+"/");
			let urlid = parts[1].split("-");
			renderInfoFeature(clubs, urlid[0]);
		} else {
			zoomToMarkersAfterLoad(map, clubs);
		};
		//syncSidebar();
	});
	clubs.on('popupopen', () => {
		document.getElementById("buttonPopup").addEventListener('click', () => {
			renderInfoFeature(clubs, document.getElementById("buttonPopup").getAttribute('value'))
		}, true);
	});
	clubs.on('click', (e) => {
		console.log(e);
	});
	map.on("moveend", () => {
		clubs.setWhere(clubs.getWhere());
		syncSidebar();
	});
	map.on('overlayremove', () => {
		clubsLayer.clearLayers();
    }); 
    window.onpopstate = () => {
        backButtonNavigation(menuUrl, map, mapElement, mapWidth, clubs, renderInfoFeature);
    }
    /*fin evenements carte et layer point depart*/
    
    //barre de recherche geocodage - saisir une adresse et creer un point	
    const searchControl = L.esri.Geocoding.geosearch({
        useMapBounds:false,
        title: 'Rechercher un lieu / une adresse',
        placeholder: 'Où ?',
        expanded:true,
        collapseAfterResult: false,
        position: 'topright',
        providers: [
            arcgisOnlineProvider,
            L.esri.Geocoding.featureLayerProvider({ //recherche des couches dans la gdb oin
				url: agolFeatureServices.clubs,
                where: clubs.getWhere(),
                token: token,
				searchFields: ['StructureCode','StructureNom'],
				label: 'CLUBS FFVELO:',
				maxResults: 7,
				bufferRadius: 1000,
				formatSuggestion: (feature) => {
					return `${feature.properties.StructureCode} - ${feature.properties.StructureNom}`;
				}
			})
        ]
    }).addTo(map);
    //geocodeur en dehors de la carte
    const esriGeocoder = searchControl.getContainer();
    setEsriGeocoder(esriGeocoder, document.getElementById('geocoder'));
    //recup la requete en cours et applique aux resultats du geocodeur
	searchControl.on("requestend", function () {
		searchControl.options.providers[1].options.where = clubs.getWhere();
	});

    const syncSidebar = (arrResultsSidebar=[]) => {
        $("#features").empty();
        clubs.eachActiveFeature(function (layer) {
            if (map.hasLayer(clubs)) {
                if (map.getBounds().contains(layer.getLatLng())) {
                    arrResultsSidebar.push(
                        sidebarTemplateClubs(
                            layer.feature.properties.StructureCode,
                            layer.feature.properties.StructureNom,
                            layer.feature.properties.Pratique_Route,
                            layer.feature.properties.Pratique_VTT_VTC,
                            layer.feature.properties.VAE,
                            layer.feature.properties.AccueilHandicap,
                            layer.feature.properties.AccueilJeune,
                            layer.feature.properties.EcoleCyclo,
                            layer.feature.properties.VeloEcole,
                            layer.feature.properties.AdrWeb,
                            layer.feature.properties.AdrCP,
                            layer.feature.properties.AdrVille
                        )
                    );
                }
            }
        });
        addResultsToSidebar(arrResultsSidebar, clubs, renderInfoFeature);
    }

    // bookmarks html
    const sidebarTemplateClubs = (id,nom,pratiqueRoute,pratiqueVttVtc,vae,accueilHandicap,accueilJeune,ecoleCyclo,veloEcole,siteweb,cp,ville) => {
        siteweb = siteweb.includes("http") ? siteweb : "http://" + siteweb;  
        return (
            `<div class="card feature-card my-2 p-2" id="${id}" data-lat="" data-lon=""> 
                <div class="col align-self-center rounded-right">
                    <h6 class="d-block text-uppercase">${nom}</h6>
                    ${pratiqueRoute ? `<span class="badge badge-pill badge-light my-1 mr-1">Route</span>`: ""}
                    ${pratiqueVttVtc ? `<span class="badge badge-pill badge-light my-1 mr-1">VTT/VTC</span>` : ""}
                    ${vae ? `<span class="badge badge-pill badge-light my-1 mr-1">Vélo à assistance électrique</span>`: ""}
                    ${accueilHandicap ? `<span class="badge badge-pill badge-light my-1 mr-1">Accueil handicap</span>`: ""}
                    ${accueilJeune ? `<span class="badge badge-pill badge-light my-1 mr-1">Accueil jeune</span>` : "" }
                    ${ecoleCyclo ? `<span class="badge badge-pill badge-light my-1 mr-1">École cyclo</span>` : ""}
                    ${veloEcole ? `<span class="badge badge-pill badge-light my-1 mr-1">Vélo-école</span>` : ""}
                    ${siteweb ? `<span class="my-1 mr-1"><br/><i class="fas fa-globe"></i> <a href='${siteweb}' target='_blank'>Site web</a></span>` : ""}
                    <span class="my-1 mr-1"><br/><i class="fas fa-map-marked-alt"></i> ${cp}, ${ville}</span>
                </div>
            </div>`
        );
    }

    //composant fiche descriptive
    const renderInfoFeature = (Layer, id) => {
        Layer.eachFeature(function (layer) {
            if(layer.feature.properties.StructureCode == id){
                let infofeatureElement = document.getElementById('infofeature');
                unmountComponentAtNode(infofeatureElement);
                let lfp=layer.feature.properties;
                render(	
                    <InfoFeature 
                        idClub={id}
                        StructureCodeDepartement = {lfp.StructureCodeDepartement}
                        StructureCodeRegion = {lfp.StructureCodeRegion}
                        nom={lfp.StructureNom}
                        descr={""}
                        AccueilHandicap={lfp.AccueilHandicap ? "Accueil Handicap" : ""}
                        AccueilJeune = {lfp.AccueilJeune ? "Accueil Jeune" : ""}
                        EcoleCyclo = {lfp.EcoleCyclo ? "École Cyclo" : ""}
                        vae = {lfp.VAE ? "Vélo à assistance électrique" : ""}
                        VeloEcole = {lfp.VeloEcole ? "Vélo-école" : ""}
                        Pratique_Route={lfp.Pratique_Route ? "Route" : ""}
                        Pratique_VTT_VTC = {lfp.Pratique_VTT_VTC ? "VTT / VTC" : ""}
                        AdrEscalier = {lfp.AdrEscalier}
                        AdrNumVoie={lfp.AdrNumVoie}
                        AdrNomVoie={lfp.AdrNomVoie}
                        AdrCP={lfp.AdrCP}
                        AdrVille={lfp.AdrVille}
                        Nom_Correspondant={lfp.Nom_Correspondant}
                        Prenom_Correspondant={lfp.Prenom_Correspondant}
                        Mail_Correspondant={lfp.Mail_Correspondant}
                        Tel_Correspondant={lfp.Tel_Correspondant}
                        logo={lfp.Logo}
                        rootContainer={infofeatureElement}
                        map={map}
                        mapWidth = {mapWidth}
                        Layer={Layer}
                        layer={layer}
                        menuUrl={menuUrl}
                    />, infofeatureElement
                );
                navigationHistory(id, menuUrl,lfp.slug);
                //gestion de la carte
                let resize = mapWidth-document.getElementById("info-feature-content").offsetWidth;
                mapElement.style.width=resize+"px";
                map.invalidateSize();
                map.setView([layer.feature.geometry.coordinates[1], layer.feature.geometry.coordinates[0]], 12);
            }
        });
    }


    const tagifyDeps = new Tagify(document.querySelector("textarea[name=tagsDeps]"), {
        enforeWhitelist: true,
        whitelist : [],
        duplicates: false,
    });

    const tagifyRegs = new Tagify(document.querySelector("textarea[name=tagsRegs]"), {
        enforeWhitelist: true,
        whitelist : [],
        duplicates: false,
    });
    

})();