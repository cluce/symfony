import {
    addResultsToSidebar,
    zoomToMarkersAfterLoad,
    agolFeatureServices,
    arcgisOnlineProvider,
    iconOin,
    setEsriGeocoder, 
    backButtonNavigation, 
    navigationHistory,
    sidebarToggleControl,
    colsLayer,
    layerControlTitles,
    geolocationButton
} from "../services/mapConfig";

import {popupTemplatePoi, popupTemplateCircuit, popupTemplateManifs} from "../components/popup";
import React from 'react';
import {render, unmountComponentAtNode} from 'react-dom';
import InfoFeature from '../components/InfoFeature';
import Tagify from '@yaireo/tagify';
import flatpickr from "flatpickr";
import { French } from 'flatpickr/dist/l10n/fr.js';
import noUiSlider from 'nouislider';
import wNumb from "wnumb";

(function mapCalendrier () {
    
    //map
    const map = L.map('map', {
        center: [47, 0],
        zoom: 5,
        minZoom: 3,
        maxZoom: 16,
        zoomControl: true,
        layers: [L.esri.basemapLayer('Topographic')]
    });
    L.control.scale().addTo(map);//barre echelle
	const defaultLayer = L.esri.basemapLayer("Topographic").addTo(map);
    map.attributionControl.addAttribution('<a target="_blank" href="https://ffvelo.fr">Fédération française de cyclotourisme</a>');

    //variables globales
    const mapElement = document.getElementById("map");
    const sidebarElement = document.getElementById("sidebar");
    const mapWidth = document.getElementById("map").offsetWidth;

    //bouton toggle sidebar
    sidebarToggleControl(L.Control, map, sidebarElement, mapElement, mapWidth);

    // controle des couches et changement de fond de carte
	const baseLayers = {
		'Topographique (Esri)': defaultLayer,
		'Satellite (Esri)': L.esri.basemapLayer('Imagery'),
		'OpenStreetMap':  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
		}),
	};
	const overlayMaps = {
		"Cols": colsLayer()
	};
	L.control.layers(baseLayers, overlayMaps, {
		position: "bottomleft"
	}).addTo(map);
	layerControlTitles();
	map.addControl(new geolocationButton(map));

    //layer manifestations
    const manifsLayer = L.markerClusterGroup({ 
        chunkedLoading: true,
        iconCreateFunction: function (cluster) {
            let count = cluster.getChildCount();
            let digits = (count + '').length;
            return L.divIcon({
              html: count,
              className: 'cluster digits-' + digits,
              iconSize: null
            });
        }
    });
	const manifs = L.esri.Cluster.featureLayer({
        url: agolFeatureServices.manifs,
        token: token,
		pointToLayer: (feature, latlng) => {
			return L.marker(latlng, {
				icon: iconOin[1]
			});
		},
		iconCreateFunction: (cluster) => {
			let count = cluster.getChildCount();
			let digits = (count + '').length;
			return L.divIcon({
			  html: count,
			  className: 'cluster digits-' + digits,
			  iconSize: null
			});
		  },
    }).addTo(manifsLayer).addTo(map);

	//popups manifs
	manifs.bindPopup((layer)=>{
        let nom = layer.feature.properties.Nom;
        let id = layer.feature.properties.ManifId;
        let type = layer.feature.properties.Type;
        let date_debut = layer.feature.properties.Accueil_DateDbt;
        let proute = layer.feature.properties.Pratique_Route ? "Route" : false;
        let pvtt = layer.feature.properties.Pratique_VTT ? "VTT" : false;
        let pgravel = layer.feature.properties.Pratique_Gravel ? "Gravel" : false;
        let pmarche = layer.feature.properties.Pratique_Marche ? "Marche" : false;
        let durable = layer.feature.properties.DevDurable ? "Éco-responsable" : false;
        let psh = layer.feature.properties.AccesPSH ? "Accueil PSH" : false;
        let spefem = layer.feature.properties.SpeFem ? "Spécifique femmes" : false;
        let email = layer.feature.properties.AdresseMail;
        let siteweb = layer.feature.properties.AdresseWeb;
        return L.Util.template(
            popupTemplateManifs(nom, id, type, date_debut, proute, pvtt, pgravel, pmarche, durable, psh, spefem, email, siteweb)
        );
    });
    
    /*evenements carte et layer point depart*/
	manifs.once('load', () => {
		if(location.pathname != menuUrl){
			let parts = location.href.split(menuUrl+"/");
			let urlid = parts[1].split("-");
			renderInfoFeature(manifs, urlid[0]);
		} else {
			zoomToMarkersAfterLoad(map, manifs);
		};
		//syncSidebar();
	});
	manifs.on('popupopen', () => {
		document.getElementById("buttonPopup").addEventListener('click', () => {
			renderInfoFeature(manifs, document.getElementById("buttonPopup").getAttribute('value'))
		}, true);
	});
	manifs.on('click', (e) => {
		console.log(e);
	});
	map.on("moveend", () => {
		manifs.setWhere(manifs.getWhere());
		syncSidebar();
	});
	map.on('overlayremove', () => {
		manifsLayer.clearLayers();
    });
    window.onpopstate = () => {
        backButtonNavigation(menuUrl, map, mapElement, mapWidth, manifs, renderInfoFeature);
    }
	/*fin evenements carte et layer point depart*/

    //barre de recherche geocodeur
    const searchControl = L.esri.Geocoding.geosearch({
        useMapBounds:false,
        title: 'Rechercher un lieu / une adresse',
        placeholder: 'Où ?',
        expanded:true,
        collapseAfterResult: false,
        position: 'topright',
        providers: [
            arcgisOnlineProvider,
            L.esri.Geocoding.featureLayerProvider({ //recherche des couches dans la gdb oin
				url: agolFeatureServices.manifs,
                where: manifs.getWhere(),
                token: token,
				searchFields: ['ManifId','Nom','Structure'],
				label: 'MANIFESTATIONS FFVELO:',
				maxResults: 7,
				bufferRadius: 1000,
				formatSuggestion: (feature) => {
					return `${feature.properties.Nom} - ${feature.properties.Type}`;
				}
			})
        ]
    }).addTo(map);
    //geocodeur en dehors de la carte
    const esriGeocoder = searchControl.getContainer();
    setEsriGeocoder(esriGeocoder, document.getElementById('geocoder'));
    //recup la requete en cours et applique aux resultats du geocodeur
	searchControl.on("requestend", function () {
		searchControl.options.providers[1].options.where = manifs.getWhere();
    });
    
     //format de la date des manifs
     const handleDate = (str) => {
        let date = str.split('-');
        return date[2] + "/" + date[1] + "/" + date[0];
    }
    
    const syncSidebar = (arrResultsSidebar=[]) => {
        $("#features").empty();
        manifs.eachActiveFeature(function (layer) { //eachActiveFeature = fonction a ajouter dans esri-leaflet-cluster.js - attention si maj de la librairie
            if (map.hasLayer(manifs)) {
                if (map.getBounds().contains(layer.getLatLng())) {
                    arrResultsSidebar.push(
                        sidebarTemplateManifs(
                            layer.feature.properties.ManifId,
                            handleDate(layer.feature.properties.Accueil_DateDbt),
                            layer.feature.properties.Nom,
                            layer.feature.properties.Type,
                            layer.feature.properties.Pratique_Route,
                            layer.feature.properties.Pratique_VTT,
                            layer.feature.properties.Pratique_Gravel,
                            layer.feature.properties.Pratique_Marche,
                            layer.feature.properties.Accueil_CP,
                            layer.feature.properties.Accueil_Commune,
                            layer.feature.properties.DevDurable,
                            layer.feature.properties.AccesPSH,
                            layer.feature.properties.SpeFem
                        )
                    );
                }
            }
        });
        addResultsToSidebar(arrResultsSidebar, manifs, renderInfoFeature);
    }

    // sidebar card html
    const sidebarTemplateManifs = (id, date, nom, type, route, vtt, gravel, marche, cp, ville, durable, psh, spefem) => {
        return (
            `<div class="card feature-card my-2 p-2" id="${id}" data-lat="" data-lon=""> 
                <div class="col align-self-center rounded-right">
                    <h5 class="d-block text-uppercase"><i class="far fa-calendar-check"></i> ${date}</h5>
                    <h6 class="d-block text-uppercase">${nom}</h6>
                    ${route ? `<span class="badge badge-pill badge-light my-1 mr-1">Route</span>` : ``}
                    ${vtt ? `<span class="badge badge-pill badge-light my-1 mr-1">VTT</span>` : ``}
                    ${gravel ? `<span class="badge badge-pill badge-light my-1 mr-1">Gravel</span>` : ``}
                    ${marche ? `<span class="badge badge-pill badge-light my-1 mr-1">Marche</span>` : ``}
                    ${durable ? `<span class="badge badge-pill badge-light my-1 mr-1">Éco-responsable</span>` : ``}
                    ${psh ? `<span class="badge badge-pill badge-light my-1 mr-1">Accès PSH</span>` : ``}
                    ${spefem ? `<span class="badge badge-pill badge-light my-1 mr-1">Spécifique femmes</span>` : ``}
                    <span class="badge badge-pill badge-light my-1 mr-1">${type}</span>
                    <span class="my-1 mr-1"><br/><i class="fas fa-map-marked-alt"></i> ${cp}, ${ville}</span>
                </div>
            </div>`
        );
    }

    //composant fiche descriptive
    const renderInfoFeature = (Layer, id) => {
        Layer.eachFeature(function (layer) {
            if(layer.feature.properties.ManifId == id){
                const handleGpxManifs = () => {
                    let gpxFiles = layer.feature.properties.FichierGpx ? layer.feature.properties.FichierGpx.split(',') : false;
                    return gpxFiles;
                }
                let infofeatureElement = document.getElementById('infofeature');
                unmountComponentAtNode(infofeatureElement);
                render(	
                    <InfoFeature 
                        idManif={id}
                        nom={layer.feature.properties.Nom}
                        AccesPSH={layer.feature.properties.AccesPSH}
                        Accueil_CP={layer.feature.properties.Accueil_CP}
                        Accueil_Commune={layer.feature.properties.Accueil_Commune}
                        Accueil_DateDbt={layer.feature.properties.Accueil_DateDbt}
                        Accueil_Lieu={layer.feature.properties.Accueil_Lieu}
                        Accueil_Adresse={layer.feature.properties.Accueil_Adresse}
                        AdresseMail={layer.feature.properties.AdresseMail}
                        AdresseWeb={layer.feature.properties.AdresseWeb}
                        Arrivee_CP={layer.feature.properties.Arrivee_CP}
                        Arrivee_Commune={layer.feature.properties.Arrivee_Commune}
                        Arrivee_DateFin={layer.feature.properties.Arrivee_DateFin}
                        Arrivee_Lieu={layer.feature.properties.Arrivee_Lieu}
                        CategorieLabel={layer.feature.properties.CategorieLabel}
                        Contact_Nom={layer.feature.properties.Contact_Nom}
                        Contact_Prenom={layer.feature.properties.Contact_Prenom}
                        DevDurable={layer.feature.properties.DevDurable}
                        FichierFlyer={layer.feature.properties.FichierFlyer}
                        FichierGpx={handleGpxManifs()}
                        InscriptionLigne={layer.feature.properties.InscriptionLigne}
                        ModeleId={layer.feature.properties.ModeleId}
                        NumeroLabel={layer.feature.properties.NumeroLabel}
                        OinManifLicTarifAdulte={layer.feature.properties.OinManifLicTarifAdulte}
                        OinManifLicTarifJeune={layer.feature.properties.OinManifLicTarifJeune}
                        OinManifNonLicTarifAdulte={layer.feature.properties.OinManifNonLicTarifAdulte}
                        OinManifNonLicTarifJeune={layer.feature.properties.OinManifNonLicTarifJeune}
                        OinManifReserveLicencie={layer.feature.properties.OinManifReserveLicencie}
                        descr={layer.feature.properties.OinManifObservation}
                        OinMarcheDist_1={layer.feature.properties.OinMarcheDist_1}
                        OinMarcheDist_2={layer.feature.properties.OinMarcheDist_2}
                        OinMarcheDist_3={layer.feature.properties.OinMarcheDist_3}
                        OinMarcheDist_4={layer.feature.properties.OinMarcheDist_4}
                        OinMarcheDist_5={layer.feature.properties.OinMarcheDist_5}
                        OinMarcheDist_6={layer.feature.properties.OinMarcheDist_6}
                        OinRouteDeniv_1={layer.feature.properties.OinRouteDeniv_1}
                        OinRouteDeniv_2={layer.feature.properties.OinRouteDeniv_2}
                        OinRouteDeniv_3={layer.feature.properties.OinRouteDeniv_3}
                        OinRouteDeniv_4={layer.feature.properties.OinRouteDeniv_4}
                        OinRouteDeniv_5={layer.feature.properties.OinRouteDeniv_5}
                        OinRouteDeniv_6={layer.feature.properties.OinRouteDeniv_6}
                        OinRouteDiff_1={layer.feature.properties.OinRouteDiff_1}
                        OinRouteDiff_2={layer.feature.properties.OinRouteDiff_2}
                        OinRouteDiff_3={layer.feature.properties.OinRouteDiff_3}
                        OinRouteDiff_4={layer.feature.properties.OinRouteDiff_4}
                        OinRouteDiff_5={layer.feature.properties.OinRouteDiff_5}
                        OinRouteDiff_6={layer.feature.properties.OinRouteDiff_6}
                        OinRouteDist_1={layer.feature.properties.OinRouteDist_1}
                        OinRouteDist_2={layer.feature.properties.OinRouteDist_2}
                        OinRouteDist_3={layer.feature.properties.OinRouteDist_3}
                        OinRouteDist_4={layer.feature.properties.OinRouteDist_4}
                        OinRouteDist_5={layer.feature.properties.OinRouteDist_5}
                        OinRouteDist_6={layer.feature.properties.OinRouteDist_6}
                        OinVTTDeniv_1={layer.feature.properties.OinVTTDeniv_1}
                        OinVTTDeniv_2={layer.feature.properties.OinVTTDeniv_2}
                        OinVTTDeniv_3={layer.feature.properties.OinVTTDeniv_3}
                        OinVTTDeniv_4={layer.feature.properties.OinVTTDeniv_4}
                        OinVTTDeniv_5={layer.feature.properties.OinVTTDeniv_5}
                        OinVTTDeniv_6={layer.feature.properties.OinVTTDeniv_6}
                        OinVTTDiff_1={layer.feature.properties.OinVTTDiff_1}
                        OinVTTDiff_2={layer.feature.properties.OinVTTDiff_2}
                        OinVTTDiff_3={layer.feature.properties.OinVTTDiff_3}
                        OinVTTDiff_4={layer.feature.properties.OinVTTDiff_4}
                        OinVTTDiff_5={layer.feature.properties.OinVTTDiff_5}
                        OinVTTDiff_6={layer.feature.properties.OinVTTDiff_6}
                        OinVTTDist_1={layer.feature.properties.OinVTTDist_1}
                        OinVTTDist_2={layer.feature.properties.OinVTTDist_2}
                        OinVTTDist_3={layer.feature.properties.OinVTTDist_3}
                        OinVTTDist_4={layer.feature.properties.OinVTTDist_4}
                        OinVTTDist_5={layer.feature.properties.OinVTTDist_5}
                        OinVTTDist_6={layer.feature.properties.OinVTTDist_6}
                        OinGravelDeniv_1={layer.feature.properties.OinGravelDeniv_1}
                        OinGravelDeniv_2={layer.feature.properties.OinGravelDeniv_2}
                        OinGravelDeniv_3={layer.feature.properties.OinGravelDeniv_3}
                        OinGravelDeniv_4={layer.feature.properties.OinGravelDeniv_4}
                        OinGravelDeniv_5={layer.feature.properties.OinGravelDeniv_5}
                        OinGravelDeniv_6={layer.feature.properties.OinGravelDeniv_6}
                        OinGravelDiff_1={layer.feature.properties.OinGravelDiff_1}
                        OinGravelDiff_2={layer.feature.properties.OinGravelDiff_2}
                        OinGravelDiff_3={layer.feature.properties.OinGravelDiff_3}
                        OinGravelDiff_4={layer.feature.properties.OinGravelDiff_4}
                        OinGravelDiff_5={layer.feature.properties.OinGravelDiff_5}
                        OinGravelDiff_6={layer.feature.properties.OinGravelDiff_6}
                        OinGravelDist_1={layer.feature.properties.OinGravelDist_1}
                        OinGravelDist_2={layer.feature.properties.OinGravelDist_2}
                        OinGravelDist_3={layer.feature.properties.OinGravelDist_3}
                        OinGravelDist_4={layer.feature.properties.OinGravelDist_4}
                        OinGravelDist_5={layer.feature.properties.OinGravelDist_5}
                        OinGravelDist_6={layer.feature.properties.OinGravelDist_6}
                        Pratique_Gravel={layer.feature.properties.Pratique_Gravel}
                        Pratique_Marche={layer.feature.properties.Pratique_Marche}
                        Pratique_Route={layer.feature.properties.Pratique_Route}
                        Pratique_VTT={layer.feature.properties.Pratique_VTT}
                        SpeFem={layer.feature.properties.SpeFem}
                        Structure={layer.feature.properties.Structure}
                        StructureDep={layer.feature.properties.StructureDep}
                        StructureReg={layer.feature.properties.StructureReg}
                        Type={layer.feature.properties.Type}
                        heureDeCloture={layer.feature.properties.heureDeCloture}
                        heures={layer.feature.properties.heures}
                        rootContainer={infofeatureElement}
                        map={map}
                        mapWidth = {mapWidth}
                        Layer={Layer}
                        layer={layer}
                        menuUrl={menuUrl}
                    />, infofeatureElement
                );
                navigationHistory(id, menuUrl,layer.feature.properties.slug);
                //gestion de la carte
                let resize = mapWidth-document.getElementById("info-feature-content").offsetWidth;
                mapElement.style.width=resize+"px";
                map.invalidateSize();
                map.setView([layer.feature.geometry.coordinates[1], layer.feature.geometry.coordinates[0]], 12);
            }
        });
    }

    /*filtres de recherches */
    const dateFilter = () => {
        flatpickr("#flatpickr", {
            locale: {
                firstDayOfWeek: 1
            },
            "locale": "fr",
            showMonths: 2,
            dateFormat: "d-m-Y",
            //defaultDate: "today",
            minDate: "today",
            mode: "range",
            altInput: true,
            disableMobile: "true",
            altFormat: "d-m-Y",
            onChange: function (selectedDates, dateStr, instance) {
                let datestart = selectedDates[0];
                let dateend = new Date(selectedDates[1]);
                console.log(datestart, dateend);
            }
        });
    };dateFilter();
	const distSlider = document.getElementById('distSlider');
	noUiSlider.create(distSlider, {
		start: [0, 250],
		connect: true,
		range: {
			'min': 0,
			'max': 250
		},
		format: wNumb({
			decimals: 0, // default is 2
		}),
		pips: {mode: 'count', values: 5},
	});
	const denivSlider = document.getElementById('denivSlider');
	noUiSlider.create(denivSlider, {
		start: [0, 1000],
		connect: true,
		range: {
			'min': 0,
			'max': 1000
		},
		format: wNumb({
			decimals: 0, // default is 2
		}),
		pips: {mode: 'count', values: 3},
	});
	const tagifyClubs = new Tagify(document.querySelector("textarea[name=tagsClubs]"), {
        enforeWhitelist: true,
        whitelist : [],
        duplicates: false,
    });

	const getFilters=(filters=[])=>{
		const elements = document.querySelectorAll(".typeVeloFilter");
		[...elements].map(item => {
			item.addEventListener('change', (e) => {
				if (item.checked === true) {
					filters.push(item.value);
				} else {
					filters.splice(filters.indexOf(item.value),1);
				}
				setFilters(filters);
			});
		});
		tagifyClubs
            .on('add', (e) => {
                filters.push(e.detail.data.value);
                filters= filters.join("");
            })
            .on('remove', (e) => {
                filters.splice(filters.indexOf(e.detail.data.value),1);
                filters= filters.join("");
            });
		distSlider.noUiSlider.on("change", (data) => {
            let distList=[
                "OinRouteDist_1", "OinRouteDist_2", "OinRouteDist_3", "OinRouteDist_4", "OinRouteDist_5", "OinRouteDist_6",
                "OinVTTDist_1", "OinVTTDist_2", "OinVTTDist_3", "OinVTTDist_4", "OinVTTDist_5", "OinVTTDist_6",
                "OinGravelDist_1", "OinGravelDist_2", "OinGravelDist_3", "OinGravelDist_4", "OinGravelDist_5", "OinGravelDist_6",
                "OinMarcheDist_1", "OinMarcheDist_2", "OinMarcheDist_3", "OinMarcheDist_4", "OinMarcheDist_5", "OinMarcheDist_6",
            ]
            distList.map((dist)=>{filters.splice(filters.indexOf(dist),1)});
            distList.map((dist)=>{
                if ((data[0] > 0) && (data[1] < 250)) {
                    filters.push(`${dist} >= ${data[0]} AND ${dist} <= ${data[1]}`);
                } else if ((data[0] > 0) && (data[1] == 250)) {
                    filters.push(`${dist} >= ${data[0]}`);
                } else if ((data[0] == 0) && (data[1] < 250)) {
                    filters.push(`${dist} > 0 AND ${dist} <= ${data[1]}`);
                }
            });
            setFilters(filters);
		});
		denivSlider.noUiSlider.on("change", (data) => {
            let denivList=[
                "OinRouteDeniv_1", "OinRouteDeniv_2", "OinRouteDeniv_3", "OinRouteDeniv_4", "OinRouteDeniv_5", "OinRouteDeniv_6",
                "OinVTTDeniv_1", "OinVTTDeniv_2", "OinVTTDeniv_3", "OinVTTDeniv_4", "OinVTTDeniv_5", "OinVTTDeniv_6",
                "OinGravelDeniv_1", "OinGravelDeniv_2", "OinGravelDeniv_3", "OinGravelDeniv_4", "OinGravelDeniv_5", "OinGravelDeniv_6",
                "OinMarcheDeniv_1", "OinMarcheDeniv_2", "OinMarcheDeniv_3", "OinMarcheDeniv_4", "OinMarcheDeniv_5", "OinMarcheDeniv_6",
            ]
            //denivList.map((deniv)=>{filters.splice(filters.indexOf(deniv),1)});
            denivList.map((deniv)=>{
                if ((data[0] > 0) && (data[1] <= 1000)) {
                    filters.push(`${deniv} >= ${data[0]} AND ${deniv} <= ${data[1]}`);
                } else if ((data[0] > 0) && (data[1] == 1000)) {
                    filters.push(`${deniv} >= ${data[0]}`);
                } else if ((data[0] == 0) && (data[1] < 1000)) {
                    filters.push(`${deniv} > 0 AND ${deniv} <= ${data[1]}`);
                }
            });
            setFilters(filters);
		});
    };getFilters();
    
	const setFilters = (filters, arrResult=[]) => {
        console.log(filters);
		let type = filters.filter(f => f.includes("Pratique_")).join(" OR ");
        let distance = filters.filter(f => f.includes("Dist_")).join(" OR ");
        let denivele = filters.filter(f => f.includes("Deniv_")).join(" OR ");

        distance.length>0 ? arrResult.push(`(${distance})`) : false;
        type.length>0 ? arrResult.push(`(${type})`) : false;
        denivele.length>0 ? arrResult.push(`(${denivele})`) : false;
        
		let result = arrResult.join(" AND ");
		//console.log(result);
		manifs.setWhere(result);
		zoomToMarkersAfterLoad (map, manifs);
	}
    /*fin des filtres de recherches */

    
})();
