import { unmountComponentAtNode } from 'react-dom';
import {popupTemplateCols} from "../components/popup";

export const pointDepartLayerGroup = L.markerClusterGroup({ chunkedLoading: true });
export const poiLayerGroup = L.markerClusterGroup({ chunkedLoading: true });
export const circuitLayerGroup = L.featureGroup();

//geojson vide pour fleches directionnelles
const arrowLayer = L.geoJson(null, {
    onEachFeature: function (feature, layer) {
        let coords = layer.getLatLngs();
        L.polylineDecorator(coords, {
            patterns: [{
                offset: 25,
                repeat: 100,
                symbol: L.Symbol.arrowHead({
                    pixelSize: 15,
                    pathOptions: {
                        fillOpacity: 1,
                        weight: 0
                    }
                })
            }]
        }).addTo(circuitLayerGroup);
    }
}).addTo(circuitLayerGroup);

//liste des couches arcgis online
export const agolFeatureServices = {
    circuit:"https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/VELOENFRANCE/FeatureServer/4",
    circuitElevation:"https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/VELOENFRANCE/FeatureServer/3",
    bcnbpfProvinces : "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/VELOENFRANCE/FeatureServer/2",
    bcnbpfProvincesCentroides : "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/BCN_BPF_PROVINCE_CENTROIDE/FeatureServer/0",
    pointDepartCircuit : "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/VELOENFRANCE/FeatureServer/1",
    poi : "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/VELOENFRANCE/FeatureServer/0",
    bonneAdresse : "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/BONNES_ADRESSES/FeatureServer/0",
    fournisseur : "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/VELOENFRANCE/FeatureServer/7",
    circuitFournisseur : "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/VELOENFRANCE/FeatureServer/6",
    cols: "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/COLS/FeatureServer/0",
    manifs: "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/OIN_TEST2/FeatureServer/0",
    clubs: "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/OIN_TEST2/FeatureServer/1",
    randonneePermanente: "https://services5.arcgis.com/x7yCK2swiqKDYsU6/arcgis/rest/services/OIN_TEST2/FeatureServer/2",
}

//url params pour requete sur les layers
export const queryString = (url) => {
    if(url){
        let params = url.split('?')[1];
        params.includes("&") ? params = params.split("&").join(" ") : false;
        params.includes("%20") ? params = params.split("%20").join(" ") : false;
        params.includes("%27") ? params = params.split("%27").join("'") : false;
        params.includes("%3C") ? params = params.split("%3C").join("<") : false;
        params.includes("%3D") ? params = params.split("%3D").join("=") : false;
        params.includes("%3E") ? params = params.split("%3E").join(">") : false;
        return params;
    } else {
        return document.getElementById("filters").value;
    }
}

//zoom auto sur markers apres chargement
export const zoomToMarkersAfterLoad = (map, markers) => {
    markers.query().where(markers.getWhere()).bounds(function (error, latlngbounds) {
        if (error) {
            console.log(error);
        } else {
            map.fitBounds(latlngbounds);
        }
    });
}

//insertion de titres dans le controle des couches
export const layerControlTitles = () => {
    $('<h6 id="mapTitle">Fonds de cartes : </h6>').insertBefore(
        'div.leaflet-control-layers-base');
    $('<h6 id="mapTitle">Points d\'intérêts :</h6>').insertBefore(
        'div.leaflet-control-layers-overlays');
}

//trace circuit onclick
export const showCircuit=(map, id, difficulte, infofeature, coordsDepart)=>{
    pointDepartLayerGroup.clearLayers();
    circuitLayerGroup.clearLayers();
    let coords=[];
    const color = () => {
        switch (difficulte) {
            case 1:
                return 'green';
            case 2:
                return 'blue';
            case 3:
                return 'red';
            case 4:
                return 'black';
            default:
                return 'blue';
        }
    }
	const circuitLayer = L.esri.featureLayer({
		url: agolFeatureServices.circuit,
        where: "IDENTIFIANT_CIRCUIT =" + id,
        style: { color: color(), opacity: 0.75, weight: 3 }
    }).addTo(circuitLayerGroup);
    circuitLayer.once('load', (e) => {
        circuitLayer.eachFeature((layer)=>{
            layer.feature.geometry.coordinates.forEach(lngLat => {
                //console.log(layer.feature.properties.IDENTIFIANT_CIRCUIT)
                //console.log(lngLat.flat().reverse());
                //lngLat.flatMap(xy => /*coords.push([xy[1], xy[0]])*/coords.push([xy[1], xy[0]]));
                coords.push([lngLat[1], lngLat[0]]);
            });
        });
        L.polylineDecorator(coords, {
            patterns: [{
                offset: 25,
                repeat: 100,
                symbol: L.Symbol.arrowHead({
                    pixelSize: 15,
                    pathOptions: {
                        fillOpacity: 1,
                        weight: 0,
                        color: color()
                    }
                })
            }]
        }).addTo(circuitLayerGroup);
        if(infofeature){
            circuitLayer.query().where(circuitLayer.getWhere()).bounds(function (error, latlngbounds) {
                if (error) {
                    //map.setView(coordsDepart,16);
                    console.log(error);
                } else {
                    map.fitBounds(latlngbounds);
                }
            });
        }
    });
    circuitLayerGroup.addTo(map);
}

//icones points de depart des circuits 1= tres facile, 2= facile, 3=difficile, 4=tres difficile
export const iconPointDepart = {
    1: L.icon({
        iconUrl: '/build/images/icon/vert.png',
        iconSize: [24, 40],
        iconAnchor: [13.5, 17.5],
    }),
    2: L.icon({
        iconUrl: '/build/images/icon/bleu.png',
        iconSize: [24, 40],
        iconAnchor: [13.5, 13.5],
    }),
    3: L.icon({
        iconUrl: '/build/images/icon/rouge.png',
        iconSize: [24, 40],
        iconAnchor: [13.5, 17.5],
    }),
    4: L.icon({
        iconUrl: '/build/images/icon/noir.png',
        iconSize: [24, 40],
        iconAnchor: [13.5, 13.5],
    }),
};

export const iconOin = {
    1: L.icon({
        iconUrl: '/build/images/icon/icon-event.png',
        iconSize: [29, 42],
        iconAnchor: [13.5, 17.5]
    }),
    2: L.icon({
        iconUrl: '/build/images/icon/icon-club.png',
        iconSize: [40, 34],
        iconAnchor: [13.5, 17.5]
    }),
};

//icones pois
export const iconPoi = {
    101: L.icon({
        iconUrl: '/build/images/icon/bcn.png',
        iconSize: [50, 50],
        iconAnchor: [13.5, 17.5],
    }),
    102: L.icon({
        iconUrl: '/build/images/icon/curiosite.png',
        iconSize: [37, 37],
        iconAnchor: [13.5, 13.5],
    }),
    104: L.icon({
        iconUrl: '/build/images/icon/pnr.png',
        iconSize: [37, 37],
        iconAnchor: [13.5, 17.5],
    }),
    106: L.icon({
        iconUrl: '/build/images/icon/station.png',
        iconSize: [37, 37],
        iconAnchor: [17.5, 13.5],
    }),
    201: L.icon({
        iconUrl: '/build/images/blocmarque/blocmarque_club.svg',
        iconSize: [37, 37],
        iconAnchor: [17.5, 13.5],
    }),
    202: L.icon({
        iconUrl: '/build/images/icon/ecole.png',
        iconSize: [37, 37],
        iconAnchor: [17.5, 13.5],
    }),
    203: L.icon({
        iconUrl: '/build/images/icon/coreg.png',
        iconSize: [37, 37],
        iconAnchor: [17.5, 13.5],
    }),
    204: L.icon({
        iconUrl: '/build/images/icon/bvtt.svg',
        iconSize: [37, 37],
        iconAnchor: [17.5, 13.5],
    }),
    205: L.icon({
        iconUrl: '/build/images/icon/tv.svg',
        iconSize: [37, 37],
        iconAnchor: [17.5, 13.5],
    }),
    206: L.icon({
        iconUrl: '/build/images/icon/codep.png',
        iconSize: [37, 37],
        iconAnchor: [17.5, 13.5],
    }),
    301: L.icon({
        iconUrl: '/build/images/icon/ba.svg',
        iconSize: [50, 50],
        iconAnchor: [13.5, 17.5],
    }),
    302: L.icon({
        iconUrl: '/build/images/icon/ba.svg',
        iconSize: [50, 50],
        iconAnchor: [13.5, 13.5],
    }),
    303: L.icon({
        iconUrl: '/build/images/icon/ba.svg',
        iconSize: [50, 50],
        iconAnchor: [13.5, 17.5],
    }),
    304: L.icon({
        iconUrl: '/build/images/icon/ba.svg',
        iconSize: [50, 50],
        iconAnchor: [17.5, 13.5],
    }),
    305: L.icon({
        iconUrl: '/build/images/icon/ba.svg',
        iconSize: [50, 50],
        iconAnchor: [17.5, 13.5],
    }),
    306: L.icon({
        iconUrl: '/build/images/icon/ba.svg',
        iconSize: [50, 50],
        iconAnchor: [17.5, 13.5],
    }),
    307: L.icon({
        iconUrl: '/build/images/icon/ba.svg',
        iconSize: [50, 50],
        iconAnchor: [17.5, 13.5],
    }),
    308: L.icon({
        iconUrl: '/build/images/icon/ba.svg',
        iconSize: [50, 50],
        iconAnchor: [17.5, 13.5],
    }),
    309: L.icon({
        iconUrl: '/build/images/icon/homecamper.png',
        iconSize: [50, 50],
        iconAnchor: [17.5, 13.5],
    }),
    311: L.icon({
        iconUrl: '/build/images/icon/ba.svg',
        iconSize: [50, 50],
        iconAnchor: [17.5, 13.5],
    }),
    401: L.icon({
        iconUrl: '/build/images/icon/gare.png',
        iconSize: [37, 37],
        iconAnchor: [17.5, 13.5],
    }),
    402: L.icon({
        iconUrl: '/build/images/icon/ot.png',
        iconSize: [37, 37],
        iconAnchor: [17.5, 13.5],
    }),
    403: L.icon({
        iconUrl: '/build/images/icon/ot.png',
        iconSize: [37, 37],
        iconAnchor: [17.5, 13.5],
    }),
};

export const iconCols = L.icon({
    iconUrl: '/build/images/icon/club-des-cent-cols.png',//'../assets/img/icon/clubs-des-cent-cols.png',
    iconSize: [35, 35],
    iconAnchor: [13.5, 17.5],
});

//libelles bonnes adresses
export const baName = {
    301: "Gîte",
    302: "Restaurant",
    303: "Réparateur - loueur de vélos",
    304: "Hôtel-restaurant",
    305: "Appart'hôtel",
    306: "Camping-Bungalow",
    307: "Chambre d'hôte",
    308: "Hébergement collectif",
    309: "Camping chez l'habitant<br />HomeCamper",
    311: "Village vacances"
}

//libelles sous types circuits
export const ssTypeCircuit= {
    101: "Boucle cyclotouristique",
    102: "Liaison",
    201: "Randonnée permanente",
    202: "L'Avenue Verte London-Paris®",
    203: "La Véloscénie®",
    204: "La ViaRhôna®",
    205: "La Vélo Francette®",
    206: "Le Tour de Manche®",
    207: "Le Canal des Deux Mers®",
    208: "La Loire à vélo® (Eurovelo 6)",
    209: "Véloroute et Voie Verte",
    210: "Le Grand Tour des Préalpes d'Azur®",
    211: "La Vélodyssée® (Eurovelo 1)",
    212: "La Grande Traversée du Jura®",
    213: "Le Tour de la Gironde à Vélo®",
    214: "La Vélomaritime®",
    301: "Semaine Fédérale",
    302: "Tour Cyclotouriste",
    303: "Toutes à vélo",
    305: "CycloMontagnardes",
    306: "Raid VTT",
    307: "La Vel'Europe",
    218: "La Voie Bleue®"
}

//libelles pois
export const poiName = {
    101: "Les plus beaux sites (BCN-BPF)",
    102: "Curiosités",
    104: "Parc naturel régional",
    106: "Station verte",
    401: "Gare SNCF",
    403: "Office de tourisme",
    204: "Base VTT",
	205: "Territoire Vélo",
};

//libelles type velo
export const typeVeloName = {
    1: "Route",
    2: "VTT",
    3: "VTC",
    4: "Gravel",
};

//libelles difficultes
export const difficulteName = {
    1: "Très facile",
    2: "Facile",
    3: "Difficile",
    4: "Très difficile"
};

//classes font awesome difficulte
export const difficulteFontAwesome = {
    1: 'fas fa-star text-success',
    2: 'fas fa-star text-primary',
    3: 'fas fa-star text-danger',
    4: 'fas fa-star text-dark'
};

//libelles bonnes adresses
export const structureType = {
    1: "Club",
    2: "Comité départemental",
    3: "Comité régional",
    4: "Membre individuel"
}

//bouton geolocalisation
export const geolocationButton = L.Control.extend({
    options: {
        position: 'bottomleft'
    },
    onAdd: function (map) {
        var container = L.DomUtil.create('div',
            'leaflet-bar leaflet-control leaflet-control-custom'
        );
        container.style.backgroundColor = "white";
        container.style.backgroundImage =
            "url('../build/images/geocoder/target.png')";
        container.style.width = '30px';
        container.style.height = '30px';
        container.style.cursor = 'pointer';
        container.onclick = function () {
            getLocationLeaflet(map);
        }
        return container;
    }
});
function onLocationFound(e) {
    geocodeReverse(e);
}
function onLocationError(e) {
    alert(e.message);
}
function getLocationLeaflet(map) {
    map.on('locationfound', onLocationFound);
    map.on('locationerror', onLocationError);
    map.locate({
        setView: true,
        maxZoom: 13
    });
}
function geocodeReverse(e) {
    let geocodeService = L.esri.Geocoding.geocodeService();
	geocodeService.reverse().latlng(e.latlng).run(function (error, result){});
}

//esri geocodeur
export const geocodeService = L.esri.Geocoding.geocodeService();
export const arcgisOnlineProvider = L.esri.Geocoding.arcgisOnlineProvider({
	countries: "FR, GLP, REU, GUF, NCL, MYT, MTQ, PYF, BLM",
	label: 'ADRESSES ET LIEUX :',
	maxResults: 3
});
export const setEsriGeocoder = (el, newParent) => {
	newParent.appendChild(el);
}

//localise le poi ou le point de depart de la sidebar sur la carte
export const locateMarker = (lat,lon,map) => {
    let highlightIcon=L.icon({
        iconUrl: '/build/images/rings.svg',
        iconSize: [100, 100],
    });
    let highLightLayerGroup = L.featureGroup();
    let highlightGeojson = {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [lon, lat]
        }
    };
    L.geoJSON(highlightGeojson, {
        pointToLayer: function (feature, latlng) {
            return L.marker(latlng, {icon: highlightIcon});
        }
    }).addTo(highLightLayerGroup);
    highLightLayerGroup.addTo(map); 
    setTimeout(()=>{
        highLightLayerGroup.clearLayers();
    }, 3000);
}

//afficher plus de resultats dans la sidebar
export const addResultsToSidebar=(arrResultsSidebar, Layer, renderInfoFeature)=>{
    $("#moreResults").remove();
    for (var i = 0; i < 10; i++) {
        $("#features").append(arrResultsSidebar[i]);
    }
    let cardResult=document.querySelectorAll('.feature-card');
    for (let i=0; i<cardResult.length; i++){
        let id=cardResult[i].getAttribute('id');
        document.getElementById(id).addEventListener('click', () => {
            renderInfoFeature(Layer, id);
        }, true);
    }
    if (arrResultsSidebar.length > 0) {
        $("#countResults").remove();
        $("#features").prepend(`<div id="countResults" class='text-center'><h5>${arrResultsSidebar.length} éléments disponibles.</h5></div>`);
        if (arrResultsSidebar.length > 10) {
            $("#features").append(
                `<button id="moreResults" class="btn btn-primary btn-block mx-auto text-uppercase m-2">Plus de résultats</button>`
            );
            document.getElementById("moreResults").addEventListener("click", () => {
                addResultsToSidebar(arrResultsSidebar, Layer, renderInfoFeature)
            }, true);
        } else {
            $("#moreResults").remove();
        }
    } else {
        $("#features").html(
            `<div class='text-center pt-5'><strong>AUCUN RÉSULTAT.</strong></div>`
        );
    }
    arrResultsSidebar.splice(0, i);
}

//gestion historique des fiches descriptives
export const navigationHistory = (id, menuLocation, slug) => {
    let url = location.href;
    let urlid = url.split("/").pop();
    slug = slug ? slug : id;
    if (urlid.includes(id)){
		history.replaceState(null, null, `${menuLocation}/${slug}`);
    } else {
        history.pushState(null, null, `${menuLocation}/${slug}`);
    }
}

//gestion des boutons precedent/suivant
export const backButtonNavigation = (menuLocation, map, mapElement, mapSize, Layer, renderInfoFeature) => {
    if((location.pathname != menuLocation)){
        let parts = location.href.split(menuLocation+"/");
        let urlid = parts[1].split("-");
        renderInfoFeature(Layer, urlid[0]);
    } else {
        let infofeatureElement = document.getElementById('infofeature');
        document.querySelectorAll(".typeVeloFilter, .difficulteFilter, .tempsFilter").forEach(item => {
            if(item.checked==true){
                item.checked=false
            }
        });
        circuitLayerGroup.clearLayers();
        poiLayerGroup.clearLayers();
        Layer.options.url && infofeatureElement.value ? Layer.setWhere(infofeatureElement.value) : false; //revient au filtre precedent
        Layer.options.url ? Layer.setWhere(queryString(location.search)) : false;
        unmountComponentAtNode(infofeatureElement);
        mapElement.style.width= mapSize+"px";
        map.invalidateSize();
        Layer.options.url ? zoomToMarkersAfterLoad (map, Layer) : false;
    }
}

/*photos amazon s3 */
const awsConfig = () => {
    AWS.config.region = "eu-central-1";
    var accessKeyId = '';
    var secretAccessKey = '';
    AWS.config.credentials = new AWS.Credentials(accessKeyId, secretAccessKey);
    return;
}
//photos mini dans fiche descr
const photoAws = (menuLocation, id) => {
   awsConfig();
   var s3Client = new AWS.S3();
   var listAmazonS3Photos = function(id) {
       var params = {
           Bucket: 'veloenfrance',
           Prefix: menuLocation + "/" + id + '/Photos/'
       };
       s3Client.listObjects(params, function(error, response) {
           if (error) {
               console.error('Error: ' + error);
           } else if (response.error) {
               console.error('Response.Error: ' + response.error);
           }
           for (photoInfo of response.Contents) {
               var photoUrl = photoInfo.Key;
               //recup les credits photos
               exifr.parse("https://s3.eu-central-1.amazonaws.com/veloenfrance/"+photoUrl)
               .then(exif => exif.Copyright ? document.getElementById("copyrightFicheDescr").innerHTML="<small>"+exif.Copyright+"</small>" : false)
               .catch(console.error)
               if (photoUrl.includes("_330")) {
                   createPhotoPanel(photoUrl);
               }
           }
       });
   }
   /* Création des photos récupérées */
   var createPhotoPanel = function(photoUrl) {
       var photoDiv = "";
       if ($('#listPhotos').children().length > 0) {
           photoDiv = $("<div class='carousel-item'>");
       } else {
           photoDiv = $("<div class='carousel-item active'>");
       }
       var photo = $(`<img /><p id="copyrightFicheDescr" class="bg-light text-dark-50" style="position:absolute; bottom:0; right:0; margin:auto;"><small></small></p>`);
       photo.attr('src', `https://s3.eu-central-1.amazonaws.com/veloenfrance/${photoUrl}`);
       photoDiv.append(photo);
       $('#listPhotos').append(photoDiv);
   }
   listAmazonS3Photos(id);
}

export function colsLayer(){
    colsLayer = L.esri.Cluster.featureLayer({
        url: agolFeatureServices.cols,
        token: token,
        pointToLayer: function (feature, latlng) {
            return L.marker(latlng, {
                icon: iconCols
            });
        }
    });
    colsLayer.bindPopup(function (layer) {
        var nom = layer.feature.properties.Nom_complet ;
        var alti = layer.feature.properties.Alti;
        var type = layer.feature.properties.Type;
        var url = layer.feature.properties.URL;
        return L.Util.template(popupTemplateCols(nom, alti, type, url));
    });
    return colsLayer;
}

export const sidebarToggleControl = (LeafletControl, map, sidebarElement, mapElement, mapSize) => {
    let infoFeatureElement = document.getElementById("infofeature");
    let toggleControl = LeafletControl.extend({
        options: {
            position: 'topleft'
        },
        onAdd: function(map) {	
            console.log(sidebarElement.offsetHeight);
            let computedStyle = getComputedStyle(sidebarElement)
            let height = sidebarElement.clientHeight // width with padding
            height -= parseFloat(computedStyle.paddingTop) + parseFloat(computedStyle.paddingBottom)
            sidebarElement.style.display="block";
            let btn = L.DomUtil.create('button');
            let fa = L.DomUtil.create('i', 'fas fa-toggle-on fa-2x');
            btn.appendChild(fa);
            btn.id = 'toggleButton';
            btn.style.width = '50px';
            btn.style.height = '50px';
            btn.style.transform = `translate(0%, ${(sidebarElement.offsetHeight)-237}%)`;
            btn.onclick=()=>{
                let resize=sidebarElement.offsetWidth;
                if(sidebarElement.style.display=="block"){
                    if(infoFeatureElement.firstChild){
                    } else {
                        sidebarElement.style.display="none";
                        mapElement.style.width=mapSize+resize+"px";
                        fa.className = "fas fa-toggle-off fa-2x";
                    }
                } else {
                    sidebarElement.style.display="block";
                    mapElement.style.width="";
                    fa.className = "fas fa-toggle-on fa-2x";
                }
                map.invalidateSize();	
            }
            return btn;
        },
    });
    map.addControl(new toggleControl());
}


