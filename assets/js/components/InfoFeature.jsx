import React, {useState, useEffect} from 'react';
import { unmountComponentAtNode } from 'react-dom';
import { ssTypeCircuit, circuitLayerGroup, poiLayerGroup } from "../services/mapConfig";

const InfoFeature = (props) => {

  const mapDiv = document.getElementById("map");

  const [toggle, setToggle] = useState(false);
  const handleToggle = () => {
    const infoFeatureContentDivSize = document.getElementById("info-feature-content").offsetWidth;
    const wrapper = document.getElementById("infofeature-wrapper");
    setToggle(!toggle);
    if(toggle==true){
      wrapper.style.display="block";
      document.getElementById("i-toggle").className="fas fa-toggle-on";
      let resize = props.mapWidth-infoFeatureContentDivSize;
			mapDiv.style.width=resize+"px";
      props.map.invalidateSize();
      [...document.querySelectorAll(".buttons")].map(item=>{
        item.style.transform = "translateX(-55px)";
      });
    } else {
      const moveButtons = infoFeatureContentDivSize-55;//55 = valeur du decalage renseigné dans le fichier app.css
      wrapper.style.display="none";
      document.getElementById("i-toggle").className="fas fa-toggle-off";
      let resize = props.mapWidth-wrapper.offsetWidth;
			mapDiv.style.width=resize+"px";
      props.map.invalidateSize();
      [...document.querySelectorAll(".buttons")].map(item=>{
        item.style.transform = "translateX("+moveButtons+"px)";
      });
    }
  };

  const handleClose = () => {
    circuitLayerGroup.clearLayers();poiLayerGroup.clearLayers();
    props.Layer.setWhere(props.whereClause);
    let url = props.locationSearch ? props.menuUrl + props.locationSearch : props.menuUrl;
    history.pushState(null, null, url);
    mapDiv.style.width = props.mapWidth+"px";
    props.map.invalidateSize();
    props.toggleButton ? props.toggleButton.style.display="block" : false;
    unmountComponentAtNode(props.rootContainer);
  }

  const handleDate = (str) => {
    let date = str.split('-');
    return date[2] + "/" + date[1] + "/" + date[0];
  }

  const [contacts, setContacts] = useState([]);
  useEffect(() => {
    props.idPoi ? 
      props.queryContactLabels.where("IDENTIFIANT_FOURNISSEUR="+props.idPoi).run((error, featureCollection)=>{
        if (error) {
          console.log(error);
          return;
        }
        for (let i=0,len=featureCollection.features.length; i<len; i++){
          setContacts(featureCollection.features[i].properties);
        }
      }) : 
    false;
  }, []);
  useEffect(() => {
    props.idCircuit ? 
      props.queryCircuitFournisseur.where("REFERENCE_CIRCUIT="+props.idCircuit).run((error, featureCollection)=>{
        if (error) {
          console.log(error);
          return;
        }
        for (let i=0,len=featureCollection.features.length; i<len; i++){
          props.queryFournisseur.where("IDENTIFIANT_FOURNISSEUR ="+featureCollection.features[i].properties.REFERENCE_FOURNISSEUR).run(function(error, featureCollection){
            if (error) {
              console.log(error);
              return;
            }
            for (let j=0,len=featureCollection.features.length; j<len; j++){
              setContacts(contacts => [...contacts, featureCollection.features[j].properties]);
            }
        });
      } 
    }) : 
    false;
  }, []);


  return (
    <div id="info-feature-content" className="container col-3 infos-features d-flex justify-content-between flex-column">
      <div id="buttons-infofeature" className="buttons">
        <button id="close-infofeature" onClick={handleClose}><i className="fas fa-times"></i></button>
        <button id="toggle-infofeature" onClick={handleToggle} ><i id="i-toggle" className="fas fa-toggle-on"></i></button>
      </div>
      <section id="infofeature-wrapper" className="overflow-auto">
        <div id="info-title" className="p-2">
          <div className="container">
          
          {props.idCircuit && 
            <>
              <div className="row">
                <div className="col">
                  <div className="container text-center">
                    <img width='80%' className='img-fluid text-center' src={"https://s3.eu-central-1.amazonaws.com/veloenfrance/CIRCUITS/"+props.idCircuit+"/Photos/mini/1_330.jpg"}/>
                    <h5 className="text-uppercase pt-3">{props.nom}</h5>
                    <span className="badge badge-pill badge-light my-1 mr-1"><i className="fas fa-bicycle"></i> {props.typevelo}</span>
                    <span className="badge badge-pill badge-light my-1 mr-1"><i className="far fa-star"></i> {props.difficulte}</span>
                    {props.distance && <span className="badge badge-pill badge-light my-1 mr-1"><i className="fas fa-fw fa-road"></i> {props.distance} km</span>}
                    {props.denivele && <span className="badge badge-pill badge-light my-1 mr-1"><i className="fas fa-chart-line"></i> {props.denivele} m</span>}
                    {props.soustype && <span className="badge badge-pill badge-light my-1 mr-1">{ssTypeCircuit[props.soustype]}</span>}
                  </div>
                </div>
              </div>
              <nav className="pt-3">
                <div className="nav nav-tabs" id="nav-tab" role="tablist">
                  <a className="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Description</a>
                  <a className="nav-item nav-link" id="nav-export-tab" data-toggle="tab" href="#nav-export" role="tab" aria-controls="nav-export" aria-selected="false">Export</a>
                  <a className="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contacts</a>
                </div>
              </nav>
              <div className="tab-content pt-3" id="nav-tabContent">
                <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                  {props.descr && <div className="row"><div className="col description overflow-auto pb-5">{props.descr}</div></div>}
                </div>
                <div className="tab-pane fade" id="nav-export" role="tabpanel" aria-labelledby="nav-export-tab">
                    <h5>Exporter au format:</h5>
                    <ul className="text-primary text-break">
                        <li><a href={`https://s3.eu-central-1.amazonaws.com/veloenfrance/CIRCUITS/${props.idCircuit}/GPX/circuit.gpx`} target="_blank" className="a-white d-block">GPX</a></li>
                        <li><a href={`https://s3.eu-central-1.amazonaws.com/veloenfrance/CIRCUITS/${props.idCircuit}/PDF/circuit.pdf`} target="_blank" className="a-white d-block mt-2" target="_blank">PDF</a></li>
                    </ul>
                </div>
                <div className="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                    {contacts.map((contact, index)=>(
                        <ul key={index}>
                          {contact.NOM ? <li>{contact.NOM}</li> : null}
                          {contact.ADRESSE ? <li>{contact.ADRESSE ? contact.ADRESSE + ", " : null} {contact.CODE_POSTAL ? contact.CODE_POSTAL + ", " : null} {contact.VILLE ? contact.VILLE : null}</li> : null }
                          {contact.TELEPHONE ? <li><a href={"tel:"+contact.TELEPHONE}>{contact.TELEPHONE}</a></li> : null}
                          {contact.EMAIL ? <li><a href={"mailto:"+contact.EMAIL}>{contact.EMAIL}</a></li> : null }
                          {contact.SITE_WEB ? <li><a href={contact.SITE_WEB}>Site web</a></li> : null }<hr style={{ display : contacts.length === index + 1 ? "none" : "block" }}/>
                        </ul>
                      )
                    )}
                </div>
              </div>
            </>
          }

          {props.idBonneAdresse ? 
            <>
             <div className="row">
                
                <div className="col">
                  <div className="container text-center">
                    {props.lien_photo1 ? <img width='80%' className='img-fluid' src={props.lien_photo1}/> : null }
                    <h5 className="text-uppercase pt-3">{props.nom_etablissement}</h5>
                    {props.hotel ? <span className="badge badge-pill badge-light my-1 mr-1">Hôtel</span> : null}
                    {props.restaurant ? <span className="badge badge-pill badge-light my-1 mr-1">Restaurant</span> : null}
                    {props.hotel_restaurant ? <span className="badge badge-pill badge-light my-1 mr-1">Hôtel-restaurant</span> : null}
                    {props.apparthotel ? <span className="badge badge-pill badge-light my-1 mr-1">Appart'hôtel</span> : null}
                    {props.camping ? <span className="badge badge-pill badge-light my-1 mr-1">Camping</span> : null}
                    {props.lodge ? <span className="badge badge-pill badge-light my-1 mr-1">Lodge</span> : null}
                    {props.bungalow ? <span className="badge badge-pill badge-light my-1 mr-1">Bungalow</span> : null}
                    {props.gite ? <span className="badge badge-pill badge-light my-1 mr-1">Gîte</span> : null}
                    {props.chambre_hote ? <span className="badge badge-pill badge-light my-1 mr-1">Chambre d'hôte</span> : null}
                    {props.cis ? <span className="badge badge-pill badge-light my-1 mr-1">Centre international de séjour (CIS)</span> : null}
                    {props.village_vacances ? <span className="badge badge-pill badge-light my-1 mr-1">Village vacances</span> : null}
                    {props.auberge_jeunesse ? <span className="badge badge-pill badge-light my-1 mr-1">Auberge de jeunesse</span> : null}
                    {props.insolite ? <span className="badge badge-pill badge-light my-1 mr-1">Hébergement insolite</span> : null}
                    {props.velociste ? <span className="badge badge-pill badge-light my-1 mr-1">Vélociste</span> : null}
                    {props.loueur_velo ? <span className="badge badge-pill badge-light my-1 mr-1">Loueur de vélos</span> : null}
                  </div>
                </div>
              </div>
              <nav className="pt-3">
                <div className="nav nav-tabs" id="nav-tab" role="tablist">
                  <a className="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Description</a>
                  <a className="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</a>
                </div>
              </nav>
              <div className="tab-content pt-3" id="nav-tabContent">
                <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                  {props.hebergement ?
                    <>
                      <h5><strong>Hébergement</strong></h5>
                      <ul>
                        {props.tarif_minimum ? <li>Tarif minimum par personne et par nuit: {props.tarif_minimum} €</li> : null }
                        {props.categorie ? <li>Catégorie: {props.categorie}</li> : null }
                        {props.nombre_chambres ? <li>Nombre de chambres: {props.nombre_chambres}</li> : null }
                        {props.nombre_personnes ? <li>Capacité d'accueil: {props.nombre_personnes} personnes</li> : null }
                        {props.nombre_emplacement_camping ? <li>Nombre d'emplacements: {props.nombre_emplacement_camping}</li> : null }
                        {props.wifi ? <li>Wifi</li> : null }
                        {props.lave_linge ? <li>Lave linge</li> : null }
                        {props.repas_emporter ? <li>Paniers repas</li> : null }
                        {props.capacite_localvelo ? <li>Capacité du local vélo : {props.capacite_localvelo}</li> : null }
                      </ul>
                      <hr/>
                    </> : null
                  }
                  {props.restauration ?
                    <>
                      <h5><strong>Restauration</strong></h5>
                      <ul>
                        {props.heure_petitdejeuner ? <li>{props.heure_petitdejeuner}</li> : null }
                        {props.heure_servicemidi ? <li>{props.heure_servicemidi}</li> : null }
                        {props.heure_servicesoir ? <li>{props.heure_servicesoir}</li> : null }
                        {props.repas_emporter ? <li>Paniers repas</li> : null }
                      </ul>
                      <hr/>
                    </> : null
                  }
                  {props.velociste == true || props.loueur_velo == true ?
                    <>
                      <h5><strong>Horaires</strong></h5>
                      <ul>
                        {props.heure_ouverture_matin_velociste? <li>Ouverture le matin à {props.heure_ouverture_matin_velociste}h</li> : null }
                        {props.heure_fermeture_midi_velociste?<li>Fermeture le midi à {props.heure_fermeture_midi_velociste}h</li> : null }
                        {props.heure_ouverture_aprem_velociste?<li>Ouverture l'après-midi à {props.heure_ouverture_aprem_velociste}h</li> : null }
                        {props.heure_fermeture_soir_velociste?<li>Fermeture le soir à {props.heure_fermeture_soir_velociste}h</li> : null }
                      </ul>
                      <hr/>
                      {props.velociste ?
                        <>
                          <h5><strong>Services</strong></h5>
                          {props.artisan_constructeur ? <li>Artisan constructeur</li> : null }
                          {props.reparation_urgente ? <li>Accepte les réparations urgentes</li> : null }
                        </> : null
                      }
                      {props.loueur_velo ?
                        <>
                          <h5><strong>Location de vélos</strong></h5>
                          {props.location_route ? <li>Vélo de route</li> : null}
                          {props.location_route_vae ? <li>Vélo de route à assistance électrique</li> : null}
                          {props.location_vtt ? <li>VTT</li> : null}
                          {props.location_vttae ? <li>VTT à assistance électrique</li> : null}
                          {props.location_vtc ? <li>VTC</li> : null}
                          {props.location_vtcae ? <li>VTC à assistance électrique</li> : null}
                          {props.location_gravel ? <li>Gravel</li> : null}
                          {props.location_gravel_vae ? <li>Gravel à assistance électrique</li> : null}
                          {props.location_ville ? <li>Vélo de ville</li> : null}
                          {props.location_ville_vae ? <li>Vélo de ville à assistance électrique</li> : null}
                          {props.location_enfant ? <li>Vélo pour enfant</li> : null}
                          {props.location_enfant_vae ? <li>Vélo pour enfant à assistance électrique</li> : null}
                        </> : null
                      }
                      <hr/>
                    </> : null
                  }
                  <h5><strong>Avantage licencié FFVélo</strong></h5>
                  <ul>
                    {props.offre_5pourcent ? <li>5% de remise</li> : null }
                    {props.offre_apero ? <li>Un apéritif offert</li> : null }
                    {props.offre_adhesion ? <li>Une adhésion à l'établissement offerte</li> : null }
                  </ul>
                  <hr/>
                  {props.descr ? 
                    <>
                      <h5><strong>Description</strong></h5>
                      <ul><li>{props.descr}</li></ul>
                    </> : null
                  }
                </div>
                <div className="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                  <ul>
                    {props.nom_etablissement ? <li>{props.nom_etablissement}</li> : null }
                    {props.adresse_etablissement ? <li>{props.adresse_etablissement}</li> : null }
                    {props.telephone_etablissement ? <li><a href={"tel:"+props.telephone_etablissement}>{props.telephone_etablissement}</a></li> : null }
                    {props.email_etablissement ? <li><a href={"mailto:"+props.email_etablissement}>{props.email_etablissement}</a></li> : null }
                    {props.siteweb ? <li><a href={props.siteweb}>Site web</a></li> : null }
                  </ul>
                </div>
              </div>
            </> : null
          }

          {props.idPoi && 
            <>
              <div className="row">
                <div className="col">
                  <div className="container text-center">
                    <img width='80%' className='img-fluid' src={"https://s3.eu-central-1.amazonaws.com/veloenfrance/POI/"+props.idPoi+"/Photos/mini/1_330.jpg"}/>
                    <h5 className="text-uppercase pt-3">{props.nom}</h5>
                    {props.soustype ? <span className="badge badge-pill badge-light my-1 mr-1">{props.soustype}</span> : null}
                  </div>
                </div>
              </div>

              <nav className="pt-3">
                <div className="nav nav-tabs" id="nav-tab" role="tablist">
                  <a className="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Description</a>
                  <a className="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contacts</a>
                </div>
              </nav>
              <div className="tab-content pt-3" id="nav-tabContent">
                <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                  {props.descr && <div className="row"><div className="col description overflow-auto pb-5">{props.descr}</div></div>}
                </div>
                <div className="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                  <ul>
                    {props.nom ? <li>{props.nom}</li> : null }
                    {contacts.TELEPHONE ? <li>{contacts.TELEPHONE}</li> : null}
                    {contacts.ADRESSE ? <li>{contacts.ADRESSE ? contacts.ADRESSE + ", " : null} {contacts.CODE_POSTAL ? contacts.CODE_POSTAL + ", " : null} {contacts.VILLE ? contacts.VILLE : null}</li> : null }
                    {contacts.EMAIL ? <li>{contacts.EMAIL}</li> : null }
                    {contacts.SITE_WEB ? <li>{contacts.SITE_WEB}</li> : null }
                  </ul>
                </div>
              </div>
            </>
          }

          {props.idManif && 
            <>
              <div className="row">
                <div className="col">
                  <h5 className="text-uppercase">{props.nom}</h5>
                  {props.Type ? <span className="badge badge-pill badge-light my-1 mr-1">{props.Type}</span> : null}
                  {props.Pratique_Route ? <span className="badge badge-pill badge-light my-1 mr-1">Route</span> : null}
                  {props.Pratique_VTT ? <span className="badge badge-pill badge-light my-1 mr-1">VTT</span> : null}
                  {props.Pratique_Gravel ? <span className="badge badge-pill badge-light my-1 mr-1">Gravel</span> : null}
                  {props.Pratique_Marche ? <span className="badge badge-pill badge-light my-1 mr-1">Marche</span> : null}
                  {props.DevDurable ? <span className="badge badge-pill badge-light my-1 mr-1">Éco-responsable</span> : null}
                  {props.AccesPSH ? <span className="badge badge-pill badge-light my-1 mr-1">Accueil PSH</span> : null}
                  {props.SpeFem ? <span className="badge badge-pill badge-light my-1 mr-1">Spécifique femmes</span> : null}
                </div>
              </div>

              <nav className="pt-3">
                <div className="nav nav-tabs" id="nav-tab" role="tablist">
                  <a className="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Infos</a>
                  <a className="nav-item nav-link" id="nav-export-tab" data-toggle="tab" href="#nav-export" role="tab" aria-controls="nav-export" aria-selected="false">Export</a>
                  <a className="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</a>
                </div>
              </nav>

              <div className="tab-content pt-3" id="nav-tabContent">

                <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

                  <div className="text-uppercase my-2"><strong>Infos Principales:</strong></div>
                  <ul className="fa-ul">
                    <li><span className="fa-li"><i className="far fa-calendar-check"></i></span>{handleDate(props.Accueil_DateDbt)}<i className="fas fa-arrow-right ml-2 mr-2"></i>{handleDate(props.Arrivee_DateFin)}</li>
                    <li><span className="fa-li"><i className="fas fa-map-marked-alt"></i></span>{props.Accueil_Lieu ? props.Accueil_Lieu + ", " : ""}{props.Accueil_Adresse ? props.Accueil_Adresse + ", ": ""}{props.Accueil_CP ? props.Accueil_CP + ", " : ""}{props.Accueil_Commune}</li>
                    {props.heures ? <li><span className="fa-li"><i className="fas fa-clock"></i></span>{props.heures}</li> : null }
                    {props.heureDeCloture ? <li><span className="fa-li"><i className="fas fa-hourglass-end"></i></span>{props.heureDeCloture}</li> : null }
                    <li><span className="fa-li"><i className="fas fa-map-pin"></i></span>{props.Arrivee_Lieu ? props.Arrivee_Lieu + ", " : ""}{props.Arrivee_CP ? props.Arrivee_CP+", " : ""}{props.Arrivee_Commune}</li>
                    {props.descr ? <li><span className="fa-li"><i className="far fa-comment-dots mr-2"></i></span>{props.descr}</li> : null }
                  </ul>
                 
                  <hr/>
                  <div className="text-uppercase my-2"><strong>Tarifs:</strong></div>
                  <ul>
                    <li>Licenciés adultes: {props.OinManifLicTarifAdulte}</li>
                    <li>Licenciés de moins de 18 ans: {props.OinManifLicTarifJeune}</li>
                  {props.OinManifReserveLicencie == false ?
                    <>
                      <li>Non licenciés adultes: {props.OinManifNonLicTarifAdulte}</li>
                      <li>Non licenciés de moins de 18 ans: {props.OinManifNonLicTarifJeune}</li>
                    </>
                     : 
                    <>
                      <li>Cette randonnée est réservée aux licenciés de la Fédération française de cyclotourisme.</li>
                    </>
                  }
                  </ul>
                  <hr/>

                  <div className="text-uppercase my-2"><strong>Distances et denivelés:</strong></div>
                  {props.Pratique_Route ? 
                    <>
                      {props.OinRouteDist_1 || props.OinRouteDist_2 || props.OinRouteDist_3 || props.OinRouteDist_4 || props.OinRouteDist_5 || props.OinRouteDist_6 ?
                        <div className="row">
                          <div className="col"><i className="fas fa-bicycle"></i> Route</div>
                        </div> : null
                      }
                      <ul>
                        {props.OinRouteDist_1 ? 
                          <li>Circuit 1: <ul><li>Distance: {props.OinRouteDist_1} km </li>{props.OinRouteDeniv_1 ? <li>Dénivelé: {props.OinRouteDeniv_1} m</li> : ""}{props.OinRouteDiff_1 ? <li>Difficulté: {props.OinRouteDiff_1}</li> : ""}</ul></li> : ""
                        }
                        {props.OinRouteDist_2 ? 
                          <li>Circuit 2: <ul><li>Distance: {props.OinRouteDist_2} km </li>{props.OinRouteDeniv_2 ? <li>Dénivelé: {props.OinRouteDeniv_2} m</li> : ""}{props.OinRouteDiff_2 ? <li>Difficulté: {props.OinRouteDiff_2}</li> : ""}</ul></li> : ""
                        }
                        {props.OinRouteDist_3 ? 
                          <li>Circuit 3: <ul><li>Distance: {props.OinRouteDist_3} km </li>{props.OinRouteDeniv_3 ? <li>Dénivelé: {props.OinRouteDeniv_3} m</li> : ""}{props.OinRouteDiff_3 ? <li>Difficulté: {props.OinRouteDiff_3}</li> : ""}</ul></li> : ""
                        }
                        {props.OinRouteDist_4 ? 
                          <li>Circuit 4: <ul><li>Distance: {props.OinRouteDist_4} km </li>{props.OinRouteDeniv_4 ? <li>Dénivelé: {props.OinRouteDeniv_4} m</li> : ""}{props.OinRouteDiff_4 ? <li>Difficulté: {props.OinRouteDiff_4}</li> : ""}</ul></li> : ""
                        }
                        {props.OinRouteDist_5 ? 
                          <li>Circuit 5: <ul><li>Distance: {props.OinRouteDist_5} km </li>{props.OinRouteDeniv_5 ? <li>Dénivelé: {props.OinRouteDeniv_5} m</li> : ""}{props.OinRouteDiff_5 ? <li>Difficulté: {props.OinRouteDiff_5}</li> : ""}</ul></li> : ""
                        }
                        {props.OinRouteDist_6 ? 
                          <li>Circuit 6: <ul><li>Distance: {props.OinRouteDist_6} km </li>{props.OinRouteDeniv_6 ? <li>Dénivelé: {props.OinRouteDeniv_6} m</li> : ""}{props.OinRouteDiff_6 ? <li>Difficulté: {props.OinRouteDiff_6}</li> : ""}</ul></li> : ""
                        }
                      </ul>
                    </> : null
                  }
                  {props.Pratique_VTT ? 
                    <>
                      {props.OinVTTDist_1 || props.OinVTTDist_2 || props.OinVTTDist_3 || props.OinVTTDist_4 || props.OinVTTDist_5 || props.OinVTTDist_6 ?
                        <div className="row">
                          <div className="col"><i className="fas fa-bicycle"></i> VTT</div>
                        </div> : null
                      }
                      <ul>
                      {props.OinVTTDist_1 ? 
                          <li>Circuit 1: <ul><li>Distance: {props.OinVTTDist_1} km </li>{props.OinVTTDeniv_1 ? <li>Dénivelé: {props.OinVTTDeniv_1} m</li> : ""}{props.OinVTTDiff_1 ? <li>Difficulté: {props.OinVTTDiff_1}</li> : ""}</ul></li> : ""
                        }
                        {props.OinVTTDist_2 ? 
                          <li>Circuit 2: <ul><li>Distance: {props.OinVTTDist_2} km </li>{props.OinVTTDeniv_2 ? <li>Dénivelé: {props.OinVTTDeniv_2} m</li> : ""}{props.OinVTTDiff_2 ? <li>Difficulté: {props.OinVTTDiff_2}</li> : ""}</ul></li> : ""
                        }
                        {props.OinVTTDist_3 ? 
                          <li>Circuit 3: <ul><li>Distance: {props.OinVTTDist_3} km </li>{props.OinVTTDeniv_3 ? <li>Dénivelé: {props.OinVTTDeniv_3} m</li> : ""}{props.OinVTTDiff_3 ? <li>Difficulté: {props.OinVTTDiff_3}</li> : ""}</ul></li> : ""
                        }
                        {props.OinVTTDist_4 ? 
                          <li>Circuit 4: <ul><li>Distance: {props.OinVTTDist_4} km </li>{props.OinVTTDeniv_4 ? <li>Dénivelé: {props.OinVTTDeniv_4} m</li> : ""}{props.OinVTTDiff_4 ? <li>Difficulté: {props.OinVTTDiff_4}</li> : ""}</ul></li> : ""
                        }
                        {props.OinVTTDist_5 ? 
                          <li>Circuit 5: <ul><li>Distance: {props.OinVTTDist_5} km </li>{props.OinVTTDeniv_5 ? <li>Dénivelé: {props.OinVTTDeniv_5} m</li> : ""}{props.OinVTTDiff_5 ? <li>Difficulté: {props.OinVTTDiff_5}</li> : ""}</ul></li> : ""
                        }
                        {props.OinVTTDist_6 ? 
                          <li>Circuit 6: <ul><li>Distance: {props.OinVTTDist_6} km </li>{props.OinVTTDeniv_6 ? <li>Dénivelé: {props.OinVTTDeniv_6} m</li> : ""}{props.OinVTTDiff_6 ? <li>Difficulté: {props.OinVTTDiff_6}</li> : ""}</ul></li> : ""
                        }
                      </ul>
                    </> : null
                  }
                  {props.Pratique_Gravel ?
                    <>
                      {props.OinGravelDist_1 || props.OinGravelDist_2 || props.OinGravelDist_3 || props.OinGravelDist_4 || props.OinGravelDist_5 || props.OinGravelDist_6 ?
                        <div className="row">
                          <div className="col"><i className="fas fa-bicycle"></i> Gravel</div>
                        </div> : null
                      }
                      <ul>
                        {props.OinGravelDist_1 ? 
                          <li>Circuit 1: <ul><li>Distance: {props.OinGravelDist_1} km </li>{props.OinGravelDeniv_1 ? <li>Dénivelé: {props.OinGravelDeniv_1} m</li> : ""}{props.OinGravelDiff_1 ? <li>Difficulté: {props.OinGravelDiff_1}</li> : ""}</ul></li> : ""
                        }
                        {props.OinGravelDist_2 ? 
                          <li>Circuit 2: <ul><li>Distance: {props.OinGravelDist_2} km </li>{props.OinGravelDeniv_2 ? <li>Dénivelé: {props.OinGravelDeniv_2} m</li> : ""}{props.OinGravelDiff_2 ? <li>Difficulté: {props.OinGravelDiff_2}</li> : ""}</ul></li> : ""
                        }
                        {props.OinGravelDist_3 ? 
                          <li>Circuit 3: <ul><li>Distance: {props.OinGravelDist_3} km </li>{props.OinGravelDeniv_3 ? <li>Dénivelé: {props.OinGravelDeniv_3} m</li> : ""}{props.OinGravelDiff_3 ? <li>Difficulté: {props.OinGravelDiff_3}</li> : ""}</ul></li> : ""
                        }
                        {props.OinGravelDist_4 ? 
                          <li>Circuit 4: <ul><li>Distance: {props.OinGravelDist_4} km </li>{props.OinGravelDeniv_4 ? <li>Dénivelé: {props.OinGravelDeniv_4} m</li> : ""}{props.OinGravelDiff_4 ? <li>Difficulté: {props.OinGravelDiff_4}</li> : ""}</ul></li> : ""
                        }
                        {props.OinGravelDist_5 ? 
                          <li>Circuit 5: <ul><li>Distance: {props.OinGravelDist_5} km </li>{props.OinGravelDeniv_5 ? <li>Dénivelé: {props.OinGravelDeniv_5} m</li> : ""}{props.OinGravelDiff_5 ? <li>Difficulté: {props.OinGravelDiff_5}</li> : ""}</ul></li> : ""
                        }
                        {props.OinGravelDist_6 ? 
                          <li>Circuit 6: <ul><li>Distance: {props.OinGravelDist_6} km </li>{props.OinGravelDeniv_6 ? <li>Dénivelé: {props.OinGravelDeniv_6} m</li> : ""}{props.OinGravelDiff_6 ? <li>Difficulté: {props.OinGravelDiff_6}</li> : ""}</ul></li> : ""
                        }
                      </ul>
                    </> : null
                  }
                  {props.Pratique_Marche ?
                    <>
                      {props.OinMarcheDist_1 || props.OinMarcheDist_2 || props.OinMarcheDist_3 || props.OinMarcheDist_4 || props.OinMarcheDist_5 || props.OinMarcheDist_6 ?
                        <div className="row">
                          <div className="col"><i className="fas fa-bicycle"></i> Marche</div>
                        </div> : null
                      }
                      <ul>
                        {props.OinMarcheDist_1 ? 
                          <li>Circuit 1: <ul><li>Distance: {props.OinMarcheDist_1} km </li></ul></li> : ""
                        }
                        {props.OinMarcheDist_2 ? 
                          <li>Circuit 2: <ul><li>Distance: {props.OinMarcheDist_2} km </li></ul></li> : ""
                        }
                        {props.OinMarcheDist_3 ? 
                          <li>Circuit 3: <ul><li>Distance: {props.OinMarcheDist_3} km </li></ul></li> : ""
                        }
                        {props.OinMarcheDist_4 ? 
                          <li>Circuit 4: <ul><li>Distance: {props.OinMarcheDist_4} km </li></ul></li> : ""
                        }
                        {props.OinMarcheDist_5 ? 
                          <li>Circuit 5: <ul><li>Distance: {props.OinMarcheDist_5} km </li></ul></li> : ""
                        }
                        {props.OinMarcheDist_6 ? 
                          <li>Circuit 6: <ul><li>Distance: {props.OinMarcheDist_6} km </li></ul></li> : ""
                        }
                      </ul>
                    </> : null
                  }
                 
                </div>

                <div className="tab-pane fade" id="nav-export" role="tabpanel" aria-labelledby="nav-export-tab">
                {props.FichierFlyer ? 
                  <><li><i className="fas fa-dl"></i> <a href={props.FichierFlyer}>Flyer</a></li><hr/></> : null
                }
                {props.FichierGpx.length > 0 ? 
                  props.FichierGpx.map((gpx, index)=>
                      <li key={gpx}><i className="fas fa-dl"></i> <a href={gpx}>Circuit {index + 1}</a></li>
                    )
                  : null
                }
                </div>

                <div className="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                  <div className="text-uppercase"><strong>Organisateur</strong></div>
                  <div className="row">
                    <div className="col">{props.Structure}</div>
                  </div>
                  <hr/>
                  <div className="text-uppercase"><strong>Correspondant·e</strong></div>
                  <ul>
                    <li>{props.Contact_Prenom} {props.Contact_Nom}</li>
                    {props.AdresseMail && 
                        <li><a href={"mailto:"+props.AdresseMail}>{props.AdresseMail}</a></li>
                    }
                    {props.AdresseWeb && 
                      <li><a href={props.AdresseWeb}>{props.AdresseWeb}</a></li>
                    }
                  </ul>
                </div>

              </div>
            </>
          }

          {props.idClub && 
            <>
              <div className="row">
                <div className="col">
                  <span className="text-uppercase"><img className='img-responsive' src={props.logo ? props.logo : '/build/images/logos/logo_ffvelo.svg'} style={{width:'20%', height:'auto'}}/></span>
                  <span className="text-uppercase" style={{fontSize:'150%'}}> {props.nom}</span>
                </div>
              </div>
              <nav className="pt-3">
                <div className="nav nav-tabs" id="nav-tab" role="tablist">
                  <a className="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Infos</a>
                  <a className="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contacts</a>
                </div>
              </nav>
              <div className="tab-content pt-3" id="nav-tabContent">
                <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                  {props.descr && <div className="row"><div className="col description">{props.descr}</div></div>}
                </div>
                <div className="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">contacts</div>
              </div>
            </>
          }

        </div>
        </div>
        <footer className="footer py-2">
          <div className="container">
            <span className="text-muted">© 2020 | Fédération française de cyclotourisme</span>
          </div>
        </footer>
      </section>
    </div>
  )
}
  
export default InfoFeature;