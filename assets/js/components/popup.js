import {ssTypeCircuit} from "../services/mapConfig";

export const popupTemplateCircuit = (nom, id, typevelo, distance, denivele, difficulte, classFa, sousType) => {
    return (
        `<div class='row' style='min-width:320px; max-width:350px;'>
            <div class='col-lg-12 col-md-12 col-sm-12 green' align='center'>
                <h5>${nom}</h5>
                ${sousType ? `<span class="badge badge-pill badge-light my-1 mr-1">${ssTypeCircuit[sousType]}</span>` : ``}
                <hr>
            </div>
        </div>
        <div class='row'>
            <div class='col-lg-12'><table width='100%' height='auto' border='0' cellpadding='0' cellspacing='0' style='font-size:14px;'>
                <table>
                    <tbody>
                        <tr>
                            <td width='60%' rowspan='5' valign='middle'>
                                <img class='img-responsive' src='https://s3.eu-central-1.amazonaws.com/veloenfrance/CIRCUITS/${id}/Photos/mini/1_130.jpg'>
                            </td>
                            <td width='5%' align='center' valign='middle' style='padding:5px;'>
                                <i class='fa fa-bicycle blue' aria-hidden='true'></i>
                            </td>
                            <td width='35%' align='left' style='padding:5px;' valign='middle'>
                                ${typevelo}
                            </td>
                        </tr>
                        ${sousType == 201 ? // si circuit = rando permanente 
                            `<tr>
                                <td width='5%' align='center' valign='middle' style='padding:5px;'>
                                    <i class='fas fa-tags blue' aria-hidden='true'></i>
                                </td>
                                <td width='35%' align='left' style='padding:5px;' valign='middle'>
                                    Randonnée permanente
                                </td>
                            </tr>` : ``
                        }
                        ${distance ?
                            `<tr>
                                <td width='5%' align='center' style='padding:5px;' valign='middle'>
                                    <i class='fa fa-road blue' aria-hidden='true'></i>
                                </td>
                                <td width='35%' align='left' style='padding:5px;' valign='middle'>
                                    ${distance} km
                                </td>
                            </tr>` : ``
                        }
                        ${denivele ?
                            `<tr>
                                <td width='5%' align='center' style='padding:5px;' valign='middle'>
                                    <i class='fas fa-chart-line blue' aria-hidden='true'></i>
                                </td>
                                <td width='35%' align='left' style='padding:5px;' valign='middle'>
                                    ${denivele} m
                                </td>
                            </tr>` : ``
                        }
                        ${difficulte ? 
                            `<tr>
                                <td width='5%' align='center' style='padding:5px;' valign='middle'>
                                    <i class='${classFa}' aria-hidden='true'></i>
                                </td>
                                <td width='35%' align='left' style='padding:5px;' valign='middle'>
                                    ${difficulte}
                                </td>
                            </tr>` : ``
                        }
                    </tbody>
                </table>
            </div>
        </div>
        <div class='row'>
            <div class='col-lg-12 col-md-12 col-sm-12 text-center'><hr>
                <button id='buttonPopup' value="${id}" class='btn btn-sm btn-outline-dark'><i class="fas fa-search-plus fa-2x mr-5 ml-5"></i></button>
            </div>
        </div>`
    );
}

export const popupTemplatePoi = (nom, id, refSousType) => {
    return (
        `<div class='row' style='min-width:320px; max-width:350px;'>
            <div class='col-lg-12 col-md-12 col-sm-12 green' align='center'>
                <h5>${nom}</h5>
            </div>
            <div class='col-lg-12 col-md-12 col-sm-12 blue' align='center'>
                <h6>${refSousType}</h6>
            </div>
        </div>
        <div class='row'>
            <div class='col-lg-12'>
                <table width='100%' height='auto' border='0' cellpadding='0' cellspacing='0' style='font-size:14px;'>
                    <tbody>
                        <tr>
                            <td align='center' width='100%' rowspan='4' valign='middle'>
                                <img class='img-responsive' src='https://s3.eu-central-1.amazonaws.com/veloenfrance/POI/${id}/Photos/mini/1_130.jpg'>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class='row'>
            <div class='col-lg-12 col-md-12 col-sm-12 text-center'><hr>
                <button id='buttonPopup' value="${id}" class='btn btn-sm btn-outline-dark'><i class="fas fa-search-plus fa-2x mr-5 ml-5"></i></button>
            </div>
        </div>`
    );
}

export const popupTemplateBonneAdresse = (nom, id, photo, soustype, tel, email, siteweb) => {
    return (
        `<div class='row' style='min-width:320px; max-width:350px;'>
            <div class='col-lg-12 col-md-12 col-sm-12 green' align='center'>
                <h6 class="text-uppercase">${nom}</h6>
                ${soustype}<hr>
            </div>
        </div>
        <div class='row'>
            <div class='col-lg-12'><table width='100%' height='auto' border='0' cellpadding='0' cellspacing='0' style='font-size:14px;'>
                <table>
                    <tbody>
                        ${photo ? 
                            `<tr>
                                <td width='60%' rowspan='5' valign='middle'>
                                    <img width='100%' class='img-responsive' src='${photo}'>
                                </td>
                            </tr>` : `` 
                        }
                        ${tel ? 
                            `<tr>
                                <td width='5%' align='center' valign='middle' style='padding:5px;'>
                                    <i class='fas fa-phone' aria-hidden='true'></i>
                                </td>
                                <td width='35%' align='left' style='padding:5px;' valign='middle'>
                                    <a href='tel:${tel}'>${tel}</a>
                                </td>
                            </tr>` : ``
                        }
                        ${email ? 
                            `<tr>
                                <td width='5%' align='center' valign='middle' style='padding:5px;'>
                                    <i class='fas fa-at' aria-hidden='true'></i>
                                </td>
                                <td width='35%' align='left' style='padding:5px;' valign='middle'>
                                    <a href='mailto:${email}'>${email}</a>
                                </td>
                            </tr>` : ``
                        }
                        ${siteweb ? 
                            `<tr>
                                <td width='5%' align='center' valign='middle' style='padding:5px;'>
                                    <i class='fas fa-globe' aria-hidden='true'></i>
                                </td>
                                <td width='35%' align='left' style='padding:5px;' valign='middle'>
                                    <a href='${siteweb}' target="_blank">Site web</a>
                                </td>
                            </tr>`: ``
                        }
                    </tbody>
                </table>
            </div>
        </div>
        <div class='row'>
            <div class='col-lg-12 col-md-12 col-sm-12 text-center'><hr>
                <button id='buttonPopup' value="${id}" class='btn btn-sm btn-outline-dark'><i class="fas fa-search-plus fa-2x mr-5 ml-5"></i></button>
            </div>
        </div>`
    );
}

export const popupTemplateManifs = (nom, id, type, date, proute, pvtt, pgravel, pmarche, durable, psh, spefem, email, siteweb) => {
    date =  date.split("-");
    date = date[2] + "/" + date[1] + "/" + date[0];
    return (
        ` <div class='row' style='min-width:320px; max-width:350px;'>
            <div class='col-lg-12 col-md-12 col-sm-12 green' align='center'>
                <h5>${nom}</h5>
                ${proute ? `<span class="badge badge-pill badge-light my-1 mr-1">${proute}</span>` : ``}
                ${pvtt ? `<span class="badge badge-pill badge-light my-1 mr-1">${pvtt}</span>` : ``}
                ${pgravel ? `<span class="badge badge-pill badge-light my-1 mr-1">${pgravel}</span>` : ``}
                ${pmarche ? `<span class="badge badge-pill badge-light my-1 mr-1">${pmarche}</span>` : ``}
                ${durable ? `<span class="badge badge-pill badge-light my-1 mr-1">${durable}</span>` : ``}
                ${psh ? `<span class="badge badge-pill badge-light my-1 mr-1">${psh}</span>` : ``}
                ${spefem ? `<span class="badge badge-pill badge-light my-1 mr-1">${spefem}</span>` : ``}
                <span class="badge badge-pill badge-light my-1 mr-1">${type}</span>
                <hr>
            </div>
        </div>
        <div class='row'>
            <div class='col-lg-12'><table width='100%' height='auto' border='0' cellpadding='0' cellspacing='0' style='font-size:14px;'>
                <table>
                    <tbody>
                        <tr>
                            <td width='60%' rowspan='5' valign='middle'>
                                <div class="text-center"><i class="far fa-calendar-check fa-2x"></i><br><h6 class="pt-2">${date}</h6></div>
                            </td>
                            ${email ? 
                            `<tr>
                                <td width='5%' align='center' valign='middle' style='padding:5px;'>
                                    <i class='fa fa-at' aria-hidden='true'></i>
                                </td>
                                <td width='35%' align='left' style='padding:5px;' valign='middle'>
                                    <a href=${"mailto:"+email}>Email</a>
                                </td>
                            </tr>` : ``}
                            ${siteweb ? 
                            `<tr>
                                <td width='5%' align='center' valign='middle' style='padding:5px;'>
                                    <i class='fa fa-globe' aria-hidden='true'></i>
                                </td>
                                <td width='35%' align='left' style='padding:5px;' valign='middle'>
                                    <a href=${siteweb}>Site web</a>
                                </td>
                            </tr>` : ``}
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class='row'>
            <div class='col-lg-12 col-md-12 col-sm-12 text-center'><hr>
                <button id='buttonPopup' value="${id}" class='btn btn-sm btn-outline-dark'><i class="fas fa-search-plus fa-2x mr-5 ml-5"></i></button>
            </div>
        </div>`
    );
}

export const popupTemplateClubs = (nom, ville, id, pratiqueRoute, pratiqueVttVtc, tel, email, siteweb, logo) => {
    siteweb = siteweb.includes("http") ? siteweb : "http://" + siteweb;
    return (
        ` <div class='row' style='min-width:320px; max-width:350px;'>
            <div class='col-lg-12 col-md-12 col-sm-12 green' align='center'>
                <h5>${nom}</h5>
                <div>${ville}</div><hr>
            </div>
        </div>
        <div class='row'>
            <div class='col-lg-12'><table width='100%' height='auto' border='0' cellpadding='0' cellspacing='0' style='font-size:14px;'>
                <table>
                    <tbody>
                        <tr>
                            <td width='60%' rowspan='5' valign='middle'>
                                <img class='img-responsive' src='${logo ? logo : "/build/images/logos/logo_ffvelo.svg"}' style='width:50%; height:auto;'>
                            </td>
                            ${pratiqueRoute ?
                                `<td width='5%' align='center' valign='middle' style='padding:5px;'>
                                    <i class='fa fa-bicycle blue' aria-hidden='true'></i>
                                </td>
                                <td width='35%' align='left' style='padding:5px;' valign='middle'>
                                    Route
                                </td>` : ``
                            }
                        </tr>
                        ${pratiqueVttVtc ?
                            `<tr>
                                <td width='5%' align='center' valign='middle' style='padding:5px;'>
                                    <i class='fa fa-bicycle blue' aria-hidden='true'></i>
                                </td>
                                <td width='35%' align='left' style='padding:5px;' valign='middle'>
                                    VTT / VTC
                                </td>
                            </tr>` : ``
                        }
                        ${tel ? 
                            `<tr>
                                <td width='5%' align='center' valign='middle' style='padding:5px;'>
                                    <i class='fas fa-phone' aria-hidden='true'></i>
                                </td>
                                <td width='35%' align='left' style='padding:5px;' valign='middle'>
                                    <a href='tel:${tel}'>${tel}</a>
                                </td>
                            </tr>` : ``
                        }
                        ${email ? 
                            `<tr>
                                <td width='5%' align='center' valign='middle' style='padding:5px;'>
                                    <i class='fas fa-at' aria-hidden='true'></i>
                                </td>
                                <td width='35%' align='left' style='padding:5px;' valign='middle'>
                                    <a href='mailto:${email}'>Email</a>
                                </td>
                            </tr>` : ``
                        }
                        ${siteweb ? 
                            `<tr>
                                <td width='5%' align='center' valign='middle' style='padding:5px;'>
                                    <i class='fas fa-globe' aria-hidden='true'></i>
                                </td>
                                <td width='35%' align='left' style='padding:5px;' valign='middle'>
                                    <a href='${siteweb}' target="_blank">Site web</a>
                                </td>
                            </tr>`: ``
                        }
                    </tbody>
                </table>
            </div>
        </div>
        <div class='row'>
            <div class='col-lg-12 col-md-12 col-sm-12 text-center'><hr>
                <button id='buttonPopup' value="${id}" class='btn btn-sm btn-outline-dark'><i class="fas fa-search-plus fa-2x mr-5 ml-5"></i></button>
            </div>
        </div>`
    );
}

export const popupTemplateCols = (nom, alti, type, url) => {
    const typeNom = {
        0 : "Routier",
        1 : "Muletier cyclable",
        10 : "Muletier cyclable"
    }
    return (
        `<div class='row' style='min-width:320px; max-width:350px;'>
            <div class='col-lg-12 col-md-12 col-sm-12 green' align='center'>
                <h5 class="my-2">${nom}</h5>
                <span class="badge badge-pill badge-light my-1 mr-1">${typeNom[type]}</span>
                <span class="badge badge-pill badge-light my-1 mr-1">${alti} m</span>
            </div>
        </div>
        <div class='row my-2'>
            <div class='col-lg-12'>
                <table width='100%' height='auto' border='0' cellpadding='0' cellspacing='0' style='font-size:14px;'>
                    <tbody>
                        <tr>
                            <td align='center' width='100%' rowspan='4' valign='middle'>
                                <img class='img-responsive' width="30%" height="auto" src='/build/images/icon/club-des-cent-cols.png'>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class='row'>
            <div class='col-lg-12 col-md-12 col-sm-12 text-center'><hr>
                <button class='btn btn-sm btn-outline-dark'><a href='${url}' target="_blank"><i class="fas fa-search-plus fa-2x mr-5 ml-5"></i></a></button>
            </div>
        </div>`
    );
}