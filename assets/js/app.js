/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
//import 'leaflet/dist/leaflet.css';
import '../css/app.css';
import '@yaireo/tagify/dist/tagify.css';
import 'nouislider/distribute/nouislider.css';
import 'flatpickr/dist/flatpickr.min.css';
import 'datatables.net-bs4/css/datatables.bootstrap4.css';

const $ = require('jquery');
import 'bootstrap';

